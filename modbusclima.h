#ifndef MODBUSCLIMA_H
#define MODBUSCLIMA_H
#include <modbus_slave.h>
#include "definitions.h"

class modbusClima : public modbus_slave
{
    Q_OBJECT
public:
    explicit modbusClima(CLIMA_MODBUS ,Qt4Modbus*);
    CLIMA_MODBUS readData();
    void writeCircuitActautor(short);
    void writeDehumidityActautor(short);
    void writeComfortRHTreshold(short);
    short getStatus();
    float getAirTemp();
    short getHumidity();
    float getFloorTemp();
    float getDewPointTemp();

    CLIMA_MODBUS _clima;

signals:
    void dataReaded(CLIMA_MODBUS);
    
public slots:

};

#endif // MODBUSCLIMA_H
