#include "screen_20.h"
#include "ui_screen_20.h"

screen_20::screen_20(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_20),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_20::~screen_20()
{
    delete ui;
}

void screen_20::on_pushButton_keyboard_clicked()
{
    Q_EMIT(showkeyboard(ui->pushButton_keyboard));
}

void screen_20::setTextEdit(QString str)
{
    ui->pushButton_keyboard->setText(str);
}

QString screen_20::scheduleName()
{
    return ui->pushButton_keyboard->text();
}
