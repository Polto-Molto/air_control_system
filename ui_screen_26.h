/********************************************************************************
** Form generated from reading UI file 'screen_26.ui'
**
** Created: Thu Apr 13 00:46:21 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREEN_26_H
#define UI_SCREEN_26_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_screen_26
{
public:
    QGridLayout *gridLayout;
    QFrame *frame_footer;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_15;
    QPushButton *pushButton_cancel;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *pushButton_home;
    QSpacerItem *horizontalSpacer_6;
    QSpacerItem *horizontalSpacer_16;
    QFrame *frame_body;
    QGridLayout *gridLayout_2;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *pushButton_4;
    QPushButton *pushButton_second;
    QPushButton *pushButton_3;
    QPushButton *pushButton_first;
    QSpacerItem *horizontalSpacer_3;
    QSpacerItem *verticalSpacer;
    QSpacerItem *verticalSpacer_2;
    QFrame *frame_header;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QWidget *screen_26)
    {
        if (screen_26->objectName().isEmpty())
            screen_26->setObjectName(QString::fromUtf8("screen_26"));
        screen_26->resize(1024, 600);
        screen_26->setMinimumSize(QSize(1024, 600));
        screen_26->setMaximumSize(QSize(1024, 600));
        gridLayout = new QGridLayout(screen_26);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame_footer = new QFrame(screen_26);
        frame_footer->setObjectName(QString::fromUtf8("frame_footer"));
        frame_footer->setMinimumSize(QSize(1024, 80));
        frame_footer->setMaximumSize(QSize(1024, 80));
        frame_footer->setFrameShape(QFrame::StyledPanel);
        frame_footer->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_footer);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_15 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_15);

        pushButton_cancel = new QPushButton(frame_footer);
        pushButton_cancel->setObjectName(QString::fromUtf8("pushButton_cancel"));
        pushButton_cancel->setMinimumSize(QSize(50, 50));
        pushButton_cancel->setMaximumSize(QSize(50, 50));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/images/Icons/Cross.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_cancel->setIcon(icon);
        pushButton_cancel->setIconSize(QSize(50, 50));
        pushButton_cancel->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_cancel);

        horizontalSpacer_5 = new QSpacerItem(353, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);

        pushButton_home = new QPushButton(frame_footer);
        pushButton_home->setObjectName(QString::fromUtf8("pushButton_home"));
        pushButton_home->setMinimumSize(QSize(50, 50));
        pushButton_home->setMaximumSize(QSize(50, 50));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/images/Icons/home.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_home->setIcon(icon1);
        pushButton_home->setIconSize(QSize(50, 50));
        pushButton_home->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_home);

        horizontalSpacer_6 = new QSpacerItem(415, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);

        horizontalSpacer_16 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_16);


        gridLayout->addWidget(frame_footer, 2, 0, 1, 1);

        frame_body = new QFrame(screen_26);
        frame_body->setObjectName(QString::fromUtf8("frame_body"));
        frame_body->setMinimumSize(QSize(1024, 470));
        frame_body->setMaximumSize(QSize(1024, 470));
        frame_body->setFrameShape(QFrame::StyledPanel);
        frame_body->setFrameShadow(QFrame::Raised);
        gridLayout_2 = new QGridLayout(frame_body);
        gridLayout_2->setSpacing(20);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_4, 1, 4, 1, 1);

        pushButton_4 = new QPushButton(frame_body);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));
        pushButton_4->setMinimumSize(QSize(285, 80));
        pushButton_4->setMaximumSize(QSize(285, 80));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(14);
        font.setBold(true);
        font.setWeight(75);
        pushButton_4->setFont(font);
        pushButton_4->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"text-align:left;\n"
"padding:20px;"));
        pushButton_4->setIconSize(QSize(50, 50));
        pushButton_4->setFlat(true);

        gridLayout_2->addWidget(pushButton_4, 2, 1, 1, 2);

        pushButton_second = new QPushButton(frame_body);
        pushButton_second->setObjectName(QString::fromUtf8("pushButton_second"));
        pushButton_second->setMinimumSize(QSize(285, 80));
        pushButton_second->setMaximumSize(QSize(285, 80));
        pushButton_second->setFont(font);
        pushButton_second->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"text-align:left;\n"
"padding:20px;"));
        pushButton_second->setIconSize(QSize(50, 50));
        pushButton_second->setFlat(true);

        gridLayout_2->addWidget(pushButton_second, 1, 2, 1, 1);

        pushButton_3 = new QPushButton(frame_body);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        pushButton_3->setMinimumSize(QSize(285, 80));
        pushButton_3->setMaximumSize(QSize(285, 80));
        pushButton_3->setFont(font);
        pushButton_3->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"text-align:left;\n"
"padding:20px;"));
        pushButton_3->setIconSize(QSize(50, 50));
        pushButton_3->setFlat(true);

        gridLayout_2->addWidget(pushButton_3, 1, 3, 1, 1);

        pushButton_first = new QPushButton(frame_body);
        pushButton_first->setObjectName(QString::fromUtf8("pushButton_first"));
        pushButton_first->setMinimumSize(QSize(285, 80));
        pushButton_first->setMaximumSize(QSize(285, 80));
        pushButton_first->setFont(font);
        pushButton_first->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"text-align:left;\n"
"padding:20px;"));
        pushButton_first->setIconSize(QSize(50, 50));
        pushButton_first->setFlat(true);

        gridLayout_2->addWidget(pushButton_first, 1, 1, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_3, 2, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer, 3, 1, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout_2->addItem(verticalSpacer_2, 0, 1, 1, 1);


        gridLayout->addWidget(frame_body, 1, 0, 1, 1);

        frame_header = new QFrame(screen_26);
        frame_header->setObjectName(QString::fromUtf8("frame_header"));
        frame_header->setMinimumSize(QSize(1024, 50));
        frame_header->setMaximumSize(QSize(1024, 50));
        frame_header->setFrameShape(QFrame::StyledPanel);
        frame_header->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_header);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(frame_header);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Sans Serif"));
        font1.setPointSize(14);
        label->setFont(font1);

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        gridLayout->addWidget(frame_header, 0, 0, 1, 1);


        retranslateUi(screen_26);

        QMetaObject::connectSlotsByName(screen_26);
    } // setupUi

    void retranslateUi(QWidget *screen_26)
    {
        screen_26->setWindowTitle(QApplication::translate("screen_26", "Form", 0, QApplication::UnicodeUTF8));
        pushButton_home->setText(QString());
        pushButton_4->setText(QApplication::translate("screen_26", "4.Fourth Floor\n"
"Activate", 0, QApplication::UnicodeUTF8));
        pushButton_second->setText(QApplication::translate("screen_26", "2.Second Floor\n"
"Unactivate", 0, QApplication::UnicodeUTF8));
        pushButton_3->setText(QApplication::translate("screen_26", "3.Third Floor\n"
"Unactivate", 0, QApplication::UnicodeUTF8));
        pushButton_first->setText(QApplication::translate("screen_26", "1.First Floor\n"
"Activate", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("screen_26", "Ventilation devices Integra Control Air", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class screen_26: public Ui_screen_26 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREEN_26_H
