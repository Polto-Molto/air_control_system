#include "screen_23.h"
#include "ui_screen_23.h"

screen_23::screen_23(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_23),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_23::~screen_23()
{
    delete ui;
}

void screen_23::reload()
{
    loadClimaData();
}

void screen_23::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_23::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(22));
}

void screen_23::on_pushButton_next_clicked()
{
    Q_EMIT(touched(24));
}

void screen_23::loadClimaData()
{
    foreach(QPushButton* button,devices.keys())
    {
        delete button;
    }
    devices.clear();

    if(devices.size() != _datamgr->climaTable()->size())
    {
        int col = 0;
        int row = 0;
        foreach(CLIMA_MODBUS clima, _datamgr->climaTable()->values())
        {
            QPushButton* button = new QPushButton(this);
            button->setStyleSheet("border-style: solid;border-width: 2px;font: 12pt Bold \"Sans Serif\";border-color: rgb(96, 153, 222);color: rgb(60, 103, 188);border-radius:12px;");
            button->setText(clima.name);
            button->setFixedSize(170,80);
            button->setObjectName(clima.config_name);
            ui->gridLayout_2->addWidget(button,row,col);

            devices.insert(button,clima);
            connect(button,SIGNAL(clicked()),this,SLOT(deviceSelected()));
            col++;
            if (col == 6)
            {
                col = 0;
                row++;
            }
        }
    }
}

void screen_23::deviceSelected()
{
    Q_EMIT(clima_device_selected(devices.value(((QPushButton*)sender()))));
    Q_EMIT(touched(25));
}
