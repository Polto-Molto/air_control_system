#ifndef MODBUS_MANAGER_H
#define MODBUS_MANAGER_H

#include <QObject>
#include <QVector>
#include <QTimer>
#include <qt4modbus.h>
#include "modbusclima.h"
#include "configmanager.h"
#include "datamanager.h"

class modbus_manager : public QObject
{
    Q_OBJECT
public:
    enum TYPE
    {
        CLIMA,
        AIR,
        SKUDO
    };

public:
    explicit modbus_manager(dataManager *,QObject *parent = 0);
    void setModbusRtuNetwork(Qt4Modbus *);
    Qt4Modbus *modbusNetwork();
    bool assignDevice(int id,int new_id,TYPE t);
    int addNewDevice(TYPE);
    bool removeDevice(TYPE);

    QVector<QString> *statusMessages();
    void clearStatusMessages();

public slots:
    void readModbusNetwrokData();
    void setClimaTable();
    void setClimaDataTables();
    void updateClima(CLIMA_MODBUS);
    void enableReadClima();
    void disableReadClima();

signals:
    void statusMessagesChanged();

private:
    bool read_status;
    bool ready_read;
    QTimer *timer;
    dataManager *_datamgr;
    Qt4Modbus* modbus_network;
    int clima_low_id_range;
    int clima_high_id_range;
    QVector<modbusClima*> *clima_devices;
    QVector<QString> *status_messages;

//    QVector<modbus_slave*> *air_devices;
//    QVector<modbus_slave*> *skudo_devices;


};

#endif // MODBUS_MANAGER_H
