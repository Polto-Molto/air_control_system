#ifndef SCREEN_6_H
#define SCREEN_6_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_6;
}

class screen_6 : public QWidget
{
    Q_OBJECT

public:
    explicit screen_6(dataManager*,QWidget *parent = 0);
    ~screen_6();
public slots:
    void on_pushButton_home_clicked();
    void on_pushButton_done_clicked();

    void on_pushButton_comfort_down_clicked();
    void on_pushButton_comfort_up_clicked();
    void on_pushButton_night_down_clicked();
    void on_pushButton_night_up_clicked();

    void setCurrentClima(CLIMA_MODBUS);

signals:
      void touched(int);

private:
    Ui::screen_6 *ui;
    dataManager *_datamgr;
    CLIMA_MODBUS _clima;
};

#endif // SCREEN_6_H
