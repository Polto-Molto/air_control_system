#include "screen_49.h"
#include "ui_screen_49.h"

screen_49::screen_49(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_49),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_49::~screen_49()
{
    delete ui;
}

void screen_49::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_49::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(22));
}

void screen_49::on_pushButton_done_clicked()
{
    Q_EMIT(touched(22));
}
