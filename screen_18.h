#ifndef SCREEN_18_H
#define SCREEN_18_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_18;
}

class screen_18 : public QWidget
{
    Q_OBJECT

public:
    explicit screen_18(dataManager*,QWidget *parent = 0);
    ~screen_18();
signals:
      void touched(int);

public slots:
      void on_pushButton_quit_clicked();
      void on_pushButton_next_day_clicked();
      void on_pushButton_next_save_clicked();

private:
    Ui::screen_18 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_18_H
