#include "screen_7.h"
#include "ui_screen_7.h"

screen_7::screen_7(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_7),
    _datamgr(dm)
{
    ui->setupUi(this);
    buttons.push_back(ui->schedule_1);
    buttons.push_back(ui->schedule_2);
    buttons.push_back(ui->schedule_3);
    buttons.push_back(ui->schedule_4);
    buttons.push_back(ui->schedule_5);
    buttons.push_back(ui->schedule_6);

    foreach(QPushButton* button, buttons)
        connect(button,SIGNAL(clicked())
                ,this,SLOT(scheduleSelected()));
    reload();
}

screen_7::~screen_7()
{
    delete ui;
}

void screen_7::reload()
{
    foreach(QPushButton* button, buttons)
        button->setText(_datamgr->scheduleData(button->objectName()).name);
}

void screen_7::scheduleSelected()
{
        foreach(QPushButton* button, buttons)
            button->setChecked(false);
        ((QPushButton*)sender())->setChecked(true);
        _schedule = ((QPushButton*)sender())->objectName();
}

void screen_7::setCurrentClima(CLIMA_MODBUS clima)
{
    _clima = clima;
    Q_EMIT(touched(7));
}

void screen_7::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_7::on_pushButton_done_clicked()
{
    _clima.schedule=_schedule;
    _datamgr->updateClimaConfigData(_clima);
    Q_EMIT(touched(5));
}

void screen_7::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(5));
}
