/********************************************************************************
** Form generated from reading UI file 'screen_12.ui'
**
** Created: Thu Apr 13 00:46:20 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREEN_12_H
#define UI_SCREEN_12_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QToolButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_screen_12
{
public:
    QGridLayout *gridLayout;
    QFrame *frame_header;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QFrame *frame_body;
    QGridLayout *gridLayout_2;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_3;
    QToolButton *toolButton_summer;
    QSpacerItem *horizontalSpacer_4;
    QToolButton *toolButton_winter;
    QSpacerItem *horizontalSpacer_7;
    QFrame *frame_footer;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_15;
    QPushButton *pushButton_cancel;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *pushButton_home;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *pushButton_done;
    QSpacerItem *horizontalSpacer_16;

    void setupUi(QWidget *screen_12)
    {
        if (screen_12->objectName().isEmpty())
            screen_12->setObjectName(QString::fromUtf8("screen_12"));
        screen_12->resize(1024, 600);
        screen_12->setMinimumSize(QSize(1024, 600));
        screen_12->setMaximumSize(QSize(1024, 600));
        gridLayout = new QGridLayout(screen_12);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame_header = new QFrame(screen_12);
        frame_header->setObjectName(QString::fromUtf8("frame_header"));
        frame_header->setMinimumSize(QSize(1024, 50));
        frame_header->setMaximumSize(QSize(1024, 50));
        frame_header->setFrameShape(QFrame::StyledPanel);
        frame_header->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_header);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(frame_header);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(14);
        label->setFont(font);

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        gridLayout->addWidget(frame_header, 0, 0, 1, 1);

        frame_body = new QFrame(screen_12);
        frame_body->setObjectName(QString::fromUtf8("frame_body"));
        frame_body->setMinimumSize(QSize(1024, 470));
        frame_body->setMaximumSize(QSize(1024, 470));
        frame_body->setFrameShape(QFrame::StyledPanel);
        frame_body->setFrameShadow(QFrame::Raised);
        gridLayout_2 = new QGridLayout(frame_body);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);

        toolButton_summer = new QToolButton(frame_body);
        toolButton_summer->setObjectName(QString::fromUtf8("toolButton_summer"));
        toolButton_summer->setMinimumSize(QSize(200, 200));
        toolButton_summer->setMaximumSize(QSize(200, 200));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Sans Serif"));
        font1.setPointSize(18);
        toolButton_summer->setFont(font1);
        toolButton_summer->setStyleSheet(QString::fromUtf8("QToolButton::checked {\n"
"background-color: rgb(217, 229, 255);\n"
"}\n"
"QToolButton{\n"
"border-style: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"text-align:left;\n"
"padding:20px;\n"
"}"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/images/Icons/Summer.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton_summer->setIcon(icon);
        toolButton_summer->setIconSize(QSize(80, 80));
        toolButton_summer->setCheckable(true);
        toolButton_summer->setChecked(false);
        toolButton_summer->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        toolButton_summer->setAutoRaise(true);

        horizontalLayout_2->addWidget(toolButton_summer);

        horizontalSpacer_4 = new QSpacerItem(100, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_4);

        toolButton_winter = new QToolButton(frame_body);
        toolButton_winter->setObjectName(QString::fromUtf8("toolButton_winter"));
        toolButton_winter->setMinimumSize(QSize(200, 200));
        toolButton_winter->setMaximumSize(QSize(200, 200));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Sans Serif"));
        font2.setPointSize(18);
        font2.setKerning(true);
        font2.setStyleStrategy(QFont::PreferDefault);
        toolButton_winter->setFont(font2);
        toolButton_winter->setFocusPolicy(Qt::TabFocus);
        toolButton_winter->setAcceptDrops(false);
        toolButton_winter->setLayoutDirection(Qt::LeftToRight);
        toolButton_winter->setAutoFillBackground(false);
        toolButton_winter->setStyleSheet(QString::fromUtf8("QToolButton::checked {\n"
"background-color: rgb(217, 229, 255);\n"
"}\n"
"QToolButton{\n"
"border-style: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"text-align:left;\n"
"padding:20px;\n"
"}"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/images/Icons/winter.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton_winter->setIcon(icon1);
        toolButton_winter->setIconSize(QSize(80, 80));
        toolButton_winter->setCheckable(true);
        toolButton_winter->setPopupMode(QToolButton::DelayedPopup);
        toolButton_winter->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        toolButton_winter->setAutoRaise(true);

        horizontalLayout_2->addWidget(toolButton_winter);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_7);


        gridLayout_2->addLayout(horizontalLayout_2, 0, 0, 1, 1);


        gridLayout->addWidget(frame_body, 1, 0, 1, 1);

        frame_footer = new QFrame(screen_12);
        frame_footer->setObjectName(QString::fromUtf8("frame_footer"));
        frame_footer->setMinimumSize(QSize(1024, 80));
        frame_footer->setMaximumSize(QSize(1024, 80));
        frame_footer->setFrameShape(QFrame::StyledPanel);
        frame_footer->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_footer);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_15 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_15);

        pushButton_cancel = new QPushButton(frame_footer);
        pushButton_cancel->setObjectName(QString::fromUtf8("pushButton_cancel"));
        pushButton_cancel->setMinimumSize(QSize(50, 50));
        pushButton_cancel->setMaximumSize(QSize(50, 50));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/images/Icons/Cross.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_cancel->setIcon(icon2);
        pushButton_cancel->setIconSize(QSize(50, 50));
        pushButton_cancel->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_cancel);

        horizontalSpacer_5 = new QSpacerItem(353, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);

        pushButton_home = new QPushButton(frame_footer);
        pushButton_home->setObjectName(QString::fromUtf8("pushButton_home"));
        pushButton_home->setMinimumSize(QSize(50, 50));
        pushButton_home->setMaximumSize(QSize(50, 50));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/images/Icons/home.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_home->setIcon(icon3);
        pushButton_home->setIconSize(QSize(50, 50));
        pushButton_home->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_home);

        horizontalSpacer_6 = new QSpacerItem(353, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);

        pushButton_done = new QPushButton(frame_footer);
        pushButton_done->setObjectName(QString::fromUtf8("pushButton_done"));
        pushButton_done->setMinimumSize(QSize(50, 50));
        pushButton_done->setMaximumSize(QSize(50, 50));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/images/Icons/check.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_done->setIcon(icon4);
        pushButton_done->setIconSize(QSize(50, 50));
        pushButton_done->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_done);

        horizontalSpacer_16 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_16);


        gridLayout->addWidget(frame_footer, 2, 0, 1, 1);


        retranslateUi(screen_12);

        QMetaObject::connectSlotsByName(screen_12);
    } // setupUi

    void retranslateUi(QWidget *screen_12)
    {
        screen_12->setWindowTitle(QApplication::translate("screen_12", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("screen_12", "Winter Summer selection", 0, QApplication::UnicodeUTF8));
        toolButton_summer->setText(QApplication::translate("screen_12", "Summer", 0, QApplication::UnicodeUTF8));
        toolButton_winter->setText(QApplication::translate("screen_12", "Winter", 0, QApplication::UnicodeUTF8));
        pushButton_home->setText(QString());
        pushButton_done->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class screen_12: public Ui_screen_12 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREEN_12_H
