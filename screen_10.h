#ifndef SCREEN_10_H
#define SCREEN_10_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_10;
}

class screen_10 : public QWidget
{
    Q_OBJECT

public:
    explicit screen_10(dataManager*,QWidget *parent = 0);
    ~screen_10();
signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_done_clicked();
      void on_pushButton_apply_all_clicked();

private:
    Ui::screen_10 *ui;
    dataManager* _datamgr;
};

#endif // SCREEN_10_H
