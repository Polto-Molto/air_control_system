#ifndef SCREEN_15_H
#define SCREEN_15_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_15;
}

class screen_15 : public QWidget
{
    Q_OBJECT

public:
    explicit screen_15(dataManager*,QWidget *parent = 0);
    ~screen_15();
signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_done_clicked();
      void on_pushButton_cancel_clicked();

      void on_pushButton_hours_up_clicked();
      void on_pushButton_hours_down_clicked();
      void on_pushButton_mins_up_clicked();
      void on_pushButton_mins_down_clicked();

private:
    Ui::screen_15 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_15_H
