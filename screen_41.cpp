#include "screen_41.h"
#include "ui_screen_41.h"

screen_41::screen_41(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_41),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_41::~screen_41()
{
    delete ui;
}

void screen_41::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_41::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(22));
}

void screen_41::on_pushButton_next_clicked()
{
    Q_EMIT(touched(42));
}

void screen_41::on_pushButton_test_clicked()
{
    Q_EMIT(touched(43));
}
