/********************************************************************************
** Form generated from reading UI file 'screen_22.ui'
**
** Created: Thu Apr 13 00:46:20 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREEN_22_H
#define UI_SCREEN_22_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_screen_22
{
public:
    QGridLayout *gridLayout;
    QFrame *frame_header;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QFrame *frame_body;
    QGridLayout *gridLayout_2;
    QPushButton *pushButton_reset;
    QPushButton *pushButton_hydronic;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *pushButton_radiant;
    QPushButton *pushButton_netwrok;
    QSpacerItem *verticalSpacer;
    QPushButton *pushButton_modbus;
    QPushButton *pushButton_ventilation;
    QSpacerItem *horizontalSpacer_4;
    QFrame *frame_footer;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_15;
    QPushButton *pushButton_cancel;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *pushButton_home;
    QSpacerItem *horizontalSpacer_6;
    QSpacerItem *horizontalSpacer_16;

    void setupUi(QWidget *screen_22)
    {
        if (screen_22->objectName().isEmpty())
            screen_22->setObjectName(QString::fromUtf8("screen_22"));
        screen_22->resize(1024, 600);
        screen_22->setMinimumSize(QSize(1024, 600));
        screen_22->setMaximumSize(QSize(1024, 600));
        gridLayout = new QGridLayout(screen_22);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame_header = new QFrame(screen_22);
        frame_header->setObjectName(QString::fromUtf8("frame_header"));
        frame_header->setMinimumSize(QSize(1024, 50));
        frame_header->setMaximumSize(QSize(1024, 50));
        frame_header->setFrameShape(QFrame::StyledPanel);
        frame_header->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_header);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(frame_header);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(14);
        label->setFont(font);

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        gridLayout->addWidget(frame_header, 0, 0, 1, 1);

        frame_body = new QFrame(screen_22);
        frame_body->setObjectName(QString::fromUtf8("frame_body"));
        frame_body->setMinimumSize(QSize(1024, 470));
        frame_body->setMaximumSize(QSize(1024, 470));
        frame_body->setFrameShape(QFrame::StyledPanel);
        frame_body->setFrameShadow(QFrame::Raised);
        gridLayout_2 = new QGridLayout(frame_body);
        gridLayout_2->setSpacing(20);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(-1, 20, -1, -1);
        pushButton_reset = new QPushButton(frame_body);
        pushButton_reset->setObjectName(QString::fromUtf8("pushButton_reset"));
        pushButton_reset->setMinimumSize(QSize(440, 80));
        pushButton_reset->setMaximumSize(QSize(440, 80));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Sans Serif"));
        font1.setPointSize(14);
        font1.setBold(true);
        font1.setWeight(75);
        pushButton_reset->setFont(font1);
        pushButton_reset->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"text-align:left;\n"
"padding:20px;"));
        pushButton_reset->setIconSize(QSize(50, 50));
        pushButton_reset->setFlat(true);

        gridLayout_2->addWidget(pushButton_reset, 1, 1, 1, 1);

        pushButton_hydronic = new QPushButton(frame_body);
        pushButton_hydronic->setObjectName(QString::fromUtf8("pushButton_hydronic"));
        pushButton_hydronic->setMinimumSize(QSize(440, 80));
        pushButton_hydronic->setMaximumSize(QSize(440, 80));
        pushButton_hydronic->setFont(font1);
        pushButton_hydronic->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"text-align:left;\n"
"padding:20px;"));
        pushButton_hydronic->setIconSize(QSize(50, 50));
        pushButton_hydronic->setFlat(true);

        gridLayout_2->addWidget(pushButton_hydronic, 0, 2, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_3, 1, 0, 1, 1);

        pushButton_radiant = new QPushButton(frame_body);
        pushButton_radiant->setObjectName(QString::fromUtf8("pushButton_radiant"));
        pushButton_radiant->setMinimumSize(QSize(440, 80));
        pushButton_radiant->setMaximumSize(QSize(440, 80));
        pushButton_radiant->setFont(font1);
        pushButton_radiant->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"text-align:left;\n"
"padding:20px;"));
        pushButton_radiant->setIconSize(QSize(50, 50));
        pushButton_radiant->setFlat(true);

        gridLayout_2->addWidget(pushButton_radiant, 0, 1, 1, 1);

        pushButton_netwrok = new QPushButton(frame_body);
        pushButton_netwrok->setObjectName(QString::fromUtf8("pushButton_netwrok"));
        pushButton_netwrok->setMinimumSize(QSize(440, 80));
        pushButton_netwrok->setMaximumSize(QSize(440, 80));
        pushButton_netwrok->setFont(font1);
        pushButton_netwrok->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"text-align:left;\n"
"padding:20px;"));
        pushButton_netwrok->setIconSize(QSize(50, 50));
        pushButton_netwrok->setFlat(true);

        gridLayout_2->addWidget(pushButton_netwrok, 2, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer, 3, 1, 1, 1);

        pushButton_modbus = new QPushButton(frame_body);
        pushButton_modbus->setObjectName(QString::fromUtf8("pushButton_modbus"));
        pushButton_modbus->setMinimumSize(QSize(440, 80));
        pushButton_modbus->setMaximumSize(QSize(440, 80));
        pushButton_modbus->setFont(font1);
        pushButton_modbus->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"text-align:left;\n"
"padding:20px;"));
        pushButton_modbus->setIconSize(QSize(50, 50));
        pushButton_modbus->setFlat(true);

        gridLayout_2->addWidget(pushButton_modbus, 2, 2, 1, 1);

        pushButton_ventilation = new QPushButton(frame_body);
        pushButton_ventilation->setObjectName(QString::fromUtf8("pushButton_ventilation"));
        pushButton_ventilation->setMinimumSize(QSize(440, 80));
        pushButton_ventilation->setMaximumSize(QSize(440, 80));
        pushButton_ventilation->setFont(font1);
        pushButton_ventilation->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"text-align:left;\n"
"padding:20px;"));
        pushButton_ventilation->setIconSize(QSize(50, 50));
        pushButton_ventilation->setFlat(true);

        gridLayout_2->addWidget(pushButton_ventilation, 1, 2, 1, 1);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_4, 1, 3, 1, 1);


        gridLayout->addWidget(frame_body, 1, 0, 1, 1);

        frame_footer = new QFrame(screen_22);
        frame_footer->setObjectName(QString::fromUtf8("frame_footer"));
        frame_footer->setMinimumSize(QSize(1024, 80));
        frame_footer->setMaximumSize(QSize(1024, 80));
        frame_footer->setFrameShape(QFrame::StyledPanel);
        frame_footer->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_footer);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_15 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_15);

        pushButton_cancel = new QPushButton(frame_footer);
        pushButton_cancel->setObjectName(QString::fromUtf8("pushButton_cancel"));
        pushButton_cancel->setMinimumSize(QSize(50, 50));
        pushButton_cancel->setMaximumSize(QSize(50, 50));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/images/Icons/Cross.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_cancel->setIcon(icon);
        pushButton_cancel->setIconSize(QSize(50, 50));
        pushButton_cancel->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_cancel);

        horizontalSpacer_5 = new QSpacerItem(353, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);

        pushButton_home = new QPushButton(frame_footer);
        pushButton_home->setObjectName(QString::fromUtf8("pushButton_home"));
        pushButton_home->setMinimumSize(QSize(50, 50));
        pushButton_home->setMaximumSize(QSize(50, 50));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/images/Icons/home.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_home->setIcon(icon1);
        pushButton_home->setIconSize(QSize(50, 50));
        pushButton_home->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_home);

        horizontalSpacer_6 = new QSpacerItem(415, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);

        horizontalSpacer_16 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_16);


        gridLayout->addWidget(frame_footer, 2, 0, 1, 1);


        retranslateUi(screen_22);

        QMetaObject::connectSlotsByName(screen_22);
    } // setupUi

    void retranslateUi(QWidget *screen_22)
    {
        screen_22->setWindowTitle(QApplication::translate("screen_22", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("screen_22", "Advanced settings", 0, QApplication::UnicodeUTF8));
        pushButton_reset->setText(QApplication::translate("screen_22", "Reset to default settings", 0, QApplication::UnicodeUTF8));
        pushButton_hydronic->setText(QApplication::translate("screen_22", "Hydronics devices Shudo", 0, QApplication::UnicodeUTF8));
        pushButton_radiant->setText(QApplication::translate("screen_22", "Radiant devices Integra Control Clima", 0, QApplication::UnicodeUTF8));
        pushButton_netwrok->setText(QApplication::translate("screen_22", "TCP/IP Network settings", 0, QApplication::UnicodeUTF8));
        pushButton_modbus->setText(QApplication::translate("screen_22", "Modbus protocol", 0, QApplication::UnicodeUTF8));
        pushButton_ventilation->setText(QApplication::translate("screen_22", "Ventilation devices Integra Control Air", 0, QApplication::UnicodeUTF8));
        pushButton_home->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class screen_22: public Ui_screen_22 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREEN_22_H
