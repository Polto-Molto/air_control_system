/********************************************************************************
** Form generated from reading UI file 'screen_13.ui'
**
** Created: Thu Apr 13 00:46:20 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREEN_13_H
#define UI_SCREEN_13_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_screen_13
{
public:
    QGridLayout *gridLayout;
    QFrame *frame_header;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QFrame *frame_footer;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_15;
    QPushButton *pushButton_cancel;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *pushButton_home;
    QSpacerItem *horizontalSpacer_6;
    QSpacerItem *horizontalSpacer_16;
    QFrame *frame_body;
    QVBoxLayout *verticalLayout_5;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout_7;
    QSpacerItem *horizontalSpacer_3;
    QFrame *frame;
    QVBoxLayout *verticalLayout;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *pushButton_first_fan;
    QLabel *label_6;
    QFrame *frame_2;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *pushButton_second_fan;
    QLabel *label_11;
    QFrame *frame_3;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_12;
    QLabel *label_13;
    QLabel *label_14;
    QLabel *label_15;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *pushButton_third_fan;
    QLabel *label_16;
    QFrame *frame_4;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_17;
    QLabel *label_18;
    QLabel *label_19;
    QLabel *label_20;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *pushButton_fourth_fan;
    QLabel *label_21;
    QSpacerItem *horizontalSpacer_4;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *screen_13)
    {
        if (screen_13->objectName().isEmpty())
            screen_13->setObjectName(QString::fromUtf8("screen_13"));
        screen_13->resize(1024, 600);
        screen_13->setMinimumSize(QSize(1024, 600));
        screen_13->setMaximumSize(QSize(1024, 600));
        gridLayout = new QGridLayout(screen_13);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame_header = new QFrame(screen_13);
        frame_header->setObjectName(QString::fromUtf8("frame_header"));
        frame_header->setMinimumSize(QSize(1024, 50));
        frame_header->setMaximumSize(QSize(1024, 50));
        frame_header->setFrameShape(QFrame::StyledPanel);
        frame_header->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_header);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(frame_header);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(14);
        label->setFont(font);

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        gridLayout->addWidget(frame_header, 0, 0, 1, 1);

        frame_footer = new QFrame(screen_13);
        frame_footer->setObjectName(QString::fromUtf8("frame_footer"));
        frame_footer->setMinimumSize(QSize(1024, 80));
        frame_footer->setMaximumSize(QSize(1024, 80));
        frame_footer->setFrameShape(QFrame::StyledPanel);
        frame_footer->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_footer);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_15 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_15);

        pushButton_cancel = new QPushButton(frame_footer);
        pushButton_cancel->setObjectName(QString::fromUtf8("pushButton_cancel"));
        pushButton_cancel->setMinimumSize(QSize(50, 50));
        pushButton_cancel->setMaximumSize(QSize(50, 50));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/images/Icons/Cross.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_cancel->setIcon(icon);
        pushButton_cancel->setIconSize(QSize(50, 50));
        pushButton_cancel->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_cancel);

        horizontalSpacer_5 = new QSpacerItem(353, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);

        pushButton_home = new QPushButton(frame_footer);
        pushButton_home->setObjectName(QString::fromUtf8("pushButton_home"));
        pushButton_home->setMinimumSize(QSize(50, 50));
        pushButton_home->setMaximumSize(QSize(50, 50));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/images/Icons/home.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_home->setIcon(icon1);
        pushButton_home->setIconSize(QSize(50, 50));
        pushButton_home->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_home);

        horizontalSpacer_6 = new QSpacerItem(353, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);

        horizontalSpacer_16 = new QSpacerItem(106, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_16);


        gridLayout->addWidget(frame_footer, 2, 0, 1, 1);

        frame_body = new QFrame(screen_13);
        frame_body->setObjectName(QString::fromUtf8("frame_body"));
        frame_body->setMinimumSize(QSize(1024, 470));
        frame_body->setMaximumSize(QSize(1024, 470));
        frame_body->setFrameShape(QFrame::StyledPanel);
        frame_body->setFrameShadow(QFrame::Raised);
        verticalLayout_5 = new QVBoxLayout(frame_body);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer_2);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(20);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_3);

        frame = new QFrame(frame_body);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setMinimumSize(QSize(210, 280));
        frame->setMaximumSize(QSize(210, 280));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        verticalLayout = new QVBoxLayout(frame);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label_2 = new QLabel(frame);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(0, 40));
        label_2->setMaximumSize(QSize(16777215, 40));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Sans Serif"));
        font1.setPointSize(18);
        label_2->setFont(font1);
        label_2->setStyleSheet(QString::fromUtf8("color:rgb(32, 103, 179);"));
        label_2->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_2);

        label_3 = new QLabel(frame);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMinimumSize(QSize(0, 28));
        label_3->setMaximumSize(QSize(16777215, 28));
        label_3->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_3);

        label_4 = new QLabel(frame);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/Rounded Rectangle 1.png")));
        label_4->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_4);

        label_5 = new QLabel(frame);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setMinimumSize(QSize(0, 28));
        label_5->setMaximumSize(QSize(16777215, 28));
        label_5->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_5);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        pushButton_first_fan = new QPushButton(frame);
        pushButton_first_fan->setObjectName(QString::fromUtf8("pushButton_first_fan"));
        pushButton_first_fan->setEnabled(true);
        pushButton_first_fan->setMinimumSize(QSize(50, 50));
        pushButton_first_fan->setMaximumSize(QSize(16777215, 50));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/images/Icons/fan_med.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_first_fan->setIcon(icon2);
        pushButton_first_fan->setIconSize(QSize(165, 50));
        pushButton_first_fan->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_first_fan);


        verticalLayout->addLayout(horizontalLayout_2);

        label_6 = new QLabel(frame);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setMinimumSize(QSize(0, 40));
        label_6->setMaximumSize(QSize(16777215, 40));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Sans Serif"));
        font2.setPointSize(16);
        label_6->setFont(font2);
        label_6->setStyleSheet(QString::fromUtf8("color:green;"));
        label_6->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_6);


        horizontalLayout_7->addWidget(frame);

        frame_2 = new QFrame(frame_body);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setMinimumSize(QSize(210, 280));
        frame_2->setMaximumSize(QSize(210, 280));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        verticalLayout_2 = new QVBoxLayout(frame_2);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_7 = new QLabel(frame_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setMinimumSize(QSize(0, 40));
        label_7->setMaximumSize(QSize(16777215, 40));
        label_7->setFont(font1);
        label_7->setStyleSheet(QString::fromUtf8("color:rgb(32, 103, 179);"));
        label_7->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_7);

        label_8 = new QLabel(frame_2);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setMinimumSize(QSize(0, 28));
        label_8->setMaximumSize(QSize(16777215, 28));
        label_8->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_8);

        label_9 = new QLabel(frame_2);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/Rounded Rectangle 1.png")));
        label_9->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_9);

        label_10 = new QLabel(frame_2);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setMinimumSize(QSize(0, 28));
        label_10->setMaximumSize(QSize(16777215, 28));
        label_10->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_10);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        pushButton_second_fan = new QPushButton(frame_2);
        pushButton_second_fan->setObjectName(QString::fromUtf8("pushButton_second_fan"));
        pushButton_second_fan->setEnabled(true);
        pushButton_second_fan->setMinimumSize(QSize(50, 50));
        pushButton_second_fan->setMaximumSize(QSize(16777215, 50));
        pushButton_second_fan->setIcon(icon2);
        pushButton_second_fan->setIconSize(QSize(165, 50));
        pushButton_second_fan->setFlat(true);

        horizontalLayout_4->addWidget(pushButton_second_fan);


        verticalLayout_2->addLayout(horizontalLayout_4);

        label_11 = new QLabel(frame_2);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setMinimumSize(QSize(0, 40));
        label_11->setMaximumSize(QSize(16777215, 40));
        label_11->setFont(font2);
        label_11->setStyleSheet(QString::fromUtf8("color:green;"));
        label_11->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_11);


        horizontalLayout_7->addWidget(frame_2);

        frame_3 = new QFrame(frame_body);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setMinimumSize(QSize(210, 280));
        frame_3->setMaximumSize(QSize(210, 280));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        verticalLayout_3 = new QVBoxLayout(frame_3);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label_12 = new QLabel(frame_3);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setMinimumSize(QSize(0, 40));
        label_12->setMaximumSize(QSize(16777215, 40));
        label_12->setFont(font1);
        label_12->setStyleSheet(QString::fromUtf8("color:rgb(32, 103, 179);"));
        label_12->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(label_12);

        label_13 = new QLabel(frame_3);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setMinimumSize(QSize(0, 28));
        label_13->setMaximumSize(QSize(16777215, 28));
        label_13->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(label_13);

        label_14 = new QLabel(frame_3);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/Rounded Rectangle 1-1.png")));
        label_14->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(label_14);

        label_15 = new QLabel(frame_3);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setMinimumSize(QSize(0, 28));
        label_15->setMaximumSize(QSize(16777215, 28));
        label_15->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(label_15);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        pushButton_third_fan = new QPushButton(frame_3);
        pushButton_third_fan->setObjectName(QString::fromUtf8("pushButton_third_fan"));
        pushButton_third_fan->setEnabled(true);
        pushButton_third_fan->setMinimumSize(QSize(50, 50));
        pushButton_third_fan->setMaximumSize(QSize(16777215, 50));
        pushButton_third_fan->setIcon(icon2);
        pushButton_third_fan->setIconSize(QSize(165, 50));
        pushButton_third_fan->setFlat(true);

        horizontalLayout_5->addWidget(pushButton_third_fan);


        verticalLayout_3->addLayout(horizontalLayout_5);

        label_16 = new QLabel(frame_3);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setMinimumSize(QSize(0, 40));
        label_16->setMaximumSize(QSize(16777215, 40));
        label_16->setFont(font2);
        label_16->setStyleSheet(QString::fromUtf8("color:green;"));
        label_16->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(label_16);


        horizontalLayout_7->addWidget(frame_3);

        frame_4 = new QFrame(frame_body);
        frame_4->setObjectName(QString::fromUtf8("frame_4"));
        frame_4->setMinimumSize(QSize(210, 280));
        frame_4->setMaximumSize(QSize(210, 280));
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);
        verticalLayout_4 = new QVBoxLayout(frame_4);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label_17 = new QLabel(frame_4);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setMinimumSize(QSize(0, 40));
        label_17->setMaximumSize(QSize(16777215, 40));
        label_17->setFont(font1);
        label_17->setStyleSheet(QString::fromUtf8("color:rgb(32, 103, 179);"));
        label_17->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(label_17);

        label_18 = new QLabel(frame_4);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setMinimumSize(QSize(0, 28));
        label_18->setMaximumSize(QSize(16777215, 28));
        label_18->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(label_18);

        label_19 = new QLabel(frame_4);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        label_19->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/Rounded Rectangle 1-2.png")));
        label_19->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(label_19);

        label_20 = new QLabel(frame_4);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setMinimumSize(QSize(0, 28));
        label_20->setMaximumSize(QSize(16777215, 28));
        label_20->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(label_20);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        pushButton_fourth_fan = new QPushButton(frame_4);
        pushButton_fourth_fan->setObjectName(QString::fromUtf8("pushButton_fourth_fan"));
        pushButton_fourth_fan->setEnabled(true);
        pushButton_fourth_fan->setMinimumSize(QSize(50, 50));
        pushButton_fourth_fan->setMaximumSize(QSize(16777215, 50));
        pushButton_fourth_fan->setIcon(icon2);
        pushButton_fourth_fan->setIconSize(QSize(165, 50));
        pushButton_fourth_fan->setFlat(true);

        horizontalLayout_6->addWidget(pushButton_fourth_fan);


        verticalLayout_4->addLayout(horizontalLayout_6);

        label_21 = new QLabel(frame_4);
        label_21->setObjectName(QString::fromUtf8("label_21"));
        label_21->setMinimumSize(QSize(0, 40));
        label_21->setMaximumSize(QSize(16777215, 40));
        label_21->setFont(font2);
        label_21->setStyleSheet(QString::fromUtf8("color:green;"));
        label_21->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(label_21);


        horizontalLayout_7->addWidget(frame_4);

        horizontalSpacer_4 = new QSpacerItem(17, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_4);


        verticalLayout_5->addLayout(horizontalLayout_7);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer);


        gridLayout->addWidget(frame_body, 1, 0, 1, 1);


        retranslateUi(screen_13);

        QMetaObject::connectSlotsByName(screen_13);
    } // setupUi

    void retranslateUi(QWidget *screen_13)
    {
        screen_13->setWindowTitle(QApplication::translate("screen_13", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("screen_13", "Ventilation Units", 0, QApplication::UnicodeUTF8));
        pushButton_home->setText(QString());
        label_2->setText(QApplication::translate("screen_13", "FIRST FLOOR", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("screen_13", "Air Quality", 0, QApplication::UnicodeUTF8));
        label_4->setText(QString());
        label_5->setText(QApplication::translate("screen_13", "Air Change", 0, QApplication::UnicodeUTF8));
        pushButton_first_fan->setText(QString());
        label_6->setText(QApplication::translate("screen_13", "AUTO", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("screen_13", "SECOND FLOOR", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("screen_13", "Air Quality", 0, QApplication::UnicodeUTF8));
        label_9->setText(QString());
        label_10->setText(QApplication::translate("screen_13", "Air Change", 0, QApplication::UnicodeUTF8));
        pushButton_second_fan->setText(QString());
        label_11->setText(QApplication::translate("screen_13", "AUTO", 0, QApplication::UnicodeUTF8));
        label_12->setText(QApplication::translate("screen_13", "THIRD FLOOR", 0, QApplication::UnicodeUTF8));
        label_13->setText(QApplication::translate("screen_13", "Air Quality", 0, QApplication::UnicodeUTF8));
        label_14->setText(QString());
        label_15->setText(QApplication::translate("screen_13", "Air Change", 0, QApplication::UnicodeUTF8));
        pushButton_third_fan->setText(QString());
        label_16->setText(QApplication::translate("screen_13", "AUTO", 0, QApplication::UnicodeUTF8));
        label_17->setText(QApplication::translate("screen_13", "FOURTH FLOOR", 0, QApplication::UnicodeUTF8));
        label_18->setText(QApplication::translate("screen_13", "Air Quality", 0, QApplication::UnicodeUTF8));
        label_19->setText(QString());
        label_20->setText(QApplication::translate("screen_13", "Air Change", 0, QApplication::UnicodeUTF8));
        pushButton_fourth_fan->setText(QString());
        label_21->setText(QApplication::translate("screen_13", "AUTO", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class screen_13: public Ui_screen_13 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREEN_13_H
