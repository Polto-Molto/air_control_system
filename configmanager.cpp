#include "configmanager.h"
#include <QDebug>

configManager::configManager(dataManager* dm,QObject *parent) :
    QObject(parent),
    _datamgr(dm)
{
    mainCF  = 0;
    climaCF = 0;
    airCF   = 0;
    skudoCF = 0;
    connect(_datamgr,SIGNAL(mainConfigDataUpdated(MAIN_CONFIG))
            ,this,SLOT(updateMainConfig(MAIN_CONFIG)));

    connect(_datamgr,SIGNAL(scheduleConfigDataUpdated(SCHEDULE))
            ,this,SLOT(updateSChedule(SCHEDULE)));

    connect(_datamgr,SIGNAL(climaConfigDataUpdated(CLIMA_MODBUS))
            ,this,SLOT(updateClima(CLIMA_MODBUS)));

    connect(_datamgr,SIGNAL(climaConfigDataTableUpdated())
            ,this,SLOT(updateClimaTable()));

}

bool configManager::setConfigFiles(QString mcf,QString dcf, QString ccf, QString acf, QString scf)
{
    mainCF  = new QSettings(mcf,QSettings::IniFormat);
    scheduleCF = new QSettings(dcf,QSettings::IniFormat);
    climaCF = new QSettings(ccf,QSettings::IniFormat);
    airCF   = new QSettings(acf,QSettings::IniFormat);
    skudoCF = new QSettings(scf,QSettings::IniFormat);
    return true;
}

bool configManager::loadMainConfig()
{
    MAIN_CONFIG main_config;

    mainCF->beginGroup("main");
    main_config.schedule_num = mainCF->value("schedule_num").toInt();
    main_config.clima_num = mainCF->value("clima_num").toInt();
    main_config.air_num = mainCF->value("air_num").toInt();
    main_config.skudo_num = mainCF->value("skudo_num").toInt();
    main_config.season = (SEASON)mainCF->value("season").toInt();
    mainCF->endGroup();
    mainCF->sync();
    _datamgr->setMainConfigData(main_config);
    return true;
}
bool configManager::updateMainConfig(MAIN_CONFIG main_config)
{
    mainCF->beginGroup("main");
    mainCF->setValue("schedule_num",main_config.schedule_num );
    mainCF->setValue("clima_num",main_config.clima_num );
    mainCF->setValue("air_num",main_config.air_num );
    mainCF->setValue("skudo_num",main_config.skudo_num );
    mainCF->setValue("season",main_config.season );
    mainCF->endGroup();

    return true;
}

bool configManager::loadScheduleConfig()
{
    QMap<QString,SCHEDULE> schedule_table;
    for(int i = 1 ; i <= _datamgr->mainData().schedule_num ; i++)
    {
        SCHEDULE schedule;
        QString num;
        QString group_name ="schedule_"+num.setNum(i);
        scheduleCF->beginGroup(group_name);
        schedule.config_name = group_name;
        schedule.name =  scheduleCF->value("name").toString();
        schedule.comfort_start = scheduleCF->value("comfort_start").toTime();
        schedule.night_start = scheduleCF->value("night_start").toTime();
        scheduleCF->endGroup();
        schedule_table.insert(group_name,schedule);
    }
    scheduleCF->sync();
    _datamgr->setScheduleConfigData(schedule_table);
    return true;
}

bool configManager::updateSChedule(SCHEDULE schedule)
{
    scheduleCF->beginGroup(schedule.config_name);
    scheduleCF->setValue("name",schedule.name );
    scheduleCF->setValue("comfort_start",schedule.comfort_start.toString() );
    scheduleCF->setValue("night_start",schedule.night_start.toString() );
    scheduleCF->endGroup();
    return true;
}

bool configManager::loadClimaConfig()
{
    QMap<QString,CLIMA_MODBUS> clima_table;

    for(int i = 1 ; i <= _datamgr->mainData().clima_num ; i++)
    {
        CLIMA_MODBUS clima;
        QString num;
        QString group_name ="clima_"+num.setNum(i);
        climaCF->beginGroup(group_name);
        clima.config_name = group_name;
        clima.id = climaCF->value("id").toInt();
        clima.name = climaCF->value("name").toString();
        clima.room_number = climaCF->value("room_number").toInt();
        clima.activate = climaCF->value("activate").toBool();
        clima.dew = climaCF->value("dew_formation").toBool();
        clima.alarm = climaCF->value("room_probe_alarm").toBool();
        clima.season = (SEASON)climaCF->value("seasonal").toInt();
        clima.schedule =  climaCF->value("schedule").toString();
        clima.comfort_start = climaCF->value("comfort_start").toTime();
        clima.night_start = climaCF->value("night_start").toTime();
        clima.winter_comfort_temp_point = climaCF->value("winter_comfort_temp_point").toInt();
        clima.winter_night_temp_point = climaCF->value("winter_night_temp_point").toInt();
        clima.summer_comfort_temp_point = climaCF->value("summer_comfort_temp_point").toInt();
        clima.summer_night_temp_point = climaCF->value("summer_night_temp_point").toInt();
        clima.summer_humidity_point = climaCF->value("summer_humidity_point").toInt();
        climaCF->endGroup();
        clima_table.insert(group_name,clima);
    }
    climaCF->sync();
    _datamgr->setClimaConfigData(clima_table);
    return true;
}

bool configManager::updateClimaTable()
{
    foreach(CLIMA_MODBUS clima , _datamgr->climaTable()->values())
    {
        updateClima(clima);
    }
}

bool configManager::updateClima(CLIMA_MODBUS clima)
{
    climaCF->beginGroup(clima.config_name);
    climaCF->setValue("config_name",clima.config_name );
    climaCF->setValue("id",clima.id );
    climaCF->setValue("name",clima.name );
    climaCF->setValue("room_number",clima.room_number );
    climaCF->setValue("activate",clima.activate );
    climaCF->setValue("dew_formation",clima.dew );
    climaCF->setValue("room_probe_alarm",clima.alarm );
    climaCF->setValue("seasonal",clima.season );
    climaCF->setValue("schedule",clima.schedule );
    climaCF->setValue("comfort_start",clima.comfort_start.toString() );
    climaCF->setValue("night_start",clima.night_start.toString());
    climaCF->setValue("winter_comfort_temp_point",clima.winter_comfort_temp_point );
    climaCF->setValue("winter_night_temp_point",clima.winter_night_temp_point );
    climaCF->setValue("summer_comfort_temp_point",clima.summer_comfort_temp_point );
    climaCF->setValue("summer_night_temp_point",clima.summer_night_temp_point );
    climaCF->setValue("summer_humidity_point",clima.summer_humidity_point );
    climaCF->endGroup();

    return true;
}


bool configManager::loadAirConfig()
{
    return true;
}

bool configManager::loadSkudoConfig()
{
    return true;
}
