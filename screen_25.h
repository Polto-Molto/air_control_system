#ifndef SCREEN_25_H
#define SCREEN_25_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_25;
}

class screen_25 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_25(dataManager *,QWidget *parent = 0);
    ~screen_25();

signals:
      void touched(int);
      void assignModbus(CLIMA_MODBUS);

public slots:
      void reload();
      void setCurrentClima(CLIMA_MODBUS);
      void setCurrentClimaName(QString);
      QString currentClimaName();


      void on_pushButton_deactivate_clicked();
      void on_pushButton_activate_clicked();

      void on_pushButton_home_clicked();
      void on_pushButton_back_clicked();
      void on_pushButton_cancel_clicked();
      void on_pushButton_edit_clicked();
      void on_pushButton_done_clicked();
      void on_pushButton_next_clicked();
      void on_pushButton_clima_id_clicked();

private:
    Ui::screen_25 *ui;
    dataManager *_datamgr;
    CLIMA_MODBUS _clima;
};

#endif // SCREEN_25_H
