#ifndef SCREEN_19_H
#define SCREEN_19_H

#include <QWidget>
#include <QPushButton>
#include "datamanager.h"

namespace Ui {
class screen_19;
}

class screen_19 : public QWidget
{
    Q_OBJECT

public:
    explicit screen_19(dataManager*,QWidget *parent = 0);
    ~screen_19();
signals:
      void touched(int);
      void selectedSchedule(QString);

public slots:
      void reload();
      void scheduleSelected();
      void setCurrentScheduleName(QString);
      QString currentScheduleName();

      void on_pushButton_edit_clicked();
      void on_pushButton_datetime_clicked();
      void on_pushButton_cancel_clicked();

private:
    Ui::screen_19 *ui;
    dataManager *_datamgr;
    QVector<QPushButton*> buttons;
    SCHEDULE _schedule;
};

#endif // SCREEN_19_H
