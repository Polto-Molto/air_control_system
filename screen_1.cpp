#include "screen_1.h"
#include "ui_screen_1.h"

screen_1::screen_1(dataManager *dm,modbus_manager *m,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_1),
    _datamgr(dm),
    _mbus(m)
{
    ui->setupUi(this);
}

screen_1::~screen_1()
{
    delete ui;
}

void screen_1::reload()
{
//    updateData( clima);
}

void screen_1::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

// status air_temp relative_humidity floor_temp dew_point_temp
// short s,float at,short rh,float ft,float dt
void screen_1::updateData(CLIMA_MODBUS clima)
{
    ui->label_air_temp->setText(QString::number(clima.air_temp,'f',1));
    ui->label_humidity->setNum(clima.relative_humidity);
}

void screen_1::closeApplication()
{
    Q_EMIT(closeApp());
}
