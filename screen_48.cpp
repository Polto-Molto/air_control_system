#include "screen_48.h"
#include "ui_screen_48.h"

screen_48::screen_48(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_48),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_48::~screen_48()
{
    delete ui;
}

void screen_48::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(22));
}

void screen_48::on_pushButton_done_clicked()
{
    Q_EMIT(touched(35));
}
