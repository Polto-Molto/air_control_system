/********************************************************************************
** Form generated from reading UI file 'keyboard.ui'
**
** Created: Thu Apr 13 00:46:21 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KEYBOARD_H
#define UI_KEYBOARD_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_keyboard
{
public:
    QGridLayout *gridLayout;
    QFrame *frame_body_2;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *pushButton_q;
    QPushButton *pushButton_w;
    QPushButton *pushButton_e;
    QPushButton *pushButton_r;
    QPushButton *pushButton_t;
    QPushButton *pushButton_y;
    QPushButton *pushButton_u;
    QPushButton *pushButton_i;
    QPushButton *pushButton_o;
    QPushButton *pushButton_p;
    QPushButton *pushButton_backspace;
    QSpacerItem *horizontalSpacer_4;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *pushButton_a;
    QPushButton *pushButton_s;
    QPushButton *pushButton_d;
    QPushButton *pushButton_f;
    QPushButton *pushButton_g;
    QPushButton *pushButton_h;
    QPushButton *pushButton_j;
    QPushButton *pushButton_k;
    QPushButton *pushButton_l;
    QPushButton *pushButton_return;
    QSpacerItem *horizontalSpacer_5;
    QHBoxLayout *horizontalLayout_7;
    QSpacerItem *horizontalSpacer_7;
    QPushButton *pushButton_lshift;
    QPushButton *pushButton_z;
    QPushButton *pushButton_x;
    QPushButton *pushButton_c;
    QPushButton *pushButton_v;
    QPushButton *pushButton_b;
    QPushButton *pushButton_n;
    QPushButton *pushButton_m;
    QPushButton *pushButton_colon;
    QPushButton *pushButton_dot;
    QPushButton *pushButton_rshift;
    QSpacerItem *horizontalSpacer_9;
    QHBoxLayout *horizontalLayout_8;
    QSpacerItem *horizontalSpacer_8;
    QPushButton *pushButton_Lnumbers;
    QPushButton *pushButton_mic;
    QPushButton *pushButton_spacebar;
    QPushButton *pushButton_Rnumbers;
    QPushButton *pushButton_lang;
    QSpacerItem *horizontalSpacer_10;

    void setupUi(QWidget *keyboard)
    {
        if (keyboard->objectName().isEmpty())
            keyboard->setObjectName(QString::fromUtf8("keyboard"));
        keyboard->resize(1024, 320);
        keyboard->setMinimumSize(QSize(1024, 320));
        keyboard->setMaximumSize(QSize(1024, 320));
        gridLayout = new QGridLayout(keyboard);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame_body_2 = new QFrame(keyboard);
        frame_body_2->setObjectName(QString::fromUtf8("frame_body_2"));
        frame_body_2->setMinimumSize(QSize(1024, 320));
        frame_body_2->setMaximumSize(QSize(1024, 320));
        frame_body_2->setStyleSheet(QString::fromUtf8("background-color: rgb(67, 67, 67);"));
        frame_body_2->setFrameShape(QFrame::StyledPanel);
        frame_body_2->setFrameShadow(QFrame::Raised);
        verticalLayout_3 = new QVBoxLayout(frame_body_2);
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(9);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setSizeConstraint(QLayout::SetFixedSize);
        horizontalSpacer_6 = new QSpacerItem(85, 20, QSizePolicy::Preferred, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_6);

        pushButton_q = new QPushButton(frame_body_2);
        pushButton_q->setObjectName(QString::fromUtf8("pushButton_q"));
        pushButton_q->setMinimumSize(QSize(70, 70));
        pushButton_q->setMaximumSize(QSize(70, 70));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(16);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(50);
        pushButton_q->setFont(font);
        pushButton_q->setAutoFillBackground(false);
        pushButton_q->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"border-radius:8px;\n"
"font: 16pt \"Sans Serif\";"));
        pushButton_q->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_q);

        pushButton_w = new QPushButton(frame_body_2);
        pushButton_w->setObjectName(QString::fromUtf8("pushButton_w"));
        pushButton_w->setMinimumSize(QSize(70, 70));
        pushButton_w->setMaximumSize(QSize(70, 70));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Sans Serif"));
        font1.setPointSize(16);
        pushButton_w->setFont(font1);
        pushButton_w->setAutoFillBackground(false);
        pushButton_w->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"border-radius:8px;"));
        pushButton_w->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_w);

        pushButton_e = new QPushButton(frame_body_2);
        pushButton_e->setObjectName(QString::fromUtf8("pushButton_e"));
        pushButton_e->setMinimumSize(QSize(70, 70));
        pushButton_e->setMaximumSize(QSize(70, 70));
        pushButton_e->setFont(font1);
        pushButton_e->setAutoFillBackground(false);
        pushButton_e->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"border-radius:8px;"));
        pushButton_e->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_e);

        pushButton_r = new QPushButton(frame_body_2);
        pushButton_r->setObjectName(QString::fromUtf8("pushButton_r"));
        pushButton_r->setMinimumSize(QSize(70, 70));
        pushButton_r->setMaximumSize(QSize(70, 70));
        pushButton_r->setFont(font1);
        pushButton_r->setAutoFillBackground(false);
        pushButton_r->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"border-radius:8px;"));
        pushButton_r->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_r);

        pushButton_t = new QPushButton(frame_body_2);
        pushButton_t->setObjectName(QString::fromUtf8("pushButton_t"));
        pushButton_t->setMinimumSize(QSize(70, 70));
        pushButton_t->setMaximumSize(QSize(70, 70));
        pushButton_t->setFont(font1);
        pushButton_t->setAutoFillBackground(false);
        pushButton_t->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"border-radius:8px;"));
        pushButton_t->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_t);

        pushButton_y = new QPushButton(frame_body_2);
        pushButton_y->setObjectName(QString::fromUtf8("pushButton_y"));
        pushButton_y->setMinimumSize(QSize(70, 70));
        pushButton_y->setMaximumSize(QSize(70, 70));
        pushButton_y->setFont(font1);
        pushButton_y->setAutoFillBackground(false);
        pushButton_y->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"border-radius:8px;"));
        pushButton_y->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_y);

        pushButton_u = new QPushButton(frame_body_2);
        pushButton_u->setObjectName(QString::fromUtf8("pushButton_u"));
        pushButton_u->setMinimumSize(QSize(70, 70));
        pushButton_u->setMaximumSize(QSize(70, 70));
        pushButton_u->setFont(font1);
        pushButton_u->setAutoFillBackground(false);
        pushButton_u->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"border-radius:8px;"));
        pushButton_u->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_u);

        pushButton_i = new QPushButton(frame_body_2);
        pushButton_i->setObjectName(QString::fromUtf8("pushButton_i"));
        pushButton_i->setMinimumSize(QSize(70, 70));
        pushButton_i->setMaximumSize(QSize(70, 70));
        pushButton_i->setFont(font1);
        pushButton_i->setAutoFillBackground(false);
        pushButton_i->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"border-radius:8px;"));
        pushButton_i->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_i);

        pushButton_o = new QPushButton(frame_body_2);
        pushButton_o->setObjectName(QString::fromUtf8("pushButton_o"));
        pushButton_o->setMinimumSize(QSize(70, 70));
        pushButton_o->setMaximumSize(QSize(70, 70));
        pushButton_o->setFont(font1);
        pushButton_o->setAutoFillBackground(false);
        pushButton_o->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"border-radius:8px;"));
        pushButton_o->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_o);

        pushButton_p = new QPushButton(frame_body_2);
        pushButton_p->setObjectName(QString::fromUtf8("pushButton_p"));
        pushButton_p->setMinimumSize(QSize(70, 70));
        pushButton_p->setMaximumSize(QSize(70, 70));
        pushButton_p->setFont(font1);
        pushButton_p->setAutoFillBackground(false);
        pushButton_p->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"border-radius:8px;"));
        pushButton_p->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_p);

        pushButton_backspace = new QPushButton(frame_body_2);
        pushButton_backspace->setObjectName(QString::fromUtf8("pushButton_backspace"));
        pushButton_backspace->setMinimumSize(QSize(70, 70));
        pushButton_backspace->setMaximumSize(QSize(70, 70));
        pushButton_backspace->setFont(font1);
        pushButton_backspace->setAutoFillBackground(false);
        pushButton_backspace->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(78, 86, 90);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));

        horizontalLayout_2->addWidget(pushButton_backspace);

        horizontalSpacer_4 = new QSpacerItem(65, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_4);


        verticalLayout_3->addLayout(horizontalLayout_2);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(9);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalLayout_6->setSizeConstraint(QLayout::SetFixedSize);
        horizontalSpacer_3 = new QSpacerItem(95, 20, QSizePolicy::Preferred, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_3);

        pushButton_a = new QPushButton(frame_body_2);
        pushButton_a->setObjectName(QString::fromUtf8("pushButton_a"));
        pushButton_a->setMinimumSize(QSize(70, 70));
        pushButton_a->setMaximumSize(QSize(70, 70));
        pushButton_a->setAutoFillBackground(false);
        pushButton_a->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;"));
        pushButton_a->setFlat(true);

        horizontalLayout_6->addWidget(pushButton_a);

        pushButton_s = new QPushButton(frame_body_2);
        pushButton_s->setObjectName(QString::fromUtf8("pushButton_s"));
        pushButton_s->setMinimumSize(QSize(70, 70));
        pushButton_s->setMaximumSize(QSize(70, 70));
        pushButton_s->setAutoFillBackground(false);
        pushButton_s->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));
        pushButton_s->setFlat(true);

        horizontalLayout_6->addWidget(pushButton_s);

        pushButton_d = new QPushButton(frame_body_2);
        pushButton_d->setObjectName(QString::fromUtf8("pushButton_d"));
        pushButton_d->setMinimumSize(QSize(70, 70));
        pushButton_d->setMaximumSize(QSize(70, 70));
        pushButton_d->setAutoFillBackground(false);
        pushButton_d->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));
        pushButton_d->setFlat(true);

        horizontalLayout_6->addWidget(pushButton_d);

        pushButton_f = new QPushButton(frame_body_2);
        pushButton_f->setObjectName(QString::fromUtf8("pushButton_f"));
        pushButton_f->setMinimumSize(QSize(70, 70));
        pushButton_f->setMaximumSize(QSize(70, 70));
        pushButton_f->setAutoFillBackground(false);
        pushButton_f->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));
        pushButton_f->setFlat(true);

        horizontalLayout_6->addWidget(pushButton_f);

        pushButton_g = new QPushButton(frame_body_2);
        pushButton_g->setObjectName(QString::fromUtf8("pushButton_g"));
        pushButton_g->setMinimumSize(QSize(70, 70));
        pushButton_g->setMaximumSize(QSize(70, 70));
        pushButton_g->setAutoFillBackground(false);
        pushButton_g->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));
        pushButton_g->setFlat(true);

        horizontalLayout_6->addWidget(pushButton_g);

        pushButton_h = new QPushButton(frame_body_2);
        pushButton_h->setObjectName(QString::fromUtf8("pushButton_h"));
        pushButton_h->setMinimumSize(QSize(70, 70));
        pushButton_h->setMaximumSize(QSize(70, 70));
        pushButton_h->setAutoFillBackground(false);
        pushButton_h->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));
        pushButton_h->setFlat(true);

        horizontalLayout_6->addWidget(pushButton_h);

        pushButton_j = new QPushButton(frame_body_2);
        pushButton_j->setObjectName(QString::fromUtf8("pushButton_j"));
        pushButton_j->setMinimumSize(QSize(70, 70));
        pushButton_j->setMaximumSize(QSize(70, 70));
        pushButton_j->setAutoFillBackground(false);
        pushButton_j->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));
        pushButton_j->setFlat(true);

        horizontalLayout_6->addWidget(pushButton_j);

        pushButton_k = new QPushButton(frame_body_2);
        pushButton_k->setObjectName(QString::fromUtf8("pushButton_k"));
        pushButton_k->setMinimumSize(QSize(70, 70));
        pushButton_k->setMaximumSize(QSize(70, 70));
        pushButton_k->setAutoFillBackground(false);
        pushButton_k->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));
        pushButton_k->setFlat(true);

        horizontalLayout_6->addWidget(pushButton_k);

        pushButton_l = new QPushButton(frame_body_2);
        pushButton_l->setObjectName(QString::fromUtf8("pushButton_l"));
        pushButton_l->setMinimumSize(QSize(70, 70));
        pushButton_l->setMaximumSize(QSize(70, 70));
        pushButton_l->setAutoFillBackground(false);
        pushButton_l->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));
        pushButton_l->setFlat(true);

        horizontalLayout_6->addWidget(pushButton_l);

        pushButton_return = new QPushButton(frame_body_2);
        pushButton_return->setObjectName(QString::fromUtf8("pushButton_return"));
        pushButton_return->setMinimumSize(QSize(120, 70));
        pushButton_return->setMaximumSize(QSize(65, 70));
        pushButton_return->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(78, 86, 90);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));

        horizontalLayout_6->addWidget(pushButton_return);

        horizontalSpacer_5 = new QSpacerItem(65, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_5);


        verticalLayout_3->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(8);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalLayout_7->setSizeConstraint(QLayout::SetFixedSize);
        horizontalSpacer_7 = new QSpacerItem(60, 20, QSizePolicy::Preferred, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_7);

        pushButton_lshift = new QPushButton(frame_body_2);
        pushButton_lshift->setObjectName(QString::fromUtf8("pushButton_lshift"));
        pushButton_lshift->setMinimumSize(QSize(70, 70));
        pushButton_lshift->setMaximumSize(QSize(70, 70));
        pushButton_lshift->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(35, 121, 102);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));

        horizontalLayout_7->addWidget(pushButton_lshift);

        pushButton_z = new QPushButton(frame_body_2);
        pushButton_z->setObjectName(QString::fromUtf8("pushButton_z"));
        pushButton_z->setMinimumSize(QSize(70, 70));
        pushButton_z->setMaximumSize(QSize(70, 70));
        pushButton_z->setAutoFillBackground(false);
        pushButton_z->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));
        pushButton_z->setFlat(true);

        horizontalLayout_7->addWidget(pushButton_z);

        pushButton_x = new QPushButton(frame_body_2);
        pushButton_x->setObjectName(QString::fromUtf8("pushButton_x"));
        pushButton_x->setMinimumSize(QSize(70, 70));
        pushButton_x->setMaximumSize(QSize(70, 70));
        pushButton_x->setAutoFillBackground(false);
        pushButton_x->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));
        pushButton_x->setFlat(true);

        horizontalLayout_7->addWidget(pushButton_x);

        pushButton_c = new QPushButton(frame_body_2);
        pushButton_c->setObjectName(QString::fromUtf8("pushButton_c"));
        pushButton_c->setMinimumSize(QSize(70, 70));
        pushButton_c->setMaximumSize(QSize(70, 70));
        pushButton_c->setAutoFillBackground(false);
        pushButton_c->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));
        pushButton_c->setFlat(true);

        horizontalLayout_7->addWidget(pushButton_c);

        pushButton_v = new QPushButton(frame_body_2);
        pushButton_v->setObjectName(QString::fromUtf8("pushButton_v"));
        pushButton_v->setMinimumSize(QSize(70, 70));
        pushButton_v->setMaximumSize(QSize(70, 70));
        pushButton_v->setAutoFillBackground(false);
        pushButton_v->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));
        pushButton_v->setFlat(true);

        horizontalLayout_7->addWidget(pushButton_v);

        pushButton_b = new QPushButton(frame_body_2);
        pushButton_b->setObjectName(QString::fromUtf8("pushButton_b"));
        pushButton_b->setMinimumSize(QSize(70, 70));
        pushButton_b->setMaximumSize(QSize(70, 70));
        pushButton_b->setAutoFillBackground(false);
        pushButton_b->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));
        pushButton_b->setFlat(true);

        horizontalLayout_7->addWidget(pushButton_b);

        pushButton_n = new QPushButton(frame_body_2);
        pushButton_n->setObjectName(QString::fromUtf8("pushButton_n"));
        pushButton_n->setMinimumSize(QSize(70, 70));
        pushButton_n->setMaximumSize(QSize(70, 70));
        pushButton_n->setAutoFillBackground(false);
        pushButton_n->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));
        pushButton_n->setFlat(true);

        horizontalLayout_7->addWidget(pushButton_n);

        pushButton_m = new QPushButton(frame_body_2);
        pushButton_m->setObjectName(QString::fromUtf8("pushButton_m"));
        pushButton_m->setMinimumSize(QSize(70, 70));
        pushButton_m->setMaximumSize(QSize(70, 70));
        pushButton_m->setAutoFillBackground(false);
        pushButton_m->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));
        pushButton_m->setFlat(true);

        horizontalLayout_7->addWidget(pushButton_m);

        pushButton_colon = new QPushButton(frame_body_2);
        pushButton_colon->setObjectName(QString::fromUtf8("pushButton_colon"));
        pushButton_colon->setMinimumSize(QSize(70, 70));
        pushButton_colon->setMaximumSize(QSize(70, 70));
        pushButton_colon->setAutoFillBackground(false);
        pushButton_colon->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));
        pushButton_colon->setFlat(true);

        horizontalLayout_7->addWidget(pushButton_colon);

        pushButton_dot = new QPushButton(frame_body_2);
        pushButton_dot->setObjectName(QString::fromUtf8("pushButton_dot"));
        pushButton_dot->setMinimumSize(QSize(70, 70));
        pushButton_dot->setMaximumSize(QSize(70, 70));
        pushButton_dot->setAutoFillBackground(false);
        pushButton_dot->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));
        pushButton_dot->setFlat(true);

        horizontalLayout_7->addWidget(pushButton_dot);

        pushButton_rshift = new QPushButton(frame_body_2);
        pushButton_rshift->setObjectName(QString::fromUtf8("pushButton_rshift"));
        pushButton_rshift->setMinimumSize(QSize(80, 70));
        pushButton_rshift->setMaximumSize(QSize(80, 70));
        pushButton_rshift->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(35, 121, 102);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));

        horizontalLayout_7->addWidget(pushButton_rshift);

        horizontalSpacer_9 = new QSpacerItem(67, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_9);


        verticalLayout_3->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(8);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        horizontalLayout_8->setSizeConstraint(QLayout::SetFixedSize);
        horizontalSpacer_8 = new QSpacerItem(60, 20, QSizePolicy::Preferred, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_8);

        pushButton_Lnumbers = new QPushButton(frame_body_2);
        pushButton_Lnumbers->setObjectName(QString::fromUtf8("pushButton_Lnumbers"));
        pushButton_Lnumbers->setMinimumSize(QSize(148, 70));
        pushButton_Lnumbers->setMaximumSize(QSize(148, 70));
        pushButton_Lnumbers->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(78, 86, 90);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));

        horizontalLayout_8->addWidget(pushButton_Lnumbers);

        pushButton_mic = new QPushButton(frame_body_2);
        pushButton_mic->setObjectName(QString::fromUtf8("pushButton_mic"));
        pushButton_mic->setMinimumSize(QSize(70, 70));
        pushButton_mic->setMaximumSize(QSize(70, 70));
        pushButton_mic->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(78, 86, 90);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));

        horizontalLayout_8->addWidget(pushButton_mic);

        pushButton_spacebar = new QPushButton(frame_body_2);
        pushButton_spacebar->setObjectName(QString::fromUtf8("pushButton_spacebar"));
        pushButton_spacebar->setMinimumSize(QSize(460, 70));
        pushButton_spacebar->setMaximumSize(QSize(430, 70));
        pushButton_spacebar->setAutoFillBackground(false);
        pushButton_spacebar->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));
        pushButton_spacebar->setFlat(true);

        horizontalLayout_8->addWidget(pushButton_spacebar);

        pushButton_Rnumbers = new QPushButton(frame_body_2);
        pushButton_Rnumbers->setObjectName(QString::fromUtf8("pushButton_Rnumbers"));
        pushButton_Rnumbers->setMinimumSize(QSize(90, 70));
        pushButton_Rnumbers->setMaximumSize(QSize(82, 70));
        pushButton_Rnumbers->setAutoFillBackground(false);
        pushButton_Rnumbers->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(78, 86, 90);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));

        horizontalLayout_8->addWidget(pushButton_Rnumbers);

        pushButton_lang = new QPushButton(frame_body_2);
        pushButton_lang->setObjectName(QString::fromUtf8("pushButton_lang"));
        pushButton_lang->setMinimumSize(QSize(60, 70));
        pushButton_lang->setMaximumSize(QSize(60, 70));
        pushButton_lang->setAutoFillBackground(false);
        pushButton_lang->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color:rgb(78, 86, 90);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));

        horizontalLayout_8->addWidget(pushButton_lang);

        horizontalSpacer_10 = new QSpacerItem(67, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_10);


        verticalLayout_3->addLayout(horizontalLayout_8);


        gridLayout->addWidget(frame_body_2, 0, 0, 1, 1);


        retranslateUi(keyboard);

        QMetaObject::connectSlotsByName(keyboard);
    } // setupUi

    void retranslateUi(QWidget *keyboard)
    {
        keyboard->setWindowTitle(QApplication::translate("keyboard", "Form", 0, QApplication::UnicodeUTF8));
        pushButton_q->setText(QApplication::translate("keyboard", "Q", 0, QApplication::UnicodeUTF8));
        pushButton_w->setText(QApplication::translate("keyboard", "W", 0, QApplication::UnicodeUTF8));
        pushButton_e->setText(QApplication::translate("keyboard", "E", 0, QApplication::UnicodeUTF8));
        pushButton_r->setText(QApplication::translate("keyboard", "R", 0, QApplication::UnicodeUTF8));
        pushButton_t->setText(QApplication::translate("keyboard", "T", 0, QApplication::UnicodeUTF8));
        pushButton_y->setText(QApplication::translate("keyboard", "Y", 0, QApplication::UnicodeUTF8));
        pushButton_u->setText(QApplication::translate("keyboard", "U", 0, QApplication::UnicodeUTF8));
        pushButton_i->setText(QApplication::translate("keyboard", "I", 0, QApplication::UnicodeUTF8));
        pushButton_o->setText(QApplication::translate("keyboard", "O", 0, QApplication::UnicodeUTF8));
        pushButton_p->setText(QApplication::translate("keyboard", "P", 0, QApplication::UnicodeUTF8));
        pushButton_backspace->setText(QApplication::translate("keyboard", "<", 0, QApplication::UnicodeUTF8));
        pushButton_a->setText(QApplication::translate("keyboard", "A", 0, QApplication::UnicodeUTF8));
        pushButton_s->setText(QApplication::translate("keyboard", "S", 0, QApplication::UnicodeUTF8));
        pushButton_d->setText(QApplication::translate("keyboard", "D", 0, QApplication::UnicodeUTF8));
        pushButton_f->setText(QApplication::translate("keyboard", "F", 0, QApplication::UnicodeUTF8));
        pushButton_g->setText(QApplication::translate("keyboard", "G", 0, QApplication::UnicodeUTF8));
        pushButton_h->setText(QApplication::translate("keyboard", "H", 0, QApplication::UnicodeUTF8));
        pushButton_j->setText(QApplication::translate("keyboard", "J", 0, QApplication::UnicodeUTF8));
        pushButton_k->setText(QApplication::translate("keyboard", "K", 0, QApplication::UnicodeUTF8));
        pushButton_l->setText(QApplication::translate("keyboard", "L", 0, QApplication::UnicodeUTF8));
        pushButton_return->setText(QApplication::translate("keyboard", "Return", 0, QApplication::UnicodeUTF8));
        pushButton_lshift->setText(QApplication::translate("keyboard", "SHIFT", 0, QApplication::UnicodeUTF8));
        pushButton_z->setText(QApplication::translate("keyboard", "Z", 0, QApplication::UnicodeUTF8));
        pushButton_x->setText(QApplication::translate("keyboard", "X", 0, QApplication::UnicodeUTF8));
        pushButton_c->setText(QApplication::translate("keyboard", "C", 0, QApplication::UnicodeUTF8));
        pushButton_v->setText(QApplication::translate("keyboard", "V", 0, QApplication::UnicodeUTF8));
        pushButton_b->setText(QApplication::translate("keyboard", "B", 0, QApplication::UnicodeUTF8));
        pushButton_n->setText(QApplication::translate("keyboard", "N", 0, QApplication::UnicodeUTF8));
        pushButton_m->setText(QApplication::translate("keyboard", "M", 0, QApplication::UnicodeUTF8));
        pushButton_colon->setText(QApplication::translate("keyboard", "!\n"
",", 0, QApplication::UnicodeUTF8));
        pushButton_dot->setText(QApplication::translate("keyboard", "?\n"
".", 0, QApplication::UnicodeUTF8));
        pushButton_rshift->setText(QApplication::translate("keyboard", "SHIFT", 0, QApplication::UnicodeUTF8));
        pushButton_Lnumbers->setText(QApplication::translate("keyboard", ".?123", 0, QApplication::UnicodeUTF8));
        pushButton_mic->setText(QApplication::translate("keyboard", "MIC", 0, QApplication::UnicodeUTF8));
        pushButton_spacebar->setText(QApplication::translate("keyboard", "SPACE", 0, QApplication::UnicodeUTF8));
        pushButton_Rnumbers->setText(QApplication::translate("keyboard", ".?123", 0, QApplication::UnicodeUTF8));
        pushButton_lang->setText(QApplication::translate("keyboard", "KEYS", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class keyboard: public Ui_keyboard {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KEYBOARD_H
