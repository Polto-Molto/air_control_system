#ifndef SCREEN_36_H
#define SCREEN_36_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_36;
}

class screen_36 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_36(dataManager*,QWidget *parent = 0);
    ~screen_36();
signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_cancel_clicked();
      void on_pushButton_done_clicked();
      void on_pushButton_comfort_up_clicked();
      void on_pushButton_comfort_down_clicked();
      void on_pushButton_night_up_clicked();
      void on_pushButton_night_down_clicked();

private:
    Ui::screen_36 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_36_H
