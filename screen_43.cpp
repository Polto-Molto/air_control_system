#include "screen_43.h"
#include "ui_screen_43.h"

screen_43::screen_43(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_43),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_43::~screen_43()
{
    delete ui;
}

void screen_43::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_43::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(41));
}

void screen_43::on_pushButton_done_clicked()
{
    Q_EMIT(touched(41));
}

void screen_43::on_pushButton_back_clicked()
{

}

void screen_43::on_pushButton_next_clicked()
{

}

void screen_43::on_pushButton_edit_clicked()
{
    Q_EMIT(touched(44));
}
