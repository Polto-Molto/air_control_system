#include "screen_40.h"
#include "ui_screen_40.h"

screen_40::screen_40(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_40),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_40::~screen_40()
{
    delete ui;
}

void screen_40::on_pushButton_done_clicked()
{
    Q_EMIT(touched(2));
}
