#ifndef SCREEN_8_H
#define SCREEN_8_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_8;
}

class screen_8 : public QWidget
{
    Q_OBJECT

public:
    explicit screen_8(dataManager*,QWidget *parent = 0);
    ~screen_8();
signals:
      void touched(int);

public slots:
    void on_pushButton_home_clicked();
    void on_pushButton_done_clicked();
    void on_pushButton_cancel_clicked();

    void on_pushButton_temp_up_clicked();
    void on_pushButton_temp_down_clicked();
    void on_pushButton_hours_up_clicked();
    void on_pushButton_hours_down_clicked();
    void on_pushButton_mins_up_clicked();
    void on_pushButton_mins_down_clicked();


private:
    Ui::screen_8 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_8_H
