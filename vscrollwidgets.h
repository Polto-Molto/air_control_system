#ifndef VSCROLLWIDGETS_H
#define VSCROLLWIDGETS_H

#include <QScrollArea>
#include <QGridLayout>

class VScrollWidgets : public QScrollArea
{
    Q_OBJECT
public:
    explicit VScrollWidgets(int,int,int,int,QObject *parent = 0);
    int vspacing;
    int hspacing;
    int cols;
    QWidget* container;
    QGridLayout* listLayout;
    QList<QWidget*> WidgetList;
    QString styleSheetFirst;
    QString styleSheetSecond;

signals:
    
public slots:
    void addWidget(QWidget*);
    void removeWidget(QWidget*);
    void clear();
};

#endif // VSCROLLWIDGETS_H
