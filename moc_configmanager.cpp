/****************************************************************************
** Meta object code from reading C++ file 'configmanager.h'
**
** Created: Tue Apr 25 16:10:32 2017
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "configmanager.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'configmanager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_configManager[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      20,   14,   15,   14, 0x0a,
      37,   14,   15,   14, 0x0a,
      67,   14,   15,   14, 0x0a,
      88,   14,   15,   14, 0x0a,
     113,   14,   15,   14, 0x0a,
     131,   14,   15,   14, 0x0a,
     150,   14,   15,   14, 0x0a,
     176,   14,   15,   14, 0x0a,
     192,   14,   15,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_configManager[] = {
    "configManager\0\0bool\0loadMainConfig()\0"
    "updateMainConfig(MAIN_CONFIG)\0"
    "loadScheduleConfig()\0updateSChedule(SCHEDULE)\0"
    "loadClimaConfig()\0updateClimaTable()\0"
    "updateClima(CLIMA_MODBUS)\0loadAirConfig()\0"
    "loadSkudoConfig()\0"
};

void configManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        configManager *_t = static_cast<configManager *>(_o);
        switch (_id) {
        case 0: { bool _r = _t->loadMainConfig();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 1: { bool _r = _t->updateMainConfig((*reinterpret_cast< MAIN_CONFIG(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 2: { bool _r = _t->loadScheduleConfig();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 3: { bool _r = _t->updateSChedule((*reinterpret_cast< SCHEDULE(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 4: { bool _r = _t->loadClimaConfig();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 5: { bool _r = _t->updateClimaTable();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 6: { bool _r = _t->updateClima((*reinterpret_cast< CLIMA_MODBUS(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 7: { bool _r = _t->loadAirConfig();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 8: { bool _r = _t->loadSkudoConfig();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData configManager::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject configManager::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_configManager,
      qt_meta_data_configManager, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &configManager::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *configManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *configManager::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_configManager))
        return static_cast<void*>(const_cast< configManager*>(this));
    return QObject::qt_metacast(_clname);
}

int configManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
