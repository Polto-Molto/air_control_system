/********************************************************************************
** Form generated from reading UI file 'screen_18.ui'
**
** Created: Thu Apr 13 00:46:20 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREEN_18_H
#define UI_SCREEN_18_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_screen_18
{
public:
    QGridLayout *gridLayout;
    QFrame *frame_header;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QFrame *frame_body;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_5;
    QFrame *frame_3;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_4;
    QLabel *label_5;
    QSpacerItem *horizontalSpacer_6;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_3;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_4;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_7;
    QPushButton *pushButton_quit;
    QPushButton *pushButton_next_day;
    QPushButton *pushButton_next_save;
    QSpacerItem *horizontalSpacer_8;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *screen_18)
    {
        if (screen_18->objectName().isEmpty())
            screen_18->setObjectName(QString::fromUtf8("screen_18"));
        screen_18->resize(1024, 600);
        screen_18->setMinimumSize(QSize(1024, 600));
        screen_18->setMaximumSize(QSize(1024, 600));
        gridLayout = new QGridLayout(screen_18);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame_header = new QFrame(screen_18);
        frame_header->setObjectName(QString::fromUtf8("frame_header"));
        frame_header->setMinimumSize(QSize(1024, 50));
        frame_header->setMaximumSize(QSize(1024, 50));
        frame_header->setFrameShape(QFrame::StyledPanel);
        frame_header->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_header);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(frame_header);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(14);
        label->setFont(font);

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        gridLayout->addWidget(frame_header, 0, 0, 1, 1);

        frame_body = new QFrame(screen_18);
        frame_body->setObjectName(QString::fromUtf8("frame_body"));
        frame_body->setMinimumSize(QSize(1024, 550));
        frame_body->setMaximumSize(QSize(1024, 550));
        frame_body->setFrameShape(QFrame::StyledPanel);
        frame_body->setFrameShadow(QFrame::Raised);
        verticalLayout = new QVBoxLayout(frame_body);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalSpacer_2 = new QSpacerItem(20, 80, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout->addItem(verticalSpacer_2);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_5);

        frame_3 = new QFrame(frame_body);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setMinimumSize(QSize(600, 70));
        frame_3->setMaximumSize(QSize(600, 70));
        frame_3->setStyleSheet(QString::fromUtf8("#frame_3{\n"
"border:1px solid rgb(0, 230, 169);\n"
"border-radius:10px;\n"
"}"));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        horizontalLayout_7 = new QHBoxLayout(frame_3);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_4 = new QLabel(frame_3);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMinimumSize(QSize(40, 40));
        label_4->setMaximumSize(QSize(40, 40));
        label_4->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/success icon.png")));

        horizontalLayout_7->addWidget(label_4);

        label_5 = new QLabel(frame_3);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setStyleSheet(QString::fromUtf8("color:rgb(0, 197, 145)"));

        horizontalLayout_7->addWidget(label_5);


        horizontalLayout_4->addWidget(frame_3);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_6);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        label_2 = new QLabel(frame_body);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(200, 100));
        label_2->setMaximumSize(QSize(200, 100));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Sans Serif"));
        font1.setPointSize(24);
        label_2->setFont(font1);
        label_2->setStyleSheet(QString::fromUtf8("color:black;"));
        label_2->setFrameShape(QFrame::NoFrame);

        horizontalLayout_3->addWidget(label_2);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_4);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(20);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_7);

        pushButton_quit = new QPushButton(frame_body);
        pushButton_quit->setObjectName(QString::fromUtf8("pushButton_quit"));
        pushButton_quit->setMinimumSize(QSize(280, 130));
        pushButton_quit->setMaximumSize(QSize(280, 130));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Sans Serif"));
        font2.setPointSize(20);
        pushButton_quit->setFont(font2);
        pushButton_quit->setStyleSheet(QString::fromUtf8("color:rgb(42, 86, 206);\n"
"border:1px solid rgb(42, 86, 206);"));
        pushButton_quit->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_quit);

        pushButton_next_day = new QPushButton(frame_body);
        pushButton_next_day->setObjectName(QString::fromUtf8("pushButton_next_day"));
        pushButton_next_day->setMinimumSize(QSize(280, 130));
        pushButton_next_day->setMaximumSize(QSize(280, 130));
        pushButton_next_day->setFont(font2);
        pushButton_next_day->setStyleSheet(QString::fromUtf8("color:rgb(42, 86, 206);\n"
"border:1px solid rgb(42, 86, 206);"));
        pushButton_next_day->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_next_day);

        pushButton_next_save = new QPushButton(frame_body);
        pushButton_next_save->setObjectName(QString::fromUtf8("pushButton_next_save"));
        pushButton_next_save->setMinimumSize(QSize(280, 130));
        pushButton_next_save->setMaximumSize(QSize(280, 130));
        pushButton_next_save->setFont(font2);
        pushButton_next_save->setStyleSheet(QString::fromUtf8("color:rgb(42, 86, 206);\n"
"border:1px solid rgb(42, 86, 206);"));
        pushButton_next_save->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_next_save);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_8);


        verticalLayout->addLayout(horizontalLayout_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        gridLayout->addWidget(frame_body, 1, 0, 1, 1);


        retranslateUi(screen_18);

        QMetaObject::connectSlotsByName(screen_18);
    } // setupUi

    void retranslateUi(QWidget *screen_18)
    {
        screen_18->setWindowTitle(QApplication::translate("screen_18", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("screen_18", "Schedule Settings", 0, QApplication::UnicodeUTF8));
        label_4->setText(QString());
        label_5->setText(QApplication::translate("screen_18", "Daily schedule saved", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("screen_18", "Whats Next?", 0, QApplication::UnicodeUTF8));
        pushButton_quit->setText(QApplication::translate("screen_18", "Quit", 0, QApplication::UnicodeUTF8));
        pushButton_next_day->setText(QApplication::translate("screen_18", "Show the next\n"
"day", 0, QApplication::UnicodeUTF8));
        pushButton_next_save->setText(QApplication::translate("screen_18", "Copy this schedule\n"
"to the next day", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class screen_18: public Ui_screen_18 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREEN_18_H
