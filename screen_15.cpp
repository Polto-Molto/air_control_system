#include "screen_15.h"
#include "ui_screen_15.h"

screen_15::screen_15(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_15),
    _datamgr(dm)
{
    ui->setupUi(this);
    ui->label_hours_value->setNum(12);
    ui->label_mins_value->setNum(30);
}

screen_15::~screen_15()
{
    delete ui;
}

void screen_15::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_15::on_pushButton_done_clicked()
{
    Q_EMIT(touched(14));
}

void screen_15::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(13));
}

void screen_15::on_pushButton_hours_up_clicked()
{
    int value = ui->label_hours_value->text().toInt();
    ui->label_hours_value->setNum(value + 1);
}

void screen_15::on_pushButton_hours_down_clicked()
{
    int value = ui->label_hours_value->text().toInt();
    ui->label_hours_value->setNum(value - 1);
}

void screen_15::on_pushButton_mins_up_clicked()
{
    int value = ui->label_mins_value->text().toInt();
    ui->label_mins_value->setNum(value + 1);
}

void screen_15::on_pushButton_mins_down_clicked()
{
    int value = ui->label_mins_value->text().toInt();
    ui->label_mins_value->setNum(value - 1);
}


