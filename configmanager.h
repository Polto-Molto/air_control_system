#ifndef CONFIGMANAGER_H
#define CONFIGMANAGER_H

#include <QObject>
#include <QSettings>
#include <QVector>
#include "definitions.h"
#include "datamanager.h"

class configManager : public QObject
{
    Q_OBJECT

public:
    explicit configManager(dataManager *,QObject *parent = 0);
    bool setConfigFiles(QString main_cf,QString schedule_cf,QString clima_cf,QString air_cf,QString skudo_cf);

public slots:

    //  main config file functions
    bool loadMainConfig();
    bool updateMainConfig(MAIN_CONFIG);

    //  schedule config file functions
    bool loadScheduleConfig();
    bool updateSChedule(SCHEDULE);

    //  clima config file functions
    bool loadClimaConfig();
    bool updateClimaTable();
    bool updateClima(CLIMA_MODBUS);

    //  air config file functions
    bool loadAirConfig();

    //  skudo config file functions
    bool loadSkudoConfig();

    
signals:

private:
    dataManager* _datamgr;
    QSettings *mainCF;
    QSettings *scheduleCF;
    QSettings *climaCF;
    QSettings *airCF;
    QSettings *skudoCF;
};

#endif // CONFIGMANAGER_H
