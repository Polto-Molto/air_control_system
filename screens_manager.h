#ifndef SCREENS_MANAGER_H
#define SCREENS_MANAGER_H

#include <QObject>
#include <qt4modbus.h>
#include <modbus_manager.h>
#include <modbusclima.h>

#include "configmanager.h"

#include "keyboard.h"
#include "keypad.h"

#include "screen_1.h"
#include "screen_2.h"
#include "screen_3.h"
#include "screen_4.h"
#include "screen_5.h"
#include "screen_6.h"
#include "screen_7.h"
#include "screen_8.h"
#include "screen_9.h"
#include "screen_10.h"

#include "screen_11.h"
#include "screen_12.h"
#include "screen_13.h"
#include "screen_14.h"
#include "screen_15.h"
#include "screen_16.h"
#include "screen_17.h"
#include "screen_18.h"
#include "screen_19.h"
#include "screen_20.h"

#include "screen_21.h"
#include "screen_22.h"
#include "screen_23.h"
#include "screen_24.h"
#include "screen_25.h"
#include "screen_26.h"
#include "screen_27.h"
#include "screen_28.h"
#include "screen_29.h"
#include "screen_30.h"

#include "screen_31.h"
#include "screen_32.h"
#include "screen_33.h"
#include "screen_34.h"
#include "screen_35.h"
#include "screen_36.h"
#include "screen_37.h"
#include "screen_38.h"
#include "screen_39.h"
#include "screen_40.h"

#include "screen_41.h"
#include "screen_42.h"
#include "screen_43.h"
#include "screen_44.h"
#include "screen_45.h"
#include "screen_46.h"
#include "screen_47.h"
#include "screen_48.h"
#include "screen_49.h"
#include "screen_50.h"

#include "screen_51.h"
#include "screen_52.h"
#include "screen_53.h"
#include "screen_54.h"
#include "screen_55.h"
#include "screen_56.h"


class screens_manager : public QObject
{
    Q_OBJECT
public:
    explicit screens_manager(dataManager*,modbus_manager *,configManager* m, QObject *parent = 0);

signals:

public slots:
    void showkeyboard(QPushButton*);
    void keyboardPressedReturn();
    void refresh_settings();

    void showkeypad(QPushButton* ledit);
    void keypadPressedReturn();

    void show_screen(int);
    void init_modbus();
    void update_data();
    void close();

    void write_circuit_act(bool);
    void write_dehumidity_act(bool);
    void write_comfort_treshold(int);

    // screens Actions
    void s_45_done(int);
    void s_23_set_clima_setting(QString);

private:
    modbus_manager* _mbmgr;
    configManager* _cfmgr;
    dataManager* _datamgr;
    Qt4Modbus *_modbus;
    QTimer *timer;


    int current_screen;
    int silent_time;

    QWidget* kb_sender;
    keyboard* kb;

    QWidget* kp_sender;
    keypad* kp;

    screen_1* s_1;
    screen_2* s_2;
    screen_3* s_3;
    screen_4* s_4;
    screen_5* s_5;
    screen_6* s_6;
    screen_7* s_7;
    screen_8* s_8;
    screen_9* s_9;
    screen_10* s_10;

    screen_11* s_11;
    screen_12* s_12;
    screen_13* s_13;
    screen_14* s_14;
    screen_15* s_15;
    screen_16* s_16;
    screen_17* s_17;
    screen_18* s_18;
    screen_19* s_19;
    screen_20* s_20;

    screen_21* s_21;
    screen_22* s_22;
    screen_23* s_23;
    screen_24* s_24;
    screen_25* s_25;
    screen_26* s_26;
    screen_27* s_27;
    screen_28* s_28;
    screen_29* s_29;
    screen_30* s_30;

    screen_31* s_31;
    screen_32* s_32;
    screen_33* s_33;
    screen_34* s_34;
    screen_35* s_35;
    screen_36* s_36;
    screen_37* s_37;
    screen_38* s_38;
    screen_39* s_39;
    screen_40* s_40;

    screen_41* s_41;
    screen_42* s_42;
    screen_43* s_43;
    screen_44* s_44;
    screen_45* s_45;
    screen_46* s_46;
    screen_47* s_47;
    screen_48* s_48;
    screen_49* s_49;
    screen_50* s_50;

    screen_51* s_51;
    screen_52* s_52;
    screen_53* s_53;
    screen_54* s_54;
    screen_55* s_55;
    screen_56* s_56;
};

#endif // SCREENS_MANAGER_H
