#ifndef SCREEN_46_H
#define SCREEN_46_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_46;
}

class screen_46 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_46(dataManager*,QWidget *parent = 0);
    ~screen_46();
signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_back_clicked();
      void on_pushButton_done_clicked();

private:
    Ui::screen_46 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_46_H
