/****************************************************************************
** Meta object code from reading C++ file 'screens_manager.h'
**
** Created: Tue Apr 25 20:01:23 2017
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "screens_manager.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'screens_manager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_screens_manager[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      17,   16,   16,   16, 0x0a,
      44,   16,   16,   16, 0x0a,
      68,   16,   16,   16, 0x0a,
      93,   87,   16,   16, 0x0a,
     118,   16,   16,   16, 0x0a,
     140,   16,   16,   16, 0x0a,
     157,   16,   16,   16, 0x0a,
     171,   16,   16,   16, 0x0a,
     185,   16,   16,   16, 0x0a,
     193,   16,   16,   16, 0x0a,
     217,   16,   16,   16, 0x0a,
     244,   16,   16,   16, 0x0a,
     272,   16,   16,   16, 0x0a,
     287,   16,   16,   16, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_screens_manager[] = {
    "screens_manager\0\0showkeyboard(QPushButton*)\0"
    "keyboardPressedReturn()\0refresh_settings()\0"
    "ledit\0showkeypad(QPushButton*)\0"
    "keypadPressedReturn()\0show_screen(int)\0"
    "init_modbus()\0update_data()\0close()\0"
    "write_circuit_act(bool)\0"
    "write_dehumidity_act(bool)\0"
    "write_comfort_treshold(int)\0s_45_done(int)\0"
    "s_23_set_clima_setting(QString)\0"
};

void screens_manager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        screens_manager *_t = static_cast<screens_manager *>(_o);
        switch (_id) {
        case 0: _t->showkeyboard((*reinterpret_cast< QPushButton*(*)>(_a[1]))); break;
        case 1: _t->keyboardPressedReturn(); break;
        case 2: _t->refresh_settings(); break;
        case 3: _t->showkeypad((*reinterpret_cast< QPushButton*(*)>(_a[1]))); break;
        case 4: _t->keypadPressedReturn(); break;
        case 5: _t->show_screen((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->init_modbus(); break;
        case 7: _t->update_data(); break;
        case 8: _t->close(); break;
        case 9: _t->write_circuit_act((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->write_dehumidity_act((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 11: _t->write_comfort_treshold((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->s_45_done((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->s_23_set_clima_setting((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData screens_manager::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject screens_manager::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_screens_manager,
      qt_meta_data_screens_manager, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &screens_manager::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *screens_manager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *screens_manager::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_screens_manager))
        return static_cast<void*>(const_cast< screens_manager*>(this));
    return QObject::qt_metacast(_clname);
}

int screens_manager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
