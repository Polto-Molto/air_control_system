#ifndef SCREEN_54_H
#define SCREEN_54_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_54;
}

class screen_54 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_54(dataManager *,QWidget *parent = 0);
    ~screen_54();
signals:
      void touched(int);

private:
    Ui::screen_54 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_54_H
