#include "modbus_manager.h"
#include <QDebug>
#include <QThread>

modbus_manager::modbus_manager(dataManager *dm,QObject *parent) :
    QObject(parent),
    _datamgr(dm)
{
    modbus_network = 0;
    clima_low_id_range = 40;
    clima_high_id_range = 70;
    clima_devices = new QVector<modbusClima*>;
    ready_read = false;

    status_messages = new QVector<QString>;
    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(readModbusNetwrokData()));
    timer->start(1000);

    connect(_datamgr,SIGNAL(climaConfigDataUpdated(CLIMA_MODBUS)),
            this,SLOT(updateClima(CLIMA_MODBUS)));
}

void modbus_manager::setClimaTable()
{
    timer->stop();
    foreach(modbusClima *clima, *clima_devices)
        delete clima;
    clima_devices->clear();

    foreach(CLIMA_MODBUS clima_para , _datamgr->climaTable()->values())
    {
        modbusClima *clima = new modbusClima(clima_para, modbus_network);
        clima_devices->push_back(clima);
    }
    timer->start(1000);
}

void modbus_manager::updateClima(CLIMA_MODBUS clima_para)
{
    timer->stop();
    ready_read = false;
    foreach(modbusClima *clima,*clima_devices)
    {
        if (clima->_clima.config_name == clima_para.config_name)
        {

            clima->_clima = clima_para;
            qDebug() <<clima_para.name << " :: " << clima->_clima.name;
            break;
        }
    }
    ready_read = true;
    timer->start(1000);
}

void modbus_manager::readModbusNetwrokData()
{
    foreach(modbusClima *clima, *clima_devices)
    {
        if(read_status && clima->_clima.activate)
        {
            ready_read = false;
            _datamgr->updateClimaValues(clima->readData());
            ready_read = true;
        }
    }
}

void modbus_manager::enableReadClima()
{
    read_status = true;
}

void modbus_manager::disableReadClima()
{
    read_status = false;
}

void modbus_manager::setClimaDataTables()
{

}

QVector<QString> *modbus_manager::statusMessages()
{
    return status_messages;
}

void modbus_manager::clearStatusMessages()
{
    status_messages->clear();
}

void modbus_manager::setModbusRtuNetwork(Qt4Modbus *m)
{
    modbus_network = m;
}

Qt4Modbus *modbus_manager::modbusNetwork()
{
    return modbus_network;
}

bool modbus_manager::assignDevice(int id,int new_id,TYPE t)
{
//    timer->stop();
    read_status = false;
    while(!ready_read);

    clearStatusMessages();
    bool state = false;
    bool new_modbus_id_state = false;
    bool assign_modbus_id_state = false;
    QString modbus_type;
    QString num_str;
    switch(t)
    {
        case CLIMA:
            id = CLIMA_INIT_ID;
            modbus_type = "CLIMA";
            break;
        case AIR:
            id = AIR_INIT_ID;
            modbus_type = "AIR";
            break;
    }

    status_messages->push_back("request change id from : "
                              +num_str.setNum(id)+" to : "
                              +num_str.setNum(new_id) ) ;

    new_modbus_id_state = modbus_network->isModbusDeviceHere(16,id);
    if(new_modbus_id_state)
    {
        status_messages->push_back("New " + modbus_type + " Founded ...");

        assign_modbus_id_state = modbus_network->isModbusDeviceHere(16,new_id);
        if(assign_modbus_id_state)
        {
            status_messages->push_back("you cannot assign to this id : "
                                      + num_str.setNum(new_id)
                                      +" This New Id ready used");
        }

        else
        {
            status_messages->push_back( "this id : "
                                       + num_str.setNum(new_id)
                                       +" is free to assign ...");

            state = modbus_network->changeModbusId(16,id,new_id);
            if(state)
            {
                status_messages->push_back("device Id changed ...");
            }
            else
                status_messages->push_back("device Id Not changed ...");
        }
    }
    else
        status_messages->push_back("New " + modbus_type + " NOT Found ...");

    Q_EMIT(statusMessagesChanged());
    read_status = true;
    return state;
}

int modbus_manager::addNewDevice(TYPE t)
{
    int free_id = modbus_network->firstFreeModbusIdInRange(16,40,50);
    if(free_id > 0)
    {
        qDebug() << "Found Free device location Id at:" << free_id;
        if(modbus_network->changeModbusId(16,1,free_id))
        {
            qDebug() << "device Id changed ...";
        }
        else
            qDebug() << "device Id Not changed ...";
    }
    else
        qDebug() << "not Found any Free location...";

  //  foreach(int i ,modbus_network->modbusRangeStatus(16,40,50))
   //     qDebug() << "## "<< i <<" ##";
}

bool modbus_manager::removeDevice(TYPE t)
{

}

