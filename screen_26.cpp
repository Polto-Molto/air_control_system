#include "screen_26.h"
#include "ui_screen_26.h"

screen_26::screen_26(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_26),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_26::~screen_26()
{
    delete ui;
}

void screen_26::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_26::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(22));
}

void screen_26::on_pushButton_first_clicked()
{
    Q_EMIT(touched(27));
}

void screen_26::on_pushButton_second_clicked()
{
    Q_EMIT(touched(27));
}
