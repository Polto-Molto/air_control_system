#include "screen_37.h"
#include "ui_screen_37.h"

screen_37::screen_37(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_37),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_37::~screen_37()
{
    delete ui;
}


void screen_37::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_37::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(4));
}

void screen_37::on_pushButton_back_clicked()
{

}

void screen_37::on_pushButton_next_clicked()
{

}

void screen_37::on_pushButton_touch_clicked()
{
    Q_EMIT(touched(39));
}

void screen_37::on_toolButton_program_clicked()
{
    Q_EMIT(touched(7));
}

void screen_37::on_toolButton_temp_clicked()
{
    Q_EMIT(touched(36));    // summer
    //Q_EMIT(touched(38));  // winter
}
