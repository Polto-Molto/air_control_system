#include "screen_47.h"
#include "ui_screen_47.h"

screen_47::screen_47(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_47),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_47::~screen_47()
{
    delete ui;
}

void screen_47::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_47::on_pushButton_back_clicked()
{
    Q_EMIT(touched(22));
}

void screen_47::on_pushButton_done_clicked()
{
    Q_EMIT(touched(41));
}
