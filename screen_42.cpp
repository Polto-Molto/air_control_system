#include "screen_42.h"
#include "ui_screen_42.h"

screen_42::screen_42(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_42),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_42::~screen_42()
{
    delete ui;
}

void screen_42::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_42::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(22));
}

void screen_42::on_pushButton_back_clicked()
{
    Q_EMIT(touched(41));
}

void screen_42::on_pushButton_test_clicked()
{
    Q_EMIT(touched(43));
}
