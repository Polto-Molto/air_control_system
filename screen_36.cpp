#include "screen_36.h"
#include "ui_screen_36.h"

screen_36::screen_36(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_36),
    _datamgr(dm)
{
    ui->setupUi(this);
    ui->label_comfort_value->setNum(20);
    ui->label_night_value->setNum(20);

}

screen_36::~screen_36()
{
    delete ui;
}

void screen_36::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_36::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(37));
}

void screen_36::on_pushButton_done_clicked()
{
    Q_EMIT(touched(37));
}

void screen_36::on_pushButton_comfort_up_clicked()
{
    int value = ui->label_comfort_value->text().toInt();
    ui->label_comfort_value->setNum(value + 1);
}

void screen_36::on_pushButton_comfort_down_clicked()
{
    int value = ui->label_comfort_value->text().toInt();
    ui->label_comfort_value->setNum(value - 1);
}

void screen_36::on_pushButton_night_up_clicked()
{
    int value = ui->label_night_value->text().toInt();
    ui->label_night_value->setNum(value + 1);
}

void screen_36::on_pushButton_night_down_clicked()
{
    int value = ui->label_night_value->text().toInt();
    ui->label_night_value->setNum(value - 1);
}

