/********************************************************************************
** Form generated from reading UI file 'screen_40.ui'
**
** Created: Thu Apr 13 00:46:21 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREEN_40_H
#define UI_SCREEN_40_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_screen_40
{
public:
    QGridLayout *gridLayout;
    QFrame *frame_footer;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_15;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *pushButton_done;
    QSpacerItem *horizontalSpacer_16;
    QFrame *frame_body;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_2;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer_2;
    QLabel *label_3;
    QLabel *label_4;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *screen_40)
    {
        if (screen_40->objectName().isEmpty())
            screen_40->setObjectName(QString::fromUtf8("screen_40"));
        screen_40->resize(1024, 600);
        screen_40->setMinimumSize(QSize(1024, 600));
        screen_40->setMaximumSize(QSize(1024, 600));
        gridLayout = new QGridLayout(screen_40);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame_footer = new QFrame(screen_40);
        frame_footer->setObjectName(QString::fromUtf8("frame_footer"));
        frame_footer->setMinimumSize(QSize(1024, 80));
        frame_footer->setMaximumSize(QSize(1024, 80));
        frame_footer->setFrameShape(QFrame::StyledPanel);
        frame_footer->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_footer);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_15 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_15);

        horizontalSpacer_6 = new QSpacerItem(353, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);

        pushButton_done = new QPushButton(frame_footer);
        pushButton_done->setObjectName(QString::fromUtf8("pushButton_done"));
        pushButton_done->setMinimumSize(QSize(50, 50));
        pushButton_done->setMaximumSize(QSize(50, 50));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/images/Icons/check.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_done->setIcon(icon);
        pushButton_done->setIconSize(QSize(50, 50));
        pushButton_done->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_done);

        horizontalSpacer_16 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_16);


        gridLayout->addWidget(frame_footer, 1, 0, 1, 1);

        frame_body = new QFrame(screen_40);
        frame_body->setObjectName(QString::fromUtf8("frame_body"));
        frame_body->setMinimumSize(QSize(1024, 520));
        frame_body->setMaximumSize(QSize(1024, 520));
        frame_body->setFrameShape(QFrame::StyledPanel);
        frame_body->setFrameShadow(QFrame::Raised);
        verticalLayout_2 = new QVBoxLayout(frame_body);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_2 = new QLabel(frame_body);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(0, 110));
        label_2->setMaximumSize(QSize(16777215, 110));
        label_2->setPixmap(QPixmap(QString::fromUtf8(":/images/images/logo.png")));
        label_2->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        label_3 = new QLabel(frame_body);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMinimumSize(QSize(0, 75));
        label_3->setMaximumSize(QSize(16777215, 75));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(16);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(9);
        label_3->setFont(font);
        label_3->setStyleSheet(QString::fromUtf8("font: 75 16pt \"Sans Serif\";\n"
"color:black;"));
        label_3->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_3);

        label_4 = new QLabel(frame_body);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Sans Serif"));
        font1.setPointSize(12);
        font1.setBold(false);
        font1.setItalic(false);
        font1.setWeight(50);
        label_4->setFont(font1);
        label_4->setStyleSheet(QString::fromUtf8("font: 12pt \"Sans Serif\";\n"
"color:black;"));
        label_4->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_4);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        verticalLayout_2->addLayout(verticalLayout);


        gridLayout->addWidget(frame_body, 0, 0, 1, 1);


        retranslateUi(screen_40);

        QMetaObject::connectSlotsByName(screen_40);
    } // setupUi

    void retranslateUi(QWidget *screen_40)
    {
        screen_40->setWindowTitle(QApplication::translate("screen_40", "Form", 0, QApplication::UnicodeUTF8));
        pushButton_done->setText(QString());
        label_2->setText(QString());
        label_3->setText(QApplication::translate("screen_40", "Integra Touchscreen", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("screen_40", "\302\251 2017 - Ideal Clima\n"
"Version: 2.0\n"
"www.idealclima.eu\n"
"assistenza@idealclima.eu", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class screen_40: public Ui_screen_40 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREEN_40_H
