#include "screen_52.h"
#include "ui_screen_52.h"

screen_52::screen_52(dataManager *dm, modbus_manager *m,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_52),
    _datamgr(dm),
    _mbus(m)
{
    ui->setupUi(this);
    connect(_mbus,SIGNAL(statusMessagesChanged())
            ,this,SLOT(setNotifications()));
}

screen_52::~screen_52()
{
    delete ui;
}

void screen_52::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_52::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(51));
}

void screen_52::on_pushButton_done_clicked()
{
    Q_EMIT(touched(51));
}

void screen_52::setNotifications()
{
    foreach(QLabel *label,messages)
        delete label;
    messages.clear();
    foreach(QString str, *_mbus->statusMessages())
    {
        QLabel *label = new QLabel(this);
        ui->verticalLayout_2->insertWidget(messages.size(),label);
        label->setStyleSheet("border:1px solid black;border-radius:10px;font: 75 36pt \"Sans Serif\";");
        label->setText(str);
        label->show();
        messages.push_back(label);
    }
}
