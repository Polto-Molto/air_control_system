#include "screen_21.h"
#include "ui_screen_21.h"

screen_21::screen_21(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_21),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_21::~screen_21()
{
    delete ui;
}

void screen_21::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_21::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(2));
}

void screen_21::on_pushButton_datetime_clicked()
{
    Q_EMIT(touched(32));
}

void screen_21::on_pushButton_display_clicked()
{
    Q_EMIT(touched(29));
}

void screen_21::on_pushButton_language_clicked()
{

}

void screen_21::on_pushButton_advanced_clicked()
{
    Q_EMIT(touched(33));
}

void screen_21::on_pushButton_info_clicked()
{
    Q_EMIT(touched(40));
}




