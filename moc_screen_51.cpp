/****************************************************************************
** Meta object code from reading C++ file 'screen_51.h'
**
** Created: Tue Apr 25 16:10:33 2017
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "screen_51.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'screen_51.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_screen_51[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x05,
      24,   10,   10,   10, 0x05,

 // slots: signature, parameters, type, tag, flags
      49,   10,   10,   10, 0x0a,
      78,   10,   10,   10, 0x0a,
     109,   10,   10,   10, 0x0a,
     138,   10,   10,   10, 0x0a,
     171,   10,   10,   10, 0x0a,
     202,   10,   10,   10, 0x0a,
     232,   10,   10,   10, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_screen_51[] = {
    "screen_51\0\0touched(int)\0"
    "showkeypad(QPushButton*)\0"
    "on_pushButton_home_clicked()\0"
    "on_pushButton_cancel_clicked()\0"
    "on_pushButton_done_clicked()\0"
    "on_pushButton_num_down_clicked()\0"
    "on_pushButton_num_up_clicked()\0"
    "on_pushButton_value_clicked()\0"
    "setCurrentClima(CLIMA_MODBUS)\0"
};

void screen_51::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        screen_51 *_t = static_cast<screen_51 *>(_o);
        switch (_id) {
        case 0: _t->touched((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->showkeypad((*reinterpret_cast< QPushButton*(*)>(_a[1]))); break;
        case 2: _t->on_pushButton_home_clicked(); break;
        case 3: _t->on_pushButton_cancel_clicked(); break;
        case 4: _t->on_pushButton_done_clicked(); break;
        case 5: _t->on_pushButton_num_down_clicked(); break;
        case 6: _t->on_pushButton_num_up_clicked(); break;
        case 7: _t->on_pushButton_value_clicked(); break;
        case 8: _t->setCurrentClima((*reinterpret_cast< CLIMA_MODBUS(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData screen_51::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject screen_51::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_screen_51,
      qt_meta_data_screen_51, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &screen_51::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *screen_51::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *screen_51::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_screen_51))
        return static_cast<void*>(const_cast< screen_51*>(this));
    return QWidget::qt_metacast(_clname);
}

int screen_51::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void screen_51::touched(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void screen_51::showkeypad(QPushButton * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
