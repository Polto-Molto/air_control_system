/********************************************************************************
** Form generated from reading UI file 'screen_25.ui'
**
** Created: Thu Apr 13 00:46:21 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREEN_25_H
#define UI_SCREEN_25_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_screen_25
{
public:
    QGridLayout *gridLayout;
    QFrame *frame_header;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QFrame *frame_footer;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_15;
    QPushButton *pushButton_back;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *pushButton_cancel;
    QSpacerItem *horizontalSpacer_17;
    QPushButton *pushButton_home;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *pushButton_edit;
    QSpacerItem *horizontalSpacer_19;
    QPushButton *pushButton_done;
    QSpacerItem *horizontalSpacer_18;
    QPushButton *pushButton_next;
    QSpacerItem *horizontalSpacer_16;
    QFrame *frame_body;
    QVBoxLayout *verticalLayout_3;
    QSpacerItem *verticalSpacer_5;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_10;
    QPushButton *pushButton_activate;
    QPushButton *pushButton_deactivate;
    QSpacerItem *horizontalSpacer_12;
    QLabel *label_2;
    QFrame *line;
    QHBoxLayout *horizontalLayout_10;
    QGridLayout *gridLayout_2;
    QHBoxLayout *horizontalLayout_8;
    QSpacerItem *horizontalSpacer_7;
    QCheckBox *checkBox_room_alarm_2;
    QHBoxLayout *horizontalLayout_9;
    QSpacerItem *horizontalSpacer_8;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout_4;
    QRadioButton *radioButton_winter;
    QSpacerItem *horizontalSpacer_14;
    QRadioButton *radioButton_summer;
    QLabel *label_5;
    QLabel *label_4;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_3;
    QCheckBox *checkBox_dew;
    QLabel *label_3;
    QLabel *label_6;
    QHBoxLayout *horizontalLayout_7;
    QSpacerItem *horizontalSpacer_4;
    QCheckBox *checkBox_room_alarm;
    QSpacerItem *horizontalSpacer_11;
    QSpacerItem *horizontalSpacer_9;
    QVBoxLayout *verticalLayout_2;
    QSpacerItem *verticalSpacer_2;
    QVBoxLayout *verticalLayout;
    QLabel *label_7;
    QSpacerItem *verticalSpacer;
    QPushButton *pushButton_clima_id;
    QSpacerItem *verticalSpacer_3;
    QSpacerItem *horizontalSpacer_13;
    QSpacerItem *verticalSpacer_4;

    void setupUi(QWidget *screen_25)
    {
        if (screen_25->objectName().isEmpty())
            screen_25->setObjectName(QString::fromUtf8("screen_25"));
        screen_25->resize(1024, 600);
        screen_25->setMinimumSize(QSize(1024, 600));
        screen_25->setMaximumSize(QSize(1024, 600));
        gridLayout = new QGridLayout(screen_25);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame_header = new QFrame(screen_25);
        frame_header->setObjectName(QString::fromUtf8("frame_header"));
        frame_header->setMinimumSize(QSize(1024, 50));
        frame_header->setMaximumSize(QSize(1024, 50));
        frame_header->setFrameShape(QFrame::StyledPanel);
        frame_header->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_header);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(frame_header);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(14);
        label->setFont(font);

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        gridLayout->addWidget(frame_header, 0, 0, 1, 1);

        frame_footer = new QFrame(screen_25);
        frame_footer->setObjectName(QString::fromUtf8("frame_footer"));
        frame_footer->setMinimumSize(QSize(1024, 80));
        frame_footer->setMaximumSize(QSize(1024, 80));
        frame_footer->setFrameShape(QFrame::StyledPanel);
        frame_footer->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_footer);
        horizontalLayout_3->setSpacing(0);
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_15 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_15);

        pushButton_back = new QPushButton(frame_footer);
        pushButton_back->setObjectName(QString::fromUtf8("pushButton_back"));
        pushButton_back->setMinimumSize(QSize(50, 50));
        pushButton_back->setMaximumSize(QSize(50, 50));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/images/Icons/back.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_back->setIcon(icon);
        pushButton_back->setIconSize(QSize(50, 50));
        pushButton_back->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_back);

        horizontalSpacer_5 = new QSpacerItem(152, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);

        pushButton_cancel = new QPushButton(frame_footer);
        pushButton_cancel->setObjectName(QString::fromUtf8("pushButton_cancel"));
        pushButton_cancel->setMinimumSize(QSize(50, 50));
        pushButton_cancel->setMaximumSize(QSize(50, 50));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/images/Icons/Cross.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_cancel->setIcon(icon1);
        pushButton_cancel->setIconSize(QSize(50, 50));
        pushButton_cancel->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_cancel);

        horizontalSpacer_17 = new QSpacerItem(151, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_17);

        pushButton_home = new QPushButton(frame_footer);
        pushButton_home->setObjectName(QString::fromUtf8("pushButton_home"));
        pushButton_home->setMinimumSize(QSize(50, 50));
        pushButton_home->setMaximumSize(QSize(50, 50));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/images/Icons/home.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_home->setIcon(icon2);
        pushButton_home->setIconSize(QSize(50, 50));
        pushButton_home->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_home);

        horizontalSpacer_6 = new QSpacerItem(84, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);

        pushButton_edit = new QPushButton(frame_footer);
        pushButton_edit->setObjectName(QString::fromUtf8("pushButton_edit"));
        pushButton_edit->setMinimumSize(QSize(50, 50));
        pushButton_edit->setMaximumSize(QSize(50, 50));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/images/Icons/rename.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_edit->setIcon(icon3);
        pushButton_edit->setIconSize(QSize(50, 50));
        pushButton_edit->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_edit);

        horizontalSpacer_19 = new QSpacerItem(85, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_19);

        pushButton_done = new QPushButton(frame_footer);
        pushButton_done->setObjectName(QString::fromUtf8("pushButton_done"));
        pushButton_done->setMinimumSize(QSize(50, 50));
        pushButton_done->setMaximumSize(QSize(50, 50));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/images/Icons/check.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_done->setIcon(icon4);
        pushButton_done->setIconSize(QSize(50, 50));
        pushButton_done->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_done);

        horizontalSpacer_18 = new QSpacerItem(84, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_18);

        pushButton_next = new QPushButton(frame_footer);
        pushButton_next->setObjectName(QString::fromUtf8("pushButton_next"));
        pushButton_next->setMinimumSize(QSize(50, 50));
        pushButton_next->setMaximumSize(QSize(50, 50));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/images/images/Icons/next.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_next->setIcon(icon5);
        pushButton_next->setIconSize(QSize(50, 50));
        pushButton_next->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_next);

        horizontalSpacer_16 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_16);


        gridLayout->addWidget(frame_footer, 2, 0, 1, 1);

        frame_body = new QFrame(screen_25);
        frame_body->setObjectName(QString::fromUtf8("frame_body"));
        frame_body->setMinimumSize(QSize(1024, 470));
        frame_body->setMaximumSize(QSize(1024, 470));
        frame_body->setFrameShape(QFrame::StyledPanel);
        frame_body->setFrameShadow(QFrame::Raised);
        verticalLayout_3 = new QVBoxLayout(frame_body);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_5);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(20);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_10);

        pushButton_activate = new QPushButton(frame_body);
        pushButton_activate->setObjectName(QString::fromUtf8("pushButton_activate"));
        pushButton_activate->setMinimumSize(QSize(270, 60));
        pushButton_activate->setMaximumSize(QSize(270, 60));
        pushButton_activate->setStyleSheet(QString::fromUtf8("QPushButton::checked {\n"
"background-color: rgb(217, 229, 255);\n"
"}\n"
"QPushButton{\n"
"border-style: solid;\n"
"border-width: 2px;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"padding:20px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"\n"
"}"));
        pushButton_activate->setCheckable(true);

        horizontalLayout_2->addWidget(pushButton_activate);

        pushButton_deactivate = new QPushButton(frame_body);
        pushButton_deactivate->setObjectName(QString::fromUtf8("pushButton_deactivate"));
        pushButton_deactivate->setMinimumSize(QSize(270, 60));
        pushButton_deactivate->setMaximumSize(QSize(270, 60));
        pushButton_deactivate->setStyleSheet(QString::fromUtf8("QPushButton::checked {\n"
"background-color: rgb(217, 229, 255);\n"
"}\n"
"QPushButton{\n"
"border-style: solid;\n"
"border-width: 2px;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"padding:20px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"\n"
"}"));
        pushButton_deactivate->setCheckable(true);

        horizontalLayout_2->addWidget(pushButton_deactivate);

        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_12);


        verticalLayout_3->addLayout(horizontalLayout_2);

        label_2 = new QLabel(frame_body);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(0, 40));
        label_2->setMaximumSize(QSize(16777215, 40));
        label_2->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(label_2);

        line = new QFrame(frame_body);
        line->setObjectName(QString::fromUtf8("line"));
        line->setMinimumSize(QSize(980, 0));
        line->setMaximumSize(QSize(980, 16777215));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout_3->addWidget(line);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(0);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_7);

        checkBox_room_alarm_2 = new QCheckBox(frame_body);
        checkBox_room_alarm_2->setObjectName(QString::fromUtf8("checkBox_room_alarm_2"));
        checkBox_room_alarm_2->setMinimumSize(QSize(60, 30));
        checkBox_room_alarm_2->setMaximumSize(QSize(60, 30));
        checkBox_room_alarm_2->setStyleSheet(QString::fromUtf8("\n"
"QCheckBox::indicator:unchecked {\n"
"    image: url(:/images/images/Icons/switcher OFF.png)\n"
"}\n"
"\n"
"QCheckBox::indicator:checked {\n"
"image:url(:/images/images/Icons/switcher ON.png)\n"
"}\n"
""));

        horizontalLayout_8->addWidget(checkBox_room_alarm_2);


        gridLayout_2->addLayout(horizontalLayout_8, 2, 2, 1, 1);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_8);

        groupBox = new QGroupBox(frame_body);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setMinimumSize(QSize(0, 40));
        groupBox->setMaximumSize(QSize(16777215, 40));
        groupBox->setStyleSheet(QString::fromUtf8("padding:0px;"));
        horizontalLayout_4 = new QHBoxLayout(groupBox);
        horizontalLayout_4->setSpacing(0);
        horizontalLayout_4->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        radioButton_winter = new QRadioButton(groupBox);
        radioButton_winter->setObjectName(QString::fromUtf8("radioButton_winter"));
        radioButton_winter->setStyleSheet(QString::fromUtf8("QRadioButton::indicator:unchecked {\n"
"    image: url(:/images/images/Icons/radio uncheck.png);\n"
"}\n"
"\n"
"QRadioButton::indicator:checked {\n"
"image:url(:/images/images/Icons/radiochecked.png);\n"
"}\n"
"QRadioButton {\n"
"color:rgb(0, 84, 127);\n"
"}\n"
""));
        radioButton_winter->setChecked(true);

        horizontalLayout_4->addWidget(radioButton_winter);

        horizontalSpacer_14 = new QSpacerItem(20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_14);

        radioButton_summer = new QRadioButton(groupBox);
        radioButton_summer->setObjectName(QString::fromUtf8("radioButton_summer"));
        radioButton_summer->setMinimumSize(QSize(0, 30));
        radioButton_summer->setMaximumSize(QSize(16777215, 30));
        radioButton_summer->setStyleSheet(QString::fromUtf8("QRadioButton::indicator:unchecked {\n"
"    image: url(:/images/images/Icons/radio uncheck.png);\n"
"}\n"
"\n"
"QRadioButton::indicator:checked {\n"
"image:url(:/images/images/Icons/radiochecked.png);\n"
"}\n"
"QRadioButton {\n"
"color:rgb(0, 84, 127);\n"
"}\n"
""));
        radioButton_summer->setIconSize(QSize(16, 16));

        horizontalLayout_4->addWidget(radioButton_summer);


        horizontalLayout_9->addWidget(groupBox);


        gridLayout_2->addLayout(horizontalLayout_9, 3, 2, 1, 1);

        label_5 = new QLabel(frame_body);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout_2->addWidget(label_5, 2, 1, 1, 1);

        label_4 = new QLabel(frame_body);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout_2->addWidget(label_4, 1, 1, 1, 1);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalSpacer_3 = new QSpacerItem(290, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_3);

        checkBox_dew = new QCheckBox(frame_body);
        checkBox_dew->setObjectName(QString::fromUtf8("checkBox_dew"));
        checkBox_dew->setMinimumSize(QSize(60, 30));
        checkBox_dew->setMaximumSize(QSize(60, 30));
        checkBox_dew->setStyleSheet(QString::fromUtf8("QCheckBox::indicator:unchecked {\n"
"    image: url(:/images/images/Icons/switcher OFF.png)\n"
"}\n"
"\n"
"QCheckBox::indicator:checked {\n"
"image:url(:/images/images/Icons/switcher ON.png)\n"
"}\n"
""));
        checkBox_dew->setChecked(true);

        horizontalLayout_6->addWidget(checkBox_dew);


        gridLayout_2->addLayout(horizontalLayout_6, 0, 2, 1, 1);

        label_3 = new QLabel(frame_body);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_2->addWidget(label_3, 0, 1, 1, 1);

        label_6 = new QLabel(frame_body);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout_2->addWidget(label_6, 3, 1, 1, 1);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_4);

        checkBox_room_alarm = new QCheckBox(frame_body);
        checkBox_room_alarm->setObjectName(QString::fromUtf8("checkBox_room_alarm"));
        checkBox_room_alarm->setMinimumSize(QSize(60, 30));
        checkBox_room_alarm->setMaximumSize(QSize(60, 30));
        checkBox_room_alarm->setStyleSheet(QString::fromUtf8("QCheckBox::indicator:unchecked {\n"
"    image: url(:/images/images/Icons/switcher OFF.png)\n"
"}\n"
"\n"
"QCheckBox::indicator:checked {\n"
"image:url(:/images/images/Icons/switcher ON.png)\n"
"}\n"
""));
        checkBox_room_alarm->setChecked(true);

        horizontalLayout_7->addWidget(checkBox_room_alarm);


        gridLayout_2->addLayout(horizontalLayout_7, 1, 2, 1, 1);

        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_11, 1, 0, 1, 1);


        horizontalLayout_10->addLayout(gridLayout_2);

        horizontalSpacer_9 = new QSpacerItem(180, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_9);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label_7 = new QLabel(frame_body);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setMinimumSize(QSize(0, 30));
        label_7->setMaximumSize(QSize(16777215, 30));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Sans Serif"));
        font1.setPointSize(12);
        label_7->setFont(font1);

        verticalLayout->addWidget(label_7);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        pushButton_clima_id = new QPushButton(frame_body);
        pushButton_clima_id->setObjectName(QString::fromUtf8("pushButton_clima_id"));
        pushButton_clima_id->setMinimumSize(QSize(150, 90));
        pushButton_clima_id->setMaximumSize(QSize(150, 90));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Sans Serif"));
        font2.setPointSize(32);
        pushButton_clima_id->setFont(font2);
        pushButton_clima_id->setStyleSheet(QString::fromUtf8("border:1px solid black;"));
        pushButton_clima_id->setFlat(true);

        verticalLayout->addWidget(pushButton_clima_id);


        verticalLayout_2->addLayout(verticalLayout);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_3);


        horizontalLayout_10->addLayout(verticalLayout_2);

        horizontalSpacer_13 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_13);


        verticalLayout_3->addLayout(horizontalLayout_10);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_4);


        gridLayout->addWidget(frame_body, 1, 0, 1, 1);


        retranslateUi(screen_25);

        QMetaObject::connectSlotsByName(screen_25);
    } // setupUi

    void retranslateUi(QWidget *screen_25)
    {
        screen_25->setWindowTitle(QApplication::translate("screen_25", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("screen_25", "Radiant device 1 - Integra Control Clima settings", 0, QApplication::UnicodeUTF8));
        pushButton_home->setText(QString());
        pushButton_edit->setText(QString());
        pushButton_done->setText(QString());
        pushButton_next->setText(QString());
        pushButton_activate->setText(QApplication::translate("screen_25", "Activate", 0, QApplication::UnicodeUTF8));
        pushButton_deactivate->setText(QApplication::translate("screen_25", "Deactivate", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("screen_25", "Room Temp: 10 C   |  RH  50%   | Radiant Temp. 8 C ", 0, QApplication::UnicodeUTF8));
        checkBox_room_alarm_2->setText(QString());
        groupBox->setTitle(QString());
        radioButton_winter->setText(QApplication::translate("screen_25", "Winter", 0, QApplication::UnicodeUTF8));
        radioButton_summer->setText(QApplication::translate("screen_25", "Summer", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("screen_25", "Room probe alarm", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("screen_25", "Room probe alarm", 0, QApplication::UnicodeUTF8));
        checkBox_dew->setText(QString());
        label_3->setText(QApplication::translate("screen_25", "Dew formation", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("screen_25", "Seasonal operation", 0, QApplication::UnicodeUTF8));
        checkBox_room_alarm->setText(QString());
        label_7->setText(QApplication::translate("screen_25", "MODBUS Address", 0, QApplication::UnicodeUTF8));
        pushButton_clima_id->setText(QApplication::translate("screen_25", "1", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class screen_25: public Ui_screen_25 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREEN_25_H
