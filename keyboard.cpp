#include "keyboard.h"
#include "ui_keyboard.h"

keyboard::keyboard(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::keyboard)
{
    ui->setupUi(this);
    setKeyMap(NORMAL);
    current_keymap = NORMAL;
    this->setWindowFlags(Qt::ToolTip);
    line_edit = 0;
}

keyboard::~keyboard()
{
    delete ui;
}

void keyboard::setSenderWidge(QPushButton* obj)
{
    line_edit = obj;
}

void keyboard::write_char(QString s)
{
    line_edit->setText(line_edit->text() + s);
}

void keyboard::remove_char()
{
    QString str = line_edit->text();
    str.chop(1);
    line_edit->setText(str);
}

void keyboard::on_pushButton_mic_clicked()
{
    //Q_EMIT(touched(2));
}

void keyboard::on_pushButton_a_clicked()
{
    write_char(ui->pushButton_a->text());
}

void keyboard::on_pushButton_b_clicked()
{
    write_char(ui->pushButton_b->text());
}

void keyboard::on_pushButton_c_clicked()
{
    write_char(ui->pushButton_c->text());
}

void keyboard::on_pushButton_d_clicked()
{
    write_char(ui->pushButton_d->text());
}

void keyboard::on_pushButton_e_clicked()
{
    write_char(ui->pushButton_e->text());
}

void keyboard::on_pushButton_f_clicked()
{
    write_char(ui->pushButton_f->text());
}

void keyboard::on_pushButton_g_clicked()
{
    write_char(ui->pushButton_g->text());
}

void keyboard::on_pushButton_h_clicked()
{
    write_char(ui->pushButton_h->text());
}

void keyboard::on_pushButton_i_clicked()
{
    write_char(ui->pushButton_i->text());
}

void keyboard::on_pushButton_j_clicked()
{
    write_char(ui->pushButton_j->text());
}

void keyboard::on_pushButton_k_clicked()
{
    write_char(ui->pushButton_k->text());
}

void keyboard::on_pushButton_l_clicked()
{
    write_char(ui->pushButton_l->text());
}

void keyboard::on_pushButton_m_clicked()
{
    write_char(ui->pushButton_m->text());
}

void keyboard::on_pushButton_n_clicked()
{
    write_char(ui->pushButton_n->text());
}

void keyboard::on_pushButton_o_clicked()
{
    write_char(ui->pushButton_o->text());
}

void keyboard::on_pushButton_p_clicked()
{
    write_char(ui->pushButton_p->text());
}

void keyboard::on_pushButton_q_clicked()
{
    write_char(ui->pushButton_q->text());
}

void keyboard::on_pushButton_r_clicked()
{
    write_char(ui->pushButton_r->text());
}

void keyboard::on_pushButton_s_clicked()
{
    write_char(ui->pushButton_s->text());
}

void keyboard::on_pushButton_t_clicked()
{
    write_char(ui->pushButton_t->text());
}

void keyboard::on_pushButton_u_clicked()
{
    write_char(ui->pushButton_u->text());
}

void keyboard::on_pushButton_v_clicked()
{
    write_char(ui->pushButton_v->text());
}

void keyboard::on_pushButton_w_clicked()
{
    write_char(ui->pushButton_w->text());
}

void keyboard::on_pushButton_x_clicked()
{
    write_char(ui->pushButton_x->text());
}

void keyboard::on_pushButton_y_clicked()
{
    write_char(ui->pushButton_y->text());
}

void keyboard::on_pushButton_z_clicked()
{
    write_char(ui->pushButton_z->text());
}

void keyboard::on_pushButton_lshift_clicked()
{
    if(current_keymap != SHIFT)
        setKeyMap(SHIFT);
    else
        setKeyMap(NORMAL);
}

void keyboard::on_pushButton_rshift_clicked()
{
    if(current_keymap != SHIFT)
        setKeyMap(SHIFT);
    else
        setKeyMap(NORMAL);}

void keyboard::on_pushButton_Lnumbers_clicked()
{
    if(current_keymap != NUMBERS)
        setKeyMap(NUMBERS);
    else
        setKeyMap(NORMAL);
}

void keyboard::on_pushButton_Rnumbers_clicked()
{
    if(current_keymap != NUMBERS)
        setKeyMap(NUMBERS);
    else
        setKeyMap(NORMAL);
}

void keyboard::on_pushButton_return_clicked()
{
    Q_EMIT(returnPressed());
}

void keyboard::on_pushButton_backspace_clicked()
{
    remove_char();
}

void keyboard::on_pushButton_spacebar_clicked()
{
    write_char(" ");
}

void keyboard::on_pushButton_lang_clicked()
{
    if(current_keymap != ARABIC)
        setKeyMap(ARABIC);
    else
        setKeyMap(NORMAL);
}

void keyboard::setKeyMap(KEYMAP map)
{
    current_keymap = map;
    switch(map)
    {
    case NORMAL:
        ui->pushButton_a->setText("a");
        ui->pushButton_b->setText("b");
        ui->pushButton_c->setText("c");
        ui->pushButton_d->setText("d");
        ui->pushButton_e->setText("e");
        ui->pushButton_f->setText("f");
        ui->pushButton_g->setText("g");
        ui->pushButton_h->setText("h");
        ui->pushButton_i->setText("i");
        ui->pushButton_j->setText("j");
        ui->pushButton_k->setText("k");
        ui->pushButton_l->setText("l");
        ui->pushButton_m->setText("m");
        ui->pushButton_n->setText("n");
        ui->pushButton_o->setText("o");
        ui->pushButton_p->setText("p");
        ui->pushButton_q->setText("q");
        ui->pushButton_r->setText("r");
        ui->pushButton_s->setText("s");
        ui->pushButton_t->setText("t");
        ui->pushButton_u->setText("u");
        ui->pushButton_v->setText("v");
        ui->pushButton_w->setText("w");
        ui->pushButton_x->setText("x");
        ui->pushButton_y->setText("y");
        ui->pushButton_z->setText("z");

        ui->pushButton_colon->setText("!\n,");
        ui->pushButton_dot->setText("?\n.");
        break;

    case SHIFT:
        ui->pushButton_a->setText("A");
        ui->pushButton_b->setText("B");
        ui->pushButton_c->setText("C");
        ui->pushButton_d->setText("D");
        ui->pushButton_e->setText("E");
        ui->pushButton_f->setText("F");
        ui->pushButton_g->setText("G");
        ui->pushButton_h->setText("H");
        ui->pushButton_i->setText("I");
        ui->pushButton_j->setText("J");
        ui->pushButton_k->setText("K");
        ui->pushButton_l->setText("L");
        ui->pushButton_m->setText("M");
        ui->pushButton_n->setText("N");
        ui->pushButton_o->setText("O");
        ui->pushButton_p->setText("P");
        ui->pushButton_q->setText("Q");
        ui->pushButton_r->setText("R");
        ui->pushButton_s->setText("S");
        ui->pushButton_t->setText("T");
        ui->pushButton_u->setText("U");
        ui->pushButton_v->setText("V");
        ui->pushButton_w->setText("W");
        ui->pushButton_x->setText("X");
        ui->pushButton_y->setText("Y");
        ui->pushButton_z->setText("Z");

        ui->pushButton_colon->setText("!");
        ui->pushButton_dot->setText("?");
        break;

    case NUMBERS:
        ui->pushButton_a->setText("[");
        ui->pushButton_b->setText("^");
        ui->pushButton_c->setText("$");
        ui->pushButton_d->setText("{");
        ui->pushButton_e->setText("3");
        ui->pushButton_f->setText("}");
        ui->pushButton_g->setText("|");
        ui->pushButton_h->setText("+");
        ui->pushButton_i->setText("8");
        ui->pushButton_j->setText("-");
        ui->pushButton_k->setText("*");
        ui->pushButton_l->setText("/");
        ui->pushButton_m->setText(":");
        ui->pushButton_n->setText("&");
        ui->pushButton_o->setText("9");
        ui->pushButton_p->setText("0");
        ui->pushButton_q->setText("1");
        ui->pushButton_r->setText("4");
        ui->pushButton_s->setText("]");
        ui->pushButton_t->setText("5");
        ui->pushButton_u->setText("7");
        ui->pushButton_v->setText("%");
        ui->pushButton_w->setText("2");
        ui->pushButton_x->setText("#");
        ui->pushButton_y->setText("6");
        ui->pushButton_z->setText("@");

        ui->pushButton_colon->setText("(");
        ui->pushButton_dot->setText(")");
        break;

    case ARABIC:
        ui->pushButton_a->setText("ش");
        ui->pushButton_b->setText("لا");
        ui->pushButton_c->setText("ؤ");
        ui->pushButton_d->setText("ي");
        ui->pushButton_e->setText("ث");
        ui->pushButton_f->setText("ب");
        ui->pushButton_g->setText("ل");
        ui->pushButton_h->setText("ا");
        ui->pushButton_i->setText("ه");
        ui->pushButton_j->setText("ت");
        ui->pushButton_k->setText("ن");
        ui->pushButton_l->setText("م");
        ui->pushButton_m->setText("ة");
        ui->pushButton_n->setText("ى");
        ui->pushButton_o->setText("خ");
        ui->pushButton_p->setText("ح");
        ui->pushButton_q->setText("ض");
        ui->pushButton_r->setText("ق");
        ui->pushButton_s->setText("س");
        ui->pushButton_t->setText("ف");
        ui->pushButton_u->setText("ع");
        ui->pushButton_v->setText("ر");
        ui->pushButton_w->setText("ص");
        ui->pushButton_x->setText("ء");
        ui->pushButton_y->setText("غ");
        ui->pushButton_z->setText("ئ");

        ui->pushButton_colon->setText("و");
        ui->pushButton_dot->setText("ز");
        break;
    }
}
