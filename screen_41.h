#ifndef SCREEN_41_H
#define SCREEN_41_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_41;
}

class screen_41 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_41(dataManager*,QWidget *parent = 0);
    ~screen_41();
signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_cancel_clicked();
      void on_pushButton_next_clicked();
      void on_pushButton_test_clicked();

private:
    Ui::screen_41 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_41_H
