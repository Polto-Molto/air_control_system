#include "screen_34.h"
#include "ui_screen_34.h"

screen_34::screen_34(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_34),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_34::~screen_34()
{
    delete ui;
}

void screen_34::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_34::on_pushButton_done_clicked()
{
    Q_EMIT(touched(33));
}
