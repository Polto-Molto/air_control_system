#include "screen_19.h"
#include "ui_screen_19.h"

screen_19::screen_19(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_19),
    _datamgr(dm)
{
    ui->setupUi(this);
    buttons.push_back(ui->schedule_1);
    buttons.push_back(ui->schedule_2);
    buttons.push_back(ui->schedule_3);
    buttons.push_back(ui->schedule_4);
    buttons.push_back(ui->schedule_5);
    buttons.push_back(ui->schedule_6);

    foreach(QPushButton* button, buttons)
        connect(button,SIGNAL(clicked())
                ,this,SLOT(scheduleSelected()));
    reload();
}

screen_19::~screen_19()
{
    delete ui;
}

void screen_19::reload()
{
    foreach(QPushButton* button, buttons)
        button->setText(_datamgr->scheduleData(button->objectName()).name);
}

void screen_19::setCurrentScheduleName(QString name)
{
    _schedule.name = name;
    _datamgr->updateScheduleData(_schedule);
}

QString screen_19::currentScheduleName()
{
    return _schedule.name;
}

void screen_19::on_pushButton_edit_clicked()
{
   Q_EMIT(selectedSchedule(_schedule.name));
   Q_EMIT(touched(20));
}

void screen_19::on_pushButton_datetime_clicked()
{
    Q_EMIT(touched(17));
}

void screen_19::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(2));
}

void screen_19::scheduleSelected()
{
    foreach(QPushButton* button, buttons)
        button->setChecked(false);
    ((QPushButton*)sender())->setChecked(true);
    _schedule = _datamgr->scheduleData(((QPushButton*)sender())->objectName());
}
