#ifndef KEYPAD_H
#define KEYPAD_H

#include <QWidget>
#include <QPushButton>

namespace Ui {
class keypad;
}

class keypad : public QWidget
{
    Q_OBJECT
    
public:
    explicit keypad(QWidget *parent = 0);
    ~keypad();
signals:
    void returnPressed();

public slots:
    void setSenderWidge(QPushButton*);

    void write_char(QString);
    void remove_char();

    void on_pushButton_0_clicked();
    void on_pushButton_1_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();
    void on_pushButton_4_clicked();
    void on_pushButton_5_clicked();
    void on_pushButton_6_clicked();
    void on_pushButton_7_clicked();
    void on_pushButton_8_clicked();
    void on_pushButton_9_clicked();
    void on_pushButton_enter_clicked();
    void on_pushButton_back_clicked();

private:
    Ui::keypad *ui;
    QPushButton *line_edit;

};

#endif // KEYPAD_H
