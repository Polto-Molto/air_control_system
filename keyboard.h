#ifndef KEYBOARD_H
#define KEYBOARD_H

#include <QWidget>
#include <QPushButton>

namespace Ui {
class keyboard;
}

class keyboard : public QWidget
{
    Q_OBJECT
public:
    enum KEYMAP{
        NORMAL,
        SHIFT,
        NUMBERS,
        ARABIC
    };
public:
    explicit keyboard(QWidget *parent = 0);
    ~keyboard();

signals:
    void returnPressed();

public slots:
    void setSenderWidge(QPushButton*);

    void write_char(QString);
    void remove_char();

    void on_pushButton_a_clicked();
    void on_pushButton_b_clicked();
    void on_pushButton_c_clicked();
    void on_pushButton_d_clicked();
    void on_pushButton_e_clicked();
    void on_pushButton_f_clicked();
    void on_pushButton_g_clicked();
    void on_pushButton_h_clicked();
    void on_pushButton_i_clicked();
    void on_pushButton_j_clicked();
    void on_pushButton_k_clicked();
    void on_pushButton_l_clicked();
    void on_pushButton_m_clicked();
    void on_pushButton_n_clicked();
    void on_pushButton_o_clicked();
    void on_pushButton_p_clicked();
    void on_pushButton_q_clicked();
    void on_pushButton_r_clicked();
    void on_pushButton_s_clicked();
    void on_pushButton_t_clicked();
    void on_pushButton_u_clicked();
    void on_pushButton_v_clicked();
    void on_pushButton_w_clicked();
    void on_pushButton_x_clicked();
    void on_pushButton_y_clicked();
    void on_pushButton_z_clicked();
    void on_pushButton_lshift_clicked();
    void on_pushButton_rshift_clicked();
    void on_pushButton_Lnumbers_clicked();
    void on_pushButton_Rnumbers_clicked();
    void on_pushButton_return_clicked();
    void on_pushButton_backspace_clicked();
    void on_pushButton_lang_clicked();
    void on_pushButton_mic_clicked();
    void on_pushButton_spacebar_clicked();

    void setKeyMap(KEYMAP);

private:
    Ui::keyboard *ui;
    QPushButton *line_edit;
    bool shift_clicked;
    KEYMAP current_keymap;
};

#endif // KEYBOARD_H
