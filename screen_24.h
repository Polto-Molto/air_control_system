#ifndef SCREEN_24_H
#define SCREEN_24_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_24;
}

class screen_24 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_24(dataManager*,QWidget *parent = 0);
    ~screen_24();

signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_cancel_clicked();
      void on_pushButton_back_clicked();

private:
    Ui::screen_24 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_24_H
