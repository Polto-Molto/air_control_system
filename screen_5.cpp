#include "screen_5.h"
#include "ui_screen_5.h"

screen_5::screen_5(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_5),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_5::~screen_5()
{
    delete ui;
}

void screen_5::reload()
{
    _clima = _datamgr->climaData(_clima.config_name);
    ui->label_temp->setNum(_clima.air_temp);
    ui->label_humidity->setNum(_clima.relative_humidity);
    ui->label_heat_surface->setNum(_clima.air_temp);
    QString str_num;
    ui->pushButton_eco_comfort->setText(str_num.setNum(_clima.summer_comfort_temp_point));
    ui->pushButton_humidit_setpoint->setText(str_num.setNum(_clima.summer_humidity_point));
    ui->toolButton_schedule->setText(_datamgr->scheduleData(_clima.schedule).name);
}

void screen_5::setCurrentClima(CLIMA_MODBUS clima)
{
    _clima = clima;
    ui->label_temp->setNum(_clima.air_temp);
    ui->label_humidity->setNum(_clima.relative_humidity);
    ui->label_heat_surface->setNum(_clima.air_temp);
    QString str_num;
    ui->pushButton_eco_comfort->setText(str_num.setNum(_clima.summer_comfort_temp_point));
    ui->pushButton_humidit_setpoint->setText(str_num.setNum(_clima.summer_humidity_point));
    ui->toolButton_schedule->setText(_datamgr->scheduleData(_clima.schedule).name);
    Q_EMIT(touched(5));
}

void screen_5::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_5::on_pushButton_next_clicked()
{
    foreach(CLIMA_MODBUS clima,_datamgr->climaTable()->values())
    {
        if(clima.room_number == (_clima.room_number+1))
        {
            _clima = clima;
            break;
        }
    }
    setCurrentClima(_clima);
}

void screen_5::on_pushButton_back_clicked()
{
    foreach(CLIMA_MODBUS clima,_datamgr->climaTable()->values())
    {
        if(clima.room_number == (_clima.room_number-1))
        {
            _clima = clima;
            break;
        }
    }
    setCurrentClima(_clima);
}

void screen_5::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(4));
}

void screen_5::on_pushButton_eco_comfort_clicked()
{
    if(_datamgr->mainData().season == WINTER)
        Q_EMIT(selectedWinterClima(_clima));
    else
        Q_EMIT(selectedSummerClima(_clima));
}

void screen_5::on_pushButton_touch_clicked()
{
    Q_EMIT(touched(8));
}

void screen_5::on_toolButton_schedule_clicked()
{
    Q_EMIT(selectedScheduleClima(_clima));
}
