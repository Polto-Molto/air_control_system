#ifndef NOTIFYLABEL_H
#define NOTIFYLABEL_H

#include <QWidget>

namespace Ui {
class NotifyLabel;
}

class NotifyLabel : public QWidget
{
    Q_OBJECT
    
public:
    explicit NotifyLabel(int width,QString text,int state,QWidget *parent = 0);
    ~NotifyLabel();

private:
    Ui::NotifyLabel *ui;
};

#endif // NOTIFYLABEL_H
