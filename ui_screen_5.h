/********************************************************************************
** Form generated from reading UI file 'screen_5.ui'
**
** Created: Thu Apr 13 12:10:40 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREEN_5_H
#define UI_SCREEN_5_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_screen_5
{
public:
    QVBoxLayout *verticalLayout;
    QFrame *frame_header;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QFrame *frame_body;
    QHBoxLayout *horizontalLayout_13;
    QSpacerItem *horizontalSpacer_34;
    QFrame *frame;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_9;
    QHBoxLayout *horizontalLayout_8;
    QSpacerItem *horizontalSpacer_21;
    QLabel *label_12;
    QLabel *label_temp;
    QLabel *label_14;
    QSpacerItem *horizontalSpacer_22;
    QLabel *label_26;
    QSpacerItem *horizontalSpacer_23;
    QHBoxLayout *horizontalLayout_9;
    QSpacerItem *horizontalSpacer_24;
    QLabel *label_23;
    QLabel *label_humidity;
    QLabel *label_25;
    QSpacerItem *horizontalSpacer_25;
    QLabel *label_17;
    QSpacerItem *horizontalSpacer_26;
    QHBoxLayout *horizontalLayout_10;
    QSpacerItem *horizontalSpacer_27;
    QLabel *label_27;
    QLabel *label_heat_surface;
    QLabel *label_29;
    QSpacerItem *horizontalSpacer_28;
    QLabel *label_30;
    QSpacerItem *horizontalSpacer_29;
    QSpacerItem *horizontalSpacer_35;
    QFrame *line_2;
    QSpacerItem *horizontalSpacer_36;
    QFrame *frame_2;
    QVBoxLayout *verticalLayout_8;
    QLabel *label_31;
    QFrame *frame_3;
    QGridLayout *gridLayout;
    QToolButton *toolButton_schedule;
    QFrame *frame_4;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_12;
    QSpacerItem *horizontalSpacer_30;
    QLabel *label_32;
    QSpacerItem *horizontalSpacer_31;
    QHBoxLayout *horizontalLayout_11;
    QSpacerItem *horizontalSpacer_32;
    QPushButton *pushButton_eco_comfort;
    QPushButton *pushButton_humidit_setpoint;
    QSpacerItem *horizontalSpacer_33;
    QSpacerItem *horizontalSpacer_37;
    QFrame *frame_footer;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_15;
    QPushButton *pushButton_back;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *pushButton_cancel;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *pushButton_home;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *pushButton_touch;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *pushButton_next;
    QSpacerItem *horizontalSpacer_16;

    void setupUi(QWidget *screen_5)
    {
        if (screen_5->objectName().isEmpty())
            screen_5->setObjectName(QString::fromUtf8("screen_5"));
        screen_5->resize(1024, 600);
        screen_5->setMinimumSize(QSize(1024, 600));
        screen_5->setMaximumSize(QSize(1024, 600));
        verticalLayout = new QVBoxLayout(screen_5);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        frame_header = new QFrame(screen_5);
        frame_header->setObjectName(QString::fromUtf8("frame_header"));
        frame_header->setMinimumSize(QSize(1024, 50));
        frame_header->setMaximumSize(QSize(1024, 50));
        frame_header->setFrameShape(QFrame::StyledPanel);
        frame_header->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_header);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(frame_header);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(14);
        label->setFont(font);

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout->addWidget(frame_header);

        frame_body = new QFrame(screen_5);
        frame_body->setObjectName(QString::fromUtf8("frame_body"));
        frame_body->setMinimumSize(QSize(1024, 470));
        frame_body->setMaximumSize(QSize(1024, 470));
        frame_body->setFrameShape(QFrame::StyledPanel);
        frame_body->setFrameShadow(QFrame::Raised);
        horizontalLayout_13 = new QHBoxLayout(frame_body);
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        horizontalSpacer_34 = new QSpacerItem(26, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_34);

        frame = new QFrame(frame_body);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setMinimumSize(QSize(425, 350));
        frame->setMaximumSize(QSize(425, 350));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        verticalLayout_6 = new QVBoxLayout(frame);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        label_9 = new QLabel(frame);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setMinimumSize(QSize(0, 60));
        label_9->setMaximumSize(QSize(16777215, 60));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Sans Serif"));
        font1.setPointSize(16);
        label_9->setFont(font1);

        verticalLayout_6->addWidget(label_9);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(0);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        horizontalSpacer_21 = new QSpacerItem(80, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_21);

        label_12 = new QLabel(frame);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setMinimumSize(QSize(30, 60));
        label_12->setMaximumSize(QSize(30, 60));
        label_12->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/Thermo-Meter-.png")));

        horizontalLayout_8->addWidget(label_12);

        label_temp = new QLabel(frame);
        label_temp->setObjectName(QString::fromUtf8("label_temp"));
        label_temp->setMinimumSize(QSize(80, 60));
        label_temp->setMaximumSize(QSize(80, 60));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Sans Serif"));
        font2.setPointSize(42);
        label_temp->setFont(font2);

        horizontalLayout_8->addWidget(label_temp);

        label_14 = new QLabel(frame);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setMinimumSize(QSize(30, 60));
        label_14->setMaximumSize(QSize(30, 60));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Sans Serif"));
        font3.setPointSize(22);
        label_14->setFont(font3);

        horizontalLayout_8->addWidget(label_14);

        horizontalSpacer_22 = new QSpacerItem(80, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_22);

        label_26 = new QLabel(frame);
        label_26->setObjectName(QString::fromUtf8("label_26"));
        label_26->setMinimumSize(QSize(60, 60));
        label_26->setMaximumSize(QSize(60, 60));
        label_26->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/fire.png")));
        label_26->setScaledContents(true);

        horizontalLayout_8->addWidget(label_26);

        horizontalSpacer_23 = new QSpacerItem(30, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_23);


        verticalLayout_6->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(0);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        horizontalSpacer_24 = new QSpacerItem(80, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_24);

        label_23 = new QLabel(frame);
        label_23->setObjectName(QString::fromUtf8("label_23"));
        label_23->setMinimumSize(QSize(30, 60));
        label_23->setMaximumSize(QSize(30, 60));
        label_23->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/Drop 2.png")));
        label_23->setScaledContents(true);

        horizontalLayout_9->addWidget(label_23);

        label_humidity = new QLabel(frame);
        label_humidity->setObjectName(QString::fromUtf8("label_humidity"));
        label_humidity->setMinimumSize(QSize(80, 60));
        label_humidity->setMaximumSize(QSize(80, 60));
        label_humidity->setFont(font2);

        horizontalLayout_9->addWidget(label_humidity);

        label_25 = new QLabel(frame);
        label_25->setObjectName(QString::fromUtf8("label_25"));
        label_25->setMinimumSize(QSize(30, 60));
        label_25->setMaximumSize(QSize(30, 60));
        label_25->setFont(font3);

        horizontalLayout_9->addWidget(label_25);

        horizontalSpacer_25 = new QSpacerItem(80, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_25);

        label_17 = new QLabel(frame);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setMinimumSize(QSize(60, 60));
        label_17->setMaximumSize(QSize(60, 60));
        label_17->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/flake.png")));
        label_17->setScaledContents(true);

        horizontalLayout_9->addWidget(label_17);

        horizontalSpacer_26 = new QSpacerItem(30, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_26);


        verticalLayout_6->addLayout(horizontalLayout_9);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(0);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        horizontalSpacer_27 = new QSpacerItem(80, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_27);

        label_27 = new QLabel(frame);
        label_27->setObjectName(QString::fromUtf8("label_27"));
        label_27->setMinimumSize(QSize(30, 60));
        label_27->setMaximumSize(QSize(30, 60));
        label_27->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/steam.png")));
        label_27->setScaledContents(true);

        horizontalLayout_10->addWidget(label_27);

        label_heat_surface = new QLabel(frame);
        label_heat_surface->setObjectName(QString::fromUtf8("label_heat_surface"));
        label_heat_surface->setMinimumSize(QSize(80, 60));
        label_heat_surface->setMaximumSize(QSize(80, 60));
        label_heat_surface->setFont(font2);

        horizontalLayout_10->addWidget(label_heat_surface);

        label_29 = new QLabel(frame);
        label_29->setObjectName(QString::fromUtf8("label_29"));
        label_29->setMinimumSize(QSize(30, 60));
        label_29->setMaximumSize(QSize(30, 60));
        label_29->setFont(font3);

        horizontalLayout_10->addWidget(label_29);

        horizontalSpacer_28 = new QSpacerItem(80, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_28);

        label_30 = new QLabel(frame);
        label_30->setObjectName(QString::fromUtf8("label_30"));
        label_30->setMinimumSize(QSize(60, 60));
        label_30->setMaximumSize(QSize(60, 60));
        label_30->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/drop.png")));
        label_30->setScaledContents(true);

        horizontalLayout_10->addWidget(label_30);

        horizontalSpacer_29 = new QSpacerItem(30, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_29);


        verticalLayout_6->addLayout(horizontalLayout_10);


        horizontalLayout_13->addWidget(frame);

        horizontalSpacer_35 = new QSpacerItem(26, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_35);

        line_2 = new QFrame(frame_body);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setMinimumSize(QSize(0, 340));
        line_2->setFrameShadow(QFrame::Raised);
        line_2->setLineWidth(2);
        line_2->setFrameShape(QFrame::VLine);

        horizontalLayout_13->addWidget(line_2);

        horizontalSpacer_36 = new QSpacerItem(25, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_36);

        frame_2 = new QFrame(frame_body);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setMinimumSize(QSize(425, 350));
        frame_2->setMaximumSize(QSize(425, 350));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        verticalLayout_8 = new QVBoxLayout(frame_2);
#ifndef Q_OS_MAC
        verticalLayout_8->setSpacing(6);
#endif
        verticalLayout_8->setContentsMargins(0, 0, 0, 0);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        label_31 = new QLabel(frame_2);
        label_31->setObjectName(QString::fromUtf8("label_31"));
        label_31->setMinimumSize(QSize(0, 60));
        label_31->setMaximumSize(QSize(16777215, 60));
        label_31->setFont(font1);

        verticalLayout_8->addWidget(label_31);

        frame_3 = new QFrame(frame_2);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setMinimumSize(QSize(0, 138));
        frame_3->setMaximumSize(QSize(16777215, 138));
        frame_3->setFrameShape(QFrame::Box);
        frame_3->setFrameShadow(QFrame::Plain);
        gridLayout = new QGridLayout(frame_3);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        toolButton_schedule = new QToolButton(frame_3);
        toolButton_schedule->setObjectName(QString::fromUtf8("toolButton_schedule"));
        toolButton_schedule->setMinimumSize(QSize(420, 130));
        toolButton_schedule->setMaximumSize(QSize(420, 130));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Sans Serif"));
        font4.setPointSize(18);
        toolButton_schedule->setFont(font4);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/images/Icons/program 3.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton_schedule->setIcon(icon);
        toolButton_schedule->setIconSize(QSize(50, 50));
        toolButton_schedule->setCheckable(false);
        toolButton_schedule->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        toolButton_schedule->setAutoRaise(true);

        gridLayout->addWidget(toolButton_schedule, 0, 0, 1, 1);


        verticalLayout_8->addWidget(frame_3);

        frame_4 = new QFrame(frame_2);
        frame_4->setObjectName(QString::fromUtf8("frame_4"));
        frame_4->setMinimumSize(QSize(0, 138));
        frame_4->setMaximumSize(QSize(16777215, 138));
        frame_4->setFrameShape(QFrame::Box);
        frame_4->setFrameShadow(QFrame::Plain);
        verticalLayout_7 = new QVBoxLayout(frame_4);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        horizontalSpacer_30 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_12->addItem(horizontalSpacer_30);

        label_32 = new QLabel(frame_4);
        label_32->setObjectName(QString::fromUtf8("label_32"));
        label_32->setMinimumSize(QSize(50, 50));
        label_32->setMaximumSize(QSize(50, 50));
        label_32->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/comfrot.png")));
        label_32->setScaledContents(true);

        horizontalLayout_12->addWidget(label_32);

        horizontalSpacer_31 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_12->addItem(horizontalSpacer_31);


        verticalLayout_7->addLayout(horizontalLayout_12);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        horizontalSpacer_32 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_32);

        pushButton_eco_comfort = new QPushButton(frame_4);
        pushButton_eco_comfort->setObjectName(QString::fromUtf8("pushButton_eco_comfort"));
        pushButton_eco_comfort->setMinimumSize(QSize(90, 60));
        pushButton_eco_comfort->setMaximumSize(QSize(90, 60));
        QFont font5;
        font5.setFamily(QString::fromUtf8("Sans Serif"));
        font5.setPointSize(28);
        pushButton_eco_comfort->setFont(font5);
        pushButton_eco_comfort->setFlat(true);

        horizontalLayout_11->addWidget(pushButton_eco_comfort);

        pushButton_humidit_setpoint = new QPushButton(frame_4);
        pushButton_humidit_setpoint->setObjectName(QString::fromUtf8("pushButton_humidit_setpoint"));
        pushButton_humidit_setpoint->setMinimumSize(QSize(90, 60));
        pushButton_humidit_setpoint->setMaximumSize(QSize(90, 60));
        QFont font6;
        font6.setFamily(QString::fromUtf8("Sans Serif"));
        font6.setPointSize(24);
        pushButton_humidit_setpoint->setFont(font6);
        pushButton_humidit_setpoint->setFlat(true);

        horizontalLayout_11->addWidget(pushButton_humidit_setpoint);

        horizontalSpacer_33 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_33);


        verticalLayout_7->addLayout(horizontalLayout_11);


        verticalLayout_8->addWidget(frame_4);


        horizontalLayout_13->addWidget(frame_2);

        horizontalSpacer_37 = new QSpacerItem(26, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_37);


        verticalLayout->addWidget(frame_body);

        frame_footer = new QFrame(screen_5);
        frame_footer->setObjectName(QString::fromUtf8("frame_footer"));
        frame_footer->setMinimumSize(QSize(1024, 80));
        frame_footer->setMaximumSize(QSize(1024, 80));
        frame_footer->setFrameShape(QFrame::StyledPanel);
        frame_footer->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(frame_footer);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_15 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_15);

        pushButton_back = new QPushButton(frame_footer);
        pushButton_back->setObjectName(QString::fromUtf8("pushButton_back"));
        pushButton_back->setMinimumSize(QSize(50, 50));
        pushButton_back->setMaximumSize(QSize(50, 50));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/images/Icons/back.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_back->setIcon(icon1);
        pushButton_back->setIconSize(QSize(50, 50));
        pushButton_back->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_back);

        horizontalSpacer_5 = new QSpacerItem(146, 17, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_5);

        pushButton_cancel = new QPushButton(frame_footer);
        pushButton_cancel->setObjectName(QString::fromUtf8("pushButton_cancel"));
        pushButton_cancel->setMinimumSize(QSize(50, 50));
        pushButton_cancel->setMaximumSize(QSize(50, 50));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/images/Icons/Cross.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_cancel->setIcon(icon2);
        pushButton_cancel->setIconSize(QSize(50, 50));
        pushButton_cancel->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_cancel);

        horizontalSpacer_3 = new QSpacerItem(145, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);

        pushButton_home = new QPushButton(frame_footer);
        pushButton_home->setObjectName(QString::fromUtf8("pushButton_home"));
        pushButton_home->setMinimumSize(QSize(50, 50));
        pushButton_home->setMaximumSize(QSize(50, 50));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/images/Icons/home.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_home->setIcon(icon3);
        pushButton_home->setIconSize(QSize(50, 50));
        pushButton_home->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_home);

        horizontalSpacer_4 = new QSpacerItem(146, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_4);

        pushButton_touch = new QPushButton(frame_footer);
        pushButton_touch->setObjectName(QString::fromUtf8("pushButton_touch"));
        pushButton_touch->setMinimumSize(QSize(50, 50));
        pushButton_touch->setMaximumSize(QSize(50, 50));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/images/Icons/hand 2.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_touch->setIcon(icon4);
        pushButton_touch->setIconSize(QSize(50, 50));
        pushButton_touch->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_touch);

        horizontalSpacer_6 = new QSpacerItem(145, 17, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_6);

        pushButton_next = new QPushButton(frame_footer);
        pushButton_next->setObjectName(QString::fromUtf8("pushButton_next"));
        pushButton_next->setMinimumSize(QSize(50, 50));
        pushButton_next->setMaximumSize(QSize(50, 50));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/images/images/Icons/next.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_next->setIcon(icon5);
        pushButton_next->setIconSize(QSize(50, 50));
        pushButton_next->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_next);

        horizontalSpacer_16 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_16);


        verticalLayout->addWidget(frame_footer);


        retranslateUi(screen_5);

        QMetaObject::connectSlotsByName(screen_5);
    } // setupUi

    void retranslateUi(QWidget *screen_5)
    {
        screen_5->setWindowTitle(QApplication::translate("screen_5", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("screen_5", "Kids Room OverView", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("screen_5", "Room Conditions", 0, QApplication::UnicodeUTF8));
        label_12->setText(QString());
        label_temp->setText(QApplication::translate("screen_5", "21", 0, QApplication::UnicodeUTF8));
        label_14->setText(QApplication::translate("screen_5", "C", 0, QApplication::UnicodeUTF8));
        label_26->setText(QString());
        label_23->setText(QString());
        label_humidity->setText(QApplication::translate("screen_5", "50", 0, QApplication::UnicodeUTF8));
        label_25->setText(QApplication::translate("screen_5", "%", 0, QApplication::UnicodeUTF8));
        label_17->setText(QString());
        label_27->setText(QString());
        label_heat_surface->setText(QApplication::translate("screen_5", "21", 0, QApplication::UnicodeUTF8));
        label_29->setText(QApplication::translate("screen_5", "C", 0, QApplication::UnicodeUTF8));
        label_30->setText(QString());
        label_31->setText(QApplication::translate("screen_5", "Schedule and setpoints", 0, QApplication::UnicodeUTF8));
        toolButton_schedule->setText(QApplication::translate("screen_5", "Program 1", 0, QApplication::UnicodeUTF8));
        toolButton_schedule->setShortcut(QString());
        label_32->setText(QString());
        pushButton_eco_comfort->setText(QApplication::translate("screen_5", "21 c", 0, QApplication::UnicodeUTF8));
        pushButton_humidit_setpoint->setText(QApplication::translate("screen_5", "50 %", 0, QApplication::UnicodeUTF8));
        pushButton_home->setText(QString());
        pushButton_next->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class screen_5: public Ui_screen_5 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREEN_5_H
