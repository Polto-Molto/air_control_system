#ifndef SCREEN_29_H
#define SCREEN_29_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_29;
}

class screen_29 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_29(dataManager*,QWidget *parent = 0);
    ~screen_29();
signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_done_clicked();
      void on_pushButton_standby_clicked();

private:
    Ui::screen_29 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_29_H
