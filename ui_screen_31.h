/********************************************************************************
** Form generated from reading UI file 'screen_31.ui'
**
** Created: Thu Apr 13 00:46:21 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREEN_31_H
#define UI_SCREEN_31_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_screen_31
{
public:
    QGridLayout *gridLayout;
    QFrame *frame_header;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QFrame *frame_body;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_11;
    QPushButton *pushButton_keyboard;
    QSpacerItem *horizontalSpacer_12;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *screen_31)
    {
        if (screen_31->objectName().isEmpty())
            screen_31->setObjectName(QString::fromUtf8("screen_31"));
        screen_31->resize(1024, 600);
        screen_31->setMinimumSize(QSize(1024, 600));
        screen_31->setMaximumSize(QSize(1024, 600));
        gridLayout = new QGridLayout(screen_31);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame_header = new QFrame(screen_31);
        frame_header->setObjectName(QString::fromUtf8("frame_header"));
        frame_header->setMinimumSize(QSize(1024, 50));
        frame_header->setMaximumSize(QSize(1024, 50));
        frame_header->setFrameShape(QFrame::NoFrame);
        frame_header->setFrameShadow(QFrame::Raised);
        frame_header->setLineWidth(0);
        horizontalLayout = new QHBoxLayout(frame_header);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(frame_header);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(14);
        label->setFont(font);

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        gridLayout->addWidget(frame_header, 0, 0, 1, 1);

        frame_body = new QFrame(screen_31);
        frame_body->setObjectName(QString::fromUtf8("frame_body"));
        frame_body->setMinimumSize(QSize(1024, 550));
        frame_body->setMaximumSize(QSize(1024, 550));
        frame_body->setFrameShape(QFrame::NoFrame);
        frame_body->setFrameShadow(QFrame::Raised);
        verticalLayout = new QVBoxLayout(frame_body);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_11 = new QSpacerItem(60, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_11);

        pushButton_keyboard = new QPushButton(frame_body);
        pushButton_keyboard->setObjectName(QString::fromUtf8("pushButton_keyboard"));
        pushButton_keyboard->setMinimumSize(QSize(0, 60));
        pushButton_keyboard->setMaximumSize(QSize(16777215, 60));
        pushButton_keyboard->setAutoFillBackground(false);
        pushButton_keyboard->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"text-align:left;\n"
"font: 75 32pt \"Sans Serif\";\n"
"padding-left:20px;\n"
"\n"
""));
        pushButton_keyboard->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_keyboard);

        horizontalSpacer_12 = new QSpacerItem(60, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_12);


        verticalLayout->addLayout(horizontalLayout_2);

        verticalSpacer = new QSpacerItem(20, 149, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        gridLayout->addWidget(frame_body, 1, 0, 1, 1);


        retranslateUi(screen_31);

        QMetaObject::connectSlotsByName(screen_31);
    } // setupUi

    void retranslateUi(QWidget *screen_31)
    {
        screen_31->setWindowTitle(QApplication::translate("screen_31", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("screen_31", "Rename", 0, QApplication::UnicodeUTF8));
        pushButton_keyboard->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class screen_31: public Ui_screen_31 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREEN_31_H
