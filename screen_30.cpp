#include "screen_30.h"
#include "ui_screen_30.h"

screen_30::screen_30(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_30),
    _datamgr(dm)
{
    ui->setupUi(this);
    ui->label_temp_value->setNum(20);
    ui->label_vmc_value->setNum(20);
}

screen_30::~screen_30()
{
    delete ui;
}

void screen_30::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_30::on_pushButton_done_clicked()
{
    Q_EMIT(touched(29));
}

void screen_30::on_pushButton_temp_up_clicked()
{
    int value = ui->label_temp_value->text().toInt();
    ui->label_temp_value->setNum(value + 1);
}

void screen_30::on_pushButton_temp_down_clicked()
{
    int value = ui->label_temp_value->text().toInt();
    ui->label_temp_value->setNum(value - 1);
}

void screen_30::on_pushButton_vmc_up_clicked()
{
    int value = ui->label_vmc_value->text().toInt();
    ui->label_vmc_value->setNum(value + 1);
}

void screen_30::on_pushButton_vmc_down_clicked()
{
    int value = ui->label_vmc_value->text().toInt();
    ui->label_vmc_value->setNum(value - 1);
}
