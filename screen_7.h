#ifndef SCREEN_7_H
#define SCREEN_7_H

#include <QWidget>
#include <QPushButton>
#include "datamanager.h"

namespace Ui {
class screen_7;
}

class screen_7 : public QWidget
{
    Q_OBJECT

public:
    explicit screen_7(dataManager*,QWidget *parent = 0);
    ~screen_7();
signals:
      void touched(int);

public slots:
    void reload();
    void on_pushButton_home_clicked();
    void on_pushButton_done_clicked();
    void on_pushButton_cancel_clicked();
    void scheduleSelected();
    void setCurrentClima(CLIMA_MODBUS);
private:
    Ui::screen_7 *ui;
    dataManager *_datamgr;
    CLIMA_MODBUS _clima;
    QVector<QPushButton*> buttons;
    QString _schedule;
};

#endif // SCREEN_7_H
