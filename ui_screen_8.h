/********************************************************************************
** Form generated from reading UI file 'screen_8.ui'
**
** Created: Thu Apr 13 00:46:20 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREEN_8_H
#define UI_SCREEN_8_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_screen_8
{
public:
    QVBoxLayout *verticalLayout_7;
    QFrame *frame_header;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QFrame *frame_body;
    QFrame *frame_2;
    QVBoxLayout *verticalLayout;
    QLabel *label_name_2;
    QLabel *label_name_3;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_7;
    QPushButton *pushButton_hours_down;
    QLabel *label_hours_value;
    QPushButton *pushButton_hours_up;
    QSpacerItem *horizontalSpacer_8;
    QLabel *label_name_4;
    QHBoxLayout *horizontalLayout_10;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *pushButton_mins_down;
    QLabel *label_mins_value;
    QPushButton *pushButton_mins_up;
    QSpacerItem *horizontalSpacer_4;
    QFrame *frame;
    QHBoxLayout *horizontalLayout_7;
    QSpacerItem *horizontalSpacer_9;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_3;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_name;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *pushButton_temp_down;
    QLabel *label_temp_value;
    QPushButton *pushButton_temp_up;
    QSpacerItem *verticalSpacer;
    QSpacerItem *horizontalSpacer_10;
    QFrame *frame_footer;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_15;
    QPushButton *pushButton_cancel;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *pushButton_home;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *pushButton_done;
    QSpacerItem *horizontalSpacer_16;

    void setupUi(QWidget *screen_8)
    {
        if (screen_8->objectName().isEmpty())
            screen_8->setObjectName(QString::fromUtf8("screen_8"));
        screen_8->resize(1024, 600);
        screen_8->setMinimumSize(QSize(1024, 600));
        screen_8->setMaximumSize(QSize(1024, 600));
        verticalLayout_7 = new QVBoxLayout(screen_8);
        verticalLayout_7->setSpacing(0);
        verticalLayout_7->setContentsMargins(0, 0, 0, 0);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        frame_header = new QFrame(screen_8);
        frame_header->setObjectName(QString::fromUtf8("frame_header"));
        frame_header->setMinimumSize(QSize(1024, 50));
        frame_header->setMaximumSize(QSize(1024, 50));
        frame_header->setFrameShape(QFrame::StyledPanel);
        frame_header->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_header);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(frame_header);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(14);
        label->setFont(font);

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout_7->addWidget(frame_header);

        frame_body = new QFrame(screen_8);
        frame_body->setObjectName(QString::fromUtf8("frame_body"));
        frame_body->setMinimumSize(QSize(1024, 470));
        frame_body->setMaximumSize(QSize(1024, 470));
        frame_body->setFrameShape(QFrame::StyledPanel);
        frame_body->setFrameShadow(QFrame::Raised);
        frame_2 = new QFrame(frame_body);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setGeometry(QRect(472, 32, 420, 280));
        frame_2->setMinimumSize(QSize(420, 280));
        frame_2->setMaximumSize(QSize(420, 280));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        verticalLayout = new QVBoxLayout(frame_2);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label_name_2 = new QLabel(frame_2);
        label_name_2->setObjectName(QString::fromUtf8("label_name_2"));
        label_name_2->setStyleSheet(QString::fromUtf8("font: 18pt Bold \"Sans Serif\";\n"
"color: black;"));
        label_name_2->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_name_2);

        label_name_3 = new QLabel(frame_2);
        label_name_3->setObjectName(QString::fromUtf8("label_name_3"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("12 Sans Serif"));
        font1.setPointSize(11);
        font1.setBold(false);
        font1.setItalic(false);
        font1.setWeight(50);
        label_name_3->setFont(font1);
        label_name_3->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_name_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_7);

        pushButton_hours_down = new QPushButton(frame_2);
        pushButton_hours_down->setObjectName(QString::fromUtf8("pushButton_hours_down"));
        pushButton_hours_down->setMinimumSize(QSize(50, 50));
        pushButton_hours_down->setMaximumSize(QSize(50, 50));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/images/Icons/minus.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_hours_down->setIcon(icon);
        pushButton_hours_down->setIconSize(QSize(50, 50));
        pushButton_hours_down->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_hours_down);

        label_hours_value = new QLabel(frame_2);
        label_hours_value->setObjectName(QString::fromUtf8("label_hours_value"));
        label_hours_value->setMinimumSize(QSize(110, 70));
        label_hours_value->setMaximumSize(QSize(110, 70));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Sans Serif"));
        font2.setPointSize(32);
        label_hours_value->setFont(font2);
        label_hours_value->setFrameShape(QFrame::Box);
        label_hours_value->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(label_hours_value);

        pushButton_hours_up = new QPushButton(frame_2);
        pushButton_hours_up->setObjectName(QString::fromUtf8("pushButton_hours_up"));
        pushButton_hours_up->setMinimumSize(QSize(50, 50));
        pushButton_hours_up->setMaximumSize(QSize(50, 50));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/images/Icons/Plus.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_hours_up->setIcon(icon1);
        pushButton_hours_up->setIconSize(QSize(50, 50));
        pushButton_hours_up->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_hours_up);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_8);


        verticalLayout->addLayout(horizontalLayout_2);

        label_name_4 = new QLabel(frame_2);
        label_name_4->setObjectName(QString::fromUtf8("label_name_4"));
        label_name_4->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_name_4);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_3);

        pushButton_mins_down = new QPushButton(frame_2);
        pushButton_mins_down->setObjectName(QString::fromUtf8("pushButton_mins_down"));
        pushButton_mins_down->setMinimumSize(QSize(50, 50));
        pushButton_mins_down->setMaximumSize(QSize(50, 50));
        pushButton_mins_down->setIcon(icon);
        pushButton_mins_down->setIconSize(QSize(50, 50));
        pushButton_mins_down->setFlat(true);

        horizontalLayout_10->addWidget(pushButton_mins_down);

        label_mins_value = new QLabel(frame_2);
        label_mins_value->setObjectName(QString::fromUtf8("label_mins_value"));
        label_mins_value->setMinimumSize(QSize(110, 70));
        label_mins_value->setMaximumSize(QSize(110, 70));
        label_mins_value->setFont(font2);
        label_mins_value->setFrameShape(QFrame::Box);
        label_mins_value->setAlignment(Qt::AlignCenter);

        horizontalLayout_10->addWidget(label_mins_value);

        pushButton_mins_up = new QPushButton(frame_2);
        pushButton_mins_up->setObjectName(QString::fromUtf8("pushButton_mins_up"));
        pushButton_mins_up->setMinimumSize(QSize(50, 50));
        pushButton_mins_up->setMaximumSize(QSize(50, 50));
        pushButton_mins_up->setIcon(icon1);
        pushButton_mins_up->setIconSize(QSize(50, 50));
        pushButton_mins_up->setFlat(true);

        horizontalLayout_10->addWidget(pushButton_mins_up);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_4);


        verticalLayout->addLayout(horizontalLayout_10);

        frame = new QFrame(frame_body);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setGeometry(QRect(32, 32, 420, 280));
        frame->setMinimumSize(QSize(420, 280));
        frame->setMaximumSize(QSize(420, 280));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout_7 = new QHBoxLayout(frame);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalSpacer_9 = new QSpacerItem(68, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_9);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_3 = new QLabel(frame);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/hand.png")));
        label_3->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_3);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_name = new QLabel(frame);
        label_name->setObjectName(QString::fromUtf8("label_name"));
        label_name->setStyleSheet(QString::fromUtf8("font: 18pt Bold \"Sans Serif\";\n"
"color: black;"));
        label_name->setAlignment(Qt::AlignCenter);

        horizontalLayout_6->addWidget(label_name);


        verticalLayout_2->addLayout(horizontalLayout_6);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        pushButton_temp_down = new QPushButton(frame);
        pushButton_temp_down->setObjectName(QString::fromUtf8("pushButton_temp_down"));
        pushButton_temp_down->setMinimumSize(QSize(50, 50));
        pushButton_temp_down->setMaximumSize(QSize(50, 50));
        pushButton_temp_down->setIcon(icon);
        pushButton_temp_down->setIconSize(QSize(50, 50));
        pushButton_temp_down->setFlat(true);

        horizontalLayout_5->addWidget(pushButton_temp_down);

        label_temp_value = new QLabel(frame);
        label_temp_value->setObjectName(QString::fromUtf8("label_temp_value"));
        label_temp_value->setMinimumSize(QSize(130, 85));
        label_temp_value->setMaximumSize(QSize(130, 85));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Sans Serif"));
        font3.setPointSize(42);
        label_temp_value->setFont(font3);
        label_temp_value->setFrameShape(QFrame::Box);
        label_temp_value->setAlignment(Qt::AlignCenter);

        horizontalLayout_5->addWidget(label_temp_value);

        pushButton_temp_up = new QPushButton(frame);
        pushButton_temp_up->setObjectName(QString::fromUtf8("pushButton_temp_up"));
        pushButton_temp_up->setMinimumSize(QSize(50, 50));
        pushButton_temp_up->setMaximumSize(QSize(50, 50));
        pushButton_temp_up->setIcon(icon1);
        pushButton_temp_up->setIconSize(QSize(50, 50));
        pushButton_temp_up->setFlat(true);

        horizontalLayout_5->addWidget(pushButton_temp_up);


        verticalLayout_2->addLayout(horizontalLayout_5);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_2->addItem(verticalSpacer);


        horizontalLayout_7->addLayout(verticalLayout_2);

        horizontalSpacer_10 = new QSpacerItem(68, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_10);


        verticalLayout_7->addWidget(frame_body);

        frame_footer = new QFrame(screen_8);
        frame_footer->setObjectName(QString::fromUtf8("frame_footer"));
        frame_footer->setMinimumSize(QSize(1024, 80));
        frame_footer->setMaximumSize(QSize(1024, 80));
        frame_footer->setFrameShape(QFrame::StyledPanel);
        frame_footer->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_footer);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_15 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_15);

        pushButton_cancel = new QPushButton(frame_footer);
        pushButton_cancel->setObjectName(QString::fromUtf8("pushButton_cancel"));
        pushButton_cancel->setMinimumSize(QSize(50, 50));
        pushButton_cancel->setMaximumSize(QSize(50, 50));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/images/Icons/Cross.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_cancel->setIcon(icon2);
        pushButton_cancel->setIconSize(QSize(50, 50));
        pushButton_cancel->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_cancel);

        horizontalSpacer_5 = new QSpacerItem(353, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);

        pushButton_home = new QPushButton(frame_footer);
        pushButton_home->setObjectName(QString::fromUtf8("pushButton_home"));
        pushButton_home->setMinimumSize(QSize(50, 50));
        pushButton_home->setMaximumSize(QSize(50, 50));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/images/Icons/home.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_home->setIcon(icon3);
        pushButton_home->setIconSize(QSize(50, 50));
        pushButton_home->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_home);

        horizontalSpacer_6 = new QSpacerItem(353, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);

        pushButton_done = new QPushButton(frame_footer);
        pushButton_done->setObjectName(QString::fromUtf8("pushButton_done"));
        pushButton_done->setMinimumSize(QSize(50, 50));
        pushButton_done->setMaximumSize(QSize(50, 50));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/images/Icons/check.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_done->setIcon(icon4);
        pushButton_done->setIconSize(QSize(50, 50));
        pushButton_done->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_done);

        horizontalSpacer_16 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_16);


        verticalLayout_7->addWidget(frame_footer);


        retranslateUi(screen_8);

        QMetaObject::connectSlotsByName(screen_8);
    } // setupUi

    void retranslateUi(QWidget *screen_8)
    {
        screen_8->setWindowTitle(QApplication::translate("screen_8", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("screen_8", "Manual temperature settings", 0, QApplication::UnicodeUTF8));
        label_name_2->setText(QApplication::translate("screen_8", "Keep this temp", 0, QApplication::UnicodeUTF8));
        label_name_3->setText(QApplication::translate("screen_8", "hours", 0, QApplication::UnicodeUTF8));
        pushButton_hours_down->setText(QString());
        label_hours_value->setText(QString());
        pushButton_hours_up->setText(QString());
        label_name_4->setText(QApplication::translate("screen_8", "mins", 0, QApplication::UnicodeUTF8));
        pushButton_mins_down->setText(QString());
        label_mins_value->setText(QString());
        pushButton_mins_up->setText(QString());
        label_3->setText(QString());
        label_name->setText(QApplication::translate("screen_8", "Keep this temp", 0, QApplication::UnicodeUTF8));
        pushButton_temp_down->setText(QString());
        label_temp_value->setText(QString());
        pushButton_temp_up->setText(QString());
        pushButton_home->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class screen_8: public Ui_screen_8 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREEN_8_H
