#include "screens_manager.h"
#include <QApplication>
#include "configmanager.h"
#include "datamanager.h"
#include "modbus_manager.h"
#include <QFile>
#include <QThread>
#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    dataManager* data_manager = new dataManager;
    modbus_manager* mbus_manager = new modbus_manager(data_manager);

    Qt4Modbus *_modbus = new Qt4Modbus("/dev/ttyS1"
                           ,Qt4Modbus::PORT_TYPE_485
                           ,9600
                           ,'N'
                           ,8
                           ,1);

    _modbus->setByteTimeOut(0,50000);
    _modbus->setResponseTimeOut(0,1000000);
    _modbus->modbusConnect();
    if(_modbus > 0)
        mbus_manager->setModbusRtuNetwork(_modbus);

    QThread *th_1 = new QThread;
    mbus_manager->moveToThread(th_1);
    qRegisterMetaType<CLIMA_MODBUS>("CLIMA_MODBUS");
    th_1->start();

    QObject::connect(data_manager,SIGNAL(climaConfigDataTableUpdated())
                     ,mbus_manager,SLOT(setClimaTable()));


    configManager *cfmgr = new configManager(data_manager);

    cfmgr->setConfigFiles("./main.conf","./schedule.conf","./clima.conf","./air.conf","skudo.conf");
    cfmgr->loadMainConfig();
    cfmgr->loadScheduleConfig();
    cfmgr->loadClimaConfig();

    QFile file("://qss/qss/main.qss");
    file.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(file.readAll());
    a.setStyleSheet(styleSheet);


    //QThread *th_2 = new QThread;

    screens_manager sm(data_manager,mbus_manager,cfmgr);
    return a.exec();
}
