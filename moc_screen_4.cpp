/****************************************************************************
** Meta object code from reading C++ file 'screen_4.h'
**
** Created: Tue Apr 25 16:09:25 2017
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "screen_4.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'screen_4.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_screen_4[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x05,
      38,    9,    9,    9, 0x05,

 // slots: signature, parameters, type, tag, flags
      51,    9,    9,    9, 0x0a,
      60,    9,    9,    9, 0x0a,
      89,    9,    9,    9, 0x0a,
     118,    9,    9,    9, 0x0a,
     147,    9,    9,    9, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_screen_4[] = {
    "screen_4\0\0selectedClima(CLIMA_MODBUS)\0"
    "touched(int)\0reload()\0"
    "on_pushButton_home_clicked()\0"
    "on_pushButton_next_clicked()\0"
    "on_pushButton_back_clicked()\0showPage()\0"
};

void screen_4::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        screen_4 *_t = static_cast<screen_4 *>(_o);
        switch (_id) {
        case 0: _t->selectedClima((*reinterpret_cast< CLIMA_MODBUS(*)>(_a[1]))); break;
        case 1: _t->touched((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->reload(); break;
        case 3: _t->on_pushButton_home_clicked(); break;
        case 4: _t->on_pushButton_next_clicked(); break;
        case 5: _t->on_pushButton_back_clicked(); break;
        case 6: _t->showPage(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData screen_4::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject screen_4::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_screen_4,
      qt_meta_data_screen_4, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &screen_4::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *screen_4::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *screen_4::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_screen_4))
        return static_cast<void*>(const_cast< screen_4*>(this));
    return QWidget::qt_metacast(_clname);
}

int screen_4::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void screen_4::selectedClima(CLIMA_MODBUS _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void screen_4::touched(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
