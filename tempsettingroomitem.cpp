#include "tempsettingroomitem.h"
#include "ui_tempsettingroomitem.h"
#include <QDebug>

tempSettingRoomItem::tempSettingRoomItem(CLIMA_MODBUS clima,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::tempSettingRoomItem),
    _clima(clima)
{
    ui->setupUi(this);
    button = new QPushButton(this);
    button->setGeometry(this->geometry());
    button->setStyleSheet("background-color:rgba(0,0,0,0);");
    ui->label_temp->setNum(_clima.air_temp);
    ui->label_temp_point->setNum(_clima.summer_comfort_temp_point);
    ui->label_room_name->setText(_clima.name);
    ui->label_room_number->setNum(_clima.room_number);
    connect(button,SIGNAL(clicked()),this,SLOT(buttonClicked()));
}

tempSettingRoomItem::~tempSettingRoomItem()
{
    delete ui;
}

void tempSettingRoomItem::buttonClicked()
{
    Q_EMIT(climaClicked(_clima));
}
