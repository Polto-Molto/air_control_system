#ifndef SCREEN_33_H
#define SCREEN_33_H

#include <QWidget>
#include "datamanager.h"
#include <QPushButton>

namespace Ui {
class screen_33;
}

class screen_33 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_33(dataManager*,QWidget *parent = 0);
    ~screen_33();

public slots:
    void on_pushButton_keyboard_clicked();
    QString password();

signals:
      void touched(int);
      void showkeyboard(QPushButton*);

private:
    Ui::screen_33 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_33_H
