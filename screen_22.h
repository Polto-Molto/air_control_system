#ifndef SCREEN_22_H
#define SCREEN_22_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_22;
}

class screen_22 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_22(dataManager*,QWidget *parent = 0);
    ~screen_22();

signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_cancel_clicked();

      void on_pushButton_hydronic_clicked();
      void on_pushButton_modbus_clicked();
      void on_pushButton_netwrok_clicked();
      void on_pushButton_radiant_clicked();
      void on_pushButton_reset_clicked();
      void on_pushButton_ventilation_clicked();

private:
    Ui::screen_22 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_22_H
