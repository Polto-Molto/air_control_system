#include "screen_50.h"
#include "ui_screen_50.h"

screen_50::screen_50(dataManager *dm, modbus_manager *m,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_50),
    _datamgr(dm),
    _mbus(m)
{
    ui->setupUi(this);
}

screen_50::~screen_50()
{
    delete ui;
}

void screen_50::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_50::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(22));
}

void screen_50::on_pushButton_done_clicked()
{
    Q_EMIT(touched(22));
}


