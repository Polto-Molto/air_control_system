#ifndef SCREEN_45_H
#define SCREEN_45_H

#include <QWidget>
#include "datamanager.h"
#include "datamanager.h"

namespace Ui {
class screen_45;
}

class screen_45 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_45(dataManager *,QWidget *parent = 0);
    ~screen_45();
signals:
      void touched(int);
      void done(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_back_clicked();
      void on_pushButton_done_clicked();

      void on_pushButton_num_down_clicked();
      void on_pushButton_num_up_clicked();

private:
    Ui::screen_45 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_45_H
