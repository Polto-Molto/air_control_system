/********************************************************************************
** Form generated from reading UI file 'notifylabel.ui'
**
** Created: Thu Apr 13 00:46:20 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NOTIFYLABEL_H
#define UI_NOTIFYLABEL_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_NotifyLabel
{
public:
    QGridLayout *gridLayout;
    QFrame *frame;
    QHBoxLayout *horizontalLayout;
    QLabel *label_logo;
    QVBoxLayout *verticalLayout;
    QLabel *label_state;
    QLabel *label_date;
    QLabel *label_text;
    QSpacerItem *horizontalSpacer;

    void setupUi(QWidget *NotifyLabel)
    {
        if (NotifyLabel->objectName().isEmpty())
            NotifyLabel->setObjectName(QString::fromUtf8("NotifyLabel"));
        NotifyLabel->resize(575, 80);
        NotifyLabel->setMinimumSize(QSize(200, 80));
        NotifyLabel->setMaximumSize(QSize(1024, 80));
        gridLayout = new QGridLayout(NotifyLabel);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame = new QFrame(NotifyLabel);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setFrameShape(QFrame::Box);
        frame->setFrameShadow(QFrame::Plain);
        frame->setLineWidth(1);
        horizontalLayout = new QHBoxLayout(frame);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_logo = new QLabel(frame);
        label_logo->setObjectName(QString::fromUtf8("label_logo"));
        label_logo->setMinimumSize(QSize(50, 50));
        label_logo->setMaximumSize(QSize(50, 50));
        label_logo->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout->addWidget(label_logo);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label_state = new QLabel(frame);
        label_state->setObjectName(QString::fromUtf8("label_state"));
        label_state->setMinimumSize(QSize(80, 25));
        label_state->setMaximumSize(QSize(80, 25));

        verticalLayout->addWidget(label_state);

        label_date = new QLabel(frame);
        label_date->setObjectName(QString::fromUtf8("label_date"));
        label_date->setMinimumSize(QSize(80, 25));
        label_date->setMaximumSize(QSize(80, 25));

        verticalLayout->addWidget(label_date);


        horizontalLayout->addLayout(verticalLayout);

        label_text = new QLabel(frame);
        label_text->setObjectName(QString::fromUtf8("label_text"));
        label_text->setMinimumSize(QSize(100, 50));
        label_text->setMaximumSize(QSize(16777215, 50));
        label_text->setFrameShape(QFrame::NoFrame);
        label_text->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label_text->setMargin(20);

        horizontalLayout->addWidget(label_text);

        horizontalSpacer = new QSpacerItem(304, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        gridLayout->addWidget(frame, 0, 0, 1, 1);


        retranslateUi(NotifyLabel);

        QMetaObject::connectSlotsByName(NotifyLabel);
    } // setupUi

    void retranslateUi(QWidget *NotifyLabel)
    {
        NotifyLabel->setWindowTitle(QApplication::translate("NotifyLabel", "Form", 0, QApplication::UnicodeUTF8));
        label_logo->setText(QString());
        label_state->setText(QString());
        label_date->setText(QString());
        label_text->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class NotifyLabel: public Ui_NotifyLabel {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NOTIFYLABEL_H
