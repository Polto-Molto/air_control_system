/********************************************************************************
** Form generated from reading UI file 'screen_2.ui'
**
** Created: Thu Apr 13 00:46:20 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREEN_2_H
#define UI_SCREEN_2_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_screen_2
{
public:
    QGridLayout *gridLayout;
    QFrame *frame_footer;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *pushButton_schedule;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *pushButton_settings;
    QSpacerItem *horizontalSpacer_7;
    QPushButton *pushButton_notifications;
    QSpacerItem *horizontalSpacer_8;
    QPushButton *pushButton_season;
    QSpacerItem *horizontalSpacer_9;
    QFrame *frame_body;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QToolButton *toolButton_temp;
    QSpacerItem *horizontalSpacer_2;
    QToolButton *toolButton_vent;
    QSpacerItem *horizontalSpacer_3;
    QSpacerItem *verticalSpacer_2;

    void setupUi(QWidget *screen_2)
    {
        if (screen_2->objectName().isEmpty())
            screen_2->setObjectName(QString::fromUtf8("screen_2"));
        screen_2->resize(1024, 600);
        screen_2->setMinimumSize(QSize(1024, 600));
        screen_2->setMaximumSize(QSize(1024, 600));
        gridLayout = new QGridLayout(screen_2);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setVerticalSpacing(0);
        frame_footer = new QFrame(screen_2);
        frame_footer->setObjectName(QString::fromUtf8("frame_footer"));
        frame_footer->setMinimumSize(QSize(1024, 80));
        frame_footer->setMaximumSize(QSize(1024, 80));
        frame_footer->setFrameShape(QFrame::StyledPanel);
        frame_footer->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(frame_footer);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_6 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_6);

        pushButton_schedule = new QPushButton(frame_footer);
        pushButton_schedule->setObjectName(QString::fromUtf8("pushButton_schedule"));
        pushButton_schedule->setMinimumSize(QSize(50, 50));
        pushButton_schedule->setMaximumSize(QSize(220, 50));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(18);
        pushButton_schedule->setFont(font);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/images/Icons/schedule.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_schedule->setIcon(icon);
        pushButton_schedule->setIconSize(QSize(50, 50));
        pushButton_schedule->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_schedule);

        horizontalSpacer_5 = new QSpacerItem(38, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_5);

        pushButton_settings = new QPushButton(frame_footer);
        pushButton_settings->setObjectName(QString::fromUtf8("pushButton_settings"));
        pushButton_settings->setMinimumSize(QSize(50, 50));
        pushButton_settings->setMaximumSize(QSize(220, 50));
        pushButton_settings->setFont(font);
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/images/Icons/settings 3.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_settings->setIcon(icon1);
        pushButton_settings->setIconSize(QSize(50, 50));
        pushButton_settings->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_settings);

        horizontalSpacer_7 = new QSpacerItem(39, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_7);

        pushButton_notifications = new QPushButton(frame_footer);
        pushButton_notifications->setObjectName(QString::fromUtf8("pushButton_notifications"));
        pushButton_notifications->setMinimumSize(QSize(50, 50));
        pushButton_notifications->setMaximumSize(QSize(220, 50));
        pushButton_notifications->setFont(font);
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/images/Icons/notification.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_notifications->setIcon(icon2);
        pushButton_notifications->setIconSize(QSize(50, 50));
        pushButton_notifications->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_notifications);

        horizontalSpacer_8 = new QSpacerItem(38, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_8);

        pushButton_season = new QPushButton(frame_footer);
        pushButton_season->setObjectName(QString::fromUtf8("pushButton_season"));
        pushButton_season->setMinimumSize(QSize(50, 50));
        pushButton_season->setMaximumSize(QSize(220, 50));
        pushButton_season->setFont(font);
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/images/Icons/season.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_season->setIcon(icon3);
        pushButton_season->setIconSize(QSize(50, 50));
        pushButton_season->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_season);

        horizontalSpacer_9 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_9);


        gridLayout->addWidget(frame_footer, 1, 0, 1, 1);

        frame_body = new QFrame(screen_2);
        frame_body->setObjectName(QString::fromUtf8("frame_body"));
        frame_body->setMinimumSize(QSize(1024, 520));
        frame_body->setMaximumSize(QSize(1024, 520));
        frame_body->setFrameShape(QFrame::StyledPanel);
        frame_body->setFrameShadow(QFrame::Raised);
        verticalLayout = new QVBoxLayout(frame_body);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(frame_body);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMinimumSize(QSize(0, 110));
        label->setMaximumSize(QSize(16777215, 110));
        label->setPixmap(QPixmap(QString::fromUtf8(":/images/images/logo.png")));
        label->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label);

        verticalSpacer = new QSpacerItem(20, 82, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        toolButton_temp = new QToolButton(frame_body);
        toolButton_temp->setObjectName(QString::fromUtf8("toolButton_temp"));
        toolButton_temp->setMinimumSize(QSize(200, 200));
        toolButton_temp->setMaximumSize(QSize(200, 200));
        toolButton_temp->setFont(font);
        toolButton_temp->setStyleSheet(QString::fromUtf8("color: rgb(96, 153, 222);"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/images/Icons/thermometer.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton_temp->setIcon(icon4);
        toolButton_temp->setIconSize(QSize(80, 80));
        toolButton_temp->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        toolButton_temp->setAutoRaise(true);

        horizontalLayout->addWidget(toolButton_temp);

        horizontalSpacer_2 = new QSpacerItem(100, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        toolButton_vent = new QToolButton(frame_body);
        toolButton_vent->setObjectName(QString::fromUtf8("toolButton_vent"));
        toolButton_vent->setMinimumSize(QSize(200, 200));
        toolButton_vent->setMaximumSize(QSize(200, 200));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Sans Serif"));
        font1.setPointSize(18);
        font1.setKerning(true);
        font1.setStyleStrategy(QFont::PreferDefault);
        toolButton_vent->setFont(font1);
        toolButton_vent->setFocusPolicy(Qt::TabFocus);
        toolButton_vent->setAcceptDrops(false);
        toolButton_vent->setLayoutDirection(Qt::LeftToRight);
        toolButton_vent->setAutoFillBackground(false);
        toolButton_vent->setStyleSheet(QString::fromUtf8("color: rgb(96, 153, 222);"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/images/images/Icons/wave.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton_vent->setIcon(icon5);
        toolButton_vent->setIconSize(QSize(80, 80));
        toolButton_vent->setPopupMode(QToolButton::DelayedPopup);
        toolButton_vent->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        toolButton_vent->setAutoRaise(true);

        horizontalLayout->addWidget(toolButton_vent);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);


        verticalLayout->addLayout(horizontalLayout);

        verticalSpacer_2 = new QSpacerItem(20, 82, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);


        gridLayout->addWidget(frame_body, 0, 0, 1, 1);


        retranslateUi(screen_2);

        QMetaObject::connectSlotsByName(screen_2);
    } // setupUi

    void retranslateUi(QWidget *screen_2)
    {
        screen_2->setWindowTitle(QApplication::translate("screen_2", "Form", 0, QApplication::UnicodeUTF8));
        pushButton_schedule->setText(QApplication::translate("screen_2", "Schedule", 0, QApplication::UnicodeUTF8));
        pushButton_settings->setText(QApplication::translate("screen_2", "Settings", 0, QApplication::UnicodeUTF8));
        pushButton_notifications->setText(QApplication::translate("screen_2", "Notifications", 0, QApplication::UnicodeUTF8));
        pushButton_season->setText(QApplication::translate("screen_2", "Season", 0, QApplication::UnicodeUTF8));
        label->setText(QString());
        toolButton_temp->setText(QApplication::translate("screen_2", "Temperature", 0, QApplication::UnicodeUTF8));
        toolButton_vent->setText(QApplication::translate("screen_2", "Ventilation", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class screen_2: public Ui_screen_2 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREEN_2_H
