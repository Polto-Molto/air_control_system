#include "screen_3.h"
#include "ui_screen_3.h"
#include "notifylabel.h"

screen_3::screen_3(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_3),
    _datamgr(dm)
{
    ui->setupUi(this);
    scroll = new VScrollWidgets(1000,10,10,1,this);
    ui->gridLayout->addWidget(scroll);
    for(int i=1 ; i < 10 ;i ++)
    {
        NotifyLabel *lab = new NotifyLabel(950,"test",0,this);
        scroll->addWidget(lab);
    }
}

screen_3::~screen_3()
{
    delete ui;
}

void screen_3::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_3::on_pushButton_next_clicked()
{

}

void screen_3::on_pushButton_back_clicked()
{

}
