#include "screen_31.h"
#include "ui_screen_31.h"

screen_31::screen_31(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_31),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_31::~screen_31()
{
    delete ui;
}

void screen_31::on_pushButton_keyboard_clicked()
{
    Q_EMIT(showkeyboard(ui->pushButton_keyboard));

}
