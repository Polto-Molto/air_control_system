#ifndef SCREEN_14_H
#define SCREEN_14_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_14;
}

class screen_14 : public QWidget
{
    Q_OBJECT

public:
    explicit screen_14(dataManager*,QWidget *parent = 0);
    ~screen_14();
signals:
      void touched(int);

public slots:
    void on_pushButton_home_clicked();
    void on_pushButton_done_clicked();
    void on_pushButton_cancel_clicked();
    void on_pushButton_touch_clicked();

private:
    Ui::screen_14 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_14_H
