#ifndef SCREEN_1_H
#define SCREEN_1_H

#include <QWidget>
#include "datamanager.h"
#include "modbus_manager.h"

namespace Ui {
class screen_1;
}

class screen_1 : public QWidget
{
    Q_OBJECT

public:
    explicit screen_1(dataManager*,modbus_manager *,QWidget *parent = 0);
    ~screen_1();

public slots:
    void reload();
    void on_pushButton_home_clicked();
    void updateData(CLIMA_MODBUS);
    void closeApplication();

signals:
      void touched(int);
      void closeApp();

protected:
  //    bool event(QEvent *);

private:
    Ui::screen_1 *ui;
    dataManager *_datamgr;
    modbus_manager *_mbus;
};

#endif // SCREEN_1_H
