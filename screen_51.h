#ifndef SCREEN_51_H
#define SCREEN_51_H

#include <QWidget>
#include <QPushButton>
#include "datamanager.h"
#include "modbus_manager.h"

namespace Ui {
class screen_51;
}

class screen_51 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_51(dataManager*,modbus_manager *,QWidget *parent = 0);
    ~screen_51();
signals:
      void touched(int);
      void showkeypad(QPushButton*);

public slots:

      void on_pushButton_home_clicked();
      void on_pushButton_cancel_clicked();
      void on_pushButton_done_clicked();
      void on_pushButton_num_down_clicked();
      void on_pushButton_num_up_clicked();
      void on_pushButton_value_clicked();

      void setCurrentClima(CLIMA_MODBUS);


private:
    Ui::screen_51 *ui;
    dataManager *_datamgr;
    modbus_manager *_mbus;
    CLIMA_MODBUS _clima;
};

#endif // SCREEN_51_H
