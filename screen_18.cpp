#include "screen_18.h"
#include "ui_screen_18.h"

screen_18::screen_18(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_18),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_18::~screen_18()
{
    delete ui;
}

void screen_18::on_pushButton_quit_clicked()
{
    Q_EMIT(touched(19));
}

void screen_18::on_pushButton_next_day_clicked()
{
    Q_EMIT(touched(17));
}

void screen_18::on_pushButton_next_save_clicked()
{
    Q_EMIT(touched(17));
}
