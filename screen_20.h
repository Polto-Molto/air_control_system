#ifndef SCREEN_20_H
#define SCREEN_20_H

#include <QWidget>
#include "datamanager.h"
#include <QPushButton>

namespace Ui {
class screen_20;
}

class screen_20 : public QWidget
{
    Q_OBJECT

public:
    explicit screen_20(dataManager*,QWidget *parent = 0);
    ~screen_20();

public slots:
    void on_pushButton_keyboard_clicked();
    void setTextEdit(QString);
    QString scheduleName();

signals:
      void touched(int);
      void showkeyboard(QPushButton*);

private:
    Ui::screen_20 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_20_H
