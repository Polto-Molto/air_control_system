/********************************************************************************
** Form generated from reading UI file 'screen_37.ui'
**
** Created: Thu Apr 13 00:46:21 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREEN_37_H
#define UI_SCREEN_37_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_screen_37
{
public:
    QGridLayout *gridLayout_3;
    QFrame *frame_header;
    QHBoxLayout *horizontalLayout_12;
    QSpacerItem *horizontalSpacer_27;
    QLabel *label_13;
    QSpacerItem *horizontalSpacer_28;
    QFrame *frame_body;
    QGridLayout *gridLayout_4;
    QVBoxLayout *verticalLayout_10;
    QSpacerItem *verticalSpacer_7;
    QLabel *label_25;
    QFrame *frame_2;
    QVBoxLayout *verticalLayout_8;
    QToolButton *toolButton_program;
    QToolButton *toolButton_temp;
    QSpacerItem *verticalSpacer_6;
    QFrame *line_2;
    QSpacerItem *horizontalSpacer_22;
    QSpacerItem *horizontalSpacer_21;
    QVBoxLayout *verticalLayout_9;
    QSpacerItem *verticalSpacer_8;
    QLabel *label_9;
    QFrame *frame;
    QGridLayout *gridLayout_2;
    QSpacerItem *horizontalSpacer_20;
    QVBoxLayout *verticalLayout_7;
    QLabel *label_14;
    QLabel *label_23;
    QLabel *label_24;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_12;
    QSpacerItem *horizontalSpacer_17;
    QLabel *label_air_temp_2;
    QLabel *label_17;
    QSpacerItem *horizontalSpacer_18;
    QLabel *label_31;
    QHBoxLayout *horizontalLayout_10;
    QSpacerItem *horizontalSpacer_29;
    QLabel *label_32;
    QSpacerItem *horizontalSpacer_30;
    QSpacerItem *verticalSpacer_3;
    QSpacerItem *horizontalSpacer_19;
    QSpacerItem *verticalSpacer_4;
    QSpacerItem *verticalSpacer_5;
    QFrame *frame_footer;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_23;
    QPushButton *pushButton_back;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *pushButton_cancel;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *pushButton_home;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *pushButton_touch;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_next;
    QSpacerItem *horizontalSpacer_26;

    void setupUi(QWidget *screen_37)
    {
        if (screen_37->objectName().isEmpty())
            screen_37->setObjectName(QString::fromUtf8("screen_37"));
        screen_37->resize(1024, 600);
        screen_37->setMinimumSize(QSize(1024, 600));
        screen_37->setMaximumSize(QSize(1024, 600));
        gridLayout_3 = new QGridLayout(screen_37);
        gridLayout_3->setSpacing(0);
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        frame_header = new QFrame(screen_37);
        frame_header->setObjectName(QString::fromUtf8("frame_header"));
        frame_header->setMinimumSize(QSize(1024, 50));
        frame_header->setMaximumSize(QSize(1024, 50));
        frame_header->setFrameShape(QFrame::StyledPanel);
        frame_header->setFrameShadow(QFrame::Raised);
        horizontalLayout_12 = new QHBoxLayout(frame_header);
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        horizontalSpacer_27 = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_12->addItem(horizontalSpacer_27);

        label_13 = new QLabel(frame_header);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(14);
        label_13->setFont(font);

        horizontalLayout_12->addWidget(label_13);

        horizontalSpacer_28 = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_12->addItem(horizontalSpacer_28);


        gridLayout_3->addWidget(frame_header, 0, 0, 1, 1);

        frame_body = new QFrame(screen_37);
        frame_body->setObjectName(QString::fromUtf8("frame_body"));
        frame_body->setMinimumSize(QSize(1024, 470));
        frame_body->setMaximumSize(QSize(1024, 470));
        frame_body->setFrameShape(QFrame::StyledPanel);
        frame_body->setFrameShadow(QFrame::Raised);
        gridLayout_4 = new QGridLayout(frame_body);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setHorizontalSpacing(20);
        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        verticalSpacer_7 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_10->addItem(verticalSpacer_7);

        label_25 = new QLabel(frame_body);
        label_25->setObjectName(QString::fromUtf8("label_25"));
        label_25->setMinimumSize(QSize(350, 60));
        label_25->setMaximumSize(QSize(16777215, 60));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Sans Serif"));
        font1.setPointSize(18);
        label_25->setFont(font1);
        label_25->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        verticalLayout_10->addWidget(label_25);

        frame_2 = new QFrame(frame_body);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setMinimumSize(QSize(420, 280));
        frame_2->setMaximumSize(QSize(420, 280));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        verticalLayout_8 = new QVBoxLayout(frame_2);
        verticalLayout_8->setSpacing(10);
        verticalLayout_8->setContentsMargins(0, 0, 0, 0);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        toolButton_program = new QToolButton(frame_2);
        toolButton_program->setObjectName(QString::fromUtf8("toolButton_program"));
        toolButton_program->setMinimumSize(QSize(420, 135));
        toolButton_program->setMaximumSize(QSize(420, 135));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Sans Serif"));
        font2.setPointSize(22);
        toolButton_program->setFont(font2);
        toolButton_program->setStyleSheet(QString::fromUtf8("border:1px solid gray;\n"
""));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/images/Icons/program 3.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton_program->setIcon(icon);
        toolButton_program->setIconSize(QSize(70, 70));
        toolButton_program->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);

        verticalLayout_8->addWidget(toolButton_program);

        toolButton_temp = new QToolButton(frame_2);
        toolButton_temp->setObjectName(QString::fromUtf8("toolButton_temp"));
        toolButton_temp->setMinimumSize(QSize(420, 135));
        toolButton_temp->setMaximumSize(QSize(420, 135));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Sans Serif"));
        font3.setPointSize(32);
        toolButton_temp->setFont(font3);
        toolButton_temp->setStyleSheet(QString::fromUtf8("border:1px solid gray;\n"
""));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/images/Icons/comfrot.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton_temp->setIcon(icon1);
        toolButton_temp->setIconSize(QSize(70, 70));
        toolButton_temp->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);

        verticalLayout_8->addWidget(toolButton_temp);


        verticalLayout_10->addWidget(frame_2);

        verticalSpacer_6 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_10->addItem(verticalSpacer_6);


        gridLayout_4->addLayout(verticalLayout_10, 0, 3, 1, 1);

        line_2 = new QFrame(frame_body);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setMinimumSize(QSize(0, 450));
        line_2->setMaximumSize(QSize(16777215, 450));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);

        gridLayout_4->addWidget(line_2, 0, 2, 1, 1);

        horizontalSpacer_22 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_22, 0, 4, 1, 1);

        horizontalSpacer_21 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_21, 0, 0, 1, 1);

        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        verticalSpacer_8 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_9->addItem(verticalSpacer_8);

        label_9 = new QLabel(frame_body);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setMinimumSize(QSize(350, 60));
        label_9->setMaximumSize(QSize(16777215, 60));
        label_9->setFont(font1);
        label_9->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        verticalLayout_9->addWidget(label_9);

        frame = new QFrame(frame_body);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setMinimumSize(QSize(420, 280));
        frame->setMaximumSize(QSize(420, 280));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        gridLayout_2 = new QGridLayout(frame);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        horizontalSpacer_20 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_20, 1, 3, 1, 1);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        label_14 = new QLabel(frame);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setMinimumSize(QSize(50, 50));
        label_14->setMaximumSize(QSize(50, 50));
        label_14->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/fire.png")));
        label_14->setScaledContents(true);

        verticalLayout_7->addWidget(label_14);

        label_23 = new QLabel(frame);
        label_23->setObjectName(QString::fromUtf8("label_23"));
        label_23->setMinimumSize(QSize(50, 50));
        label_23->setMaximumSize(QSize(50, 50));
        label_23->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/flake.png")));
        label_23->setScaledContents(true);

        verticalLayout_7->addWidget(label_23);

        label_24 = new QLabel(frame);
        label_24->setObjectName(QString::fromUtf8("label_24"));
        label_24->setMinimumSize(QSize(50, 50));
        label_24->setMaximumSize(QSize(50, 50));
        label_24->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/drop.png")));
        label_24->setScaledContents(true);

        verticalLayout_7->addWidget(label_24);


        gridLayout_2->addLayout(verticalLayout_7, 1, 2, 1, 1);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_12 = new QLabel(frame);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setMinimumSize(QSize(25, 50));
        label_12->setMaximumSize(QSize(25, 50));
        label_12->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/Thermo-Meter-.png")));
        label_12->setScaledContents(true);

        horizontalLayout_7->addWidget(label_12);

        horizontalSpacer_17 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_17);

        label_air_temp_2 = new QLabel(frame);
        label_air_temp_2->setObjectName(QString::fromUtf8("label_air_temp_2"));
        label_air_temp_2->setMinimumSize(QSize(0, 100));
        label_air_temp_2->setMaximumSize(QSize(16777215, 100));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Sans Serif"));
        font4.setPointSize(52);
        label_air_temp_2->setFont(font4);

        horizontalLayout_7->addWidget(label_air_temp_2);

        label_17 = new QLabel(frame);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        QFont font5;
        font5.setFamily(QString::fromUtf8("Sans Serif"));
        font5.setPointSize(40);
        label_17->setFont(font5);

        horizontalLayout_7->addWidget(label_17);

        horizontalSpacer_18 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_18);


        verticalLayout_5->addLayout(horizontalLayout_7);

        label_31 = new QLabel(frame);
        label_31->setObjectName(QString::fromUtf8("label_31"));
        label_31->setMinimumSize(QSize(200, 0));
        QFont font6;
        font6.setPointSize(18);
        label_31->setFont(font6);
        label_31->setScaledContents(false);
        label_31->setAlignment(Qt::AlignCenter);

        verticalLayout_5->addWidget(label_31);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        horizontalSpacer_29 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_29);

        label_32 = new QLabel(frame);
        label_32->setObjectName(QString::fromUtf8("label_32"));
        label_32->setMinimumSize(QSize(165, 50));
        label_32->setMaximumSize(QSize(165, 50));
        label_32->setFrameShape(QFrame::NoFrame);
        label_32->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/fan_med.png")));
        label_32->setScaledContents(true);
        label_32->setAlignment(Qt::AlignCenter);

        horizontalLayout_10->addWidget(label_32);

        horizontalSpacer_30 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_30);


        verticalLayout_5->addLayout(horizontalLayout_10);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer_3);


        gridLayout_2->addLayout(verticalLayout_5, 1, 1, 1, 1);

        horizontalSpacer_19 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_19, 1, 0, 1, 1);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer_4, 0, 1, 1, 1);


        verticalLayout_9->addWidget(frame);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_9->addItem(verticalSpacer_5);


        gridLayout_4->addLayout(verticalLayout_9, 0, 1, 1, 1);


        gridLayout_3->addWidget(frame_body, 1, 0, 1, 1);

        frame_footer = new QFrame(screen_37);
        frame_footer->setObjectName(QString::fromUtf8("frame_footer"));
        frame_footer->setMinimumSize(QSize(1024, 80));
        frame_footer->setMaximumSize(QSize(1024, 80));
        frame_footer->setFrameShape(QFrame::StyledPanel);
        frame_footer->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_footer);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer_23 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_23);

        pushButton_back = new QPushButton(frame_footer);
        pushButton_back->setObjectName(QString::fromUtf8("pushButton_back"));
        pushButton_back->setMinimumSize(QSize(50, 50));
        pushButton_back->setMaximumSize(QSize(50, 50));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/images/Icons/back.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_back->setIcon(icon2);
        pushButton_back->setIconSize(QSize(50, 50));
        pushButton_back->setFlat(true);

        horizontalLayout->addWidget(pushButton_back);

        horizontalSpacer_3 = new QSpacerItem(146, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);

        pushButton_cancel = new QPushButton(frame_footer);
        pushButton_cancel->setObjectName(QString::fromUtf8("pushButton_cancel"));
        pushButton_cancel->setMinimumSize(QSize(50, 50));
        pushButton_cancel->setMaximumSize(QSize(50, 50));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/images/Icons/Cross.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_cancel->setIcon(icon3);
        pushButton_cancel->setIconSize(QSize(50, 50));
        pushButton_cancel->setFlat(true);

        horizontalLayout->addWidget(pushButton_cancel);

        horizontalSpacer_4 = new QSpacerItem(145, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);

        pushButton_home = new QPushButton(frame_footer);
        pushButton_home->setObjectName(QString::fromUtf8("pushButton_home"));
        pushButton_home->setMinimumSize(QSize(50, 50));
        pushButton_home->setMaximumSize(QSize(50, 50));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/images/Icons/home.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_home->setIcon(icon4);
        pushButton_home->setIconSize(QSize(50, 50));
        pushButton_home->setFlat(true);

        horizontalLayout->addWidget(pushButton_home);

        horizontalSpacer_2 = new QSpacerItem(146, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        pushButton_touch = new QPushButton(frame_footer);
        pushButton_touch->setObjectName(QString::fromUtf8("pushButton_touch"));
        pushButton_touch->setMinimumSize(QSize(50, 50));
        pushButton_touch->setMaximumSize(QSize(50, 50));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/images/images/Icons/hand 2.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_touch->setIcon(icon5);
        pushButton_touch->setIconSize(QSize(50, 50));
        pushButton_touch->setFlat(true);

        horizontalLayout->addWidget(pushButton_touch);

        horizontalSpacer = new QSpacerItem(145, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_next = new QPushButton(frame_footer);
        pushButton_next->setObjectName(QString::fromUtf8("pushButton_next"));
        pushButton_next->setMinimumSize(QSize(50, 50));
        pushButton_next->setMaximumSize(QSize(50, 50));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/images/images/Icons/next.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_next->setIcon(icon6);
        pushButton_next->setIconSize(QSize(50, 50));
        pushButton_next->setFlat(true);

        horizontalLayout->addWidget(pushButton_next);

        horizontalSpacer_26 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_26);


        gridLayout_3->addWidget(frame_footer, 2, 0, 1, 1);


        retranslateUi(screen_37);

        QMetaObject::connectSlotsByName(screen_37);
    } // setupUi

    void retranslateUi(QWidget *screen_37)
    {
        screen_37->setWindowTitle(QApplication::translate("screen_37", "Form", 0, QApplication::UnicodeUTF8));
        label_13->setText(QApplication::translate("screen_37", "1-Kids Room - Hydronic Unit", 0, QApplication::UnicodeUTF8));
        label_25->setText(QApplication::translate("screen_37", "Schedule and setpoints", 0, QApplication::UnicodeUTF8));
        toolButton_program->setText(QApplication::translate("screen_37", "Program 1", 0, QApplication::UnicodeUTF8));
        toolButton_temp->setText(QApplication::translate("screen_37", "21 C", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("screen_37", "Room Condition", 0, QApplication::UnicodeUTF8));
        label_23->setText(QString());
        label_24->setText(QString());
        label_12->setText(QString());
        label_air_temp_2->setText(QApplication::translate("screen_37", "21", 0, QApplication::UnicodeUTF8));
        label_17->setText(QApplication::translate("screen_37", "\302\260C", 0, QApplication::UnicodeUTF8));
        label_31->setText(QApplication::translate("screen_37", "Fan Speed", 0, QApplication::UnicodeUTF8));
        label_32->setText(QString());
        pushButton_home->setText(QString());
        pushButton_next->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class screen_37: public Ui_screen_37 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREEN_37_H
