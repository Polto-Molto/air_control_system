/****************************************************************************
** Meta object code from reading C++ file 'datamanager.h'
**
** Created: Tue Apr 25 16:10:43 2017
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "datamanager.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'datamanager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_dataManager[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      18,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x05,
      48,   12,   12,   12, 0x05,
      84,   12,   12,   12, 0x05,
     121,   12,   12,   12, 0x05,
     154,   12,   12,   12, 0x05,

 // slots: signature, parameters, type, tag, flags
     200,  189,  184,   12, 0x0a,
     234,   12,  184,   12, 0x0a,
     265,   12,  184,   12, 0x0a,
     311,   12,  184,   12, 0x0a,
     370,   12,  358,   12, 0x0a,
     381,   12,  184,   12, 0x0a,
     418,   12,  409,   12, 0x0a,
     440,   12,  184,   12, 0x0a,
     493,   12,  469,   12, 0x0a,
     522,   12,  509,   12, 0x0a,
     541,   12,  184,   12, 0x0a,
     577,   12,  184,   12, 0x0a,
     637,   12,  609,   12, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_dataManager[] = {
    "dataManager\0\0mainConfigDataUpdated(MAIN_CONFIG)\0"
    "scheduleConfigDataUpdated(SCHEDULE)\0"
    "climaConfigDataUpdated(CLIMA_MODBUS)\0"
    "climaValuesUpdated(CLIMA_MODBUS)\0"
    "climaConfigDataTableUpdated()\0bool\0"
    "climaTable\0init(QMap<QString,CLIMA_MODBUS>*)\0"
    "setMainConfigData(MAIN_CONFIG)\0"
    "setScheduleConfigData(QMap<QString,SCHEDULE>)\0"
    "setClimaConfigData(QMap<QString,CLIMA_MODBUS>)\0"
    "MAIN_CONFIG\0mainData()\0"
    "updateMainData(MAIN_CONFIG)\0SCHEDULE\0"
    "scheduleData(QString)\0"
    "updateScheduleData(SCHEDULE)\0"
    "QMap<QString,SCHEDULE>*\0scheduleTable()\0"
    "CLIMA_MODBUS\0climaData(QString)\0"
    "updateClimaConfigData(CLIMA_MODBUS)\0"
    "updateClimaValues(CLIMA_MODBUS)\0"
    "QMap<QString,CLIMA_MODBUS>*\0climaTable()\0"
};

void dataManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        dataManager *_t = static_cast<dataManager *>(_o);
        switch (_id) {
        case 0: _t->mainConfigDataUpdated((*reinterpret_cast< MAIN_CONFIG(*)>(_a[1]))); break;
        case 1: _t->scheduleConfigDataUpdated((*reinterpret_cast< SCHEDULE(*)>(_a[1]))); break;
        case 2: _t->climaConfigDataUpdated((*reinterpret_cast< CLIMA_MODBUS(*)>(_a[1]))); break;
        case 3: _t->climaValuesUpdated((*reinterpret_cast< CLIMA_MODBUS(*)>(_a[1]))); break;
        case 4: _t->climaConfigDataTableUpdated(); break;
        case 5: { bool _r = _t->init((*reinterpret_cast< QMap<QString,CLIMA_MODBUS>*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 6: { bool _r = _t->setMainConfigData((*reinterpret_cast< MAIN_CONFIG(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 7: { bool _r = _t->setScheduleConfigData((*reinterpret_cast< QMap<QString,SCHEDULE>(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 8: { bool _r = _t->setClimaConfigData((*reinterpret_cast< QMap<QString,CLIMA_MODBUS>(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 9: { MAIN_CONFIG _r = _t->mainData();
            if (_a[0]) *reinterpret_cast< MAIN_CONFIG*>(_a[0]) = _r; }  break;
        case 10: { bool _r = _t->updateMainData((*reinterpret_cast< MAIN_CONFIG(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 11: { SCHEDULE _r = _t->scheduleData((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< SCHEDULE*>(_a[0]) = _r; }  break;
        case 12: { bool _r = _t->updateScheduleData((*reinterpret_cast< SCHEDULE(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 13: { QMap<QString,SCHEDULE>* _r = _t->scheduleTable();
            if (_a[0]) *reinterpret_cast< QMap<QString,SCHEDULE>**>(_a[0]) = _r; }  break;
        case 14: { CLIMA_MODBUS _r = _t->climaData((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< CLIMA_MODBUS*>(_a[0]) = _r; }  break;
        case 15: { bool _r = _t->updateClimaConfigData((*reinterpret_cast< CLIMA_MODBUS(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 16: { bool _r = _t->updateClimaValues((*reinterpret_cast< CLIMA_MODBUS(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 17: { QMap<QString,CLIMA_MODBUS>* _r = _t->climaTable();
            if (_a[0]) *reinterpret_cast< QMap<QString,CLIMA_MODBUS>**>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData dataManager::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject dataManager::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_dataManager,
      qt_meta_data_dataManager, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &dataManager::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *dataManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *dataManager::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_dataManager))
        return static_cast<void*>(const_cast< dataManager*>(this));
    return QObject::qt_metacast(_clname);
}

int dataManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 18)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    }
    return _id;
}

// SIGNAL 0
void dataManager::mainConfigDataUpdated(MAIN_CONFIG _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void dataManager::scheduleConfigDataUpdated(SCHEDULE _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void dataManager::climaConfigDataUpdated(CLIMA_MODBUS _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void dataManager::climaValuesUpdated(CLIMA_MODBUS _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void dataManager::climaConfigDataTableUpdated()
{
    QMetaObject::activate(this, &staticMetaObject, 4, 0);
}
QT_END_MOC_NAMESPACE
