#include "screen_27.h"
#include "ui_screen_27.h"

screen_27::screen_27(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_27),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_27::~screen_27()
{
    delete ui;
}


void screen_27::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_27::on_pushButton_back_clicked()
{

}

void screen_27::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(26));
}

void screen_27::on_pushButton_edit_clicked()
{
    Q_EMIT(touched(31));
}

void screen_27::on_pushButton_done_clicked()
{
    Q_EMIT(touched(26));
}

void screen_27::on_pushButton_next_clicked()
{

}
