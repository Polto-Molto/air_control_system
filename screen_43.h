#ifndef SCREEN_43_H
#define SCREEN_43_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_43;
}

class screen_43 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_43(dataManager*,QWidget *parent = 0);
    ~screen_43();
signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_cancel_clicked();
      void on_pushButton_done_clicked();
      void on_pushButton_back_clicked();
      void on_pushButton_next_clicked();
      void on_pushButton_edit_clicked();

private:
    Ui::screen_43 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_43_H
