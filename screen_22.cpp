#include "screen_22.h"
#include "ui_screen_22.h"

screen_22::screen_22(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_22),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_22::~screen_22()
{
    delete ui;
}

void screen_22::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_22::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(21));
}

void screen_22::on_pushButton_radiant_clicked()
{
    Q_EMIT(touched(45));
}

void screen_22::on_pushButton_ventilation_clicked()
{
    Q_EMIT(touched(46));
}

void screen_22::on_pushButton_hydronic_clicked()
{
    Q_EMIT(touched(47));
}

void screen_22::on_pushButton_reset_clicked()
{
    Q_EMIT(touched(48));
}

void screen_22::on_pushButton_netwrok_clicked()
{
    Q_EMIT(touched(49));
}

void screen_22::on_pushButton_modbus_clicked()
{
    Q_EMIT(touched(50));
}
