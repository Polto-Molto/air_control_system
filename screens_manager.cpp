#include "screens_manager.h"
#include <QDebug>
#include <QTimer>
#include <QApplication>

#define KEYBOARD_SCREEN 1000
#define KEYPAD_SCREEN 2000

screens_manager::screens_manager(dataManager* dm,modbus_manager *mb, configManager* m,QObject *parent) : QObject(parent)
{
    _cfmgr = m;
    _datamgr = dm;
    _mbmgr = mb;

    kb = new keyboard;
    kb_sender = 0;

    kp = new keypad;
    kp_sender = 0;

    s_1 = new screen_1(_datamgr,_mbmgr);
    s_2 = new screen_2(_datamgr,_mbmgr);
    s_3 = new screen_3(_datamgr);
    s_4 = new screen_4(_datamgr);
    s_5 = new screen_5(_datamgr);
    s_6 = new screen_6(_datamgr);
    s_7 = new screen_7(_datamgr);
    s_8 = new screen_8(_datamgr);
    s_9 = new screen_9(_datamgr);
    s_10 = new screen_10(_datamgr);

    s_11 = new screen_11(_datamgr);
    s_12 = new screen_12(_datamgr);
    s_13 = new screen_13(_datamgr);
    s_14 = new screen_14(_datamgr);
    s_15 = new screen_15(_datamgr);
    s_16 = new screen_16(_datamgr);
    s_17 = new screen_17(_datamgr);
    s_18 = new screen_18(_datamgr);
    s_19 = new screen_19(_datamgr);
    s_20 = new screen_20(_datamgr);

    s_21 = new screen_21(_datamgr);
    s_22 = new screen_22(_datamgr);
    s_23 = new screen_23(_datamgr);
    s_24 = new screen_24(_datamgr);
    s_25 = new screen_25(_datamgr);
    s_26 = new screen_26(_datamgr);
    s_27 = new screen_27(_datamgr);
    s_28 = new screen_28(_datamgr);
    s_29 = new screen_29(_datamgr);
    s_30 = new screen_30(_datamgr);

    s_31 = new screen_31(_datamgr);
    s_32 = new screen_32(_datamgr);
    s_33 = new screen_33(_datamgr);
    s_34 = new screen_34(_datamgr);
    s_35 = new screen_35(_datamgr);
    s_36 = new screen_36(_datamgr);
    s_37 = new screen_37(_datamgr);
    s_38 = new screen_38(_datamgr);
    s_39 = new screen_39(_datamgr);
    s_40 = new screen_40(_datamgr);

    s_41 = new screen_41(_datamgr);
    s_42 = new screen_42(_datamgr);
    s_43 = new screen_43(_datamgr);
    s_44 = new screen_44(_datamgr);
    s_45 = new screen_45(_datamgr);
    s_46 = new screen_46(_datamgr);
    s_47 = new screen_47(_datamgr);
    s_48 = new screen_48(_datamgr);
    s_49 = new screen_49(_datamgr);
    s_50 = new screen_50(_datamgr,_mbmgr);

    s_51 = new screen_51(_datamgr,_mbmgr);
    s_52 = new screen_52(_datamgr,_mbmgr);
    s_53 = new screen_53(_datamgr);
    s_54 = new screen_54(_datamgr);
    s_55 = new screen_55(_datamgr);
    s_56 = new screen_56(_datamgr);

    silent_time = 0;
    current_screen = 1;
    s_1->showFullScreen();

    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(update_data()));
    timer->start(1000);

    QObject::connect(s_1,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_2,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_3,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_4,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_5,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_6,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_7,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_8,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_9,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_10,SIGNAL(touched(int)),this,SLOT(show_screen(int)));

    QObject::connect(s_11,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_12,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_13,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_14,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_15,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_16,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_17,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_18,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_19,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_20,SIGNAL(touched(int)),this,SLOT(show_screen(int)));

    QObject::connect(s_21,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_22,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_23,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_24,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_25,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_26,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_27,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_28,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_29,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_30,SIGNAL(touched(int)),this,SLOT(show_screen(int)));

    QObject::connect(s_31,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_32,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_33,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_34,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_35,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_36,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_37,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_38,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_39,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_40,SIGNAL(touched(int)),this,SLOT(show_screen(int)));

    QObject::connect(s_41,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_42,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_43,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_44,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_45,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_46,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_47,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_48,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_49,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_50,SIGNAL(touched(int)),this,SLOT(show_screen(int)));

    QObject::connect(s_51,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_52,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_53,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_54,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_55,SIGNAL(touched(int)),this,SLOT(show_screen(int)));
    QObject::connect(s_56,SIGNAL(touched(int)),this,SLOT(show_screen(int)));

// onscreen keyboard
    QObject::connect(s_20,SIGNAL(showkeyboard(QPushButton*)),this,SLOT(showkeyboard(QPushButton*)));
    QObject::connect(s_28,SIGNAL(showkeyboard(QPushButton*)),this,SLOT(showkeyboard(QPushButton*)));
    QObject::connect(s_31,SIGNAL(showkeyboard(QPushButton*)),this,SLOT(showkeyboard(QPushButton*)));
    QObject::connect(s_33,SIGNAL(showkeyboard(QPushButton*)),this,SLOT(showkeyboard(QPushButton*)));
    QObject::connect(s_44,SIGNAL(showkeyboard(QPushButton*)),this,SLOT(showkeyboard(QPushButton*)));

// onscreen keypad
    QObject::connect(s_51,SIGNAL(showkeypad(QPushButton*)),this,SLOT(showkeypad(QPushButton*)));

// screens done Action
 //   QObject::connect(s_45,SIGNAL(done(int))
  //                   ,_cfmgr,SLOT(setClimaNum(int)));

    QObject::connect(s_4,SIGNAL(selectedClima(CLIMA_MODBUS))
                     ,s_5,SLOT(setCurrentClima(CLIMA_MODBUS)));

    QObject::connect(s_5,SIGNAL(selectedWinterClima(CLIMA_MODBUS))
                     ,s_6,SLOT(setCurrentClima(CLIMA_MODBUS)));

    QObject::connect(s_5,SIGNAL(selectedSummerClima(CLIMA_MODBUS))
                     ,s_11,SLOT(setCurrentClima(CLIMA_MODBUS)));

    QObject::connect(s_5,SIGNAL(selectedScheduleClima(CLIMA_MODBUS))
                     ,s_7,SLOT(setCurrentClima(CLIMA_MODBUS)));

    QObject::connect(s_23,SIGNAL(clima_device_selected(CLIMA_MODBUS))
                     ,s_25,SLOT(setCurrentClima(CLIMA_MODBUS)));


    QObject::connect(s_25,SIGNAL(assignModbus(CLIMA_MODBUS))
                     ,s_51,SLOT(setCurrentClima(CLIMA_MODBUS)));

    QObject::connect(s_19,SIGNAL(selectedSchedule(SCHEDULE)),
                     s_20,SLOT(setTextEdit(QString)));

    connect(kb,SIGNAL(returnPressed()),this,SLOT(keyboardPressedReturn()));
    connect(kp,SIGNAL(returnPressed()),this,SLOT(keypadPressedReturn()));
    //connect(clima1,SIGNAL(dataReaded(short,float,short,float,float,short,short,short)),this,SLOT(update_comming_data(short,float,short,float,float,short,short,short)));
}

void screens_manager::write_circuit_act(bool state)
{
  //  clima1->writeCircuitActautor((short) state);
}

void screens_manager::write_dehumidity_act(bool state)
{
  //  clima1->writeDehumidityActautor((short) state);
}

void screens_manager::write_comfort_treshold(int val)
{
   // clima1->writeComfortRHTreshold((short) val);
}

void screens_manager::close()
{
    ((QApplication*)this->parent())->exit(0);
}

void screens_manager::init_modbus()
{

}

void screens_manager::update_data()
{
    if(current_screen != 1)
        silent_time++;
    if(silent_time > 240)
    {
        silent_time = 0;
        show_screen(1);
    }
    s_1->updateData(_datamgr->climaData("clima_1"));
}

void screens_manager::showkeyboard(QPushButton* ledit)
{
    kb_sender = (QWidget*)sender();
    if(current_screen != KEYBOARD_SCREEN )
    {
        if(kb_sender == s_25)
            s_28->setTextEdit(s_25->currentClimaName());

        current_screen = KEYBOARD_SCREEN;
        kb->setSenderWidge(ledit);
        kb->setGeometry(0,280,1024,320);
        kb->show();
        kb->activateWindow();
        kb->raise();
    }
}

void screens_manager::showkeypad(QPushButton* ledit)
{
    kp_sender = (QWidget*)sender();
    if(current_screen != KEYPAD_SCREEN )
    {
        current_screen = KEYPAD_SCREEN;
        kp->setSenderWidge(ledit);
        kp->setGeometry(0,280,1024,320);
        kp->show();
        kp->activateWindow();
        kp->raise();
    }
}


void screens_manager::keyboardPressedReturn()
{
    qDebug() << kb_sender;
    if(kb_sender == s_20)
    {
        s_19->setCurrentScheduleName(s_20->scheduleName());
        show_screen(19);
    }
    else if(kb_sender == s_28)
    {
        s_25->setCurrentClimaName(s_28->climaName());
        show_screen(25);
    }
    else if(kb_sender == s_33)
    {
        if(s_33->password() == "test")
            show_screen(22);
        else
            show_screen(34);;
    }
}

void screens_manager::keypadPressedReturn()
{

}

void screens_manager::s_45_done(int i)
{
//    _cfmgr->setClimaNum(i);
//    s_23->loadClimaData();
//    show_screen(23);
}

void screens_manager::s_23_set_clima_setting(QString clima)
{
  //  s_25->setCurrentClima(clima);
 //   show_screen(25);
}

void screens_manager::refresh_settings()
{
    //s_28->setTextEdit(s_25->currentClimaName());
    //s_23->loadClimaData();
}

void screens_manager::show_screen(int i)
{
    switch (i) {

    case 1:
        current_screen = 1;
        s_1->reload();
        s_1->showFullScreen();
        s_1->activateWindow();
        s_1->raise();
        break;

    case 2:
        current_screen = 2;
        s_2->showFullScreen();
        s_2->activateWindow();
        s_2->raise();
        break;

    case 3:
        current_screen = 3;
        s_3->showFullScreen();
        s_3->activateWindow();
        s_3->raise();
        break;

    case 4:
        current_screen = 4;
        s_4->reload();
        s_4->showFullScreen();
        s_4->activateWindow();
        s_4->raise();
        break;

    case 5:
        current_screen = 5;
        s_5->reload();
        s_5->showFullScreen();
        s_5->activateWindow();
        s_5->raise();
        break;

    case 6:
        current_screen = 6;
        s_7->reload();
        s_6->showFullScreen();
        s_6->activateWindow();
        s_6->raise();
        break;

    case 7:
        current_screen = 7;
        s_7->showFullScreen();
        s_7->activateWindow();
        s_7->raise();
        break;

    case 8:
        current_screen = 8;
        s_8->showFullScreen();
        s_8->activateWindow();
        s_8->raise();
        break;

    case 9:
        current_screen = 9;
        s_9->showFullScreen();
        s_9->activateWindow();
        s_9->raise();
        break;

    case 10:
        current_screen = 10;
        s_10->showFullScreen();
        s_10->activateWindow();
        s_10->raise();
        break;

    case 11:
        current_screen = 11;
        s_11->showFullScreen();
        s_11->activateWindow();
        s_11->raise();
        break;

    case 12:
        current_screen = 12;
        s_12->showFullScreen();
        s_12->activateWindow();
        s_12->raise();
        break;

    case 13:
        current_screen = 13;
        s_13->showFullScreen();
        s_13->activateWindow();
        s_13->raise();
        break;

    case 14:
        current_screen = 14;
        s_14->showFullScreen();
        s_14->activateWindow();
        s_14->raise();
        break;

    case 15:
        current_screen = 15;
        s_15->showFullScreen();
        s_15->activateWindow();
        s_15->raise();
        break;

    case 16:
        current_screen = 16;
        s_16->showFullScreen();
        s_16->activateWindow();
        s_16->raise();
        break;

    case 17:
        current_screen = 17;
        s_17->showFullScreen();
        s_17->activateWindow();
        s_17->raise();
        break;

    case 18:
        current_screen = 18;
        s_18->showFullScreen();
        s_18->activateWindow();
        s_18->raise();
        break;

    case 19:
        current_screen = 19;
        s_19->showFullScreen();
        s_19->activateWindow();
        s_19->raise();
        break;

    case 20:
        current_screen = 20;
        s_20->showFullScreen();
        s_20->activateWindow();
        s_20->raise();
        break;

    case 21:
        current_screen = 21;
        s_21->showFullScreen();
        s_21->activateWindow();
        s_21->raise();
        break;

    case 22:
        current_screen = 22;
        s_22->showFullScreen();
        s_22->activateWindow();
        s_22->raise();
        break;

    case 23:
        current_screen = 23;
        s_23->reload();
        s_23->showFullScreen();
        s_23->activateWindow();
        s_23->raise();
        break;

    case 24:
        current_screen = 24;
        s_24->showFullScreen();
        s_24->activateWindow();
        s_24->raise();
        break;

    case 25:
        current_screen = 25;
      //  s_25->reload();
        s_25->showFullScreen();
        s_25->activateWindow();
        s_25->raise();
        break;

    case 26:
        current_screen = 26;
        s_26->showFullScreen();
        s_26->activateWindow();
        s_26->raise();
        break;

    case 27:
        current_screen = 27;
        s_27->showFullScreen();
        s_27->activateWindow();
        s_27->raise();
        break;

    case 28:
        refresh_settings();
        current_screen = 28;
        s_28->showFullScreen();
        s_28->activateWindow();
        s_28->raise();
        break;

    case 29:
        current_screen = 29;
        s_29->showFullScreen();
        s_29->activateWindow();
        s_29->raise();
        break;

    case 30:
        current_screen = 30;
        s_30->showFullScreen();
        s_30->activateWindow();
        s_30->raise();
        break;

    case 31:
        current_screen = 31;
        s_31->showFullScreen();
        s_31->activateWindow();
        s_31->raise();
        break;

    case 32:
        current_screen = 32;
        s_32->showFullScreen();
        s_32->activateWindow();
        s_32->raise();
        break;

    case 33:
        current_screen = 33;
        s_33->showFullScreen();
        s_33->activateWindow();
        s_33->raise();
        break;

    case 34:
        current_screen = 34;
        s_34->showFullScreen();
        s_34->activateWindow();
        s_34->raise();
        break;

    case 35:
        current_screen = 35;
        s_35->showFullScreen();
        s_35->activateWindow();
        s_35->raise();
        break;

    case 36:
        current_screen = 36;
        s_36->showFullScreen();
        s_36->activateWindow();
        s_36->raise();
        break;

    case 37:
        current_screen = 37;
        s_37->showFullScreen();
        s_37->activateWindow();
        s_37->raise();
        break;

    case 38:
        current_screen = 38;
        s_38->showFullScreen();
        s_38->activateWindow();
        s_38->raise();
        break;

    case 39:
        current_screen = 39;
        s_39->showFullScreen();
        s_39->activateWindow();
        s_39->raise();
        break;

    case 40:
        current_screen = 40;
        s_40->showFullScreen();
        s_40->activateWindow();
        s_40->raise();
        break;

    case 41:
        current_screen = 41;
        s_41->showFullScreen();
        s_41->activateWindow();
        s_41->raise();
        break;

    case 42:
        current_screen = 42;
        s_42->showFullScreen();
        s_42->activateWindow();
        s_42->raise();
        break;

    case 43:
        current_screen = 43;
        s_43->showFullScreen();
        s_43->activateWindow();
        s_43->raise();
        break;

    case 44:
        current_screen = 44;
        s_44->showFullScreen();
        s_44->activateWindow();
        s_44->raise();
        break;

    case 45:
        current_screen = 45;
        s_45->showFullScreen();
        s_45->activateWindow();
        s_45->raise();
        break;

    case 46:
        current_screen = 46;
        s_46->showFullScreen();
        s_46->activateWindow();
        s_46->raise();
        break;

    case 47:
        current_screen = 47;
        s_47->showFullScreen();
        s_47->activateWindow();
        s_47->raise();
        break;

    case 48:
        current_screen = 48;
        s_48->showFullScreen();
        s_48->activateWindow();
        s_48->raise();
        break;

    case 49:
        current_screen = 49;
        s_49->showFullScreen();
        s_49->activateWindow();
        s_49->raise();
        break;

    case 50:
        current_screen = 50;
        s_50->showFullScreen();
        s_50->activateWindow();
        s_50->raise();
        break;

    case 51:
        current_screen = 51;
        s_51->showFullScreen();
        s_51->activateWindow();
        s_51->raise();
        break;

    case 52:
        current_screen = 52;
        s_52->showFullScreen();
        s_52->activateWindow();
        s_52->raise();
        break;

    case 53:
        current_screen = 53;
        s_53->showFullScreen();
        s_53->activateWindow();
        s_53->raise();
        break;

    case 54:
        current_screen = 54;
        s_54->showFullScreen();
        s_54->activateWindow();
        s_54->raise();
        break;

    case 55:
        current_screen = 55;
        s_55->showFullScreen();
        s_55->activateWindow();
        s_55->raise();
        break;

    case 56:
        current_screen = 56;
        s_56->showFullScreen();
        s_56->activateWindow();
        s_56->raise();
        break;
    }

}
