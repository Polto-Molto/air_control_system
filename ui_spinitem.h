/********************************************************************************
** Form generated from reading UI file 'spinitem.ui'
**
** Created: Thu Apr 13 00:46:21 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SPINITEM_H
#define UI_SPINITEM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_spinItem
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QLabel *label_name;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton_sub;
    QLabel *label_value;
    QPushButton *pushButton_add;

    void setupUi(QWidget *spinItem)
    {
        if (spinItem->objectName().isEmpty())
            spinItem->setObjectName(QString::fromUtf8("spinItem"));
        spinItem->resize(300, 130);
        spinItem->setMinimumSize(QSize(300, 130));
        spinItem->setMaximumSize(QSize(300, 130));
        verticalLayout = new QVBoxLayout(spinItem);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        label = new QLabel(spinItem);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMinimumSize(QSize(35, 35));
        label->setMaximumSize(QSize(35, 35));

        horizontalLayout_2->addWidget(label);

        label_name = new QLabel(spinItem);
        label_name->setObjectName(QString::fromUtf8("label_name"));
        label_name->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(label_name);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pushButton_sub = new QPushButton(spinItem);
        pushButton_sub->setObjectName(QString::fromUtf8("pushButton_sub"));
        pushButton_sub->setMinimumSize(QSize(50, 50));
        pushButton_sub->setMaximumSize(QSize(50, 50));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/images/Icons/minus.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_sub->setIcon(icon);
        pushButton_sub->setIconSize(QSize(50, 50));
        pushButton_sub->setFlat(true);

        horizontalLayout->addWidget(pushButton_sub);

        label_value = new QLabel(spinItem);
        label_value->setObjectName(QString::fromUtf8("label_value"));
        label_value->setMinimumSize(QSize(130, 85));
        label_value->setMaximumSize(QSize(130, 85));
        label_value->setFrameShape(QFrame::Box);
        label_value->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(label_value);

        pushButton_add = new QPushButton(spinItem);
        pushButton_add->setObjectName(QString::fromUtf8("pushButton_add"));
        pushButton_add->setMinimumSize(QSize(50, 50));
        pushButton_add->setMaximumSize(QSize(50, 50));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/images/Icons/Plus.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_add->setIcon(icon1);
        pushButton_add->setIconSize(QSize(50, 50));
        pushButton_add->setFlat(true);

        horizontalLayout->addWidget(pushButton_add);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(spinItem);

        QMetaObject::connectSlotsByName(spinItem);
    } // setupUi

    void retranslateUi(QWidget *spinItem)
    {
        spinItem->setWindowTitle(QApplication::translate("spinItem", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QString());
        label_name->setText(QApplication::translate("spinItem", "Spin Name", 0, QApplication::UnicodeUTF8));
        pushButton_sub->setText(QString());
        label_value->setText(QString());
        pushButton_add->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class spinItem: public Ui_spinItem {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SPINITEM_H
