#include "screen_2.h"
#include "ui_screen_2.h"

screen_2::screen_2(dataManager *dm,modbus_manager *m,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_2),
    _datamgr(dm),
    _mbus(m)
{
    ui->setupUi(this);
}

screen_2::~screen_2()
{
    delete ui;
}

void screen_2::on_pushButton_schedule_clicked()
{
    Q_EMIT(touched(19));
}

void screen_2::on_pushButton_settings_clicked()
{
    Q_EMIT(touched(21));
}

void screen_2::on_pushButton_notifications_clicked()
{
    Q_EMIT(touched(3));
}

void screen_2::on_pushButton_season_clicked()
{
    Q_EMIT(touched(12));
}

void screen_2::on_toolButton_temp_clicked()
{
    Q_EMIT(touched(4));
}

void screen_2::on_toolButton_vent_clicked()
{
    Q_EMIT(touched(13));
}
