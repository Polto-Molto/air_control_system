#include "screen_45.h"
#include "ui_screen_45.h"

screen_45::screen_45(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_45),
    _datamgr(dm)
{
    ui->setupUi(this);
    ui->label_clima_num->setNum((int)_datamgr->mainData().clima_num);
}

screen_45::~screen_45()
{
    delete ui;
}

void screen_45::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_45::on_pushButton_back_clicked()
{
    Q_EMIT(touched(22));
}

void screen_45::on_pushButton_done_clicked()
{
    MAIN_CONFIG main_config = _datamgr->mainData();
    if(ui->label_clima_num->text().toInt() != main_config.clima_num)
    {
        main_config.clima_num = ui->label_clima_num->text().toInt();
       _datamgr->updateMainData(main_config);
    }
    Q_EMIT(touched(23));
}

void screen_45::on_pushButton_num_down_clicked()
{
    int num = ui->label_clima_num->text().toInt() -1;
    if(num < 0 )
        num = 0;
    ui->label_clima_num->setNum(num);
}

void screen_45::on_pushButton_num_up_clicked()
{
    int num = ui->label_clima_num->text().toInt() +1;
    if(num > 30)
        num = 30;
    ui->label_clima_num->setNum(num);
}
