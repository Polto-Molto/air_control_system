/****************************************************************************
** Meta object code from reading C++ file 'screen_33.h'
**
** Created: Tue Apr 25 16:10:07 2017
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "screen_33.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'screen_33.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_screen_33[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x05,
      24,   10,   10,   10, 0x05,

 // slots: signature, parameters, type, tag, flags
      51,   10,   10,   10, 0x0a,
      92,   10,   84,   10, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_screen_33[] = {
    "screen_33\0\0touched(int)\0"
    "showkeyboard(QPushButton*)\0"
    "on_pushButton_keyboard_clicked()\0"
    "QString\0password()\0"
};

void screen_33::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        screen_33 *_t = static_cast<screen_33 *>(_o);
        switch (_id) {
        case 0: _t->touched((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->showkeyboard((*reinterpret_cast< QPushButton*(*)>(_a[1]))); break;
        case 2: _t->on_pushButton_keyboard_clicked(); break;
        case 3: { QString _r = _t->password();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData screen_33::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject screen_33::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_screen_33,
      qt_meta_data_screen_33, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &screen_33::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *screen_33::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *screen_33::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_screen_33))
        return static_cast<void*>(const_cast< screen_33*>(this));
    return QWidget::qt_metacast(_clname);
}

int screen_33::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void screen_33::touched(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void screen_33::showkeyboard(QPushButton * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
