/********************************************************************************
** Form generated from reading UI file 'screen_43.ui'
**
** Created: Thu Apr 13 00:46:21 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREEN_43_H
#define UI_SCREEN_43_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_screen_43
{
public:
    QGridLayout *gridLayout;
    QFrame *frame_body;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *pushButton_activate;
    QPushButton *pushButton_deactivate;
    QSpacerItem *horizontalSpacer_4;
    QSpacerItem *verticalSpacer_4;
    QLabel *label_2;
    QSpacerItem *verticalSpacer;
    QFrame *line;
    QGridLayout *gridLayout_2;
    QLabel *label_5;
    QLabel *label_4;
    QLabel *label_3;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_11;
    QRadioButton *radioButton_cool;
    QRadioButton *radioButton_heat;
    QSpacerItem *horizontalSpacer_10;
    QSpacerItem *horizontalSpacer_12;
    QSpacerItem *horizontalSpacer_13;
    QSpacerItem *verticalSpacer_3;
    QFrame *frame_header;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QFrame *frame_footer;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_15;
    QPushButton *pushButton_back;
    QSpacerItem *horizontalSpacer_7;
    QPushButton *pushButton_cancel;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *pushButton_home;
    QSpacerItem *horizontalSpacer_9;
    QPushButton *pushButton_edit;
    QSpacerItem *horizontalSpacer_8;
    QPushButton *pushButton_done;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *pushButton_next;
    QSpacerItem *horizontalSpacer_16;

    void setupUi(QWidget *screen_43)
    {
        if (screen_43->objectName().isEmpty())
            screen_43->setObjectName(QString::fromUtf8("screen_43"));
        screen_43->resize(1024, 600);
        screen_43->setMinimumSize(QSize(1024, 600));
        screen_43->setMaximumSize(QSize(1024, 600));
        gridLayout = new QGridLayout(screen_43);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame_body = new QFrame(screen_43);
        frame_body->setObjectName(QString::fromUtf8("frame_body"));
        frame_body->setMinimumSize(QSize(1024, 470));
        frame_body->setMaximumSize(QSize(1024, 470));
        frame_body->setFrameShape(QFrame::StyledPanel);
        frame_body->setFrameShadow(QFrame::Raised);
        verticalLayout = new QVBoxLayout(frame_body);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout->addItem(verticalSpacer_2);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);

        pushButton_activate = new QPushButton(frame_body);
        pushButton_activate->setObjectName(QString::fromUtf8("pushButton_activate"));
        pushButton_activate->setMinimumSize(QSize(270, 65));
        pushButton_activate->setMaximumSize(QSize(270, 64));
        pushButton_activate->setAutoFillBackground(false);
        pushButton_activate->setStyleSheet(QString::fromUtf8("QPushButton::checked {\n"
"background-color: rgb(217, 229, 255);\n"
"}\n"
"QPushButton{\n"
"border-style: solid;\n"
"border-width: 2px;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"padding:20px;\n"
"}"));
        pushButton_activate->setFlat(true);

        horizontalLayout_4->addWidget(pushButton_activate);

        pushButton_deactivate = new QPushButton(frame_body);
        pushButton_deactivate->setObjectName(QString::fromUtf8("pushButton_deactivate"));
        pushButton_deactivate->setMinimumSize(QSize(270, 65));
        pushButton_deactivate->setMaximumSize(QSize(270, 64));
        pushButton_deactivate->setAutoFillBackground(false);
        pushButton_deactivate->setStyleSheet(QString::fromUtf8("QPushButton::checked {\n"
"background-color: rgb(217, 229, 255);\n"
"}\n"
"QPushButton{\n"
"border-style: solid;\n"
"border-width: 2px;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"padding:20px;\n"
"}"));
        pushButton_deactivate->setInputMethodHints(Qt::ImhNone);

        horizontalLayout_4->addWidget(pushButton_deactivate);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_4);


        verticalLayout->addLayout(horizontalLayout_4);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_4);

        label_2 = new QLabel(frame_body);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        line = new QFrame(frame_body);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label_5 = new QLabel(frame_body);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout_2->addWidget(label_5, 0, 3, 1, 1);

        label_4 = new QLabel(frame_body);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMinimumSize(QSize(150, 85));
        label_4->setMaximumSize(QSize(150, 85));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(28);
        label_4->setFont(font);
        label_4->setFrameShape(QFrame::Box);
        label_4->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_4, 1, 3, 1, 1);

        label_3 = new QLabel(frame_body);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_2->addWidget(label_3, 0, 1, 1, 1);

        groupBox = new QGroupBox(frame_body);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        horizontalLayout_3 = new QHBoxLayout(groupBox);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_11);

        radioButton_cool = new QRadioButton(groupBox);
        radioButton_cool->setObjectName(QString::fromUtf8("radioButton_cool"));
        radioButton_cool->setStyleSheet(QString::fromUtf8("QRadioButton::indicator:unchecked {\n"
"    image: url(:/images/images/Icons/radio uncheck.png);\n"
"}\n"
"\n"
"QRadioButton::indicator:checked {\n"
"image:url(:/images/images/Icons/radiochecked.png);\n"
"}\n"
"QRadioButton {\n"
"color:rgb(0, 84, 127);\n"
"}\n"
""));

        horizontalLayout_3->addWidget(radioButton_cool);

        radioButton_heat = new QRadioButton(groupBox);
        radioButton_heat->setObjectName(QString::fromUtf8("radioButton_heat"));
        radioButton_heat->setStyleSheet(QString::fromUtf8("QRadioButton::indicator:unchecked {\n"
"    image: url(:/images/images/Icons/radio uncheck.png);\n"
"}\n"
"\n"
"QRadioButton::indicator:checked {\n"
"image:url(:/images/images/Icons/radiochecked.png);\n"
"}\n"
"QRadioButton {\n"
"color:rgb(0, 84, 127);\n"
"}\n"
""));
        radioButton_heat->setChecked(true);

        horizontalLayout_3->addWidget(radioButton_heat);

        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_10);


        gridLayout_2->addWidget(groupBox, 0, 2, 1, 1);

        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_12, 0, 0, 1, 1);

        horizontalSpacer_13 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_13, 0, 4, 1, 1);


        verticalLayout->addLayout(gridLayout_2);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_3);


        gridLayout->addWidget(frame_body, 1, 0, 1, 1);

        frame_header = new QFrame(screen_43);
        frame_header->setObjectName(QString::fromUtf8("frame_header"));
        frame_header->setMinimumSize(QSize(1024, 50));
        frame_header->setMaximumSize(QSize(1024, 50));
        frame_header->setFrameShape(QFrame::StyledPanel);
        frame_header->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_header);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(frame_header);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Sans Serif"));
        font1.setPointSize(14);
        label->setFont(font1);

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        gridLayout->addWidget(frame_header, 0, 0, 1, 1);

        frame_footer = new QFrame(screen_43);
        frame_footer->setObjectName(QString::fromUtf8("frame_footer"));
        frame_footer->setMinimumSize(QSize(1024, 80));
        frame_footer->setMaximumSize(QSize(1024, 80));
        frame_footer->setFrameShape(QFrame::StyledPanel);
        frame_footer->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(frame_footer);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_15 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_15);

        pushButton_back = new QPushButton(frame_footer);
        pushButton_back->setObjectName(QString::fromUtf8("pushButton_back"));
        pushButton_back->setMinimumSize(QSize(50, 50));
        pushButton_back->setMaximumSize(QSize(50, 50));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/images/Icons/back.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_back->setIcon(icon);
        pushButton_back->setIconSize(QSize(50, 50));
        pushButton_back->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_back);

        horizontalSpacer_7 = new QSpacerItem(152, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_7);

        pushButton_cancel = new QPushButton(frame_footer);
        pushButton_cancel->setObjectName(QString::fromUtf8("pushButton_cancel"));
        pushButton_cancel->setMinimumSize(QSize(50, 50));
        pushButton_cancel->setMaximumSize(QSize(50, 50));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/images/Icons/Cross.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_cancel->setIcon(icon1);
        pushButton_cancel->setIconSize(QSize(50, 50));
        pushButton_cancel->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_cancel);

        horizontalSpacer_5 = new QSpacerItem(151, 17, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_5);

        pushButton_home = new QPushButton(frame_footer);
        pushButton_home->setObjectName(QString::fromUtf8("pushButton_home"));
        pushButton_home->setMinimumSize(QSize(50, 50));
        pushButton_home->setMaximumSize(QSize(50, 50));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/images/Icons/home.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_home->setIcon(icon2);
        pushButton_home->setIconSize(QSize(50, 50));
        pushButton_home->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_home);

        horizontalSpacer_9 = new QSpacerItem(84, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_9);

        pushButton_edit = new QPushButton(frame_footer);
        pushButton_edit->setObjectName(QString::fromUtf8("pushButton_edit"));
        pushButton_edit->setMinimumSize(QSize(50, 50));
        pushButton_edit->setMaximumSize(QSize(50, 50));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/images/Icons/rename.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_edit->setIcon(icon3);
        pushButton_edit->setIconSize(QSize(50, 50));
        pushButton_edit->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_edit);

        horizontalSpacer_8 = new QSpacerItem(85, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_8);

        pushButton_done = new QPushButton(frame_footer);
        pushButton_done->setObjectName(QString::fromUtf8("pushButton_done"));
        pushButton_done->setMinimumSize(QSize(50, 50));
        pushButton_done->setMaximumSize(QSize(50, 50));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/images/Icons/check.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_done->setIcon(icon4);
        pushButton_done->setIconSize(QSize(50, 50));
        pushButton_done->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_done);

        horizontalSpacer_6 = new QSpacerItem(84, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_6);

        pushButton_next = new QPushButton(frame_footer);
        pushButton_next->setObjectName(QString::fromUtf8("pushButton_next"));
        pushButton_next->setMinimumSize(QSize(50, 50));
        pushButton_next->setMaximumSize(QSize(50, 50));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/images/images/Icons/next.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_next->setIcon(icon5);
        pushButton_next->setIconSize(QSize(50, 50));
        pushButton_next->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_next);

        horizontalSpacer_16 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_16);


        gridLayout->addWidget(frame_footer, 2, 0, 1, 1);


        retranslateUi(screen_43);

        QMetaObject::connectSlotsByName(screen_43);
    } // setupUi

    void retranslateUi(QWidget *screen_43)
    {
        screen_43->setWindowTitle(QApplication::translate("screen_43", "Form", 0, QApplication::UnicodeUTF8));
        pushButton_activate->setText(QApplication::translate("screen_43", "Activate", 0, QApplication::UnicodeUTF8));
        pushButton_deactivate->setText(QApplication::translate("screen_43", "Deactivate", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("screen_43", "Fan speed : 0/1/2/3/4/5/6 | Room temp: 8 C", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("screen_43", "MODBUS Address", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("screen_43", "#001", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("screen_43", "Function Mode", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QString());
        radioButton_cool->setText(QApplication::translate("screen_43", "Cooling", 0, QApplication::UnicodeUTF8));
        radioButton_heat->setText(QApplication::translate("screen_43", "Heating", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("screen_43", "Hydronic unit Skudo 1 - Settings", 0, QApplication::UnicodeUTF8));
        pushButton_home->setText(QString());
        pushButton_edit->setText(QString());
        pushButton_done->setText(QString());
        pushButton_next->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class screen_43: public Ui_screen_43 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREEN_43_H
