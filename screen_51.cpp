#include "screen_51.h"
#include "ui_screen_51.h"

screen_51::screen_51(dataManager *dm, modbus_manager *m,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_51),
    _datamgr(dm),
    _mbus(m)
{
    ui->setupUi(this);
}

screen_51::~screen_51()
{
    delete ui;
}

void screen_51::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_51::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(25));
}

void screen_51::on_pushButton_value_clicked()
{
    Q_EMIT(showkeypad(ui->pushButton_value));
}

void screen_51::on_pushButton_done_clicked()
{
    Q_EMIT(touched(52));
    bool state = _mbus->assignDevice(_clima.id
                        ,ui->pushButton_value->text().toInt()
                        ,modbus_manager::CLIMA);
    if(state)
    {
        _clima.id = ui->pushButton_value->text().toInt();
        _datamgr->updateClimaConfigData(_clima);
    }
}


void screen_51::on_pushButton_num_down_clicked()
{
    QString num_str;
    ui->pushButton_value->setText(num_str.setNum(ui->pushButton_value->text().toInt() - 1 ));
}

void screen_51::on_pushButton_num_up_clicked()
{
    QString num_str;
    ui->pushButton_value->setText(num_str.setNum(ui->pushButton_value->text().toInt() + 1 ));
}


void screen_51::setCurrentClima(CLIMA_MODBUS clima)
{
  _clima = clima;
}
