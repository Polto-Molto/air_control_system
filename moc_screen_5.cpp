/****************************************************************************
** Meta object code from reading C++ file 'screen_5.h'
**
** Created: Tue Apr 25 17:38:48 2017
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "screen_5.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'screen_5.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_screen_5[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x05,
      44,    9,    9,    9, 0x05,
      78,    9,    9,    9, 0x05,
     114,    9,    9,    9, 0x05,
     142,    9,    9,    9, 0x05,

 // slots: signature, parameters, type, tag, flags
     155,    9,    9,    9, 0x0a,
     164,    9,    9,    9, 0x0a,
     193,    9,    9,    9, 0x0a,
     226,    9,    9,    9, 0x0a,
     255,    9,    9,    9, 0x0a,
     286,    9,    9,    9, 0x0a,
     322,    9,    9,    9, 0x0a,
     351,    9,    9,    9, 0x0a,
     381,    9,    9,    9, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_screen_5[] = {
    "screen_5\0\0selectedWinterClima(CLIMA_MODBUS)\0"
    "selectedSummerClima(CLIMA_MODBUS)\0"
    "selectedScheduleClima(CLIMA_MODBUS)\0"
    "selectedClima(CLIMA_MODBUS)\0touched(int)\0"
    "reload()\0on_pushButton_home_clicked()\0"
    "on_toolButton_schedule_clicked()\0"
    "on_pushButton_back_clicked()\0"
    "on_pushButton_cancel_clicked()\0"
    "on_pushButton_eco_comfort_clicked()\0"
    "on_pushButton_next_clicked()\0"
    "on_pushButton_touch_clicked()\0"
    "setCurrentClima(CLIMA_MODBUS)\0"
};

void screen_5::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        screen_5 *_t = static_cast<screen_5 *>(_o);
        switch (_id) {
        case 0: _t->selectedWinterClima((*reinterpret_cast< CLIMA_MODBUS(*)>(_a[1]))); break;
        case 1: _t->selectedSummerClima((*reinterpret_cast< CLIMA_MODBUS(*)>(_a[1]))); break;
        case 2: _t->selectedScheduleClima((*reinterpret_cast< CLIMA_MODBUS(*)>(_a[1]))); break;
        case 3: _t->selectedClima((*reinterpret_cast< CLIMA_MODBUS(*)>(_a[1]))); break;
        case 4: _t->touched((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->reload(); break;
        case 6: _t->on_pushButton_home_clicked(); break;
        case 7: _t->on_toolButton_schedule_clicked(); break;
        case 8: _t->on_pushButton_back_clicked(); break;
        case 9: _t->on_pushButton_cancel_clicked(); break;
        case 10: _t->on_pushButton_eco_comfort_clicked(); break;
        case 11: _t->on_pushButton_next_clicked(); break;
        case 12: _t->on_pushButton_touch_clicked(); break;
        case 13: _t->setCurrentClima((*reinterpret_cast< CLIMA_MODBUS(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData screen_5::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject screen_5::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_screen_5,
      qt_meta_data_screen_5, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &screen_5::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *screen_5::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *screen_5::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_screen_5))
        return static_cast<void*>(const_cast< screen_5*>(this));
    return QWidget::qt_metacast(_clname);
}

int screen_5::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    }
    return _id;
}

// SIGNAL 0
void screen_5::selectedWinterClima(CLIMA_MODBUS _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void screen_5::selectedSummerClima(CLIMA_MODBUS _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void screen_5::selectedScheduleClima(CLIMA_MODBUS _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void screen_5::selectedClima(CLIMA_MODBUS _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void screen_5::touched(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_END_MOC_NAMESPACE
