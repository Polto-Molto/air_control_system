#ifndef SCREEN_26_H
#define SCREEN_26_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_26;
}

class screen_26 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_26(dataManager*,QWidget *parent = 0);
    ~screen_26();

signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_cancel_clicked();
      void on_pushButton_first_clicked();
      void on_pushButton_second_clicked();

private:
    Ui::screen_26 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_26_H
