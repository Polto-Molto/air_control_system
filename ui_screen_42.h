/********************************************************************************
** Form generated from reading UI file 'screen_42.ui'
**
** Created: Thu Apr 13 00:46:21 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREEN_42_H
#define UI_SCREEN_42_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_screen_42
{
public:
    QGridLayout *gridLayout;
    QFrame *frame_header;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QFrame *frame_body;
    QGridLayout *gridLayout_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QPushButton *pushButton_test;
    QPushButton *pushButton_5;
    QPushButton *pushButton_2;
    QSpacerItem *verticalSpacer;
    QFrame *frame_footer;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_15;
    QPushButton *pushButton_back;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *pushButton_cancel;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *pushButton_home;
    QSpacerItem *horizontalSpacer_6;
    QSpacerItem *horizontalSpacer_16;

    void setupUi(QWidget *screen_42)
    {
        if (screen_42->objectName().isEmpty())
            screen_42->setObjectName(QString::fromUtf8("screen_42"));
        screen_42->resize(1024, 600);
        screen_42->setMinimumSize(QSize(1024, 600));
        screen_42->setMaximumSize(QSize(1024, 600));
        gridLayout = new QGridLayout(screen_42);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame_header = new QFrame(screen_42);
        frame_header->setObjectName(QString::fromUtf8("frame_header"));
        frame_header->setMinimumSize(QSize(1024, 50));
        frame_header->setMaximumSize(QSize(1024, 50));
        frame_header->setFrameShape(QFrame::StyledPanel);
        frame_header->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_header);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(frame_header);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(14);
        label->setFont(font);

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        gridLayout->addWidget(frame_header, 0, 0, 1, 1);

        frame_body = new QFrame(screen_42);
        frame_body->setObjectName(QString::fromUtf8("frame_body"));
        frame_body->setMinimumSize(QSize(1024, 470));
        frame_body->setMaximumSize(QSize(1024, 470));
        frame_body->setFrameShape(QFrame::StyledPanel);
        frame_body->setFrameShadow(QFrame::Raised);
        gridLayout_2 = new QGridLayout(frame_body);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        pushButton_3 = new QPushButton(frame_body);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        pushButton_3->setMinimumSize(QSize(170, 80));
        pushButton_3->setMaximumSize(QSize(170, 80));
        pushButton_3->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_3, 0, 2, 1, 1);

        pushButton_4 = new QPushButton(frame_body);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));
        pushButton_4->setMinimumSize(QSize(170, 80));
        pushButton_4->setMaximumSize(QSize(170, 80));
        pushButton_4->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_4, 0, 3, 1, 1);

        pushButton_test = new QPushButton(frame_body);
        pushButton_test->setObjectName(QString::fromUtf8("pushButton_test"));
        pushButton_test->setMinimumSize(QSize(170, 80));
        pushButton_test->setMaximumSize(QSize(170, 80));
        pushButton_test->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_test, 0, 0, 1, 1);

        pushButton_5 = new QPushButton(frame_body);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));
        pushButton_5->setMinimumSize(QSize(170, 80));
        pushButton_5->setMaximumSize(QSize(170, 80));
        pushButton_5->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_5, 0, 4, 1, 1);

        pushButton_2 = new QPushButton(frame_body);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setMinimumSize(QSize(170, 80));
        pushButton_2->setMaximumSize(QSize(170, 80));
        pushButton_2->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_2, 0, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer, 1, 3, 1, 1);


        gridLayout->addWidget(frame_body, 1, 0, 1, 1);

        frame_footer = new QFrame(screen_42);
        frame_footer->setObjectName(QString::fromUtf8("frame_footer"));
        frame_footer->setMinimumSize(QSize(1024, 80));
        frame_footer->setMaximumSize(QSize(1024, 80));
        frame_footer->setFrameShape(QFrame::StyledPanel);
        frame_footer->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_footer);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_15 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_15);

        pushButton_back = new QPushButton(frame_footer);
        pushButton_back->setObjectName(QString::fromUtf8("pushButton_back"));
        pushButton_back->setMinimumSize(QSize(50, 50));
        pushButton_back->setMaximumSize(QSize(50, 50));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/images/Icons/back.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_back->setIcon(icon);
        pushButton_back->setIconSize(QSize(50, 50));
        pushButton_back->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_back);

        horizontalSpacer_3 = new QSpacerItem(151, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        pushButton_cancel = new QPushButton(frame_footer);
        pushButton_cancel->setObjectName(QString::fromUtf8("pushButton_cancel"));
        pushButton_cancel->setMinimumSize(QSize(50, 50));
        pushButton_cancel->setMaximumSize(QSize(50, 50));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/images/Icons/Cross.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_cancel->setIcon(icon1);
        pushButton_cancel->setIconSize(QSize(50, 50));
        pushButton_cancel->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_cancel);

        horizontalSpacer_5 = new QSpacerItem(152, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);

        pushButton_home = new QPushButton(frame_footer);
        pushButton_home->setObjectName(QString::fromUtf8("pushButton_home"));
        pushButton_home->setMinimumSize(QSize(50, 50));
        pushButton_home->setMaximumSize(QSize(50, 50));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/images/Icons/home.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_home->setIcon(icon2);
        pushButton_home->setIconSize(QSize(50, 50));
        pushButton_home->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_home);

        horizontalSpacer_6 = new QSpacerItem(403, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);

        horizontalSpacer_16 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_16);


        gridLayout->addWidget(frame_footer, 2, 0, 1, 1);


        retranslateUi(screen_42);

        QMetaObject::connectSlotsByName(screen_42);
    } // setupUi

    void retranslateUi(QWidget *screen_42)
    {
        screen_42->setWindowTitle(QApplication::translate("screen_42", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("screen_42", "Hydronic Units - Skudo 1/2", 0, QApplication::UnicodeUTF8));
        pushButton_3->setText(QApplication::translate("screen_42", "23. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
        pushButton_4->setText(QApplication::translate("screen_42", "24. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
        pushButton_test->setText(QApplication::translate("screen_42", "21. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
        pushButton_5->setText(QApplication::translate("screen_42", "25. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("screen_42", "22. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
        pushButton_back->setText(QString());
        pushButton_home->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class screen_42: public Ui_screen_42 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREEN_42_H
