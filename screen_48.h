#ifndef SCREEN_48_H
#define SCREEN_48_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_48;
}

class screen_48 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_48(dataManager*,QWidget *parent = 0);
    ~screen_48();
signals:
      void touched(int);

public slots:
      void on_pushButton_cancel_clicked();
      void on_pushButton_done_clicked();

private:
    Ui::screen_48 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_48_H
