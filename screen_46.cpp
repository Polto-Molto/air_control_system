#include "screen_46.h"
#include "ui_screen_46.h"

screen_46::screen_46(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_46),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_46::~screen_46()
{
    delete ui;
}

void screen_46::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_46::on_pushButton_back_clicked()
{
    Q_EMIT(touched(22));
}

void screen_46::on_pushButton_done_clicked()
{
    Q_EMIT(touched(26));
}
