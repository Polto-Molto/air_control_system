#include "screen_17.h"
#include "ui_screen_17.h"

screen_17::screen_17(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_17),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_17::~screen_17()
{
    delete ui;
}
void screen_17::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_17::on_pushButton_done_clicked()
{
    Q_EMIT(touched(18));
}

void screen_17::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(19));
}

void screen_17::on_pushButton_program_clicked()
{
    Q_EMIT(touched(20));
}
