#include "screen_25.h"
#include "ui_screen_25.h"

screen_25::screen_25(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_25),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_25::~screen_25()
{
    delete ui;
}

void screen_25::reload()
{
    QString num_str;
    _clima = _datamgr->climaData(_clima.config_name);
    ui->pushButton_clima_id->setText(num_str.setNum(_clima.id));
}

void screen_25::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_25::on_pushButton_back_clicked()
{

}

void screen_25::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(23));
}

void screen_25::on_pushButton_edit_clicked()
{
    Q_EMIT(touched(28));
}

void screen_25::on_pushButton_deactivate_clicked()
{
    ui->pushButton_activate->setChecked(false);
}

void screen_25::on_pushButton_activate_clicked()
{
    ui->pushButton_deactivate->setChecked(false);
}

void screen_25::setCurrentClimaName(QString str)
{
    _clima.name = str;
}

QString screen_25::currentClimaName()
{
    return _clima.name;
}

void screen_25::on_pushButton_done_clicked()
{
    _clima.id = ui->pushButton_clima_id->text().toInt();
    _clima.dew = ui->checkBox_dew->isChecked();
    _clima.alarm =  ui->checkBox_room_alarm->isChecked();
    _clima.activate = ui->pushButton_activate->isChecked();
    if(ui->radioButton_winter->isChecked())
        _clima.season = WINTER;
    else
        _clima.season = SUMMER;
    qDebug() << "from 25 : " << _clima.name;
    _datamgr->updateClimaConfigData(_clima);
    Q_EMIT(touched(23));
}


void screen_25::on_pushButton_next_clicked()
{

}

void screen_25::setCurrentClima(CLIMA_MODBUS clima)
{
    QString num_str;
    _clima = clima;
    ui->pushButton_clima_id->setText(num_str.setNum(_clima.id));
    ui->checkBox_dew->setChecked(_clima.dew);
    ui->checkBox_room_alarm->setChecked(_clima.alarm);
    ui->pushButton_activate->setChecked(_clima.activate);
    ui->pushButton_deactivate->setEnabled(!_clima.activate);
    switch(_clima.season)
    {
        case WINTER:
        ui->radioButton_winter->setChecked(true);
        break;
        case SUMMER:
        ui->radioButton_summer->setChecked(true);
        break;
    }
}

void screen_25::on_pushButton_clima_id_clicked()
{
    Q_EMIT(assignModbus(_clima));
    Q_EMIT(touched(51));
}
