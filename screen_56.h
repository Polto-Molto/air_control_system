#ifndef SCREEN_56_H
#define SCREEN_56_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_56;
}

class screen_56 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_56(dataManager *,QWidget *parent = 0);
    ~screen_56();
signals:
      void touched(int);

private:
    Ui::screen_56 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_56_H
