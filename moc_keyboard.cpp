/****************************************************************************
** Meta object code from reading C++ file 'keyboard.h'
**
** Created: Thu Apr 13 00:48:33 2017
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "keyboard.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'keyboard.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_keyboard[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      40,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x05,

 // slots: signature, parameters, type, tag, flags
      26,    9,    9,    9, 0x0a,
      55,    9,    9,    9, 0x0a,
      75,    9,    9,    9, 0x0a,
      89,    9,    9,    9, 0x0a,
     115,    9,    9,    9, 0x0a,
     141,    9,    9,    9, 0x0a,
     167,    9,    9,    9, 0x0a,
     193,    9,    9,    9, 0x0a,
     219,    9,    9,    9, 0x0a,
     245,    9,    9,    9, 0x0a,
     271,    9,    9,    9, 0x0a,
     297,    9,    9,    9, 0x0a,
     323,    9,    9,    9, 0x0a,
     349,    9,    9,    9, 0x0a,
     375,    9,    9,    9, 0x0a,
     401,    9,    9,    9, 0x0a,
     427,    9,    9,    9, 0x0a,
     453,    9,    9,    9, 0x0a,
     479,    9,    9,    9, 0x0a,
     505,    9,    9,    9, 0x0a,
     531,    9,    9,    9, 0x0a,
     557,    9,    9,    9, 0x0a,
     583,    9,    9,    9, 0x0a,
     609,    9,    9,    9, 0x0a,
     635,    9,    9,    9, 0x0a,
     661,    9,    9,    9, 0x0a,
     687,    9,    9,    9, 0x0a,
     713,    9,    9,    9, 0x0a,
     739,    9,    9,    9, 0x0a,
     765,    9,    9,    9, 0x0a,
     796,    9,    9,    9, 0x0a,
     827,    9,    9,    9, 0x0a,
     860,    9,    9,    9, 0x0a,
     893,    9,    9,    9, 0x0a,
     924,    9,    9,    9, 0x0a,
     958,    9,    9,    9, 0x0a,
     987,    9,    9,    9, 0x0a,
    1015,    9,    9,    9, 0x0a,
    1048,    9,    9,    9, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_keyboard[] = {
    "keyboard\0\0returnPressed()\0"
    "setSenderWidge(QPushButton*)\0"
    "write_char(QString)\0remove_char()\0"
    "on_pushButton_a_clicked()\0"
    "on_pushButton_b_clicked()\0"
    "on_pushButton_c_clicked()\0"
    "on_pushButton_d_clicked()\0"
    "on_pushButton_e_clicked()\0"
    "on_pushButton_f_clicked()\0"
    "on_pushButton_g_clicked()\0"
    "on_pushButton_h_clicked()\0"
    "on_pushButton_i_clicked()\0"
    "on_pushButton_j_clicked()\0"
    "on_pushButton_k_clicked()\0"
    "on_pushButton_l_clicked()\0"
    "on_pushButton_m_clicked()\0"
    "on_pushButton_n_clicked()\0"
    "on_pushButton_o_clicked()\0"
    "on_pushButton_p_clicked()\0"
    "on_pushButton_q_clicked()\0"
    "on_pushButton_r_clicked()\0"
    "on_pushButton_s_clicked()\0"
    "on_pushButton_t_clicked()\0"
    "on_pushButton_u_clicked()\0"
    "on_pushButton_v_clicked()\0"
    "on_pushButton_w_clicked()\0"
    "on_pushButton_x_clicked()\0"
    "on_pushButton_y_clicked()\0"
    "on_pushButton_z_clicked()\0"
    "on_pushButton_lshift_clicked()\0"
    "on_pushButton_rshift_clicked()\0"
    "on_pushButton_Lnumbers_clicked()\0"
    "on_pushButton_Rnumbers_clicked()\0"
    "on_pushButton_return_clicked()\0"
    "on_pushButton_backspace_clicked()\0"
    "on_pushButton_lang_clicked()\0"
    "on_pushButton_mic_clicked()\0"
    "on_pushButton_spacebar_clicked()\0"
    "setKeyMap(KEYMAP)\0"
};

void keyboard::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        keyboard *_t = static_cast<keyboard *>(_o);
        switch (_id) {
        case 0: _t->returnPressed(); break;
        case 1: _t->setSenderWidge((*reinterpret_cast< QPushButton*(*)>(_a[1]))); break;
        case 2: _t->write_char((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->remove_char(); break;
        case 4: _t->on_pushButton_a_clicked(); break;
        case 5: _t->on_pushButton_b_clicked(); break;
        case 6: _t->on_pushButton_c_clicked(); break;
        case 7: _t->on_pushButton_d_clicked(); break;
        case 8: _t->on_pushButton_e_clicked(); break;
        case 9: _t->on_pushButton_f_clicked(); break;
        case 10: _t->on_pushButton_g_clicked(); break;
        case 11: _t->on_pushButton_h_clicked(); break;
        case 12: _t->on_pushButton_i_clicked(); break;
        case 13: _t->on_pushButton_j_clicked(); break;
        case 14: _t->on_pushButton_k_clicked(); break;
        case 15: _t->on_pushButton_l_clicked(); break;
        case 16: _t->on_pushButton_m_clicked(); break;
        case 17: _t->on_pushButton_n_clicked(); break;
        case 18: _t->on_pushButton_o_clicked(); break;
        case 19: _t->on_pushButton_p_clicked(); break;
        case 20: _t->on_pushButton_q_clicked(); break;
        case 21: _t->on_pushButton_r_clicked(); break;
        case 22: _t->on_pushButton_s_clicked(); break;
        case 23: _t->on_pushButton_t_clicked(); break;
        case 24: _t->on_pushButton_u_clicked(); break;
        case 25: _t->on_pushButton_v_clicked(); break;
        case 26: _t->on_pushButton_w_clicked(); break;
        case 27: _t->on_pushButton_x_clicked(); break;
        case 28: _t->on_pushButton_y_clicked(); break;
        case 29: _t->on_pushButton_z_clicked(); break;
        case 30: _t->on_pushButton_lshift_clicked(); break;
        case 31: _t->on_pushButton_rshift_clicked(); break;
        case 32: _t->on_pushButton_Lnumbers_clicked(); break;
        case 33: _t->on_pushButton_Rnumbers_clicked(); break;
        case 34: _t->on_pushButton_return_clicked(); break;
        case 35: _t->on_pushButton_backspace_clicked(); break;
        case 36: _t->on_pushButton_lang_clicked(); break;
        case 37: _t->on_pushButton_mic_clicked(); break;
        case 38: _t->on_pushButton_spacebar_clicked(); break;
        case 39: _t->setKeyMap((*reinterpret_cast< KEYMAP(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData keyboard::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject keyboard::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_keyboard,
      qt_meta_data_keyboard, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &keyboard::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *keyboard::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *keyboard::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_keyboard))
        return static_cast<void*>(const_cast< keyboard*>(this));
    return QWidget::qt_metacast(_clname);
}

int keyboard::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 40)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 40;
    }
    return _id;
}

// SIGNAL 0
void keyboard::returnPressed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
