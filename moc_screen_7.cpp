/****************************************************************************
** Meta object code from reading C++ file 'screen_7.h'
**
** Created: Tue Apr 25 17:38:49 2017
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "screen_7.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'screen_7.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_screen_7[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x05,

 // slots: signature, parameters, type, tag, flags
      23,    9,    9,    9, 0x0a,
      32,    9,    9,    9, 0x0a,
      61,    9,    9,    9, 0x0a,
      90,    9,    9,    9, 0x0a,
     121,    9,    9,    9, 0x0a,
     140,    9,    9,    9, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_screen_7[] = {
    "screen_7\0\0touched(int)\0reload()\0"
    "on_pushButton_home_clicked()\0"
    "on_pushButton_done_clicked()\0"
    "on_pushButton_cancel_clicked()\0"
    "scheduleSelected()\0setCurrentClima(CLIMA_MODBUS)\0"
};

void screen_7::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        screen_7 *_t = static_cast<screen_7 *>(_o);
        switch (_id) {
        case 0: _t->touched((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->reload(); break;
        case 2: _t->on_pushButton_home_clicked(); break;
        case 3: _t->on_pushButton_done_clicked(); break;
        case 4: _t->on_pushButton_cancel_clicked(); break;
        case 5: _t->scheduleSelected(); break;
        case 6: _t->setCurrentClima((*reinterpret_cast< CLIMA_MODBUS(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData screen_7::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject screen_7::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_screen_7,
      qt_meta_data_screen_7, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &screen_7::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *screen_7::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *screen_7::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_screen_7))
        return static_cast<void*>(const_cast< screen_7*>(this));
    return QWidget::qt_metacast(_clname);
}

int screen_7::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void screen_7::touched(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
