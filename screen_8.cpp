#include "screen_8.h"
#include "ui_screen_8.h"

screen_8::screen_8(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_8),
    _datamgr(dm)
{
    ui->setupUi(this);
    ui->label_temp_value->setNum(20);
    ui->label_mins_value->setNum(30);
    ui->label_hours_value->setNum(12);
}

screen_8::~screen_8()
{
    delete ui;
}

void screen_8::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_8::on_pushButton_done_clicked()
{
    Q_EMIT(touched(9));
}

void screen_8::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(5));
}

void screen_8::on_pushButton_temp_up_clicked()
{
    int value = ui->label_temp_value->text().toInt();
    ui->label_temp_value->setNum(value + 1);
}

void screen_8::on_pushButton_temp_down_clicked()
{
    int value = ui->label_temp_value->text().toInt();
    ui->label_temp_value->setNum(value - 1);
}

void screen_8::on_pushButton_hours_up_clicked()
{
    int value = ui->label_hours_value->text().toInt();
    ui->label_hours_value->setNum(value + 1);
}

void screen_8::on_pushButton_hours_down_clicked()
{
    int value = ui->label_hours_value->text().toInt();
    ui->label_hours_value->setNum(value - 1);
}

void screen_8::on_pushButton_mins_up_clicked()
{
    int value = ui->label_mins_value->text().toInt();
    ui->label_mins_value->setNum(value + 1);
}

void screen_8::on_pushButton_mins_down_clicked()
{
    int value = ui->label_mins_value->text().toInt();
    ui->label_mins_value->setNum(value - 1);
}
