/****************************************************************************
** Meta object code from reading C++ file 'screen_11.h'
**
** Created: Tue Apr 25 16:09:35 2017
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "screen_11.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'screen_11.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_screen_11[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x05,

 // slots: signature, parameters, type, tag, flags
      24,   10,   10,   10, 0x0a,
      53,   10,   10,   10, 0x0a,
      82,   10,   10,   10, 0x0a,
     113,   10,   10,   10, 0x0a,
     148,   10,   10,   10, 0x0a,
     185,   10,   10,   10, 0x0a,
     221,   10,   10,   10, 0x0a,
     259,   10,   10,   10, 0x0a,
     292,   10,   10,   10, 0x0a,
     327,   10,   10,   10, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_screen_11[] = {
    "screen_11\0\0touched(int)\0"
    "on_pushButton_home_clicked()\0"
    "on_pushButton_done_clicked()\0"
    "on_pushButton_cancel_clicked()\0"
    "on_pushButton_comfort_up_clicked()\0"
    "on_pushButton_comfort_down_clicked()\0"
    "on_pushButton_humidity_up_clicked()\0"
    "on_pushButton_humidity_down_clicked()\0"
    "on_pushButton_night_up_clicked()\0"
    "on_pushButton_night_down_clicked()\0"
    "setCurrentClima(CLIMA_MODBUS)\0"
};

void screen_11::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        screen_11 *_t = static_cast<screen_11 *>(_o);
        switch (_id) {
        case 0: _t->touched((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->on_pushButton_home_clicked(); break;
        case 2: _t->on_pushButton_done_clicked(); break;
        case 3: _t->on_pushButton_cancel_clicked(); break;
        case 4: _t->on_pushButton_comfort_up_clicked(); break;
        case 5: _t->on_pushButton_comfort_down_clicked(); break;
        case 6: _t->on_pushButton_humidity_up_clicked(); break;
        case 7: _t->on_pushButton_humidity_down_clicked(); break;
        case 8: _t->on_pushButton_night_up_clicked(); break;
        case 9: _t->on_pushButton_night_down_clicked(); break;
        case 10: _t->setCurrentClima((*reinterpret_cast< CLIMA_MODBUS(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData screen_11::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject screen_11::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_screen_11,
      qt_meta_data_screen_11, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &screen_11::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *screen_11::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *screen_11::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_screen_11))
        return static_cast<void*>(const_cast< screen_11*>(this));
    return QWidget::qt_metacast(_clname);
}

int screen_11::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void screen_11::touched(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
