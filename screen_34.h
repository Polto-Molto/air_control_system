#ifndef SCREEN_34_H
#define SCREEN_34_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_34;
}

class screen_34 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_34(dataManager*,QWidget *parent = 0);
    ~screen_34();
signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_done_clicked();

private:
    Ui::screen_34 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_34_H
