/********************************************************************************
** Form generated from reading UI file 'screen_9.ui'
**
** Created: Thu Apr 13 00:46:20 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREEN_9_H
#define UI_SCREEN_9_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_screen_9
{
public:
    QGridLayout *gridLayout;
    QFrame *frame_header;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QFrame *frame_body;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_3;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer;
    QLabel *label_2;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *pushButton_apply_only;
    QPushButton *pushButton_apply_all;
    QSpacerItem *verticalSpacer_3;
    QSpacerItem *horizontalSpacer_4;

    void setupUi(QWidget *screen_9)
    {
        if (screen_9->objectName().isEmpty())
            screen_9->setObjectName(QString::fromUtf8("screen_9"));
        screen_9->resize(1024, 600);
        screen_9->setMinimumSize(QSize(1024, 600));
        screen_9->setMaximumSize(QSize(1042, 630));
        gridLayout = new QGridLayout(screen_9);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame_header = new QFrame(screen_9);
        frame_header->setObjectName(QString::fromUtf8("frame_header"));
        frame_header->setMinimumSize(QSize(1024, 50));
        frame_header->setMaximumSize(QSize(1024, 50));
        frame_header->setFrameShape(QFrame::StyledPanel);
        frame_header->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_header);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(frame_header);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(14);
        label->setFont(font);

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        gridLayout->addWidget(frame_header, 0, 0, 1, 1);

        frame_body = new QFrame(screen_9);
        frame_body->setObjectName(QString::fromUtf8("frame_body"));
        frame_body->setMinimumSize(QSize(1024, 550));
        frame_body->setMaximumSize(QSize(1024, 550));
        frame_body->setFrameShape(QFrame::StyledPanel);
        frame_body->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_body);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_3 = new QSpacerItem(167, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalSpacer = new QSpacerItem(20, 80, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout->addItem(verticalSpacer);

        label_2 = new QLabel(frame_body);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(650, 80));
        label_2->setMaximumSize(QSize(650, 80));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Sans Serif"));
        font1.setPointSize(18);
        label_2->setFont(font1);
        label_2->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
""));
        label_2->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_2);

        verticalSpacer_2 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout->addItem(verticalSpacer_2);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(20);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        pushButton_apply_only = new QPushButton(frame_body);
        pushButton_apply_only->setObjectName(QString::fromUtf8("pushButton_apply_only"));
        pushButton_apply_only->setMinimumSize(QSize(280, 130));
        pushButton_apply_only->setMaximumSize(QSize(280, 130));
        pushButton_apply_only->setFont(font1);
        pushButton_apply_only->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
""));
        pushButton_apply_only->setIconSize(QSize(50, 50));
        pushButton_apply_only->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_apply_only);

        pushButton_apply_all = new QPushButton(frame_body);
        pushButton_apply_all->setObjectName(QString::fromUtf8("pushButton_apply_all"));
        pushButton_apply_all->setMinimumSize(QSize(280, 130));
        pushButton_apply_all->setMaximumSize(QSize(280, 130));
        pushButton_apply_all->setFont(font1);
        pushButton_apply_all->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
""));
        pushButton_apply_all->setIconSize(QSize(50, 50));
        pushButton_apply_all->setFlat(true);

        horizontalLayout_2->addWidget(pushButton_apply_all);


        verticalLayout->addLayout(horizontalLayout_2);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_3);


        horizontalLayout_3->addLayout(verticalLayout);

        horizontalSpacer_4 = new QSpacerItem(167, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_4);


        gridLayout->addWidget(frame_body, 1, 0, 1, 1);


        retranslateUi(screen_9);

        QMetaObject::connectSlotsByName(screen_9);
    } // setupUi

    void retranslateUi(QWidget *screen_9)
    {
        screen_9->setWindowTitle(QApplication::translate("screen_9", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("screen_9", "Manual temperature saved", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("screen_9", "Would you like to apply this manual temperature\n"
"to all room?", 0, QApplication::UnicodeUTF8));
        pushButton_apply_only->setText(QApplication::translate("screen_9", "Apply only to\n"
"this room", 0, QApplication::UnicodeUTF8));
        pushButton_apply_all->setText(QApplication::translate("screen_9", "Apply to all\n"
"room", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class screen_9: public Ui_screen_9 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREEN_9_H
