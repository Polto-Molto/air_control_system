#ifndef SCREEN_52_H
#define SCREEN_52_H

#include <QWidget>
#include "datamanager.h"
#include "modbus_manager.h"
#include <QLabel>

namespace Ui {
class screen_52;
}

class screen_52 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_52(dataManager*,modbus_manager * ,QWidget *parent = 0);
    ~screen_52();
signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_cancel_clicked();
      void on_pushButton_done_clicked();

      void setNotifications();
private:
    Ui::screen_52 *ui;
    dataManager *_datamgr;
    modbus_manager *_mbus;

    QVector<QLabel*> messages;

};

#endif // SCREEN_52_H
