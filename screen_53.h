#ifndef SCREEN_53_H
#define SCREEN_53_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_53;
}

class screen_53 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_53(dataManager*,QWidget *parent = 0);
    ~screen_53();
signals:
      void touched(int);

private:
    Ui::screen_53 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_53_H
