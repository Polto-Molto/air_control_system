/********************************************************************************
** Form generated from reading UI file 'screen_7.ui'
**
** Created: Thu Apr 13 12:57:08 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREEN_7_H
#define UI_SCREEN_7_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_screen_7
{
public:
    QGridLayout *gridLayout;
    QFrame *frame_header;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QFrame *frame_body;
    QGridLayout *gridLayout_2;
    QPushButton *schedule_3;
    QPushButton *schedule_2;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *schedule_1;
    QPushButton *schedule_5;
    QSpacerItem *verticalSpacer;
    QPushButton *schedule_6;
    QPushButton *schedule_4;
    QSpacerItem *horizontalSpacer_4;
    QFrame *frame_footer;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_15;
    QPushButton *pushButton_cancel;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *pushButton_home;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *pushButton_done;
    QSpacerItem *horizontalSpacer_16;

    void setupUi(QWidget *screen_7)
    {
        if (screen_7->objectName().isEmpty())
            screen_7->setObjectName(QString::fromUtf8("screen_7"));
        screen_7->resize(1024, 600);
        screen_7->setMinimumSize(QSize(1024, 600));
        screen_7->setMaximumSize(QSize(1024, 600));
        gridLayout = new QGridLayout(screen_7);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame_header = new QFrame(screen_7);
        frame_header->setObjectName(QString::fromUtf8("frame_header"));
        frame_header->setMinimumSize(QSize(1024, 50));
        frame_header->setMaximumSize(QSize(1024, 50));
        frame_header->setFrameShape(QFrame::StyledPanel);
        frame_header->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_header);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(frame_header);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(14);
        label->setFont(font);

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        gridLayout->addWidget(frame_header, 0, 0, 1, 1);

        frame_body = new QFrame(screen_7);
        frame_body->setObjectName(QString::fromUtf8("frame_body"));
        frame_body->setMinimumSize(QSize(1024, 470));
        frame_body->setMaximumSize(QSize(1024, 470));
        frame_body->setFrameShape(QFrame::StyledPanel);
        frame_body->setFrameShadow(QFrame::Raised);
        gridLayout_2 = new QGridLayout(frame_body);
        gridLayout_2->setSpacing(20);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(-1, 20, -1, -1);
        schedule_3 = new QPushButton(frame_body);
        schedule_3->setObjectName(QString::fromUtf8("schedule_3"));
        schedule_3->setMinimumSize(QSize(440, 80));
        schedule_3->setMaximumSize(QSize(440, 80));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Sans Serif"));
        font1.setPointSize(18);
        schedule_3->setFont(font1);
        schedule_3->setStyleSheet(QString::fromUtf8("QPushButton::checked {\n"
"background-color: rgb(217, 229, 255);\n"
"}\n"
"QPushButton{\n"
"border-style: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"text-align:left;\n"
"padding:20px;\n"
"}"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/images/Icons/program icon.png"), QSize(), QIcon::Normal, QIcon::Off);
        schedule_3->setIcon(icon);
        schedule_3->setIconSize(QSize(50, 50));
        schedule_3->setCheckable(true);
        schedule_3->setChecked(false);
        schedule_3->setFlat(true);

        gridLayout_2->addWidget(schedule_3, 1, 1, 1, 1);

        schedule_2 = new QPushButton(frame_body);
        schedule_2->setObjectName(QString::fromUtf8("schedule_2"));
        schedule_2->setMinimumSize(QSize(440, 80));
        schedule_2->setMaximumSize(QSize(440, 80));
        schedule_2->setFont(font1);
        schedule_2->setStyleSheet(QString::fromUtf8("QPushButton::checked {\n"
"background-color: rgb(217, 229, 255);\n"
"}\n"
"QPushButton{\n"
"border-style: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"text-align:left;\n"
"padding:20px;\n"
"}"));
        schedule_2->setIcon(icon);
        schedule_2->setIconSize(QSize(50, 50));
        schedule_2->setCheckable(true);
        schedule_2->setChecked(false);
        schedule_2->setFlat(true);

        gridLayout_2->addWidget(schedule_2, 0, 2, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_3, 1, 0, 1, 1);

        schedule_1 = new QPushButton(frame_body);
        schedule_1->setObjectName(QString::fromUtf8("schedule_1"));
        schedule_1->setMinimumSize(QSize(440, 80));
        schedule_1->setMaximumSize(QSize(440, 80));
        schedule_1->setFont(font1);
        schedule_1->setStyleSheet(QString::fromUtf8("QPushButton::checked {\n"
"background-color: rgb(217, 229, 255);\n"
"}\n"
"QPushButton{\n"
"border-style: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"text-align:left;\n"
"padding:20px;\n"
"}"));
        schedule_1->setIcon(icon);
        schedule_1->setIconSize(QSize(50, 50));
        schedule_1->setCheckable(true);
        schedule_1->setChecked(false);
        schedule_1->setFlat(true);

        gridLayout_2->addWidget(schedule_1, 0, 1, 1, 1);

        schedule_5 = new QPushButton(frame_body);
        schedule_5->setObjectName(QString::fromUtf8("schedule_5"));
        schedule_5->setMinimumSize(QSize(440, 80));
        schedule_5->setMaximumSize(QSize(440, 80));
        schedule_5->setFont(font1);
        schedule_5->setStyleSheet(QString::fromUtf8("QPushButton::checked {\n"
"background-color: rgb(217, 229, 255);\n"
"}\n"
"QPushButton{\n"
"border-style: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"text-align:left;\n"
"padding:20px;\n"
"}"));
        schedule_5->setIcon(icon);
        schedule_5->setIconSize(QSize(50, 50));
        schedule_5->setCheckable(true);
        schedule_5->setChecked(false);
        schedule_5->setFlat(true);

        gridLayout_2->addWidget(schedule_5, 2, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer, 3, 1, 1, 1);

        schedule_6 = new QPushButton(frame_body);
        schedule_6->setObjectName(QString::fromUtf8("schedule_6"));
        schedule_6->setMinimumSize(QSize(440, 80));
        schedule_6->setMaximumSize(QSize(440, 80));
        schedule_6->setFont(font1);
        schedule_6->setStyleSheet(QString::fromUtf8("QPushButton::checked {\n"
"background-color: rgb(217, 229, 255);\n"
"}\n"
"QPushButton{\n"
"border-style: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"text-align:left;\n"
"padding:20px;\n"
"}"));
        schedule_6->setIcon(icon);
        schedule_6->setIconSize(QSize(50, 50));
        schedule_6->setCheckable(true);
        schedule_6->setChecked(false);
        schedule_6->setFlat(true);

        gridLayout_2->addWidget(schedule_6, 2, 2, 1, 1);

        schedule_4 = new QPushButton(frame_body);
        schedule_4->setObjectName(QString::fromUtf8("schedule_4"));
        schedule_4->setMinimumSize(QSize(440, 80));
        schedule_4->setMaximumSize(QSize(440, 80));
        schedule_4->setFont(font1);
        schedule_4->setStyleSheet(QString::fromUtf8("QPushButton::checked {\n"
"background-color: rgb(217, 229, 255);\n"
"}\n"
"QPushButton{\n"
"border-style: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"text-align:left;\n"
"padding:20px;\n"
"}"));
        schedule_4->setIcon(icon);
        schedule_4->setIconSize(QSize(50, 50));
        schedule_4->setCheckable(true);
        schedule_4->setChecked(false);
        schedule_4->setFlat(true);

        gridLayout_2->addWidget(schedule_4, 1, 2, 1, 1);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_4, 1, 3, 1, 1);


        gridLayout->addWidget(frame_body, 1, 0, 1, 1);

        frame_footer = new QFrame(screen_7);
        frame_footer->setObjectName(QString::fromUtf8("frame_footer"));
        frame_footer->setMinimumSize(QSize(1024, 80));
        frame_footer->setMaximumSize(QSize(1024, 80));
        frame_footer->setFrameShape(QFrame::StyledPanel);
        frame_footer->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_footer);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_15 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_15);

        pushButton_cancel = new QPushButton(frame_footer);
        pushButton_cancel->setObjectName(QString::fromUtf8("pushButton_cancel"));
        pushButton_cancel->setMinimumSize(QSize(50, 50));
        pushButton_cancel->setMaximumSize(QSize(50, 50));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/images/Icons/Cross.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_cancel->setIcon(icon1);
        pushButton_cancel->setIconSize(QSize(50, 50));
        pushButton_cancel->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_cancel);

        horizontalSpacer_5 = new QSpacerItem(353, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);

        pushButton_home = new QPushButton(frame_footer);
        pushButton_home->setObjectName(QString::fromUtf8("pushButton_home"));
        pushButton_home->setMinimumSize(QSize(50, 50));
        pushButton_home->setMaximumSize(QSize(50, 50));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/images/Icons/home.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_home->setIcon(icon2);
        pushButton_home->setIconSize(QSize(50, 50));
        pushButton_home->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_home);

        horizontalSpacer_6 = new QSpacerItem(353, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);

        pushButton_done = new QPushButton(frame_footer);
        pushButton_done->setObjectName(QString::fromUtf8("pushButton_done"));
        pushButton_done->setMinimumSize(QSize(50, 50));
        pushButton_done->setMaximumSize(QSize(50, 50));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/images/Icons/check.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_done->setIcon(icon3);
        pushButton_done->setIconSize(QSize(50, 50));
        pushButton_done->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_done);

        horizontalSpacer_16 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_16);


        gridLayout->addWidget(frame_footer, 2, 0, 1, 1);


        retranslateUi(screen_7);

        QMetaObject::connectSlotsByName(screen_7);
    } // setupUi

    void retranslateUi(QWidget *screen_7)
    {
        screen_7->setWindowTitle(QApplication::translate("screen_7", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("screen_7", "1- Kids Room - Assign schedule", 0, QApplication::UnicodeUTF8));
        schedule_3->setText(QApplication::translate("screen_7", "Work", 0, QApplication::UnicodeUTF8));
        schedule_2->setText(QApplication::translate("screen_7", "Program 1", 0, QApplication::UnicodeUTF8));
        schedule_1->setText(QApplication::translate("screen_7", "Program 1", 0, QApplication::UnicodeUTF8));
        schedule_5->setText(QApplication::translate("screen_7", "Anti Freeze", 0, QApplication::UnicodeUTF8));
        schedule_6->setText(QApplication::translate("screen_7", "Anti Freeze", 0, QApplication::UnicodeUTF8));
        schedule_4->setText(QApplication::translate("screen_7", "Work", 0, QApplication::UnicodeUTF8));
        pushButton_home->setText(QString());
        pushButton_done->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class screen_7: public Ui_screen_7 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREEN_7_H
