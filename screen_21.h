#ifndef SCREEN_21_H
#define SCREEN_21_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_21;
}

class screen_21 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_21(dataManager*,QWidget *parent = 0);
    ~screen_21();

signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_cancel_clicked();

      void on_pushButton_advanced_clicked();
      void on_pushButton_datetime_clicked();
      void on_pushButton_info_clicked();
      void on_pushButton_display_clicked();
      void on_pushButton_language_clicked();

private:
    Ui::screen_21 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_21_H
