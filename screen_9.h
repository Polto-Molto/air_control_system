#ifndef SCREEN_9_H
#define SCREEN_9_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_9;
}

class screen_9 : public QWidget
{
    Q_OBJECT

public:
    explicit screen_9(dataManager*,QWidget *parent = 0);
    ~screen_9();
signals:
      void touched(int);

public slots:
    void on_pushButton_apply_only_clicked();
    void on_pushButton_apply_all_clicked();

private:
    Ui::screen_9 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_9_H
