#include "screen_33.h"
#include "ui_screen_33.h"

screen_33::screen_33(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_33),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_33::~screen_33()
{
    delete ui;
}

void screen_33::on_pushButton_keyboard_clicked()
{
    Q_EMIT(showkeyboard(ui->pushButton_keyboard));

}

QString screen_33::password()
{
    QString pass = ui->pushButton_keyboard->text();
    ui->pushButton_keyboard->setText("");
    return pass;
}
