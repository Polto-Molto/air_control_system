#ifndef SCREEN_5_H
#define SCREEN_5_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_5;
}

class screen_5 : public QWidget
{
    Q_OBJECT

public:
    explicit screen_5(dataManager*,QWidget *parent = 0);
    ~screen_5();

public slots:
    void reload();

    void on_pushButton_home_clicked();
    void on_toolButton_schedule_clicked();
    void on_pushButton_back_clicked();
    void on_pushButton_cancel_clicked();
    void on_pushButton_eco_comfort_clicked();
    void on_pushButton_next_clicked();
    void on_pushButton_touch_clicked();

    void setCurrentClima(CLIMA_MODBUS);

signals:
    void selectedWinterClima(CLIMA_MODBUS);
    void selectedSummerClima(CLIMA_MODBUS);
    void selectedScheduleClima(CLIMA_MODBUS);
    void selectedClima(CLIMA_MODBUS);
    void touched(int);

private:
    Ui::screen_5 *ui;
    dataManager *_datamgr;
    CLIMA_MODBUS _clima;
};

#endif // SCREEN_5_H
