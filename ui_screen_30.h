/********************************************************************************
** Form generated from reading UI file 'screen_30.ui'
**
** Created: Thu Apr 13 00:46:21 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREEN_30_H
#define UI_SCREEN_30_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_screen_30
{
public:
    QGridLayout *gridLayout;
    QFrame *frame_footer;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_15;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *pushButton_home;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *pushButton_done;
    QSpacerItem *horizontalSpacer_16;
    QFrame *frame_header;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QFrame *frame_body;
    QGridLayout *gridLayout_2;
    QSpacerItem *horizontalSpacer_3;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *pushButton_vmc_down;
    QLabel *label_vmc_value;
    QPushButton *pushButton_vmc_up;
    QSpacerItem *verticalSpacer_2;
    QLabel *label_3;
    QSpacerItem *verticalSpacer;
    QLabel *label_2;
    QLabel *label_4;
    QLabel *label_5;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *pushButton_temp_down;
    QLabel *label_temp_value;
    QPushButton *pushButton_temp_up;
    QSpacerItem *horizontalSpacer_4;

    void setupUi(QWidget *screen_30)
    {
        if (screen_30->objectName().isEmpty())
            screen_30->setObjectName(QString::fromUtf8("screen_30"));
        screen_30->resize(1024, 600);
        screen_30->setMinimumSize(QSize(1024, 600));
        screen_30->setMaximumSize(QSize(1024, 600));
        gridLayout = new QGridLayout(screen_30);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame_footer = new QFrame(screen_30);
        frame_footer->setObjectName(QString::fromUtf8("frame_footer"));
        frame_footer->setMinimumSize(QSize(1024, 80));
        frame_footer->setMaximumSize(QSize(1024, 80));
        frame_footer->setFrameShape(QFrame::StyledPanel);
        frame_footer->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_footer);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_15 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_15);

        horizontalSpacer_5 = new QSpacerItem(415, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);

        pushButton_home = new QPushButton(frame_footer);
        pushButton_home->setObjectName(QString::fromUtf8("pushButton_home"));
        pushButton_home->setMinimumSize(QSize(50, 50));
        pushButton_home->setMaximumSize(QSize(50, 50));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/images/Icons/home.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_home->setIcon(icon);
        pushButton_home->setIconSize(QSize(50, 50));
        pushButton_home->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_home);

        horizontalSpacer_6 = new QSpacerItem(353, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);

        pushButton_done = new QPushButton(frame_footer);
        pushButton_done->setObjectName(QString::fromUtf8("pushButton_done"));
        pushButton_done->setMinimumSize(QSize(50, 50));
        pushButton_done->setMaximumSize(QSize(50, 50));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/images/Icons/check.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_done->setIcon(icon1);
        pushButton_done->setIconSize(QSize(50, 50));
        pushButton_done->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_done);

        horizontalSpacer_16 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_16);


        gridLayout->addWidget(frame_footer, 2, 0, 1, 1);

        frame_header = new QFrame(screen_30);
        frame_header->setObjectName(QString::fromUtf8("frame_header"));
        frame_header->setMinimumSize(QSize(1024, 50));
        frame_header->setMaximumSize(QSize(1024, 50));
        frame_header->setFrameShape(QFrame::StyledPanel);
        frame_header->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_header);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(frame_header);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(14);
        label->setFont(font);

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        gridLayout->addWidget(frame_header, 0, 0, 1, 1);

        frame_body = new QFrame(screen_30);
        frame_body->setObjectName(QString::fromUtf8("frame_body"));
        frame_body->setMinimumSize(QSize(1024, 470));
        frame_body->setMaximumSize(QSize(1024, 470));
        frame_body->setFrameShape(QFrame::StyledPanel);
        frame_body->setFrameShadow(QFrame::Raised);
        gridLayout_2 = new QGridLayout(frame_body);
        gridLayout_2->setSpacing(40);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_3, 1, 0, 1, 1);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        pushButton_vmc_down = new QPushButton(frame_body);
        pushButton_vmc_down->setObjectName(QString::fromUtf8("pushButton_vmc_down"));
        pushButton_vmc_down->setMinimumSize(QSize(50, 50));
        pushButton_vmc_down->setMaximumSize(QSize(50, 50));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/images/Icons/minus.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_vmc_down->setIcon(icon2);
        pushButton_vmc_down->setIconSize(QSize(50, 50));
        pushButton_vmc_down->setFlat(true);

        horizontalLayout_6->addWidget(pushButton_vmc_down);

        label_vmc_value = new QLabel(frame_body);
        label_vmc_value->setObjectName(QString::fromUtf8("label_vmc_value"));
        label_vmc_value->setMinimumSize(QSize(130, 85));
        label_vmc_value->setMaximumSize(QSize(130, 85));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Sans Serif"));
        font1.setPointSize(42);
        label_vmc_value->setFont(font1);
        label_vmc_value->setFrameShape(QFrame::Box);
        label_vmc_value->setAlignment(Qt::AlignCenter);

        horizontalLayout_6->addWidget(label_vmc_value);

        pushButton_vmc_up = new QPushButton(frame_body);
        pushButton_vmc_up->setObjectName(QString::fromUtf8("pushButton_vmc_up"));
        pushButton_vmc_up->setMinimumSize(QSize(50, 50));
        pushButton_vmc_up->setMaximumSize(QSize(50, 50));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/images/Icons/Plus.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_vmc_up->setIcon(icon3);
        pushButton_vmc_up->setIconSize(QSize(50, 50));
        pushButton_vmc_up->setFlat(true);

        horizontalLayout_6->addWidget(pushButton_vmc_up);


        gridLayout_2->addLayout(horizontalLayout_6, 2, 2, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout_2->addItem(verticalSpacer_2, 0, 2, 1, 1);

        label_3 = new QLabel(frame_body);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_2->addWidget(label_3, 2, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer, 3, 2, 1, 1);

        label_2 = new QLabel(frame_body);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_2->addWidget(label_2, 1, 1, 1, 1);

        label_4 = new QLabel(frame_body);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Sans Serif"));
        font2.setPointSize(22);
        label_4->setFont(font2);

        gridLayout_2->addWidget(label_4, 1, 3, 1, 1);

        label_5 = new QLabel(frame_body);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setFont(font2);

        gridLayout_2->addWidget(label_5, 2, 3, 1, 1);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        pushButton_temp_down = new QPushButton(frame_body);
        pushButton_temp_down->setObjectName(QString::fromUtf8("pushButton_temp_down"));
        pushButton_temp_down->setMinimumSize(QSize(50, 50));
        pushButton_temp_down->setMaximumSize(QSize(50, 50));
        pushButton_temp_down->setIcon(icon2);
        pushButton_temp_down->setIconSize(QSize(50, 50));
        pushButton_temp_down->setFlat(true);

        horizontalLayout_5->addWidget(pushButton_temp_down);

        label_temp_value = new QLabel(frame_body);
        label_temp_value->setObjectName(QString::fromUtf8("label_temp_value"));
        label_temp_value->setMinimumSize(QSize(130, 85));
        label_temp_value->setMaximumSize(QSize(130, 85));
        label_temp_value->setFont(font1);
        label_temp_value->setFrameShape(QFrame::Box);
        label_temp_value->setAlignment(Qt::AlignCenter);

        horizontalLayout_5->addWidget(label_temp_value);

        pushButton_temp_up = new QPushButton(frame_body);
        pushButton_temp_up->setObjectName(QString::fromUtf8("pushButton_temp_up"));
        pushButton_temp_up->setMinimumSize(QSize(50, 50));
        pushButton_temp_up->setMaximumSize(QSize(50, 50));
        pushButton_temp_up->setIcon(icon3);
        pushButton_temp_up->setIconSize(QSize(50, 50));
        pushButton_temp_up->setFlat(true);

        horizontalLayout_5->addWidget(pushButton_temp_up);


        gridLayout_2->addLayout(horizontalLayout_5, 1, 2, 1, 1);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_4, 1, 4, 1, 1);


        gridLayout->addWidget(frame_body, 1, 0, 1, 1);


        retranslateUi(screen_30);

        QMetaObject::connectSlotsByName(screen_30);
    } // setupUi

    void retranslateUi(QWidget *screen_30)
    {
        screen_30->setWindowTitle(QApplication::translate("screen_30", "Form", 0, QApplication::UnicodeUTF8));
        pushButton_home->setText(QString());
        pushButton_done->setText(QString());
        label->setText(QApplication::translate("screen_30", "Devices to be shown on standby", 0, QApplication::UnicodeUTF8));
        pushButton_vmc_down->setText(QString());
        label_vmc_value->setText(QString());
        pushButton_vmc_up->setText(QString());
        label_3->setText(QApplication::translate("screen_30", "VMC integra device number", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("screen_30", "Temperature device number", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("screen_30", "Kids Room", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("screen_30", "First Room", 0, QApplication::UnicodeUTF8));
        pushButton_temp_down->setText(QString());
        label_temp_value->setText(QString());
        pushButton_temp_up->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class screen_30: public Ui_screen_30 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREEN_30_H
