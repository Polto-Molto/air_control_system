#ifndef SCREEN_42_H
#define SCREEN_42_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_42;
}

class screen_42 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_42(dataManager*,QWidget *parent = 0);
    ~screen_42();
signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_cancel_clicked();
      void on_pushButton_back_clicked();
      void on_pushButton_test_clicked();

private:
    Ui::screen_42 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_42_H
