/****************************************************************************
** Meta object code from reading C++ file 'keypad.h'
**
** Created: Thu Apr 13 00:48:42 2017
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "keypad.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'keypad.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_keypad[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
       8,    7,    7,    7, 0x05,

 // slots: signature, parameters, type, tag, flags
      24,    7,    7,    7, 0x0a,
      53,    7,    7,    7, 0x0a,
      73,    7,    7,    7, 0x0a,
      87,    7,    7,    7, 0x0a,
     113,    7,    7,    7, 0x0a,
     139,    7,    7,    7, 0x0a,
     165,    7,    7,    7, 0x0a,
     191,    7,    7,    7, 0x0a,
     217,    7,    7,    7, 0x0a,
     243,    7,    7,    7, 0x0a,
     269,    7,    7,    7, 0x0a,
     295,    7,    7,    7, 0x0a,
     321,    7,    7,    7, 0x0a,
     347,    7,    7,    7, 0x0a,
     377,    7,    7,    7, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_keypad[] = {
    "keypad\0\0returnPressed()\0"
    "setSenderWidge(QPushButton*)\0"
    "write_char(QString)\0remove_char()\0"
    "on_pushButton_0_clicked()\0"
    "on_pushButton_1_clicked()\0"
    "on_pushButton_2_clicked()\0"
    "on_pushButton_3_clicked()\0"
    "on_pushButton_4_clicked()\0"
    "on_pushButton_5_clicked()\0"
    "on_pushButton_6_clicked()\0"
    "on_pushButton_7_clicked()\0"
    "on_pushButton_8_clicked()\0"
    "on_pushButton_9_clicked()\0"
    "on_pushButton_enter_clicked()\0"
    "on_pushButton_back_clicked()\0"
};

void keypad::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        keypad *_t = static_cast<keypad *>(_o);
        switch (_id) {
        case 0: _t->returnPressed(); break;
        case 1: _t->setSenderWidge((*reinterpret_cast< QPushButton*(*)>(_a[1]))); break;
        case 2: _t->write_char((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->remove_char(); break;
        case 4: _t->on_pushButton_0_clicked(); break;
        case 5: _t->on_pushButton_1_clicked(); break;
        case 6: _t->on_pushButton_2_clicked(); break;
        case 7: _t->on_pushButton_3_clicked(); break;
        case 8: _t->on_pushButton_4_clicked(); break;
        case 9: _t->on_pushButton_5_clicked(); break;
        case 10: _t->on_pushButton_6_clicked(); break;
        case 11: _t->on_pushButton_7_clicked(); break;
        case 12: _t->on_pushButton_8_clicked(); break;
        case 13: _t->on_pushButton_9_clicked(); break;
        case 14: _t->on_pushButton_enter_clicked(); break;
        case 15: _t->on_pushButton_back_clicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData keypad::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject keypad::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_keypad,
      qt_meta_data_keypad, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &keypad::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *keypad::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *keypad::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_keypad))
        return static_cast<void*>(const_cast< keypad*>(this));
    return QWidget::qt_metacast(_clname);
}

int keypad::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    }
    return _id;
}

// SIGNAL 0
void keypad::returnPressed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
