#include "screen_44.h"
#include "ui_screen_44.h"

screen_44::screen_44(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_44),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_44::~screen_44()
{
    delete ui;
}

void screen_44::on_pushButton_keyboard_clicked()
{
    Q_EMIT(showkeyboard(ui->pushButton_keyboard));

}
