/********************************************************************************
** Form generated from reading UI file 'screen_38.ui'
**
** Created: Thu Apr 13 00:46:21 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREEN_38_H
#define UI_SCREEN_38_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_screen_38
{
public:
    QGridLayout *gridLayout;
    QFrame *frame_header;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QFrame *frame_body;
    QGridLayout *gridLayout_2;
    QSpacerItem *horizontalSpacer_10;
    QFrame *frame_2;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_7;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_4;
    QLabel *label_name_2;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *pushButton_night_down;
    QLabel *label_night_value;
    QPushButton *pushButton_night_up;
    QSpacerItem *verticalSpacer_2;
    QSpacerItem *horizontalSpacer_8;
    QSpacerItem *horizontalSpacer_9;
    QFrame *frame;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_3;
    QVBoxLayout *verticalLayout;
    QLabel *label_3;
    QLabel *label_name;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *pushButton_comfort_down;
    QLabel *label_comfort_value;
    QPushButton *pushButton_comfort_up;
    QSpacerItem *verticalSpacer;
    QSpacerItem *horizontalSpacer_4;
    QSpacerItem *verticalSpacer_3;
    QSpacerItem *verticalSpacer_4;
    QFrame *frame_footer;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_15;
    QPushButton *pushButton_cancel;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *pushButton_home;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *pushButton_done;
    QSpacerItem *horizontalSpacer_16;

    void setupUi(QWidget *screen_38)
    {
        if (screen_38->objectName().isEmpty())
            screen_38->setObjectName(QString::fromUtf8("screen_38"));
        screen_38->resize(1024, 600);
        screen_38->setMinimumSize(QSize(1024, 600));
        screen_38->setMaximumSize(QSize(1024, 600));
        gridLayout = new QGridLayout(screen_38);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame_header = new QFrame(screen_38);
        frame_header->setObjectName(QString::fromUtf8("frame_header"));
        frame_header->setMinimumSize(QSize(1024, 50));
        frame_header->setMaximumSize(QSize(1024, 50));
        frame_header->setFrameShape(QFrame::StyledPanel);
        frame_header->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_header);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(frame_header);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(14);
        label->setFont(font);

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        gridLayout->addWidget(frame_header, 0, 0, 1, 1);

        frame_body = new QFrame(screen_38);
        frame_body->setObjectName(QString::fromUtf8("frame_body"));
        frame_body->setMinimumSize(QSize(1024, 470));
        frame_body->setMaximumSize(QSize(1024, 470));
        frame_body->setFrameShape(QFrame::StyledPanel);
        frame_body->setFrameShadow(QFrame::Raised);
        gridLayout_2 = new QGridLayout(frame_body);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setHorizontalSpacing(20);
        horizontalSpacer_10 = new QSpacerItem(49, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_10, 2, 3, 1, 1);

        frame_2 = new QFrame(frame_body);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setMinimumSize(QSize(420, 280));
        frame_2->setMaximumSize(QSize(420, 280));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        horizontalLayout_4 = new QHBoxLayout(frame_2);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer_7 = new QSpacerItem(68, 20, QSizePolicy::Preferred, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_7);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_4 = new QLabel(frame_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/night.png")));
        label_4->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_4);

        label_name_2 = new QLabel(frame_2);
        label_name_2->setObjectName(QString::fromUtf8("label_name_2"));
        label_name_2->setStyleSheet(QString::fromUtf8("font: 18pt Bold \"Sans Serif\";\n"
"color: black;"));
        label_name_2->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_name_2);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        pushButton_night_down = new QPushButton(frame_2);
        pushButton_night_down->setObjectName(QString::fromUtf8("pushButton_night_down"));
        pushButton_night_down->setMinimumSize(QSize(50, 50));
        pushButton_night_down->setMaximumSize(QSize(50, 50));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/images/Icons/minus.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_night_down->setIcon(icon);
        pushButton_night_down->setIconSize(QSize(50, 50));
        pushButton_night_down->setFlat(true);

        horizontalLayout_6->addWidget(pushButton_night_down);

        label_night_value = new QLabel(frame_2);
        label_night_value->setObjectName(QString::fromUtf8("label_night_value"));
        label_night_value->setMinimumSize(QSize(130, 85));
        label_night_value->setMaximumSize(QSize(130, 85));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Sans Serif"));
        font1.setPointSize(42);
        label_night_value->setFont(font1);
        label_night_value->setFrameShape(QFrame::Box);
        label_night_value->setAlignment(Qt::AlignCenter);

        horizontalLayout_6->addWidget(label_night_value);

        pushButton_night_up = new QPushButton(frame_2);
        pushButton_night_up->setObjectName(QString::fromUtf8("pushButton_night_up"));
        pushButton_night_up->setMinimumSize(QSize(50, 50));
        pushButton_night_up->setMaximumSize(QSize(50, 50));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/images/Icons/Plus.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_night_up->setIcon(icon1);
        pushButton_night_up->setIconSize(QSize(50, 50));
        pushButton_night_up->setFlat(true);

        horizontalLayout_6->addWidget(pushButton_night_up);


        verticalLayout_2->addLayout(horizontalLayout_6);

        verticalSpacer_2 = new QSpacerItem(20, 45, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_2->addItem(verticalSpacer_2);


        horizontalLayout_4->addLayout(verticalLayout_2);

        horizontalSpacer_8 = new QSpacerItem(68, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_8);


        gridLayout_2->addWidget(frame_2, 1, 2, 2, 1);

        horizontalSpacer_9 = new QSpacerItem(49, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_9, 1, 0, 1, 1);

        frame = new QFrame(frame_body);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setMinimumSize(QSize(420, 280));
        frame->setMaximumSize(QSize(420, 280));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(frame);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_3 = new QSpacerItem(68, 20, QSizePolicy::Preferred, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label_3 = new QLabel(frame);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/comfrot.png")));
        label_3->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_3);

        label_name = new QLabel(frame);
        label_name->setObjectName(QString::fromUtf8("label_name"));
        label_name->setStyleSheet(QString::fromUtf8("font: 18pt Bold \"Sans Serif\";\n"
"color: black;"));
        label_name->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_name);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        pushButton_comfort_down = new QPushButton(frame);
        pushButton_comfort_down->setObjectName(QString::fromUtf8("pushButton_comfort_down"));
        pushButton_comfort_down->setMinimumSize(QSize(50, 50));
        pushButton_comfort_down->setMaximumSize(QSize(50, 50));
        pushButton_comfort_down->setIcon(icon);
        pushButton_comfort_down->setIconSize(QSize(50, 50));
        pushButton_comfort_down->setFlat(true);

        horizontalLayout_5->addWidget(pushButton_comfort_down);

        label_comfort_value = new QLabel(frame);
        label_comfort_value->setObjectName(QString::fromUtf8("label_comfort_value"));
        label_comfort_value->setMinimumSize(QSize(130, 85));
        label_comfort_value->setMaximumSize(QSize(130, 85));
        label_comfort_value->setFont(font1);
        label_comfort_value->setFrameShape(QFrame::Box);
        label_comfort_value->setAlignment(Qt::AlignCenter);

        horizontalLayout_5->addWidget(label_comfort_value);

        pushButton_comfort_up = new QPushButton(frame);
        pushButton_comfort_up->setObjectName(QString::fromUtf8("pushButton_comfort_up"));
        pushButton_comfort_up->setMinimumSize(QSize(50, 50));
        pushButton_comfort_up->setMaximumSize(QSize(50, 50));
        pushButton_comfort_up->setIcon(icon1);
        pushButton_comfort_up->setIconSize(QSize(50, 50));
        pushButton_comfort_up->setFlat(true);

        horizontalLayout_5->addWidget(pushButton_comfort_up);


        verticalLayout->addLayout(horizontalLayout_5);

        verticalSpacer = new QSpacerItem(20, 45, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout->addItem(verticalSpacer);


        horizontalLayout_2->addLayout(verticalLayout);

        horizontalSpacer_4 = new QSpacerItem(68, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_4);


        gridLayout_2->addWidget(frame, 1, 1, 2, 1);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer_3, 3, 2, 1, 1);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout_2->addItem(verticalSpacer_4, 0, 1, 1, 1);


        gridLayout->addWidget(frame_body, 1, 0, 1, 1);

        frame_footer = new QFrame(screen_38);
        frame_footer->setObjectName(QString::fromUtf8("frame_footer"));
        frame_footer->setMinimumSize(QSize(1024, 80));
        frame_footer->setMaximumSize(QSize(1024, 80));
        frame_footer->setFrameShape(QFrame::StyledPanel);
        frame_footer->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_footer);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_15 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_15);

        pushButton_cancel = new QPushButton(frame_footer);
        pushButton_cancel->setObjectName(QString::fromUtf8("pushButton_cancel"));
        pushButton_cancel->setMinimumSize(QSize(50, 50));
        pushButton_cancel->setMaximumSize(QSize(50, 50));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/images/Icons/Cross.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_cancel->setIcon(icon2);
        pushButton_cancel->setIconSize(QSize(50, 50));
        pushButton_cancel->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_cancel);

        horizontalSpacer_5 = new QSpacerItem(353, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);

        pushButton_home = new QPushButton(frame_footer);
        pushButton_home->setObjectName(QString::fromUtf8("pushButton_home"));
        pushButton_home->setMinimumSize(QSize(50, 50));
        pushButton_home->setMaximumSize(QSize(50, 50));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/images/Icons/home.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_home->setIcon(icon3);
        pushButton_home->setIconSize(QSize(50, 50));
        pushButton_home->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_home);

        horizontalSpacer_6 = new QSpacerItem(353, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);

        pushButton_done = new QPushButton(frame_footer);
        pushButton_done->setObjectName(QString::fromUtf8("pushButton_done"));
        pushButton_done->setMinimumSize(QSize(50, 50));
        pushButton_done->setMaximumSize(QSize(50, 50));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/images/Icons/check.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_done->setIcon(icon4);
        pushButton_done->setIconSize(QSize(50, 50));
        pushButton_done->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_done);

        horizontalSpacer_16 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_16);


        gridLayout->addWidget(frame_footer, 2, 0, 1, 1);


        retranslateUi(screen_38);

        QMetaObject::connectSlotsByName(screen_38);
    } // setupUi

    void retranslateUi(QWidget *screen_38)
    {
        screen_38->setWindowTitle(QApplication::translate("screen_38", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("screen_38", "1- Kids Room - Winter set points Hydronic Unit", 0, QApplication::UnicodeUTF8));
        label_4->setText(QString());
        label_name_2->setText(QApplication::translate("screen_38", "Night", 0, QApplication::UnicodeUTF8));
        pushButton_night_down->setText(QString());
        label_night_value->setText(QString());
        pushButton_night_up->setText(QString());
        label_3->setText(QString());
        label_name->setText(QApplication::translate("screen_38", "Comfort", 0, QApplication::UnicodeUTF8));
        pushButton_comfort_down->setText(QString());
        label_comfort_value->setText(QString());
        pushButton_comfort_up->setText(QString());
        pushButton_home->setText(QString());
        pushButton_done->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class screen_38: public Ui_screen_38 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREEN_38_H
