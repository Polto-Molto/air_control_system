/********************************************************************************
** Form generated from reading UI file 'screen_39.ui'
**
** Created: Thu Apr 13 00:46:21 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREEN_39_H
#define UI_SCREEN_39_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_screen_39
{
public:
    QGridLayout *gridLayout;
    QFrame *frame_header;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QFrame *frame_footer;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_15;
    QPushButton *pushButton_cancel;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *pushButton_home;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *pushButton_done;
    QSpacerItem *horizontalSpacer_16;
    QFrame *frame_body;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QFrame *frame;
    QHBoxLayout *horizontalLayout_7;
    QSpacerItem *horizontalSpacer_9;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_3;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_name;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *pushButton_temp_down;
    QLabel *label_temp_value;
    QPushButton *pushButton_temp_up;
    QSpacerItem *verticalSpacer;
    QSpacerItem *horizontalSpacer_10;
    QFrame *frame_2;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_9;
    QSpacerItem *horizontalSpacer_7;
    QVBoxLayout *verticalLayout_5;
    QLabel *label_name_2;
    QHBoxLayout *horizontalLayout_8;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_6;
    QLabel *label_4;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_7;
    QLabel *label_5;
    QSpacerItem *horizontalSpacer_8;
    QSlider *horizontalSlider;
    QSpacerItem *verticalSpacer_3;
    QFrame *frame_3;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_4;
    QLabel *label_2;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QSpacerItem *horizontalSpacer_3;

    void setupUi(QWidget *screen_39)
    {
        if (screen_39->objectName().isEmpty())
            screen_39->setObjectName(QString::fromUtf8("screen_39"));
        screen_39->resize(1024, 600);
        screen_39->setMinimumSize(QSize(1024, 600));
        screen_39->setMaximumSize(QSize(1024, 600));
        gridLayout = new QGridLayout(screen_39);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame_header = new QFrame(screen_39);
        frame_header->setObjectName(QString::fromUtf8("frame_header"));
        frame_header->setMinimumSize(QSize(1024, 50));
        frame_header->setMaximumSize(QSize(1024, 50));
        frame_header->setFrameShape(QFrame::StyledPanel);
        frame_header->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_header);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(frame_header);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(14);
        label->setFont(font);

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        gridLayout->addWidget(frame_header, 0, 0, 1, 1);

        frame_footer = new QFrame(screen_39);
        frame_footer->setObjectName(QString::fromUtf8("frame_footer"));
        frame_footer->setMinimumSize(QSize(1024, 80));
        frame_footer->setMaximumSize(QSize(1024, 80));
        frame_footer->setFrameShape(QFrame::StyledPanel);
        frame_footer->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_footer);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_15 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_15);

        pushButton_cancel = new QPushButton(frame_footer);
        pushButton_cancel->setObjectName(QString::fromUtf8("pushButton_cancel"));
        pushButton_cancel->setMinimumSize(QSize(50, 50));
        pushButton_cancel->setMaximumSize(QSize(50, 50));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/images/Icons/Cross.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_cancel->setIcon(icon);
        pushButton_cancel->setIconSize(QSize(50, 50));
        pushButton_cancel->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_cancel);

        horizontalSpacer_5 = new QSpacerItem(353, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);

        pushButton_home = new QPushButton(frame_footer);
        pushButton_home->setObjectName(QString::fromUtf8("pushButton_home"));
        pushButton_home->setMinimumSize(QSize(50, 50));
        pushButton_home->setMaximumSize(QSize(50, 50));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/images/Icons/home.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_home->setIcon(icon1);
        pushButton_home->setIconSize(QSize(50, 50));
        pushButton_home->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_home);

        horizontalSpacer_6 = new QSpacerItem(353, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);

        pushButton_done = new QPushButton(frame_footer);
        pushButton_done->setObjectName(QString::fromUtf8("pushButton_done"));
        pushButton_done->setMinimumSize(QSize(50, 50));
        pushButton_done->setMaximumSize(QSize(50, 50));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/images/Icons/check.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_done->setIcon(icon2);
        pushButton_done->setIconSize(QSize(50, 50));
        pushButton_done->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_done);

        horizontalSpacer_16 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_16);


        gridLayout->addWidget(frame_footer, 2, 0, 1, 1);

        frame_body = new QFrame(screen_39);
        frame_body->setObjectName(QString::fromUtf8("frame_body"));
        frame_body->setMinimumSize(QSize(1024, 470));
        frame_body->setMaximumSize(QSize(1024, 470));
        frame_body->setFrameShape(QFrame::StyledPanel);
        frame_body->setFrameShadow(QFrame::Raised);
        layoutWidget = new QWidget(frame_body);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(30, 30, 864, 404));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setSpacing(20);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(20);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        frame = new QFrame(layoutWidget);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setMinimumSize(QSize(420, 280));
        frame->setMaximumSize(QSize(420, 280));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout_7 = new QHBoxLayout(frame);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalSpacer_9 = new QSpacerItem(68, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_9);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_3 = new QLabel(frame);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/hand.png")));
        label_3->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_3);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_name = new QLabel(frame);
        label_name->setObjectName(QString::fromUtf8("label_name"));
        label_name->setStyleSheet(QString::fromUtf8("font: 18pt Bold \"Sans Serif\";\n"
"color: black;"));
        label_name->setAlignment(Qt::AlignCenter);

        horizontalLayout_6->addWidget(label_name);


        verticalLayout_2->addLayout(horizontalLayout_6);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        pushButton_temp_down = new QPushButton(frame);
        pushButton_temp_down->setObjectName(QString::fromUtf8("pushButton_temp_down"));
        pushButton_temp_down->setMinimumSize(QSize(50, 50));
        pushButton_temp_down->setMaximumSize(QSize(50, 50));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/images/Icons/minus.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_temp_down->setIcon(icon3);
        pushButton_temp_down->setIconSize(QSize(50, 50));
        pushButton_temp_down->setFlat(true);

        horizontalLayout_5->addWidget(pushButton_temp_down);

        label_temp_value = new QLabel(frame);
        label_temp_value->setObjectName(QString::fromUtf8("label_temp_value"));
        label_temp_value->setMinimumSize(QSize(130, 85));
        label_temp_value->setMaximumSize(QSize(130, 85));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Sans Serif"));
        font1.setPointSize(42);
        label_temp_value->setFont(font1);
        label_temp_value->setFrameShape(QFrame::Box);
        label_temp_value->setAlignment(Qt::AlignCenter);

        horizontalLayout_5->addWidget(label_temp_value);

        pushButton_temp_up = new QPushButton(frame);
        pushButton_temp_up->setObjectName(QString::fromUtf8("pushButton_temp_up"));
        pushButton_temp_up->setMinimumSize(QSize(50, 50));
        pushButton_temp_up->setMaximumSize(QSize(50, 50));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/images/Icons/Plus.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_temp_up->setIcon(icon4);
        pushButton_temp_up->setIconSize(QSize(50, 50));
        pushButton_temp_up->setFlat(true);

        horizontalLayout_5->addWidget(pushButton_temp_up);


        verticalLayout_2->addLayout(horizontalLayout_5);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_2->addItem(verticalSpacer);


        horizontalLayout_7->addLayout(verticalLayout_2);

        horizontalSpacer_10 = new QSpacerItem(68, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_10);


        horizontalLayout_2->addWidget(frame);

        frame_2 = new QFrame(layoutWidget);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setMinimumSize(QSize(420, 280));
        frame_2->setMaximumSize(QSize(420, 280));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        verticalLayout_6 = new QVBoxLayout(frame_2);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(9, -1, -1, -1);
        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_7);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        label_name_2 = new QLabel(frame_2);
        label_name_2->setObjectName(QString::fromUtf8("label_name_2"));
        label_name_2->setStyleSheet(QString::fromUtf8("font: 18pt Bold \"Sans Serif\";\n"
"color: black;"));
        label_name_2->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        verticalLayout_5->addWidget(label_name_2);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label_6 = new QLabel(frame_2);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(label_6);

        label_4 = new QLabel(frame_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMinimumSize(QSize(110, 70));
        label_4->setMaximumSize(QSize(110, 70));
        label_4->setStyleSheet(QString::fromUtf8("font: 75 40pt \"Sans Serif\";"));
        label_4->setFrameShape(QFrame::Box);
        label_4->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(label_4);


        horizontalLayout_8->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label_7 = new QLabel(frame_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(label_7);

        label_5 = new QLabel(frame_2);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setMinimumSize(QSize(110, 70));
        label_5->setMaximumSize(QSize(110, 70));
        label_5->setStyleSheet(QString::fromUtf8("font: 75 40pt \"Sans Serif\";"));
        label_5->setFrameShape(QFrame::Box);
        label_5->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(label_5);


        horizontalLayout_8->addLayout(verticalLayout_4);


        verticalLayout_5->addLayout(horizontalLayout_8);


        horizontalLayout_9->addLayout(verticalLayout_5);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_8);


        verticalLayout_6->addLayout(horizontalLayout_9);

        horizontalSlider = new QSlider(frame_2);
        horizontalSlider->setObjectName(QString::fromUtf8("horizontalSlider"));
        horizontalSlider->setStyleSheet(QString::fromUtf8("QSlider::groove:horizontal {\n"
"    border: 1px solid #999999;\n"
"height:1px;\n"
"margin:15px;\n"
"}\n"
"QSlider::handle:horizontal {\n"
"	image: url(:/images/images/Icons/slide round.png);\n"
"    width: 35px;\n"
"    height:35px;\n"
"    margin:-12px;\n"
"}"));
        horizontalSlider->setOrientation(Qt::Horizontal);

        verticalLayout_6->addWidget(horizontalSlider);

        verticalSpacer_3 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_6->addItem(verticalSpacer_3);


        horizontalLayout_2->addWidget(frame_2);


        verticalLayout->addLayout(horizontalLayout_2);

        frame_3 = new QFrame(layoutWidget);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setMinimumSize(QSize(860, 100));
        frame_3->setMaximumSize(QSize(860, 100));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        horizontalLayout_4 = new QHBoxLayout(frame_3);
        horizontalLayout_4->setSpacing(20);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_4);

        label_2 = new QLabel(frame_3);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(180, 60));
        label_2->setMaximumSize(QSize(180, 60));
        label_2->setStyleSheet(QString::fromUtf8("font: 20pt Bold \"Sans Serif\";\n"
"color: black;"));

        horizontalLayout_4->addWidget(label_2);

        pushButton = new QPushButton(frame_3);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setMinimumSize(QSize(90, 60));
        pushButton->setMaximumSize(QSize(90, 60));
        pushButton->setStyleSheet(QString::fromUtf8("QPushButton::checked {\n"
"background-color: rgb(217, 229, 255);\n"
"}\n"
"QPushButton{\n"
"border-style: solid;\n"
"border-width: 1px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"color: black;\n"
"}\n"
""));
        pushButton->setCheckable(true);
        pushButton->setChecked(false);

        horizontalLayout_4->addWidget(pushButton);

        pushButton_2 = new QPushButton(frame_3);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setMinimumSize(QSize(90, 60));
        pushButton_2->setMaximumSize(QSize(90, 60));
        pushButton_2->setStyleSheet(QString::fromUtf8("QPushButton::checked {\n"
"background-color: rgb(217, 229, 255);\n"
"}\n"
"QPushButton{\n"
"border-style: solid;\n"
"border-width: 1px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"color: black;\n"
"}\n"
""));
        pushButton_2->setCheckable(true);
        pushButton_2->setChecked(false);

        horizontalLayout_4->addWidget(pushButton_2);

        pushButton_3 = new QPushButton(frame_3);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        pushButton_3->setMinimumSize(QSize(90, 60));
        pushButton_3->setMaximumSize(QSize(90, 60));
        pushButton_3->setStyleSheet(QString::fromUtf8("QPushButton::checked {\n"
"background-color: rgb(217, 229, 255);\n"
"}\n"
"QPushButton{\n"
"border-style: solid;\n"
"border-width: 1px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"color: black;\n"
"}\n"
""));
        pushButton_3->setCheckable(true);
        pushButton_3->setChecked(false);

        horizontalLayout_4->addWidget(pushButton_3);

        pushButton_4 = new QPushButton(frame_3);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));
        pushButton_4->setMinimumSize(QSize(90, 60));
        pushButton_4->setMaximumSize(QSize(90, 60));
        pushButton_4->setStyleSheet(QString::fromUtf8("QPushButton::checked {\n"
"background-color: rgb(217, 229, 255);\n"
"}\n"
"QPushButton{\n"
"border-style: solid;\n"
"border-width: 1px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"color: black;\n"
"}\n"
""));
        pushButton_4->setCheckable(true);
        pushButton_4->setChecked(false);

        horizontalLayout_4->addWidget(pushButton_4);

        pushButton_5 = new QPushButton(frame_3);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));
        pushButton_5->setMinimumSize(QSize(90, 60));
        pushButton_5->setMaximumSize(QSize(90, 60));
        pushButton_5->setStyleSheet(QString::fromUtf8("QPushButton::checked {\n"
"background-color: rgb(217, 229, 255);\n"
"}\n"
"QPushButton{\n"
"border-style: solid;\n"
"border-width: 1px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"color: black;\n"
"}\n"
""));
        pushButton_5->setCheckable(true);
        pushButton_5->setChecked(false);

        horizontalLayout_4->addWidget(pushButton_5);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);


        verticalLayout->addWidget(frame_3);


        gridLayout->addWidget(frame_body, 1, 0, 1, 1);


        retranslateUi(screen_39);

        QMetaObject::connectSlotsByName(screen_39);
    } // setupUi

    void retranslateUi(QWidget *screen_39)
    {
        screen_39->setWindowTitle(QApplication::translate("screen_39", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("screen_39", "Manual temperature settings", 0, QApplication::UnicodeUTF8));
        pushButton_home->setText(QString());
        pushButton_done->setText(QString());
        label_3->setText(QString());
        label_name->setText(QApplication::translate("screen_39", "Keep this temp", 0, QApplication::UnicodeUTF8));
        pushButton_temp_down->setText(QString());
        label_temp_value->setText(QString());
        pushButton_temp_up->setText(QString());
        label_name_2->setText(QApplication::translate("screen_39", "For", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("screen_39", "hours", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("screen_39", "05", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("screen_39", "mins", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("screen_39", "15", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("screen_39", "Fan Speed", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("screen_39", "Off", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("screen_39", "Min", 0, QApplication::UnicodeUTF8));
        pushButton_3->setText(QApplication::translate("screen_39", "Med", 0, QApplication::UnicodeUTF8));
        pushButton_4->setText(QApplication::translate("screen_39", "Max", 0, QApplication::UnicodeUTF8));
        pushButton_5->setText(QApplication::translate("screen_39", "Auto", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class screen_39: public Ui_screen_39 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREEN_39_H
