#ifndef SCREEN_28_H
#define SCREEN_28_H

#include <QWidget>
#include "datamanager.h"
#include <QPushButton>

namespace Ui {
class screen_28;
}

class screen_28 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_28(dataManager*,QWidget *parent = 0);
    ~screen_28();

public slots:
    void on_pushButton_keyboard_clicked();
    void setTextEdit(QString);
    QString climaName();

signals:
      void touched(int);
      void showkeyboard(QPushButton*);

private:
    Ui::screen_28 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_28_H
