#include "screen_35.h"
#include "ui_screen_35.h"

screen_35::screen_35(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_35),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_35::~screen_35()
{
    delete ui;
}

void screen_35::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_35::on_pushButton_done_clicked()
{
    Q_EMIT(touched(22));
}
