#ifndef SCREEN_2_H
#define SCREEN_2_H

#include <QWidget>
#include "datamanager.h"
#include "modbus_manager.h"
#include <QDebug>

namespace Ui {
class screen_2;
}

class screen_2 : public QWidget
{
    Q_OBJECT

public:
    explicit screen_2(dataManager*,modbus_manager * ,QWidget *parent = 0);
    ~screen_2();

public slots:
    void on_pushButton_schedule_clicked();
    void on_pushButton_settings_clicked();
    void on_pushButton_notifications_clicked();
    void on_pushButton_season_clicked();

    void on_toolButton_temp_clicked();
    void on_toolButton_vent_clicked();

signals:
    void touched(int);

protected:

private:
    Ui::screen_2 *ui;
    dataManager *_datamgr;
    modbus_manager *_mbus;
};

#endif // SCREEN_2_H
