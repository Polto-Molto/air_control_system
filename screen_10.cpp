#include "screen_10.h"
#include "ui_screen_10.h"

screen_10::screen_10(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_10),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_10::~screen_10()
{
    delete ui;
}

void screen_10::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_10::on_pushButton_done_clicked()
{
    Q_EMIT(touched(5));
}

void screen_10::on_pushButton_apply_all_clicked()
{
   // Q_EMIT(touched(5));
}

