/********************************************************************************
** Form generated from reading UI file 'screen_41.ui'
**
** Created: Thu Apr 13 00:46:21 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREEN_41_H
#define UI_SCREEN_41_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_screen_41
{
public:
    QGridLayout *gridLayout;
    QFrame *frame_header;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QFrame *frame_footer;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_15;
    QPushButton *pushButton_cancel;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *pushButton_home;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *pushButton_next;
    QSpacerItem *horizontalSpacer_16;
    QFrame *frame_body;
    QGridLayout *gridLayout_2;
    QPushButton *pushButton_test;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QPushButton *pushButton_8;
    QPushButton *pushButton_7;
    QPushButton *pushButton_6;
    QPushButton *pushButton_9;
    QPushButton *pushButton_10;
    QPushButton *pushButton_13;
    QPushButton *pushButton_12;
    QPushButton *pushButton_11;
    QPushButton *pushButton_14;
    QPushButton *pushButton_15;
    QPushButton *pushButton_18;
    QPushButton *pushButton_17;
    QPushButton *pushButton_16;
    QPushButton *pushButton_19;
    QPushButton *pushButton_20;

    void setupUi(QWidget *screen_41)
    {
        if (screen_41->objectName().isEmpty())
            screen_41->setObjectName(QString::fromUtf8("screen_41"));
        screen_41->resize(1024, 600);
        screen_41->setMinimumSize(QSize(1024, 600));
        screen_41->setMaximumSize(QSize(1024, 600));
        gridLayout = new QGridLayout(screen_41);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame_header = new QFrame(screen_41);
        frame_header->setObjectName(QString::fromUtf8("frame_header"));
        frame_header->setMinimumSize(QSize(1024, 50));
        frame_header->setMaximumSize(QSize(1024, 50));
        frame_header->setFrameShape(QFrame::StyledPanel);
        frame_header->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_header);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(frame_header);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(14);
        label->setFont(font);

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        gridLayout->addWidget(frame_header, 0, 0, 1, 1);

        frame_footer = new QFrame(screen_41);
        frame_footer->setObjectName(QString::fromUtf8("frame_footer"));
        frame_footer->setMinimumSize(QSize(1024, 80));
        frame_footer->setMaximumSize(QSize(1024, 80));
        frame_footer->setFrameShape(QFrame::StyledPanel);
        frame_footer->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_footer);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_15 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_15);

        pushButton_cancel = new QPushButton(frame_footer);
        pushButton_cancel->setObjectName(QString::fromUtf8("pushButton_cancel"));
        pushButton_cancel->setMinimumSize(QSize(50, 50));
        pushButton_cancel->setMaximumSize(QSize(50, 50));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/images/Icons/Cross.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_cancel->setIcon(icon);
        pushButton_cancel->setIconSize(QSize(50, 50));
        pushButton_cancel->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_cancel);

        horizontalSpacer_5 = new QSpacerItem(353, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);

        pushButton_home = new QPushButton(frame_footer);
        pushButton_home->setObjectName(QString::fromUtf8("pushButton_home"));
        pushButton_home->setMinimumSize(QSize(50, 50));
        pushButton_home->setMaximumSize(QSize(50, 50));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/images/Icons/home.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_home->setIcon(icon1);
        pushButton_home->setIconSize(QSize(50, 50));
        pushButton_home->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_home);

        horizontalSpacer_6 = new QSpacerItem(353, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);

        pushButton_next = new QPushButton(frame_footer);
        pushButton_next->setObjectName(QString::fromUtf8("pushButton_next"));
        pushButton_next->setMinimumSize(QSize(50, 50));
        pushButton_next->setMaximumSize(QSize(50, 50));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/images/Icons/next.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_next->setIcon(icon2);
        pushButton_next->setIconSize(QSize(50, 50));
        pushButton_next->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_next);

        horizontalSpacer_16 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_16);


        gridLayout->addWidget(frame_footer, 2, 0, 1, 1);

        frame_body = new QFrame(screen_41);
        frame_body->setObjectName(QString::fromUtf8("frame_body"));
        frame_body->setMinimumSize(QSize(1024, 470));
        frame_body->setMaximumSize(QSize(1024, 470));
        frame_body->setFrameShape(QFrame::StyledPanel);
        frame_body->setFrameShadow(QFrame::Raised);
        gridLayout_2 = new QGridLayout(frame_body);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        pushButton_test = new QPushButton(frame_body);
        pushButton_test->setObjectName(QString::fromUtf8("pushButton_test"));
        pushButton_test->setMinimumSize(QSize(170, 80));
        pushButton_test->setMaximumSize(QSize(170, 80));
        pushButton_test->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_test, 0, 0, 1, 1);

        pushButton_2 = new QPushButton(frame_body);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setMinimumSize(QSize(170, 80));
        pushButton_2->setMaximumSize(QSize(170, 80));
        pushButton_2->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_2, 0, 1, 1, 1);

        pushButton_3 = new QPushButton(frame_body);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        pushButton_3->setMinimumSize(QSize(170, 80));
        pushButton_3->setMaximumSize(QSize(170, 80));
        pushButton_3->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_3, 0, 2, 1, 1);

        pushButton_4 = new QPushButton(frame_body);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));
        pushButton_4->setMinimumSize(QSize(170, 80));
        pushButton_4->setMaximumSize(QSize(170, 80));
        pushButton_4->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_4, 0, 3, 1, 1);

        pushButton_5 = new QPushButton(frame_body);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));
        pushButton_5->setMinimumSize(QSize(170, 80));
        pushButton_5->setMaximumSize(QSize(170, 80));
        pushButton_5->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_5, 0, 4, 1, 1);

        pushButton_8 = new QPushButton(frame_body);
        pushButton_8->setObjectName(QString::fromUtf8("pushButton_8"));
        pushButton_8->setMinimumSize(QSize(170, 80));
        pushButton_8->setMaximumSize(QSize(170, 80));
        pushButton_8->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_8, 1, 0, 1, 1);

        pushButton_7 = new QPushButton(frame_body);
        pushButton_7->setObjectName(QString::fromUtf8("pushButton_7"));
        pushButton_7->setMinimumSize(QSize(170, 80));
        pushButton_7->setMaximumSize(QSize(170, 80));
        pushButton_7->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_7, 1, 1, 1, 1);

        pushButton_6 = new QPushButton(frame_body);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));
        pushButton_6->setMinimumSize(QSize(170, 80));
        pushButton_6->setMaximumSize(QSize(170, 80));
        pushButton_6->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_6, 1, 2, 1, 1);

        pushButton_9 = new QPushButton(frame_body);
        pushButton_9->setObjectName(QString::fromUtf8("pushButton_9"));
        pushButton_9->setMinimumSize(QSize(170, 80));
        pushButton_9->setMaximumSize(QSize(170, 80));
        pushButton_9->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_9, 1, 3, 1, 1);

        pushButton_10 = new QPushButton(frame_body);
        pushButton_10->setObjectName(QString::fromUtf8("pushButton_10"));
        pushButton_10->setMinimumSize(QSize(170, 80));
        pushButton_10->setMaximumSize(QSize(170, 80));
        pushButton_10->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_10, 1, 4, 1, 1);

        pushButton_13 = new QPushButton(frame_body);
        pushButton_13->setObjectName(QString::fromUtf8("pushButton_13"));
        pushButton_13->setMinimumSize(QSize(170, 80));
        pushButton_13->setMaximumSize(QSize(170, 80));
        pushButton_13->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_13, 2, 0, 1, 1);

        pushButton_12 = new QPushButton(frame_body);
        pushButton_12->setObjectName(QString::fromUtf8("pushButton_12"));
        pushButton_12->setMinimumSize(QSize(170, 80));
        pushButton_12->setMaximumSize(QSize(170, 80));
        pushButton_12->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_12, 2, 1, 1, 1);

        pushButton_11 = new QPushButton(frame_body);
        pushButton_11->setObjectName(QString::fromUtf8("pushButton_11"));
        pushButton_11->setMinimumSize(QSize(170, 80));
        pushButton_11->setMaximumSize(QSize(170, 80));
        pushButton_11->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_11, 2, 2, 1, 1);

        pushButton_14 = new QPushButton(frame_body);
        pushButton_14->setObjectName(QString::fromUtf8("pushButton_14"));
        pushButton_14->setMinimumSize(QSize(170, 80));
        pushButton_14->setMaximumSize(QSize(170, 80));
        pushButton_14->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_14, 2, 3, 1, 1);

        pushButton_15 = new QPushButton(frame_body);
        pushButton_15->setObjectName(QString::fromUtf8("pushButton_15"));
        pushButton_15->setMinimumSize(QSize(170, 80));
        pushButton_15->setMaximumSize(QSize(170, 80));
        pushButton_15->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_15, 2, 4, 1, 1);

        pushButton_18 = new QPushButton(frame_body);
        pushButton_18->setObjectName(QString::fromUtf8("pushButton_18"));
        pushButton_18->setMinimumSize(QSize(170, 80));
        pushButton_18->setMaximumSize(QSize(170, 80));
        pushButton_18->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_18, 3, 0, 1, 1);

        pushButton_17 = new QPushButton(frame_body);
        pushButton_17->setObjectName(QString::fromUtf8("pushButton_17"));
        pushButton_17->setMinimumSize(QSize(170, 80));
        pushButton_17->setMaximumSize(QSize(170, 80));
        pushButton_17->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_17, 3, 1, 1, 1);

        pushButton_16 = new QPushButton(frame_body);
        pushButton_16->setObjectName(QString::fromUtf8("pushButton_16"));
        pushButton_16->setMinimumSize(QSize(170, 80));
        pushButton_16->setMaximumSize(QSize(170, 80));
        pushButton_16->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_16, 3, 2, 1, 1);

        pushButton_19 = new QPushButton(frame_body);
        pushButton_19->setObjectName(QString::fromUtf8("pushButton_19"));
        pushButton_19->setMinimumSize(QSize(170, 80));
        pushButton_19->setMaximumSize(QSize(170, 80));
        pushButton_19->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_19, 3, 3, 1, 1);

        pushButton_20 = new QPushButton(frame_body);
        pushButton_20->setObjectName(QString::fromUtf8("pushButton_20"));
        pushButton_20->setMinimumSize(QSize(170, 80));
        pushButton_20->setMaximumSize(QSize(170, 80));
        pushButton_20->setStyleSheet(QString::fromUtf8("border-style: solid;\n"
"border-width: 2px;\n"
"font: 12pt Bold \"Sans Serif\";\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(60, 103, 188);\n"
"border-radius:12px;"));

        gridLayout_2->addWidget(pushButton_20, 3, 4, 1, 1);


        gridLayout->addWidget(frame_body, 1, 0, 1, 1);


        retranslateUi(screen_41);

        QMetaObject::connectSlotsByName(screen_41);
    } // setupUi

    void retranslateUi(QWidget *screen_41)
    {
        screen_41->setWindowTitle(QApplication::translate("screen_41", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("screen_41", "Hydronic Units - Skudo 1/2", 0, QApplication::UnicodeUTF8));
        pushButton_home->setText(QString());
        pushButton_next->setText(QString());
        pushButton_test->setText(QApplication::translate("screen_41", "1. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("screen_41", "2. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
        pushButton_3->setText(QApplication::translate("screen_41", "3. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
        pushButton_4->setText(QApplication::translate("screen_41", "4. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
        pushButton_5->setText(QApplication::translate("screen_41", "5. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
        pushButton_8->setText(QApplication::translate("screen_41", "6. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
        pushButton_7->setText(QApplication::translate("screen_41", "7. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
        pushButton_6->setText(QApplication::translate("screen_41", "8. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
        pushButton_9->setText(QApplication::translate("screen_41", "9. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
        pushButton_10->setText(QApplication::translate("screen_41", "10. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
        pushButton_13->setText(QApplication::translate("screen_41", "11. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
        pushButton_12->setText(QApplication::translate("screen_41", "12. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
        pushButton_11->setText(QApplication::translate("screen_41", "13. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
        pushButton_14->setText(QApplication::translate("screen_41", "14. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
        pushButton_15->setText(QApplication::translate("screen_41", "15. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
        pushButton_18->setText(QApplication::translate("screen_41", "16. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
        pushButton_17->setText(QApplication::translate("screen_41", "17. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
        pushButton_16->setText(QApplication::translate("screen_41", "18. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
        pushButton_19->setText(QApplication::translate("screen_41", "19. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
        pushButton_20->setText(QApplication::translate("screen_41", "20. Activated\n"
"Kid room", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class screen_41: public Ui_screen_41 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREEN_41_H
