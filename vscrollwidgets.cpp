#include "vscrollwidgets.h"
#include <QDebug>

VScrollWidgets::VScrollWidgets(int  w ,int hs,int vs,int c,QObject *parent)
{
    setWidgetResizable(true);
    this->setStyleSheet("background-color:black;");
    this->vspacing = vs;
    this->hspacing = hs;
    this->cols = c;
    this->setFrameShape(NoFrame);
    setStyleSheet("background:rgba(0,0,0,0);");

    container = new QWidget;
    container->setGeometry(0,0,(w+hs)*c,0);
    //container->setFixedWidth((w+hs)*c);
    container->setObjectName("camSettingContainer");
    //container->setStyleSheet("background:rgba(200, 0, 200, 255);");

    this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  //  this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    listLayout = new QGridLayout(container);
    listLayout->setHorizontalSpacing(hs);
    listLayout->setVerticalSpacing(vs);
    listLayout->setMargin(0);
    this->setWidget(container);
    /*
    styleSheetFirst = "background:rgba(200, 200, 200, 255);\
                        color:rgb(80,80,80); \
                        border: 0px ;\
                        font: 22pt Bold 'Ubuntu';\
                        border-radius: 8px;\
                        padding: 0px;\
                        margin:0px;";

    styleSheetSecond = "background:rgba(220, 220, 220, 255);\
                        color:rgb(80,80,80); \
                        border: 0px;\
                        font: 22pt Bold 'Ubuntu';\
                        border-radius: 8px;\
                        padding: 0px;\
                        margin:0px;";
                        */
}

void VScrollWidgets::addWidget( QWidget* w )
{
    int colNum = WidgetList.count()%cols;
    int rowNum = (int) WidgetList.count()/cols;
    container->setFixedHeight( (w->height()+vspacing)*(rowNum+1));
    WidgetList << w;
    w->setParent( container );

    if ((WidgetList.count()%2) == 0)
        w->setStyleSheet(styleSheetFirst);
    else
        w->setStyleSheet(styleSheetSecond);

    listLayout->addWidget(w,rowNum,colNum );
    container->adjustSize();
}

void VScrollWidgets::removeWidget( QWidget* w )
{
    container->setFixedHeight( container->height() - w->height()-vspacing);
    WidgetList.removeOne(w);
    listLayout->removeWidget( w );
}

void VScrollWidgets::clear()
{
    foreach (QWidget* w, WidgetList) {
        w->close();
        delete w;
    }
    WidgetList.clear();
    container->setFixedHeight( 0);
}
