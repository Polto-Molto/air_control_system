#include "keypad.h"
#include "ui_keypad.h"

keypad::keypad(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::keypad)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::ToolTip);
    line_edit = 0;
}

keypad::~keypad()
{
    delete ui;
}

void keypad::setSenderWidge(QPushButton* obj)
{
    line_edit = obj;
}

void keypad::write_char(QString s)
{
    line_edit->setText(line_edit->text() + s);
}

void keypad::remove_char()
{
    QString str = line_edit->text();
    str.chop(1);
    line_edit->setText(str);
}

void keypad::on_pushButton_0_clicked()
{
    write_char(ui->pushButton_0->text());
}

void keypad::on_pushButton_1_clicked()
{
    write_char(ui->pushButton_1->text());
}

void keypad::on_pushButton_2_clicked()
{
    write_char(ui->pushButton_2->text());
}

void keypad::on_pushButton_3_clicked()
{
    write_char(ui->pushButton_3->text());
}

void keypad::on_pushButton_4_clicked()
{
    write_char(ui->pushButton_4->text());
}

void keypad::on_pushButton_5_clicked()
{
    write_char(ui->pushButton_5->text());
}

void keypad::on_pushButton_6_clicked()
{
    write_char(ui->pushButton_6->text());
}

void keypad::on_pushButton_7_clicked()
{
    write_char(ui->pushButton_7->text());
}

void keypad::on_pushButton_8_clicked()
{
    write_char(ui->pushButton_8->text());
}

void keypad::on_pushButton_9_clicked()
{
    write_char(ui->pushButton_9->text());
}

void keypad::on_pushButton_enter_clicked()
{
    Q_EMIT(returnPressed());
}

void keypad::on_pushButton_back_clicked()
{
    remove_char();
}
