#ifndef SCREEN_55_H
#define SCREEN_55_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_55;
}

class screen_55 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_55(dataManager *,QWidget *parent = 0);
    ~screen_55();
signals:
      void touched(int);

private:
    Ui::screen_55 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_55_H
