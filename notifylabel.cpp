#include "notifylabel.h"
#include "ui_notifylabel.h"
#include <QDate>

NotifyLabel::NotifyLabel(int w,QString n,int t,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NotifyLabel)
{
    ui->setupUi(this);

    ui->frame->setFixedWidth(w);
    if(t == 0)
    {
        ui->label_logo->setStyleSheet("border-image: url(:/images/images/icons/icons 15.svg);");
        ui->label_state->setText("SUCCESS");
        ui->label_date->setText(QDate::currentDate().toString());
    }
    else
    {
        ui->frame->setStyleSheet("border-radius: 8px;color:green; border: 1px solid #00FF00;");
    }

}

NotifyLabel::~NotifyLabel()
{
    delete ui;
}
