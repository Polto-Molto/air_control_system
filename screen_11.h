#ifndef SCREEN_11_H
#define SCREEN_11_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_11;
}

class screen_11 : public QWidget
{
    Q_OBJECT

public:
    explicit screen_11(dataManager*,QWidget *parent = 0);
    ~screen_11();
signals:
      void touched(int);


public slots:
    void on_pushButton_home_clicked();
    void on_pushButton_done_clicked();
    void on_pushButton_cancel_clicked();

    void on_pushButton_comfort_up_clicked();
    void on_pushButton_comfort_down_clicked();
    void on_pushButton_humidity_up_clicked();
    void on_pushButton_humidity_down_clicked();
    void on_pushButton_night_up_clicked();
    void on_pushButton_night_down_clicked();

    void setCurrentClima(CLIMA_MODBUS);

private:
    Ui::screen_11 *ui;
    dataManager *_datamgr;
    CLIMA_MODBUS _clima;
};

#endif // SCREEN_11_H
