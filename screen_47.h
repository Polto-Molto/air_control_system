#ifndef SCREEN_47_H
#define SCREEN_47_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_47;
}

class screen_47 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_47(dataManager*,QWidget *parent = 0);
    ~screen_47();
signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_back_clicked();
      void on_pushButton_done_clicked();

private:
    Ui::screen_47 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_47_H
