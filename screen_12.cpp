#include "screen_12.h"
#include "ui_screen_12.h"

screen_12::screen_12(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_12),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_12::~screen_12()
{
    delete ui;
}

void screen_12::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_12::on_pushButton_done_clicked()
{
    Q_EMIT(touched(2));
}

void screen_12::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(2));
}

