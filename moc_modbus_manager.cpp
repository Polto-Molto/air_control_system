/****************************************************************************
** Meta object code from reading C++ file 'modbus_manager.h'
**
** Created: Tue Apr 25 16:10:41 2017
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "modbus_manager.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'modbus_manager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_modbus_manager[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x05,

 // slots: signature, parameters, type, tag, flags
      40,   15,   15,   15, 0x0a,
      64,   15,   15,   15, 0x0a,
      80,   15,   15,   15, 0x0a,
     101,   15,   15,   15, 0x0a,
     127,   15,   15,   15, 0x0a,
     145,   15,   15,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_modbus_manager[] = {
    "modbus_manager\0\0statusMessagesChanged()\0"
    "readModbusNetwrokData()\0setClimaTable()\0"
    "setClimaDataTables()\0updateClima(CLIMA_MODBUS)\0"
    "enableReadClima()\0disableReadClima()\0"
};

void modbus_manager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        modbus_manager *_t = static_cast<modbus_manager *>(_o);
        switch (_id) {
        case 0: _t->statusMessagesChanged(); break;
        case 1: _t->readModbusNetwrokData(); break;
        case 2: _t->setClimaTable(); break;
        case 3: _t->setClimaDataTables(); break;
        case 4: _t->updateClima((*reinterpret_cast< CLIMA_MODBUS(*)>(_a[1]))); break;
        case 5: _t->enableReadClima(); break;
        case 6: _t->disableReadClima(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData modbus_manager::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject modbus_manager::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_modbus_manager,
      qt_meta_data_modbus_manager, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &modbus_manager::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *modbus_manager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *modbus_manager::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_modbus_manager))
        return static_cast<void*>(const_cast< modbus_manager*>(this));
    return QObject::qt_metacast(_clname);
}

int modbus_manager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void modbus_manager::statusMessagesChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
