#ifndef SCREEN_23_H
#define SCREEN_23_H

#include <QWidget>
#include <QPushButton>
#include "datamanager.h"

namespace Ui {
class screen_23;
}

class screen_23 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_23(dataManager *,QWidget *parent = 0);
    ~screen_23();

signals:
      void touched(int);
      void clima_device_selected(CLIMA_MODBUS);

public slots:
      void reload();
      void on_pushButton_home_clicked();
      void on_pushButton_cancel_clicked();
      void on_pushButton_next_clicked();

      void loadClimaData();
      void deviceSelected();
private:
    Ui::screen_23 *ui;
    dataManager *_datamgr;
    QMap<QPushButton*,CLIMA_MODBUS> devices;
};

#endif // SCREEN_23_H
