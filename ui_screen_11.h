/********************************************************************************
** Form generated from reading UI file 'screen_11.ui'
**
** Created: Thu Apr 13 00:46:20 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREEN_11_H
#define UI_SCREEN_11_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_screen_11
{
public:
    QGridLayout *gridLayout;
    QFrame *frame_header;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QFrame *frame_body;
    QGridLayout *gridLayout_2;
    QFrame *frame_3;
    QHBoxLayout *horizontalLayout_9;
    QSpacerItem *horizontalSpacer_13;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_10;
    QSpacerItem *horizontalSpacer_14;
    QLabel *label_5;
    QLabel *label_name_3;
    QSpacerItem *horizontalSpacer_17;
    QHBoxLayout *horizontalLayout_11;
    QPushButton *pushButton_night_down;
    QLabel *label_night_value;
    QPushButton *pushButton_night_up;
    QSpacerItem *horizontalSpacer_18;
    QSpacerItem *horizontalSpacer_20;
    QFrame *frame;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_9;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_4;
    QLabel *label_3;
    QLabel *label_name;
    QSpacerItem *horizontalSpacer_3;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *pushButton_comfort_down;
    QLabel *label_comfort_value;
    QPushButton *pushButton_comfort_up;
    QSpacerItem *horizontalSpacer_10;
    QFrame *frame_2;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_11;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_7;
    QSpacerItem *horizontalSpacer_7;
    QLabel *label_4;
    QLabel *label_name_2;
    QSpacerItem *horizontalSpacer_8;
    QHBoxLayout *horizontalLayout_8;
    QPushButton *pushButton_humidity_down;
    QLabel *label_humidity_value;
    QPushButton *pushButton_humidity_up;
    QSpacerItem *horizontalSpacer_12;
    QSpacerItem *horizontalSpacer_19;
    QSpacerItem *verticalSpacer;
    QFrame *frame_footer;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_15;
    QPushButton *pushButton_cancel;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *pushButton_home;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *pushButton_done;
    QSpacerItem *horizontalSpacer_16;

    void setupUi(QWidget *screen_11)
    {
        if (screen_11->objectName().isEmpty())
            screen_11->setObjectName(QString::fromUtf8("screen_11"));
        screen_11->resize(1024, 600);
        screen_11->setMinimumSize(QSize(1024, 600));
        screen_11->setMaximumSize(QSize(1024, 600));
        gridLayout = new QGridLayout(screen_11);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame_header = new QFrame(screen_11);
        frame_header->setObjectName(QString::fromUtf8("frame_header"));
        frame_header->setMinimumSize(QSize(1024, 50));
        frame_header->setMaximumSize(QSize(1024, 50));
        frame_header->setFrameShape(QFrame::NoFrame);
        frame_header->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_header);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label = new QLabel(frame_header);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(14);
        label->setFont(font);

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(460, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        gridLayout->addWidget(frame_header, 0, 0, 1, 1);

        frame_body = new QFrame(screen_11);
        frame_body->setObjectName(QString::fromUtf8("frame_body"));
        frame_body->setMinimumSize(QSize(1024, 470));
        frame_body->setMaximumSize(QSize(1024, 470));
        frame_body->setFrameShape(QFrame::NoFrame);
        frame_body->setFrameShadow(QFrame::Raised);
        gridLayout_2 = new QGridLayout(frame_body);
        gridLayout_2->setSpacing(20);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 40, 0, 0);
        frame_3 = new QFrame(frame_body);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setMinimumSize(QSize(435, 195));
        frame_3->setMaximumSize(QSize(435, 195));
        frame_3->setStyleSheet(QString::fromUtf8("#frame_3 {\n"
"border-style: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"}"));
        frame_3->setFrameShape(QFrame::Box);
        frame_3->setFrameShadow(QFrame::Raised);
        horizontalLayout_9 = new QHBoxLayout(frame_3);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        horizontalSpacer_13 = new QSpacerItem(68, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_13);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        horizontalSpacer_14 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_14);

        label_5 = new QLabel(frame_3);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/night.png")));
        label_5->setAlignment(Qt::AlignCenter);

        horizontalLayout_10->addWidget(label_5);

        label_name_3 = new QLabel(frame_3);
        label_name_3->setObjectName(QString::fromUtf8("label_name_3"));
        label_name_3->setStyleSheet(QString::fromUtf8("font: 18pt Bold \"Sans Serif\";\n"
"color: black;"));
        label_name_3->setAlignment(Qt::AlignCenter);

        horizontalLayout_10->addWidget(label_name_3);

        horizontalSpacer_17 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_17);


        verticalLayout_3->addLayout(horizontalLayout_10);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        pushButton_night_down = new QPushButton(frame_3);
        pushButton_night_down->setObjectName(QString::fromUtf8("pushButton_night_down"));
        pushButton_night_down->setMinimumSize(QSize(50, 50));
        pushButton_night_down->setMaximumSize(QSize(50, 50));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/images/Icons/minus.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_night_down->setIcon(icon);
        pushButton_night_down->setIconSize(QSize(50, 50));
        pushButton_night_down->setFlat(true);

        horizontalLayout_11->addWidget(pushButton_night_down);

        label_night_value = new QLabel(frame_3);
        label_night_value->setObjectName(QString::fromUtf8("label_night_value"));
        label_night_value->setMinimumSize(QSize(130, 85));
        label_night_value->setMaximumSize(QSize(130, 85));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Sans Serif"));
        font1.setPointSize(32);
        label_night_value->setFont(font1);
        label_night_value->setFrameShape(QFrame::Box);
        label_night_value->setAlignment(Qt::AlignCenter);

        horizontalLayout_11->addWidget(label_night_value);

        pushButton_night_up = new QPushButton(frame_3);
        pushButton_night_up->setObjectName(QString::fromUtf8("pushButton_night_up"));
        pushButton_night_up->setMinimumSize(QSize(50, 50));
        pushButton_night_up->setMaximumSize(QSize(50, 50));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/images/Icons/Plus.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_night_up->setIcon(icon1);
        pushButton_night_up->setIconSize(QSize(50, 50));
        pushButton_night_up->setFlat(true);

        horizontalLayout_11->addWidget(pushButton_night_up);


        verticalLayout_3->addLayout(horizontalLayout_11);


        horizontalLayout_9->addLayout(verticalLayout_3);

        horizontalSpacer_18 = new QSpacerItem(68, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_18);


        gridLayout_2->addWidget(frame_3, 0, 2, 1, 1);

        horizontalSpacer_20 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_20, 0, 3, 1, 1);

        frame = new QFrame(frame_body);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setMinimumSize(QSize(435, 195));
        frame->setMaximumSize(QSize(435, 195));
        frame->setStyleSheet(QString::fromUtf8("#frame {\n"
"border-style: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"}"));
        frame->setFrameShape(QFrame::Box);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout_4 = new QHBoxLayout(frame);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer_9 = new QSpacerItem(68, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_9);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_4);

        label_3 = new QLabel(frame);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/comfrot.png")));
        label_3->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(label_3);

        label_name = new QLabel(frame);
        label_name->setObjectName(QString::fromUtf8("label_name"));
        label_name->setStyleSheet(QString::fromUtf8("font: 18pt Bold \"Sans Serif\";\n"
"color: black;"));
        label_name->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(label_name);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        pushButton_comfort_down = new QPushButton(frame);
        pushButton_comfort_down->setObjectName(QString::fromUtf8("pushButton_comfort_down"));
        pushButton_comfort_down->setMinimumSize(QSize(50, 50));
        pushButton_comfort_down->setMaximumSize(QSize(50, 50));
        pushButton_comfort_down->setIcon(icon);
        pushButton_comfort_down->setIconSize(QSize(50, 50));
        pushButton_comfort_down->setFlat(true);

        horizontalLayout_5->addWidget(pushButton_comfort_down);

        label_comfort_value = new QLabel(frame);
        label_comfort_value->setObjectName(QString::fromUtf8("label_comfort_value"));
        label_comfort_value->setMinimumSize(QSize(130, 85));
        label_comfort_value->setMaximumSize(QSize(130, 85));
        label_comfort_value->setFont(font1);
        label_comfort_value->setFrameShape(QFrame::Box);
        label_comfort_value->setAlignment(Qt::AlignCenter);

        horizontalLayout_5->addWidget(label_comfort_value);

        pushButton_comfort_up = new QPushButton(frame);
        pushButton_comfort_up->setObjectName(QString::fromUtf8("pushButton_comfort_up"));
        pushButton_comfort_up->setMinimumSize(QSize(50, 50));
        pushButton_comfort_up->setMaximumSize(QSize(50, 50));
        pushButton_comfort_up->setIcon(icon1);
        pushButton_comfort_up->setIconSize(QSize(50, 50));
        pushButton_comfort_up->setFlat(true);

        horizontalLayout_5->addWidget(pushButton_comfort_up);


        verticalLayout->addLayout(horizontalLayout_5);


        horizontalLayout_4->addLayout(verticalLayout);

        horizontalSpacer_10 = new QSpacerItem(68, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_10);


        gridLayout_2->addWidget(frame, 0, 1, 1, 1);

        frame_2 = new QFrame(frame_body);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setMinimumSize(QSize(435, 195));
        frame_2->setMaximumSize(QSize(435, 195));
        frame_2->setStyleSheet(QString::fromUtf8("#frame_2 {\n"
"border-style: solid;\n"
"border-width: 1px;\n"
"border-color: rgb(96, 153, 222);\n"
"color: rgb(96, 153, 222);\n"
"border-radius:10px;\n"
"}"));
        frame_2->setFrameShape(QFrame::Box);
        frame_2->setFrameShadow(QFrame::Raised);
        horizontalLayout_6 = new QHBoxLayout(frame_2);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalSpacer_11 = new QSpacerItem(68, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_11);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_7);

        label_4 = new QLabel(frame_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/humidity.png")));
        label_4->setAlignment(Qt::AlignCenter);

        horizontalLayout_7->addWidget(label_4);

        label_name_2 = new QLabel(frame_2);
        label_name_2->setObjectName(QString::fromUtf8("label_name_2"));
        label_name_2->setStyleSheet(QString::fromUtf8("font: 18pt Bold \"Sans Serif\";\n"
"color: black;"));
        label_name_2->setAlignment(Qt::AlignCenter);

        horizontalLayout_7->addWidget(label_name_2);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_8);


        verticalLayout_2->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        pushButton_humidity_down = new QPushButton(frame_2);
        pushButton_humidity_down->setObjectName(QString::fromUtf8("pushButton_humidity_down"));
        pushButton_humidity_down->setMinimumSize(QSize(50, 50));
        pushButton_humidity_down->setMaximumSize(QSize(50, 50));
        pushButton_humidity_down->setIcon(icon);
        pushButton_humidity_down->setIconSize(QSize(50, 50));
        pushButton_humidity_down->setFlat(true);

        horizontalLayout_8->addWidget(pushButton_humidity_down);

        label_humidity_value = new QLabel(frame_2);
        label_humidity_value->setObjectName(QString::fromUtf8("label_humidity_value"));
        label_humidity_value->setMinimumSize(QSize(130, 85));
        label_humidity_value->setMaximumSize(QSize(130, 85));
        label_humidity_value->setFont(font1);
        label_humidity_value->setFrameShape(QFrame::Box);
        label_humidity_value->setAlignment(Qt::AlignCenter);

        horizontalLayout_8->addWidget(label_humidity_value);

        pushButton_humidity_up = new QPushButton(frame_2);
        pushButton_humidity_up->setObjectName(QString::fromUtf8("pushButton_humidity_up"));
        pushButton_humidity_up->setMinimumSize(QSize(50, 50));
        pushButton_humidity_up->setMaximumSize(QSize(50, 50));
        pushButton_humidity_up->setIcon(icon1);
        pushButton_humidity_up->setIconSize(QSize(50, 50));
        pushButton_humidity_up->setFlat(true);

        horizontalLayout_8->addWidget(pushButton_humidity_up);


        verticalLayout_2->addLayout(horizontalLayout_8);


        horizontalLayout_6->addLayout(verticalLayout_2);

        horizontalSpacer_12 = new QSpacerItem(68, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_12);


        gridLayout_2->addWidget(frame_2, 1, 1, 1, 1);

        horizontalSpacer_19 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_19, 0, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer, 2, 1, 1, 1);

        frame->raise();
        frame_2->raise();
        frame_3->raise();

        gridLayout->addWidget(frame_body, 1, 0, 1, 1);

        frame_footer = new QFrame(screen_11);
        frame_footer->setObjectName(QString::fromUtf8("frame_footer"));
        frame_footer->setMinimumSize(QSize(1024, 80));
        frame_footer->setMaximumSize(QSize(1024, 80));
        frame_footer->setFrameShape(QFrame::NoFrame);
        frame_footer->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_footer);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_15 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_15);

        pushButton_cancel = new QPushButton(frame_footer);
        pushButton_cancel->setObjectName(QString::fromUtf8("pushButton_cancel"));
        pushButton_cancel->setMinimumSize(QSize(50, 50));
        pushButton_cancel->setMaximumSize(QSize(50, 50));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/images/Icons/Cross.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_cancel->setIcon(icon2);
        pushButton_cancel->setIconSize(QSize(50, 50));
        pushButton_cancel->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_cancel);

        horizontalSpacer_5 = new QSpacerItem(353, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);

        pushButton_home = new QPushButton(frame_footer);
        pushButton_home->setObjectName(QString::fromUtf8("pushButton_home"));
        pushButton_home->setMinimumSize(QSize(50, 50));
        pushButton_home->setMaximumSize(QSize(50, 50));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/images/Icons/home.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_home->setIcon(icon3);
        pushButton_home->setIconSize(QSize(50, 50));
        pushButton_home->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_home);

        horizontalSpacer_6 = new QSpacerItem(353, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);

        pushButton_done = new QPushButton(frame_footer);
        pushButton_done->setObjectName(QString::fromUtf8("pushButton_done"));
        pushButton_done->setMinimumSize(QSize(50, 50));
        pushButton_done->setMaximumSize(QSize(50, 50));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/images/Icons/check.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_done->setIcon(icon4);
        pushButton_done->setIconSize(QSize(50, 50));
        pushButton_done->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_done);

        horizontalSpacer_16 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_16);


        gridLayout->addWidget(frame_footer, 2, 0, 1, 1);


        retranslateUi(screen_11);

        QMetaObject::connectSlotsByName(screen_11);
    } // setupUi

    void retranslateUi(QWidget *screen_11)
    {
        screen_11->setWindowTitle(QApplication::translate("screen_11", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("screen_11", "1- Kids Room -Summer set Points", 0, QApplication::UnicodeUTF8));
        label_5->setText(QString());
        label_name_3->setText(QApplication::translate("screen_11", "Night", 0, QApplication::UnicodeUTF8));
        pushButton_night_down->setText(QString());
        label_night_value->setText(QString());
        pushButton_night_up->setText(QString());
        label_3->setText(QString());
        label_name->setText(QApplication::translate("screen_11", "Comfort", 0, QApplication::UnicodeUTF8));
        pushButton_comfort_down->setText(QString());
        label_comfort_value->setText(QString());
        pushButton_comfort_up->setText(QString());
        label_4->setText(QString());
        label_name_2->setText(QApplication::translate("screen_11", "Humidity", 0, QApplication::UnicodeUTF8));
        pushButton_humidity_down->setText(QString());
        label_humidity_value->setText(QString());
        pushButton_humidity_up->setText(QString());
        pushButton_home->setText(QString());
        pushButton_done->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class screen_11: public Ui_screen_11 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREEN_11_H
