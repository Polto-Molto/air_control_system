/********************************************************************************
** Form generated from reading UI file 'keypad.ui'
**
** Created: Thu Apr 13 00:46:22 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KEYPAD_H
#define UI_KEYPAD_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_keypad
{
public:
    QGridLayout *gridLayout;
    QPushButton *pushButton_1;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QPushButton *pushButton_6;
    QPushButton *pushButton_7;
    QPushButton *pushButton_8;
    QPushButton *pushButton_9;
    QPushButton *pushButton_back;
    QPushButton *pushButton_0;
    QPushButton *pushButton_enter;

    void setupUi(QWidget *keypad)
    {
        if (keypad->objectName().isEmpty())
            keypad->setObjectName(QString::fromUtf8("keypad"));
        keypad->resize(250, 320);
        keypad->setMinimumSize(QSize(25, 320));
        keypad->setMaximumSize(QSize(250, 320));
        keypad->setStyleSheet(QString::fromUtf8("background-color: rgb(67, 67, 67);"));
        gridLayout = new QGridLayout(keypad);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        pushButton_1 = new QPushButton(keypad);
        pushButton_1->setObjectName(QString::fromUtf8("pushButton_1"));
        pushButton_1->setMinimumSize(QSize(70, 70));
        pushButton_1->setMaximumSize(QSize(70, 70));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(16);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(50);
        pushButton_1->setFont(font);
        pushButton_1->setAutoFillBackground(false);
        pushButton_1->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"border-radius:8px;\n"
"font: 16pt \"Sans Serif\";"));
        pushButton_1->setFlat(true);

        gridLayout->addWidget(pushButton_1, 0, 0, 1, 1);

        pushButton_2 = new QPushButton(keypad);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setMinimumSize(QSize(70, 70));
        pushButton_2->setMaximumSize(QSize(70, 70));
        pushButton_2->setFont(font);
        pushButton_2->setAutoFillBackground(false);
        pushButton_2->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"border-radius:8px;\n"
"font: 16pt \"Sans Serif\";"));
        pushButton_2->setFlat(true);

        gridLayout->addWidget(pushButton_2, 0, 1, 1, 1);

        pushButton_3 = new QPushButton(keypad);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        pushButton_3->setMinimumSize(QSize(70, 70));
        pushButton_3->setMaximumSize(QSize(70, 70));
        pushButton_3->setFont(font);
        pushButton_3->setAutoFillBackground(false);
        pushButton_3->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"border-radius:8px;\n"
"font: 16pt \"Sans Serif\";"));
        pushButton_3->setFlat(true);

        gridLayout->addWidget(pushButton_3, 0, 2, 1, 1);

        pushButton_4 = new QPushButton(keypad);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));
        pushButton_4->setMinimumSize(QSize(70, 70));
        pushButton_4->setMaximumSize(QSize(70, 70));
        pushButton_4->setFont(font);
        pushButton_4->setAutoFillBackground(false);
        pushButton_4->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"border-radius:8px;\n"
"font: 16pt \"Sans Serif\";"));
        pushButton_4->setFlat(true);

        gridLayout->addWidget(pushButton_4, 1, 0, 1, 1);

        pushButton_5 = new QPushButton(keypad);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));
        pushButton_5->setMinimumSize(QSize(70, 70));
        pushButton_5->setMaximumSize(QSize(70, 70));
        pushButton_5->setFont(font);
        pushButton_5->setAutoFillBackground(false);
        pushButton_5->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"border-radius:8px;\n"
"font: 16pt \"Sans Serif\";"));
        pushButton_5->setFlat(true);

        gridLayout->addWidget(pushButton_5, 1, 1, 1, 1);

        pushButton_6 = new QPushButton(keypad);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));
        pushButton_6->setMinimumSize(QSize(70, 70));
        pushButton_6->setMaximumSize(QSize(70, 70));
        pushButton_6->setFont(font);
        pushButton_6->setAutoFillBackground(false);
        pushButton_6->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"border-radius:8px;\n"
"font: 16pt \"Sans Serif\";"));
        pushButton_6->setFlat(true);

        gridLayout->addWidget(pushButton_6, 1, 2, 1, 1);

        pushButton_7 = new QPushButton(keypad);
        pushButton_7->setObjectName(QString::fromUtf8("pushButton_7"));
        pushButton_7->setMinimumSize(QSize(70, 70));
        pushButton_7->setMaximumSize(QSize(70, 70));
        pushButton_7->setFont(font);
        pushButton_7->setAutoFillBackground(false);
        pushButton_7->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"border-radius:8px;\n"
"font: 16pt \"Sans Serif\";"));
        pushButton_7->setFlat(true);

        gridLayout->addWidget(pushButton_7, 2, 0, 1, 1);

        pushButton_8 = new QPushButton(keypad);
        pushButton_8->setObjectName(QString::fromUtf8("pushButton_8"));
        pushButton_8->setMinimumSize(QSize(70, 70));
        pushButton_8->setMaximumSize(QSize(70, 70));
        pushButton_8->setFont(font);
        pushButton_8->setAutoFillBackground(false);
        pushButton_8->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"border-radius:8px;\n"
"font: 16pt \"Sans Serif\";"));
        pushButton_8->setFlat(true);

        gridLayout->addWidget(pushButton_8, 2, 1, 1, 1);

        pushButton_9 = new QPushButton(keypad);
        pushButton_9->setObjectName(QString::fromUtf8("pushButton_9"));
        pushButton_9->setMinimumSize(QSize(70, 70));
        pushButton_9->setMaximumSize(QSize(70, 70));
        pushButton_9->setFont(font);
        pushButton_9->setAutoFillBackground(false);
        pushButton_9->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"border-radius:8px;\n"
"font: 16pt \"Sans Serif\";"));
        pushButton_9->setFlat(true);

        gridLayout->addWidget(pushButton_9, 2, 2, 1, 1);

        pushButton_back = new QPushButton(keypad);
        pushButton_back->setObjectName(QString::fromUtf8("pushButton_back"));
        pushButton_back->setMinimumSize(QSize(70, 70));
        pushButton_back->setMaximumSize(QSize(70, 70));
        pushButton_back->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(35, 121, 102);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));

        gridLayout->addWidget(pushButton_back, 3, 0, 1, 1);

        pushButton_0 = new QPushButton(keypad);
        pushButton_0->setObjectName(QString::fromUtf8("pushButton_0"));
        pushButton_0->setMinimumSize(QSize(70, 70));
        pushButton_0->setMaximumSize(QSize(70, 70));
        pushButton_0->setFont(font);
        pushButton_0->setAutoFillBackground(false);
        pushButton_0->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(120,120,120);\n"
"border-radius:8px;\n"
"font: 16pt \"Sans Serif\";"));
        pushButton_0->setFlat(true);

        gridLayout->addWidget(pushButton_0, 3, 1, 1, 1);

        pushButton_enter = new QPushButton(keypad);
        pushButton_enter->setObjectName(QString::fromUtf8("pushButton_enter"));
        pushButton_enter->setMinimumSize(QSize(70, 70));
        pushButton_enter->setMaximumSize(QSize(70, 70));
        pushButton_enter->setStyleSheet(QString::fromUtf8("color:white;\n"
"background-color: rgb(35, 121, 102);\n"
"font: 16pt \"Sans Serif\";\n"
"border-radius:8px;\n"
""));

        gridLayout->addWidget(pushButton_enter, 3, 2, 1, 1);


        retranslateUi(keypad);

        QMetaObject::connectSlotsByName(keypad);
    } // setupUi

    void retranslateUi(QWidget *keypad)
    {
        keypad->setWindowTitle(QApplication::translate("keypad", "Form", 0, QApplication::UnicodeUTF8));
        pushButton_1->setText(QApplication::translate("keypad", "1", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("keypad", "2", 0, QApplication::UnicodeUTF8));
        pushButton_3->setText(QApplication::translate("keypad", "3", 0, QApplication::UnicodeUTF8));
        pushButton_4->setText(QApplication::translate("keypad", "4", 0, QApplication::UnicodeUTF8));
        pushButton_5->setText(QApplication::translate("keypad", "5", 0, QApplication::UnicodeUTF8));
        pushButton_6->setText(QApplication::translate("keypad", "6", 0, QApplication::UnicodeUTF8));
        pushButton_7->setText(QApplication::translate("keypad", "7", 0, QApplication::UnicodeUTF8));
        pushButton_8->setText(QApplication::translate("keypad", "8", 0, QApplication::UnicodeUTF8));
        pushButton_9->setText(QApplication::translate("keypad", "9", 0, QApplication::UnicodeUTF8));
        pushButton_back->setText(QApplication::translate("keypad", "<", 0, QApplication::UnicodeUTF8));
        pushButton_0->setText(QApplication::translate("keypad", "0", 0, QApplication::UnicodeUTF8));
        pushButton_enter->setText(QApplication::translate("keypad", "Enter", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class keypad: public Ui_keypad {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KEYPAD_H
