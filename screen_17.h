#ifndef SCREEN_17_H
#define SCREEN_17_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_17;
}

class screen_17 : public QWidget
{
    Q_OBJECT

public:
    explicit screen_17(dataManager*,QWidget *parent = 0);
    ~screen_17();
signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_done_clicked();
      void on_pushButton_cancel_clicked();

      void on_pushButton_program_clicked();
private:
    Ui::screen_17 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_17_H
