#ifndef SCREEN_39_H
#define SCREEN_39_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_39;
}

class screen_39 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_39(dataManager*,QWidget *parent = 0);
    ~screen_39();
signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_cancel_clicked();
      void on_pushButton_done_clicked();
      void on_pushButton_temp_up_clicked();
      void on_pushButton_temp_down_clicked();

private:
    Ui::screen_39 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_39_H
