#ifndef SCREEN_37_H
#define SCREEN_37_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_37;
}

class screen_37 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_37(dataManager*,QWidget *parent = 0);
    ~screen_37();
signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_cancel_clicked();
      void on_pushButton_back_clicked();
      void on_pushButton_next_clicked();
      void on_pushButton_touch_clicked();

      void on_toolButton_program_clicked();
      void on_toolButton_temp_clicked();

private:
    Ui::screen_37 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_37_H
