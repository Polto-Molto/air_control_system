#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#include <QString>
#include <QTime>
#include <QDebug>

#define CLIMA_INIT_ID 1
#define AIR_INIT_ID 10

#define SKUDO_START_RANGE 11
#define SKUDO_END_RANGE 20

#define AIR_START_RANGE 21
#define AIR_END_RANGE 40

#define CLIMA_START_RANGE 41
#define CLIMA_END_RANGE 70

enum SEASON{
    WINTER,
    SUMMER
};


struct MAIN_CONFIG
{
    unsigned int schedule_num;
    unsigned int clima_num;
    unsigned int air_num;
    unsigned int skudo_num;
    SEASON season;

};

struct SCHEDULE
{
    QString config_name;
    QString name;
    QTime comfort_start;
    QTime night_start;
};

struct CLIMA_MODBUS
{
    QString config_name;
    int id;
    QString name;
    int room_number;
    bool activate;
    bool dew;
    bool alarm;
    SEASON season;

    short status;
    float air_temp;
    short relative_humidity;
    float floor_temp;
    float dew_point_temp;

    short circuit_actuator;
    short dehumidity_actuator;
    short comfort_rh_threshold;

    QString schedule;
    QTime comfort_start;
    QTime night_start;

    int winter_comfort_temp_point;
    int summer_comfort_temp_point;

    int winter_night_temp_point;
    int summer_night_temp_point;

    int summer_humidity_point;

    QTime temp_timer_start;
    int temp_timer;
    int temp_timer_value;

    CLIMA_MODBUS()
    {
        config_name = "";
         id = CLIMA_INIT_ID;
         name ="defualt_name";
         room_number = 0;
         activate = false;
         dew =false;
         alarm = false;
         season = WINTER;

         status = 0;
         air_temp = 0 ;
         relative_humidity = 0;
         floor_temp = 0;
         dew_point_temp = 0;

         circuit_actuator = 0;
         dehumidity_actuator = 0;
         comfort_rh_threshold = 0;

         schedule = "schedule_1";
         comfort_start = QTime(7,30,0);
         night_start = QTime(18,0,0);

         winter_comfort_temp_point = 0;
         summer_comfort_temp_point = 0;

         winter_night_temp_point = 0;
         summer_night_temp_point = 0;

         summer_humidity_point = 0;

         temp_timer_start = QTime(0,0,0);
         temp_timer = 0;
         temp_timer_value = 21;

    }
};

struct AIR_MODBUS
{

};

struct SKUDO_MODBUS
{

};
#endif // DEFINITIONS_H
