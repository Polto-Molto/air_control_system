#include "screen_6.h"
#include "ui_screen_6.h"

screen_6::screen_6(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_6),
    _datamgr(dm)
{
    ui->setupUi(this);
    ui->label_comfort_value->setNum(20);
    ui->label_night_value->setNum(20);
}

screen_6::~screen_6()
{
    delete ui;
}

void screen_6::setCurrentClima(CLIMA_MODBUS clima)
{
    _clima = clima;
    ui->label_comfort_value->setNum(_clima.winter_comfort_temp_point);
    ui->label_night_value->setNum(_clima.winter_night_temp_point);
    Q_EMIT(touched(6));
}


void screen_6::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_6::on_pushButton_done_clicked()
{
    _clima.winter_comfort_temp_point = ui->label_comfort_value->text().toInt();
    _clima.winter_night_temp_point = ui->label_night_value->text().toInt();
    _datamgr->updateClimaConfigData(_clima);
    Q_EMIT(touched(5));
}

void screen_6::on_pushButton_comfort_up_clicked()
{
    int value = ui->label_comfort_value->text().toInt();
    ui->label_comfort_value->setNum(value + 1);
}

void screen_6::on_pushButton_comfort_down_clicked()
{
    int value = ui->label_comfort_value->text().toInt();
    ui->label_comfort_value->setNum(value - 1);
}

void screen_6::on_pushButton_night_up_clicked()
{
    int value = ui->label_night_value->text().toInt();
    ui->label_night_value->setNum(value + 1);
}

void screen_6::on_pushButton_night_down_clicked()
{
    int value = ui->label_night_value->text().toInt();
    ui->label_night_value->setNum(value - 1);
}
