#ifndef SCREEN_30_H
#define SCREEN_30_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_30;
}

class screen_30 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_30(dataManager*,QWidget *parent = 0);
    ~screen_30();
signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_done_clicked();
      void on_pushButton_temp_up_clicked();
      void on_pushButton_temp_down_clicked();
      void on_pushButton_vmc_up_clicked();
      void on_pushButton_vmc_down_clicked();

private:
    Ui::screen_30 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_30_H
