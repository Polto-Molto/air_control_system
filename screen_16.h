#ifndef SCREEN_16_H
#define SCREEN_16_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_16;
}

class screen_16 : public QWidget
{
    Q_OBJECT

public:
    explicit screen_16(dataManager*,QWidget *parent = 0);
    ~screen_16();
signals:
      void touched(int);

private:
    Ui::screen_16 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_16_H
