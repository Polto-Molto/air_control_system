#ifndef SCREEN_3_H
#define SCREEN_3_H

#include <QWidget>
#include "datamanager.h"

#include "vscrollwidgets.h"

namespace Ui {
class screen_3;
}

class screen_3 : public QWidget
{
    Q_OBJECT

public:
    explicit screen_3(dataManager*,QWidget *parent = 0);
    ~screen_3();
public slots:
    void on_pushButton_home_clicked();
    void on_pushButton_next_clicked();
    void on_pushButton_back_clicked();

signals:
      void touched(int);

protected:

private:
    Ui::screen_3 *ui;
    dataManager *_datamgr;
    VScrollWidgets *scroll;
};

#endif // SCREEN_3_H
