#ifndef SCREEN_13_H
#define SCREEN_13_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_13;
}

class screen_13 : public QWidget
{
    Q_OBJECT

public:
    explicit screen_13(dataManager *,QWidget *parent = 0);
    ~screen_13();
signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_cancel_clicked();

      void on_pushButton_first_fan_clicked();
      void on_pushButton_second_fan_clicked();
      void on_pushButton_third_fan_clicked();
      void on_pushButton_fourth_fan_clicked();

private:
    Ui::screen_13 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_13_H
