#ifndef TEMPSETTINGROOMITEM_H
#define TEMPSETTINGROOMITEM_H

#include <QWidget>
#include <QPushButton>
#include <definitions.h>

namespace Ui {
class tempSettingRoomItem;
}

class tempSettingRoomItem : public QWidget
{
    Q_OBJECT
    
public:
    explicit tempSettingRoomItem(CLIMA_MODBUS,QWidget *parent = 0);
    ~tempSettingRoomItem();

signals:
    void climaClicked(CLIMA_MODBUS);
public slots:
    void buttonClicked();

private:
    Ui::tempSettingRoomItem *ui;
    QPushButton *button;
    CLIMA_MODBUS _clima;
};

#endif // TEMPSETTINGROOMITEM_H
