#ifndef SCREEN_44_H
#define SCREEN_44_H

#include <QWidget>
#include "datamanager.h"
#include <QPushButton>

namespace Ui {
class screen_44;
}

class screen_44 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_44(dataManager*,QWidget *parent = 0);
    ~screen_44();

public slots:
    void on_pushButton_keyboard_clicked();

signals:
      void touched(int);
      void showkeyboard(QPushButton*);

private:
    Ui::screen_44 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_44_H
