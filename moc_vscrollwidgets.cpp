/****************************************************************************
** Meta object code from reading C++ file 'vscrollwidgets.h'
**
** Created: Thu Apr 13 00:47:44 2017
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "vscrollwidgets.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'vscrollwidgets.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_VScrollWidgets[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x0a,
      36,   15,   15,   15, 0x0a,
      59,   15,   15,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_VScrollWidgets[] = {
    "VScrollWidgets\0\0addWidget(QWidget*)\0"
    "removeWidget(QWidget*)\0clear()\0"
};

void VScrollWidgets::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        VScrollWidgets *_t = static_cast<VScrollWidgets *>(_o);
        switch (_id) {
        case 0: _t->addWidget((*reinterpret_cast< QWidget*(*)>(_a[1]))); break;
        case 1: _t->removeWidget((*reinterpret_cast< QWidget*(*)>(_a[1]))); break;
        case 2: _t->clear(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData VScrollWidgets::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject VScrollWidgets::staticMetaObject = {
    { &QScrollArea::staticMetaObject, qt_meta_stringdata_VScrollWidgets,
      qt_meta_data_VScrollWidgets, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &VScrollWidgets::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *VScrollWidgets::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *VScrollWidgets::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_VScrollWidgets))
        return static_cast<void*>(const_cast< VScrollWidgets*>(this));
    return QScrollArea::qt_metacast(_clname);
}

int VScrollWidgets::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QScrollArea::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
