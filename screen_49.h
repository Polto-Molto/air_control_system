#ifndef SCREEN_49_H
#define SCREEN_49_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_49;
}

class screen_49 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_49(dataManager*,QWidget *parent = 0);
    ~screen_49();
signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_cancel_clicked();
      void on_pushButton_done_clicked();

private:
    Ui::screen_49 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_49_H
