/****************************************************************************
** Meta object code from reading C++ file 'screen_23.h'
**
** Created: Tue Apr 25 16:09:53 2017
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "screen_23.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'screen_23.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_screen_23[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x05,
      24,   10,   10,   10, 0x05,

 // slots: signature, parameters, type, tag, flags
      60,   10,   10,   10, 0x0a,
      69,   10,   10,   10, 0x0a,
      98,   10,   10,   10, 0x0a,
     129,   10,   10,   10, 0x0a,
     158,   10,   10,   10, 0x0a,
     174,   10,   10,   10, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_screen_23[] = {
    "screen_23\0\0touched(int)\0"
    "clima_device_selected(CLIMA_MODBUS)\0"
    "reload()\0on_pushButton_home_clicked()\0"
    "on_pushButton_cancel_clicked()\0"
    "on_pushButton_next_clicked()\0"
    "loadClimaData()\0deviceSelected()\0"
};

void screen_23::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        screen_23 *_t = static_cast<screen_23 *>(_o);
        switch (_id) {
        case 0: _t->touched((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->clima_device_selected((*reinterpret_cast< CLIMA_MODBUS(*)>(_a[1]))); break;
        case 2: _t->reload(); break;
        case 3: _t->on_pushButton_home_clicked(); break;
        case 4: _t->on_pushButton_cancel_clicked(); break;
        case 5: _t->on_pushButton_next_clicked(); break;
        case 6: _t->loadClimaData(); break;
        case 7: _t->deviceSelected(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData screen_23::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject screen_23::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_screen_23,
      qt_meta_data_screen_23, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &screen_23::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *screen_23::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *screen_23::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_screen_23))
        return static_cast<void*>(const_cast< screen_23*>(this));
    return QWidget::qt_metacast(_clname);
}

int screen_23::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void screen_23::touched(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void screen_23::clima_device_selected(CLIMA_MODBUS _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
