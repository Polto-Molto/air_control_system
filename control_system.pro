#-------------------------------------------------
#
# Project created by QtCreator 2017-01-01T23:30:25
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = control_system
TEMPLATE = app
INCLUDEPATH = -I /qt4_modbus_lib/include

LIBS += -L/qt4_modbus_lib/lib

LIBS += /qt4_modbus_lib/lib/libQt4Modbus.so.1.0.0
LIBS += /qt4_modbus_lib/lib/libmodbus.so

SOURCES += main.cpp\
    modbusclima.cpp \
    vscrollwidgets.cpp \
    notifylabel.cpp \
    screens_manager.cpp \
    screen_1.cpp \
    screen_2.cpp \
    screen_3.cpp \
    screen_4.cpp \
    screen_5.cpp \
    screen_6.cpp \
    screen_7.cpp \
    screen_8.cpp \
    screen_9.cpp \
    screen_10.cpp \
    screen_11.cpp \
    screen_12.cpp \
    screen_13.cpp \
    screen_14.cpp \
    screen_15.cpp \
    screen_16.cpp \
    screen_17.cpp \
    screen_18.cpp \
    screen_19.cpp \
    screen_20.cpp \
    screen_21.cpp \
    screen_22.cpp \
    screen_23.cpp \
    screen_24.cpp \
    screen_25.cpp \
    screen_26.cpp \
    screen_27.cpp \
    screen_28.cpp \
    screen_29.cpp \
    screen_30.cpp \
    screen_31.cpp \
    screen_32.cpp \
    screen_33.cpp \
    screen_34.cpp \
    screen_35.cpp \
    screen_36.cpp \
    screen_37.cpp \
    screen_38.cpp \
    screen_39.cpp \
    screen_40.cpp \
    screen_41.cpp \
    screen_42.cpp \
    screen_43.cpp \
    screen_44.cpp \
    screen_45.cpp \
    screen_46.cpp \
    screen_47.cpp \
    screen_48.cpp \
    screen_49.cpp \
    screen_50.cpp \
    tempsettingroomitem.cpp \
    keyboard.cpp \
    configmanager.cpp \
    screen_51.cpp \
    screen_52.cpp \
    screen_53.cpp \
    screen_54.cpp \
    screen_55.cpp \
    screen_56.cpp \
    modbus_manager.cpp \
    datamanager.cpp \
    keypad.cpp

HEADERS  += \
    screens_manager.h \
    modbusclima.h \
    vscrollwidgets.h \
    notifylabel.h \
    screen_1.h \
    screen_2.h \
    screen_3.h \
    screen_4.h \
    screen_5.h \
    screen_6.h \
    screen_7.h \
    screen_8.h \
    screen_9.h \
    screen_10.h \
    screen_11.h \
    screen_12.h \
    screen_13.h \
    screen_14.h \
    screen_15.h \
    screen_16.h \
    screen_17.h \
    screen_18.h \
    screen_19.h \
    screen_20.h \
    screen_21.h \
    screen_22.h \
    screen_23.h \
    screen_24.h \
    screen_25.h \
    screen_26.h \
    screen_27.h \
    screen_28.h \
    screen_29.h \
    screen_30.h \
    screen_31.h \
    screen_32.h \
    screen_33.h \
    screen_34.h \
    screen_35.h \
    screen_36.h \
    screen_37.h \
    screen_38.h \
    screen_39.h \
    screen_40.h \
    screen_41.h \
    screen_42.h \
    screen_43.h \
    screen_44.h \
    screen_45.h \
    screen_46.h \
    screen_47.h \
    screen_48.h \
    screen_49.h \
    screen_50.h \
    tempsettingroomitem.h \
    keyboard.h \
    configmanager.h \
    screen_51.h \
    screen_52.h \
    screen_53.h \
    screen_54.h \
    screen_55.h \
    screen_56.h \
    modbus_manager.h \
    definitions.h \
    datamanager.h \
    keypad.h

FORMS    += \
    notifylabel.ui \
    screen_1.ui \
    screen_2.ui \
    screen_3.ui \
    screen_4.ui \
    screen_5.ui \
    screen_6.ui \
    screen_7.ui \
    screen_8.ui \
    screen_9.ui \
    screen_10.ui \
    screen_11.ui \
    screen_12.ui \
    screen_13.ui \
    screen_14.ui \
    screen_15.ui \
    screen_16.ui \
    screen_17.ui \
    screen_18.ui \
    screen_19.ui \
    screen_20.ui \
    screen_21.ui \
    screen_22.ui \
    screen_23.ui \
    screen_24.ui \
    screen_25.ui \
    screen_26.ui \
    screen_27.ui \
    screen_28.ui \
    screen_29.ui \
    screen_30.ui \
    screen_31.ui \
    screen_32.ui \
    screen_33.ui \
    screen_34.ui \
    screen_35.ui \
    screen_36.ui \
    screen_37.ui \
    screen_38.ui \
    screen_39.ui \
    screen_40.ui \
    screen_41.ui \
    screen_42.ui \
    screen_43.ui \
    screen_44.ui \
    screen_45.ui \
    screen_46.ui \
    screen_47.ui \
    screen_48.ui \
    screen_49.ui \
    screen_50.ui \
    tempsettingroomitem.ui \
    keyboard.ui \
    screen_51.ui \
    screen_52.ui \
    screen_53.ui \
    screen_54.ui \
    screen_55.ui \
    screen_56.ui \
    keypad.ui

RESOURCES += \
    images.qrc \
    qss.qrc
