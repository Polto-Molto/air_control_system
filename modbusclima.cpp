#include "modbusclima.h"
#include <QDebug>

union {
    float val;
    unsigned char bytes[4];
}c_to_f;

modbusClima::modbusClima(CLIMA_MODBUS clima,Qt4Modbus *m):
    modbus_slave(clima.id,m)
{
    _clima = clima;
}

CLIMA_MODBUS modbusClima::readData()
{
    this->start();
    QVector<quint16> data;
    data = this->readHoldingRegisters(0,8);
    if(data.size() > 0)
    {
        _clima.status = data.at(0);
        quint16 x;

        c_to_f.bytes[3] = data.at(2) & 0xff;
        c_to_f.bytes[2] = (data.at(2)>>8) & 0xff;
        c_to_f.bytes[1] = data.at(1) & 0xff;
        c_to_f.bytes[0] = (data.at(1)>>8) & 0xff;
        _clima.air_temp = c_to_f.val;

        _clima.relative_humidity = data.at(3);

        c_to_f.bytes[3] = data.at(5) & 0xff;
        c_to_f.bytes[2] = (data.at(5)>>8) & 0xff;
        c_to_f.bytes[1] = data.at(4) & 0xff;
        c_to_f.bytes[0] = (data.at(4)>>8) & 0xff;
        _clima.floor_temp = c_to_f.val;

        c_to_f.bytes[3] = data.at(7) & 0xff;
        c_to_f.bytes[2] = (data.at(7)>>8) & 0xff;
        c_to_f.bytes[1] = data.at(6) & 0xff;
        c_to_f.bytes[0] = (data.at(6)>>8) & 0xff;
        _clima.dew_point_temp = c_to_f.val;
        data.clear();
    }

    data =  this->readHoldingRegisters(16,3);
    if(data.size() > 0)
    {
        _clima.circuit_actuator = (short) data.value(0);
        _clima.dehumidity_actuator = (short) data.value(1);
        _clima.comfort_rh_threshold = (short) data.value(2);
        data.clear();
    }
    return _clima;
}

void modbusClima::writeCircuitActautor(short val)
{
    this->start();
    QVector<quint16> vec;
    vec << val;
    this->writeHoldingRegisters(16,vec);
}

void modbusClima::writeDehumidityActautor(short val)
{
    this->start();
    QVector<quint16> vec;
    vec << val;
    this->writeHoldingRegisters(17,vec);
}

void modbusClima::writeComfortRHTreshold(short val)
{
    this->start();
    QVector<quint16> vec;
    vec << val;
    this->writeHoldingRegisters(18,vec);
}


short modbusClima::getStatus()
{
    return _clima.status;
}

float modbusClima::getAirTemp()
{
    return _clima.air_temp;
}

short modbusClima::getHumidity()
{
    return _clima.relative_humidity;
}

float modbusClima::getFloorTemp()
{
    return _clima.floor_temp;
}

float modbusClima::getDewPointTemp()
{
    return _clima.dew_point_temp;
}
