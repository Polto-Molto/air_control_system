#ifndef SCREEN_50_H
#define SCREEN_50_H

#include <QWidget>
#include "datamanager.h"
#include "modbus_manager.h"

namespace Ui {
class screen_50;
}

class screen_50 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_50(dataManager*, modbus_manager *,QWidget *parent = 0);
    ~screen_50();
signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_cancel_clicked();
      void on_pushButton_done_clicked();


private:
    Ui::screen_50 *ui;
    dataManager *_datamgr;
    modbus_manager *_mbus;

};

#endif // SCREEN_50_H
