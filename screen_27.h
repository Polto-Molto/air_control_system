#ifndef SCREEN_27_H
#define SCREEN_27_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_27;
}

class screen_27 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_27(dataManager*,QWidget *parent = 0);
    ~screen_27();

signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_back_clicked();
      void on_pushButton_cancel_clicked();
      void on_pushButton_edit_clicked();
      void on_pushButton_done_clicked();
      void on_pushButton_next_clicked();

private:
    Ui::screen_27 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_27_H
