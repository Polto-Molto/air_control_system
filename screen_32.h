#ifndef SCREEN_32_H
#define SCREEN_32_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_32;
}

class screen_32 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_32(dataManager*,QWidget *parent = 0);
    ~screen_32();
signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_done_clicked();

private:
    Ui::screen_32 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_32_H
