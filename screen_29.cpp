#include "screen_29.h"
#include "ui_screen_29.h"

screen_29::screen_29(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_29),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_29::~screen_29()
{
    delete ui;
}

void screen_29::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_29::on_pushButton_done_clicked()
{
    Q_EMIT(touched(21));
}

void screen_29::on_pushButton_standby_clicked()
{
    Q_EMIT(touched(30));
}
