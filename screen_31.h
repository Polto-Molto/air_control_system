#ifndef SCREEN_31_H
#define SCREEN_31_H

#include <QWidget>
#include "datamanager.h"
#include <QPushButton>

namespace Ui {
class screen_31;
}

class screen_31 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_31(dataManager*,QWidget *parent = 0);
    ~screen_31();

public slots:
    void on_pushButton_keyboard_clicked();

signals:
      void touched(int);
      void showkeyboard(QPushButton*);

private:
    Ui::screen_31 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_31_H
