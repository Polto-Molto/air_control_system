#ifndef SCREEN_4_H
#define SCREEN_4_H

#include <QWidget>
#include "datamanager.h"
#include "tempsettingroomitem.h"

namespace Ui {
class screen_4;
}

class screen_4 : public QWidget
{
    Q_OBJECT

public:
    explicit screen_4(dataManager*,QWidget *parent = 0);
    ~screen_4();
public slots:
    void reload();
    void on_pushButton_home_clicked();
    void on_pushButton_next_clicked();
    void on_pushButton_back_clicked();
    void showPage();

signals:
    void selectedClima(CLIMA_MODBUS);
    void touched(int);

private:
    Ui::screen_4 *ui;
    dataManager *_datamgr;
    int _pages;
    int current_page;
    QVector<tempSettingRoomItem *> items;
};

#endif // SCREEN_4_H
