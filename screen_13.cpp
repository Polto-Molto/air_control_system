#include "screen_13.h"
#include "ui_screen_13.h"

screen_13::screen_13(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_13),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_13::~screen_13()
{
    delete ui;
}

void screen_13::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_13::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(2));
}

void screen_13::on_pushButton_first_fan_clicked()
{
    Q_EMIT(touched(14));
}

void screen_13::on_pushButton_second_fan_clicked()
{
    Q_EMIT(touched(14));
}

void screen_13::on_pushButton_third_fan_clicked()
{
    Q_EMIT(touched(14));
}

void screen_13::on_pushButton_fourth_fan_clicked()
{
    Q_EMIT(touched(14));
}
