#include "screen_4.h"
#include "ui_screen_4.h"

screen_4::screen_4(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_4),
    _datamgr(dm)
{
    ui->setupUi(this);
    current_page = 1;
    reload();
}

screen_4::~screen_4()
{
    delete ui;
}

void screen_4::reload()
{
    _pages = (_datamgr->mainData().clima_num / 8) +1;
    showPage();
}

void screen_4::showPage()
{
    foreach(tempSettingRoomItem *item,items)
        delete item;
    items.clear();
    foreach(CLIMA_MODBUS clima,_datamgr->climaTable()->values())
    {
        int i = clima.room_number%8;
        if(clima.room_number >= (current_page*8)-7 && clima.room_number <= current_page*8)
        {
            tempSettingRoomItem *item = new tempSettingRoomItem(clima,this);
            connect(item,SIGNAL(climaClicked(CLIMA_MODBUS)),this,SIGNAL(selectedClima(CLIMA_MODBUS)));
            ui->gridLayout->addWidget(item,i/2,i%2);
            items.push_back(item);
        }
    }
}

void screen_4::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_4::on_pushButton_next_clicked()
{
    if(current_page < _pages)
     current_page++;
    showPage();
}

void screen_4::on_pushButton_back_clicked()
{
    if(current_page > 1)
     current_page--;
    showPage();
}
