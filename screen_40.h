#ifndef SCREEN_40_H
#define SCREEN_40_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_40;
}

class screen_40 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_40(dataManager*,QWidget *parent = 0);
    ~screen_40();
signals:
      void touched(int);

public slots:
      void on_pushButton_done_clicked();

private:
    Ui::screen_40 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_40_H
