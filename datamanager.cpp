#include "datamanager.h"
#include <QDebug>

dataManager::dataManager(QObject *parent) :
    QObject(parent)
{
    main_config =  new MAIN_CONFIG;
    _schedule = new QMap<QString,SCHEDULE>;
    _clima = new QMap<QString,CLIMA_MODBUS>;
}

bool dataManager::init(QMap<QString,CLIMA_MODBUS> *climaTable)
{
    foreach(QString key , climaTable->keys())
        _clima->insert(key,climaTable->value(key));
    return true;
}

bool dataManager::setMainConfigData(MAIN_CONFIG md)
{
    *main_config = md;
    return true;
}

bool dataManager::setScheduleConfigData(QMap<QString,SCHEDULE> sd)
{
   *_schedule = sd;
    return true;
}

bool dataManager::setClimaConfigData(QMap<QString,CLIMA_MODBUS> cd)
{
    *_clima = cd;
    Q_EMIT(climaConfigDataTableUpdated());
    return true;
}

MAIN_CONFIG dataManager::mainData()
{
    return *main_config;
}

bool dataManager::updateMainData(MAIN_CONFIG m_conf)
{
    if(main_config->clima_num < m_conf.clima_num)
    {
        for(int i =main_config->clima_num ;i < m_conf.clima_num;i++)
        {
            QString str_num;
            str_num.setNum(i+1);
            CLIMA_MODBUS clima;
            clima.config_name = "clima_"+str_num;
            clima.room_number = i+1;
            clima.schedule = "schedule_1";
            _clima->insert(clima.config_name,clima);
        }
        Q_EMIT(climaConfigDataTableUpdated());

    }

    if(main_config->air_num < m_conf.air_num)
    {
        // addNewAir
    }

    if(main_config->skudo_num < m_conf.skudo_num)
    {
        // addNewSkudo
    }
    main_config->schedule_num = m_conf.schedule_num;
    main_config->clima_num = m_conf.clima_num;
    main_config->air_num = m_conf.air_num;
    main_config->skudo_num = m_conf.skudo_num;
    main_config->season = m_conf.season;
    Q_EMIT(mainConfigDataUpdated(m_conf));
    return true;
}

SCHEDULE dataManager::scheduleData(QString schedule_group)
{
    return _schedule->value(schedule_group);
}

bool dataManager::updateScheduleData(SCHEDULE schedule)
{
    _schedule->insert(schedule.config_name,schedule);
    Q_EMIT(scheduleConfigDataUpdated(schedule));
    return true;
}

QMap<QString,SCHEDULE> *dataManager::scheduleTable()
{
    return _schedule;
}


CLIMA_MODBUS dataManager::climaData(QString clima_group)
{
    return _clima->value(clima_group);
}

bool dataManager::updateClimaConfigData(CLIMA_MODBUS clima)
{
    _clima->insert(clima.config_name,clima);
    Q_EMIT(climaConfigDataUpdated(clima));
    return true;
}

bool dataManager::updateClimaValues(CLIMA_MODBUS clima)
{
    _clima->insert(clima.config_name,clima);
    Q_EMIT(climaValuesUpdated(clima));
    return true;
}

QMap<QString,CLIMA_MODBUS> *dataManager::climaTable()
{
    return _clima;
}
