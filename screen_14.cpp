#include "screen_14.h"
#include "ui_screen_14.h"

screen_14::screen_14(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_14),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_14::~screen_14()
{
    delete ui;
}

void screen_14::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_14::on_pushButton_done_clicked()
{
    Q_EMIT(touched(13));
}

void screen_14::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(13));
}

void screen_14::on_pushButton_touch_clicked()
{
    Q_EMIT(touched(15));
}
