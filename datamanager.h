#ifndef DATAMANAGER_H
#define DATAMANAGER_H

#include <QObject>
#include <QMap>
#include "definitions.h"

class dataManager : public QObject
{
    Q_OBJECT
public:
    explicit dataManager(QObject *parent = 0);
    
signals:
    void mainConfigDataUpdated(MAIN_CONFIG);
    void scheduleConfigDataUpdated(SCHEDULE);
    void climaConfigDataUpdated(CLIMA_MODBUS);
    void climaValuesUpdated(CLIMA_MODBUS);
    void climaConfigDataTableUpdated();

public slots:
    bool init(QMap<QString,CLIMA_MODBUS> *climaTable);
    bool setMainConfigData(MAIN_CONFIG);
    bool setScheduleConfigData(QMap<QString,SCHEDULE>);
    bool setClimaConfigData(QMap<QString,CLIMA_MODBUS>);

    MAIN_CONFIG mainData();
    bool updateMainData(MAIN_CONFIG);

    SCHEDULE scheduleData(QString);
    bool updateScheduleData(SCHEDULE);
    QMap<QString,SCHEDULE> *scheduleTable();

    CLIMA_MODBUS climaData(QString);
    bool updateClimaConfigData(CLIMA_MODBUS);
    bool updateClimaValues(CLIMA_MODBUS);
    QMap<QString,CLIMA_MODBUS> *climaTable();


private:
    MAIN_CONFIG *main_config;
    QMap<QString,SCHEDULE> *_schedule;
    QMap<QString,CLIMA_MODBUS> *_clima;

};

#endif // DATAMANAGER_H
