/********************************************************************************
** Form generated from reading UI file 'screen_17.ui'
**
** Created: Thu Apr 13 00:46:20 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREEN_17_H
#define UI_SCREEN_17_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_screen_17
{
public:
    QGridLayout *gridLayout;
    QFrame *frame_footer;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_15;
    QPushButton *pushButton_cancel;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *pushButton_home;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *pushButton_done;
    QSpacerItem *horizontalSpacer_16;
    QFrame *frame_body;
    QGridLayout *gridLayout_3;
    QSpacerItem *verticalSpacer_5;
    QSpacerItem *verticalSpacer_4;
    QFrame *frame;
    QVBoxLayout *verticalLayout_4;
    QSpacerItem *verticalSpacer_2;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_7;
    QPushButton *pushButton_program;
    QSpacerItem *horizontalSpacer_8;
    QLabel *label;
    QSpacerItem *verticalSpacer_3;
    QHBoxLayout *horizontalLayout_5;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *label_4;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_2;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_4;
    QLabel *label_5;
    QSpacerItem *horizontalSpacer_3;
    QLabel *label_3;
    QSpacerItem *verticalSpacer;
    QGridLayout *gridLayout_2;
    QPushButton *pushButton_45;
    QPushButton *pushButton_47;
    QPushButton *pushButton_44;
    QPushButton *pushButton_17;
    QPushButton *pushButton_46;
    QPushButton *pushButton_10;
    QPushButton *pushButton_13;
    QPushButton *pushButton_43;
    QPushButton *pushButton_15;
    QPushButton *pushButton_26;
    QPushButton *pushButton_24;
    QPushButton *pushButton_23;
    QPushButton *pushButton_16;
    QPushButton *pushButton_11;
    QPushButton *pushButton_3;
    QPushButton *pushButton_48;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_8;
    QPushButton *pushButton_31;
    QPushButton *pushButton_33;
    QPushButton *pushButton_22;
    QPushButton *pushButton_29;
    QPushButton *pushButton_37;
    QPushButton *pushButton_39;
    QPushButton *pushButton_27;
    QPushButton *pushButton_7;
    QPushButton *pushButton_21;
    QPushButton *pushButton_19;
    QPushButton *pushButton_28;
    QPushButton *pushButton_35;
    QPushButton *pushButton_41;
    QPushButton *pushButton_5;
    QPushButton *pushButton_32;
    QPushButton *pushButton_18;
    QPushButton *pushButton_14;
    QPushButton *pushButton_4;
    QPushButton *pushButton_34;
    QPushButton *pushButton_38;
    QPushButton *pushButton_40;
    QPushButton *pushButton_9;
    QPushButton *pushButton_12;
    QPushButton *pushButton_25;
    QPushButton *pushButton_30;
    QPushButton *pushButton_36;
    QPushButton *pushButton_42;
    QPushButton *pushButton_20;
    QPushButton *pushButton_6;
    QSpacerItem *horizontalSpacer_9;
    QSpacerItem *horizontalSpacer_10;

    void setupUi(QWidget *screen_17)
    {
        if (screen_17->objectName().isEmpty())
            screen_17->setObjectName(QString::fromUtf8("screen_17"));
        screen_17->resize(1024, 600);
        screen_17->setMinimumSize(QSize(1024, 600));
        screen_17->setMaximumSize(QSize(1024, 600));
        gridLayout = new QGridLayout(screen_17);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame_footer = new QFrame(screen_17);
        frame_footer->setObjectName(QString::fromUtf8("frame_footer"));
        frame_footer->setMinimumSize(QSize(1024, 80));
        frame_footer->setMaximumSize(QSize(1024, 80));
        frame_footer->setFrameShape(QFrame::StyledPanel);
        frame_footer->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_footer);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_15 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_15);

        pushButton_cancel = new QPushButton(frame_footer);
        pushButton_cancel->setObjectName(QString::fromUtf8("pushButton_cancel"));
        pushButton_cancel->setMinimumSize(QSize(50, 50));
        pushButton_cancel->setMaximumSize(QSize(50, 50));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/images/Icons/Cross.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_cancel->setIcon(icon);
        pushButton_cancel->setIconSize(QSize(50, 50));
        pushButton_cancel->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_cancel);

        horizontalSpacer_5 = new QSpacerItem(353, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);

        pushButton_home = new QPushButton(frame_footer);
        pushButton_home->setObjectName(QString::fromUtf8("pushButton_home"));
        pushButton_home->setMinimumSize(QSize(50, 50));
        pushButton_home->setMaximumSize(QSize(50, 50));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/images/Icons/home.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_home->setIcon(icon1);
        pushButton_home->setIconSize(QSize(50, 50));
        pushButton_home->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_home);

        horizontalSpacer_6 = new QSpacerItem(353, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);

        pushButton_done = new QPushButton(frame_footer);
        pushButton_done->setObjectName(QString::fromUtf8("pushButton_done"));
        pushButton_done->setMinimumSize(QSize(50, 50));
        pushButton_done->setMaximumSize(QSize(50, 50));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/images/Icons/check.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_done->setIcon(icon2);
        pushButton_done->setIconSize(QSize(50, 50));
        pushButton_done->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_done);

        horizontalSpacer_16 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_16);


        gridLayout->addWidget(frame_footer, 1, 0, 1, 1);

        frame_body = new QFrame(screen_17);
        frame_body->setObjectName(QString::fromUtf8("frame_body"));
        frame_body->setMinimumSize(QSize(1024, 520));
        frame_body->setMaximumSize(QSize(1024, 520));
        frame_body->setFrameShape(QFrame::StyledPanel);
        frame_body->setFrameShadow(QFrame::Raised);
        gridLayout_3 = new QGridLayout(frame_body);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setHorizontalSpacing(12);
        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_3->addItem(verticalSpacer_5, 0, 2, 1, 1);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_3->addItem(verticalSpacer_4, 2, 1, 1, 1);

        frame = new QFrame(frame_body);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setMinimumSize(QSize(250, 402));
        frame->setMaximumSize(QSize(250, 402));
        frame->setStyleSheet(QString::fromUtf8("background-color: rgb(231, 240, 255)"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        verticalLayout_4 = new QVBoxLayout(frame);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_2);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer_7 = new QSpacerItem(18, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_7);

        pushButton_program = new QPushButton(frame);
        pushButton_program->setObjectName(QString::fromUtf8("pushButton_program"));
        pushButton_program->setMinimumSize(QSize(90, 90));
        pushButton_program->setMaximumSize(QSize(90, 90));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/images/Icons/program 2.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_program->setIcon(icon3);
        pushButton_program->setIconSize(QSize(90, 90));
        pushButton_program->setFlat(true);

        horizontalLayout_4->addWidget(pushButton_program);

        horizontalSpacer_8 = new QSpacerItem(18, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_8);


        verticalLayout_3->addLayout(horizontalLayout_4);

        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(18);
        label->setFont(font);
        label->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(label);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_3);


        verticalLayout_4->addLayout(verticalLayout_3);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(18, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label_4 = new QLabel(frame);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMinimumSize(QSize(50, 50));
        label_4->setMaximumSize(QSize(50, 50));
        label_4->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/home filled.png")));

        horizontalLayout->addWidget(label_4);

        horizontalSpacer_2 = new QSpacerItem(18, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout_2->addLayout(horizontalLayout);

        label_2 = new QLabel(frame);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Sans Serif"));
        font1.setPointSize(14);
        font1.setBold(false);
        font1.setWeight(50);
        label_2->setFont(font1);
        label_2->setStyleSheet(QString::fromUtf8("color:black;"));
        label_2->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_2);


        horizontalLayout_5->addLayout(verticalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_4 = new QSpacerItem(18, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_4);

        label_5 = new QLabel(frame);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setMinimumSize(QSize(50, 50));
        label_5->setMaximumSize(QSize(50, 50));
        label_5->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/moon-filled.png")));

        horizontalLayout_2->addWidget(label_5);

        horizontalSpacer_3 = new QSpacerItem(18, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);


        verticalLayout->addLayout(horizontalLayout_2);

        label_3 = new QLabel(frame);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font1);
        label_3->setStyleSheet(QString::fromUtf8("color:black;"));
        label_3->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_3);


        horizontalLayout_5->addLayout(verticalLayout);


        verticalLayout_4->addLayout(horizontalLayout_5);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer);


        gridLayout_3->addWidget(frame, 1, 2, 1, 1);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        pushButton_45 = new QPushButton(frame_body);
        pushButton_45->setObjectName(QString::fromUtf8("pushButton_45"));
        pushButton_45->setMinimumSize(QSize(105, 45));
        pushButton_45->setMaximumSize(QSize(105, 45));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Sans Serif"));
        font2.setPointSize(14);
        pushButton_45->setFont(font2);
        pushButton_45->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/images/Icons/house.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_45->setIcon(icon4);
        pushButton_45->setIconSize(QSize(30, 30));
        pushButton_45->setFlat(true);

        gridLayout_2->addWidget(pushButton_45, 7, 3, 1, 1);

        pushButton_47 = new QPushButton(frame_body);
        pushButton_47->setObjectName(QString::fromUtf8("pushButton_47"));
        pushButton_47->setMinimumSize(QSize(105, 45));
        pushButton_47->setMaximumSize(QSize(105, 45));
        pushButton_47->setFont(font2);
        pushButton_47->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_47->setIcon(icon4);
        pushButton_47->setIconSize(QSize(30, 30));
        pushButton_47->setFlat(true);

        gridLayout_2->addWidget(pushButton_47, 7, 4, 1, 1);

        pushButton_44 = new QPushButton(frame_body);
        pushButton_44->setObjectName(QString::fromUtf8("pushButton_44"));
        pushButton_44->setMinimumSize(QSize(105, 45));
        pushButton_44->setMaximumSize(QSize(105, 45));
        pushButton_44->setFont(font2);
        pushButton_44->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_44->setIcon(icon4);
        pushButton_44->setIconSize(QSize(30, 30));
        pushButton_44->setFlat(true);

        gridLayout_2->addWidget(pushButton_44, 7, 2, 1, 1);

        pushButton_17 = new QPushButton(frame_body);
        pushButton_17->setObjectName(QString::fromUtf8("pushButton_17"));
        pushButton_17->setMinimumSize(QSize(105, 45));
        pushButton_17->setMaximumSize(QSize(105, 45));
        pushButton_17->setFont(font2);
        pushButton_17->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(155, 209, 255);\n"
""));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/images/images/Icons/moon.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_17->setIcon(icon5);
        pushButton_17->setIconSize(QSize(30, 30));
        pushButton_17->setFlat(true);

        gridLayout_2->addWidget(pushButton_17, 2, 5, 1, 1);

        pushButton_46 = new QPushButton(frame_body);
        pushButton_46->setObjectName(QString::fromUtf8("pushButton_46"));
        pushButton_46->setMinimumSize(QSize(105, 45));
        pushButton_46->setMaximumSize(QSize(105, 45));
        pushButton_46->setFont(font2);
        pushButton_46->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_46->setIcon(icon4);
        pushButton_46->setIconSize(QSize(30, 30));
        pushButton_46->setFlat(true);

        gridLayout_2->addWidget(pushButton_46, 7, 1, 1, 1);

        pushButton_10 = new QPushButton(frame_body);
        pushButton_10->setObjectName(QString::fromUtf8("pushButton_10"));
        pushButton_10->setMinimumSize(QSize(105, 45));
        pushButton_10->setMaximumSize(QSize(105, 45));
        pushButton_10->setFont(font2);
        pushButton_10->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_10->setIcon(icon4);
        pushButton_10->setIconSize(QSize(30, 30));
        pushButton_10->setFlat(true);

        gridLayout_2->addWidget(pushButton_10, 1, 4, 1, 1);

        pushButton_13 = new QPushButton(frame_body);
        pushButton_13->setObjectName(QString::fromUtf8("pushButton_13"));
        pushButton_13->setMinimumSize(QSize(105, 45));
        pushButton_13->setMaximumSize(QSize(105, 45));
        pushButton_13->setFont(font2);
        pushButton_13->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_13->setIcon(icon4);
        pushButton_13->setIconSize(QSize(30, 30));
        pushButton_13->setFlat(true);

        gridLayout_2->addWidget(pushButton_13, 2, 2, 1, 1);

        pushButton_43 = new QPushButton(frame_body);
        pushButton_43->setObjectName(QString::fromUtf8("pushButton_43"));
        pushButton_43->setMinimumSize(QSize(105, 45));
        pushButton_43->setMaximumSize(QSize(105, 45));
        pushButton_43->setFont(font2);
        pushButton_43->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_43->setIcon(icon4);
        pushButton_43->setIconSize(QSize(30, 30));
        pushButton_43->setFlat(true);

        gridLayout_2->addWidget(pushButton_43, 7, 0, 1, 1);

        pushButton_15 = new QPushButton(frame_body);
        pushButton_15->setObjectName(QString::fromUtf8("pushButton_15"));
        pushButton_15->setMinimumSize(QSize(105, 45));
        pushButton_15->setMaximumSize(QSize(105, 45));
        pushButton_15->setFont(font2);
        pushButton_15->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_15->setIcon(icon4);
        pushButton_15->setIconSize(QSize(30, 30));
        pushButton_15->setFlat(true);

        gridLayout_2->addWidget(pushButton_15, 2, 1, 1, 1);

        pushButton_26 = new QPushButton(frame_body);
        pushButton_26->setObjectName(QString::fromUtf8("pushButton_26"));
        pushButton_26->setMinimumSize(QSize(105, 45));
        pushButton_26->setMaximumSize(QSize(105, 45));
        pushButton_26->setFont(font2);
        pushButton_26->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(155, 209, 255);\n"
""));
        pushButton_26->setIcon(icon5);
        pushButton_26->setIconSize(QSize(30, 30));
        pushButton_26->setFlat(true);

        gridLayout_2->addWidget(pushButton_26, 4, 0, 1, 1);

        pushButton_24 = new QPushButton(frame_body);
        pushButton_24->setObjectName(QString::fromUtf8("pushButton_24"));
        pushButton_24->setMinimumSize(QSize(105, 45));
        pushButton_24->setMaximumSize(QSize(105, 45));
        pushButton_24->setFont(font2);
        pushButton_24->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(155, 209, 255);\n"
""));
        pushButton_24->setIcon(icon5);
        pushButton_24->setIconSize(QSize(30, 30));
        pushButton_24->setFlat(true);

        gridLayout_2->addWidget(pushButton_24, 3, 3, 1, 1);

        pushButton_23 = new QPushButton(frame_body);
        pushButton_23->setObjectName(QString::fromUtf8("pushButton_23"));
        pushButton_23->setMinimumSize(QSize(105, 45));
        pushButton_23->setMaximumSize(QSize(105, 45));
        pushButton_23->setFont(font2);
        pushButton_23->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(155, 209, 255);\n"
""));
        pushButton_23->setIcon(icon5);
        pushButton_23->setIconSize(QSize(30, 30));
        pushButton_23->setFlat(true);

        gridLayout_2->addWidget(pushButton_23, 3, 5, 1, 1);

        pushButton_16 = new QPushButton(frame_body);
        pushButton_16->setObjectName(QString::fromUtf8("pushButton_16"));
        pushButton_16->setMinimumSize(QSize(105, 45));
        pushButton_16->setMaximumSize(QSize(105, 45));
        pushButton_16->setFont(font2);
        pushButton_16->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(155, 209, 255);\n"
""));
        pushButton_16->setIcon(icon5);
        pushButton_16->setIconSize(QSize(30, 30));
        pushButton_16->setFlat(true);

        gridLayout_2->addWidget(pushButton_16, 2, 4, 1, 1);

        pushButton_11 = new QPushButton(frame_body);
        pushButton_11->setObjectName(QString::fromUtf8("pushButton_11"));
        pushButton_11->setMinimumSize(QSize(105, 45));
        pushButton_11->setMaximumSize(QSize(105, 45));
        pushButton_11->setFont(font2);
        pushButton_11->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_11->setIcon(icon4);
        pushButton_11->setIconSize(QSize(30, 30));
        pushButton_11->setFlat(true);

        gridLayout_2->addWidget(pushButton_11, 1, 5, 1, 1);

        pushButton_3 = new QPushButton(frame_body);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        pushButton_3->setMinimumSize(QSize(105, 45));
        pushButton_3->setMaximumSize(QSize(105, 45));
        pushButton_3->setFont(font2);
        pushButton_3->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_3->setIcon(icon4);
        pushButton_3->setIconSize(QSize(30, 30));
        pushButton_3->setFlat(true);

        gridLayout_2->addWidget(pushButton_3, 0, 2, 1, 1);

        pushButton_48 = new QPushButton(frame_body);
        pushButton_48->setObjectName(QString::fromUtf8("pushButton_48"));
        pushButton_48->setMinimumSize(QSize(105, 45));
        pushButton_48->setMaximumSize(QSize(105, 45));
        pushButton_48->setFont(font2);
        pushButton_48->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_48->setIcon(icon4);
        pushButton_48->setIconSize(QSize(30, 30));
        pushButton_48->setFlat(true);

        gridLayout_2->addWidget(pushButton_48, 7, 5, 1, 1);

        pushButton = new QPushButton(frame_body);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setMinimumSize(QSize(105, 45));
        pushButton->setMaximumSize(QSize(105, 45));
        pushButton->setFont(font2);
        pushButton->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton->setIcon(icon4);
        pushButton->setIconSize(QSize(30, 30));
        pushButton->setFlat(true);

        gridLayout_2->addWidget(pushButton, 0, 0, 1, 1);

        pushButton_2 = new QPushButton(frame_body);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setMinimumSize(QSize(105, 45));
        pushButton_2->setMaximumSize(QSize(105, 45));
        pushButton_2->setFont(font2);
        pushButton_2->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_2->setIcon(icon4);
        pushButton_2->setIconSize(QSize(30, 30));
        pushButton_2->setFlat(true);

        gridLayout_2->addWidget(pushButton_2, 0, 1, 1, 1);

        pushButton_8 = new QPushButton(frame_body);
        pushButton_8->setObjectName(QString::fromUtf8("pushButton_8"));
        pushButton_8->setMinimumSize(QSize(105, 45));
        pushButton_8->setMaximumSize(QSize(105, 45));
        pushButton_8->setFont(font2);
        pushButton_8->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_8->setIcon(icon4);
        pushButton_8->setIconSize(QSize(30, 30));
        pushButton_8->setFlat(true);

        gridLayout_2->addWidget(pushButton_8, 1, 0, 1, 1);

        pushButton_31 = new QPushButton(frame_body);
        pushButton_31->setObjectName(QString::fromUtf8("pushButton_31"));
        pushButton_31->setMinimumSize(QSize(105, 45));
        pushButton_31->setMaximumSize(QSize(105, 45));
        pushButton_31->setFont(font2);
        pushButton_31->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(155, 209, 255);\n"
""));
        pushButton_31->setIcon(icon5);
        pushButton_31->setIconSize(QSize(30, 30));
        pushButton_31->setFlat(true);

        gridLayout_2->addWidget(pushButton_31, 5, 2, 1, 1);

        pushButton_33 = new QPushButton(frame_body);
        pushButton_33->setObjectName(QString::fromUtf8("pushButton_33"));
        pushButton_33->setMinimumSize(QSize(105, 45));
        pushButton_33->setMaximumSize(QSize(105, 45));
        pushButton_33->setFont(font2);
        pushButton_33->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(155, 209, 255);\n"
""));
        pushButton_33->setIcon(icon5);
        pushButton_33->setIconSize(QSize(30, 30));
        pushButton_33->setFlat(true);

        gridLayout_2->addWidget(pushButton_33, 5, 1, 1, 1);

        pushButton_22 = new QPushButton(frame_body);
        pushButton_22->setObjectName(QString::fromUtf8("pushButton_22"));
        pushButton_22->setMinimumSize(QSize(105, 45));
        pushButton_22->setMaximumSize(QSize(105, 45));
        pushButton_22->setFont(font2);
        pushButton_22->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(155, 209, 255);\n"
""));
        pushButton_22->setIcon(icon5);
        pushButton_22->setIconSize(QSize(30, 30));
        pushButton_22->setFlat(true);

        gridLayout_2->addWidget(pushButton_22, 3, 4, 1, 1);

        pushButton_29 = new QPushButton(frame_body);
        pushButton_29->setObjectName(QString::fromUtf8("pushButton_29"));
        pushButton_29->setMinimumSize(QSize(105, 45));
        pushButton_29->setMaximumSize(QSize(105, 45));
        pushButton_29->setFont(font2);
        pushButton_29->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(155, 209, 255);\n"
""));
        pushButton_29->setIcon(icon5);
        pushButton_29->setIconSize(QSize(30, 30));
        pushButton_29->setFlat(true);

        gridLayout_2->addWidget(pushButton_29, 4, 5, 1, 1);

        pushButton_37 = new QPushButton(frame_body);
        pushButton_37->setObjectName(QString::fromUtf8("pushButton_37"));
        pushButton_37->setMinimumSize(QSize(105, 45));
        pushButton_37->setMaximumSize(QSize(105, 45));
        pushButton_37->setFont(font2);
        pushButton_37->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_37->setIcon(icon4);
        pushButton_37->setIconSize(QSize(30, 30));
        pushButton_37->setFlat(true);

        gridLayout_2->addWidget(pushButton_37, 6, 2, 1, 1);

        pushButton_39 = new QPushButton(frame_body);
        pushButton_39->setObjectName(QString::fromUtf8("pushButton_39"));
        pushButton_39->setMinimumSize(QSize(105, 45));
        pushButton_39->setMaximumSize(QSize(105, 45));
        pushButton_39->setFont(font2);
        pushButton_39->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_39->setIcon(icon4);
        pushButton_39->setIconSize(QSize(30, 30));
        pushButton_39->setFlat(true);

        gridLayout_2->addWidget(pushButton_39, 6, 1, 1, 1);

        pushButton_27 = new QPushButton(frame_body);
        pushButton_27->setObjectName(QString::fromUtf8("pushButton_27"));
        pushButton_27->setMinimumSize(QSize(105, 45));
        pushButton_27->setMaximumSize(QSize(105, 45));
        pushButton_27->setFont(font2);
        pushButton_27->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(155, 209, 255);\n"
""));
        pushButton_27->setIcon(icon5);
        pushButton_27->setIconSize(QSize(30, 30));
        pushButton_27->setFlat(true);

        gridLayout_2->addWidget(pushButton_27, 4, 1, 1, 1);

        pushButton_7 = new QPushButton(frame_body);
        pushButton_7->setObjectName(QString::fromUtf8("pushButton_7"));
        pushButton_7->setMinimumSize(QSize(105, 45));
        pushButton_7->setMaximumSize(QSize(105, 45));
        pushButton_7->setFont(font2);
        pushButton_7->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_7->setIcon(icon4);
        pushButton_7->setIconSize(QSize(30, 30));
        pushButton_7->setFlat(true);

        gridLayout_2->addWidget(pushButton_7, 1, 2, 1, 1);

        pushButton_21 = new QPushButton(frame_body);
        pushButton_21->setObjectName(QString::fromUtf8("pushButton_21"));
        pushButton_21->setMinimumSize(QSize(105, 45));
        pushButton_21->setMaximumSize(QSize(105, 45));
        pushButton_21->setFont(font2);
        pushButton_21->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(155, 209, 255);\n"
""));
        pushButton_21->setIcon(icon5);
        pushButton_21->setIconSize(QSize(30, 30));
        pushButton_21->setFlat(true);

        gridLayout_2->addWidget(pushButton_21, 3, 1, 1, 1);

        pushButton_19 = new QPushButton(frame_body);
        pushButton_19->setObjectName(QString::fromUtf8("pushButton_19"));
        pushButton_19->setMinimumSize(QSize(105, 45));
        pushButton_19->setMaximumSize(QSize(105, 45));
        pushButton_19->setFont(font2);
        pushButton_19->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(155, 209, 255);\n"
""));
        pushButton_19->setIcon(icon5);
        pushButton_19->setIconSize(QSize(30, 30));
        pushButton_19->setFlat(true);

        gridLayout_2->addWidget(pushButton_19, 3, 2, 1, 1);

        pushButton_28 = new QPushButton(frame_body);
        pushButton_28->setObjectName(QString::fromUtf8("pushButton_28"));
        pushButton_28->setMinimumSize(QSize(105, 45));
        pushButton_28->setMaximumSize(QSize(105, 45));
        pushButton_28->setFont(font2);
        pushButton_28->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(155, 209, 255);\n"
""));
        pushButton_28->setIcon(icon5);
        pushButton_28->setIconSize(QSize(30, 30));
        pushButton_28->setFlat(true);

        gridLayout_2->addWidget(pushButton_28, 4, 4, 1, 1);

        pushButton_35 = new QPushButton(frame_body);
        pushButton_35->setObjectName(QString::fromUtf8("pushButton_35"));
        pushButton_35->setMinimumSize(QSize(105, 45));
        pushButton_35->setMaximumSize(QSize(105, 45));
        pushButton_35->setFont(font2);
        pushButton_35->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(155, 209, 255);\n"
""));
        pushButton_35->setIcon(icon5);
        pushButton_35->setIconSize(QSize(30, 30));
        pushButton_35->setFlat(true);

        gridLayout_2->addWidget(pushButton_35, 5, 5, 1, 1);

        pushButton_41 = new QPushButton(frame_body);
        pushButton_41->setObjectName(QString::fromUtf8("pushButton_41"));
        pushButton_41->setMinimumSize(QSize(105, 45));
        pushButton_41->setMaximumSize(QSize(105, 45));
        pushButton_41->setFont(font2);
        pushButton_41->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_41->setIcon(icon4);
        pushButton_41->setIconSize(QSize(30, 30));
        pushButton_41->setFlat(true);

        gridLayout_2->addWidget(pushButton_41, 6, 5, 1, 1);

        pushButton_5 = new QPushButton(frame_body);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));
        pushButton_5->setMinimumSize(QSize(105, 45));
        pushButton_5->setMaximumSize(QSize(105, 45));
        pushButton_5->setFont(font2);
        pushButton_5->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_5->setIcon(icon4);
        pushButton_5->setIconSize(QSize(30, 30));
        pushButton_5->setFlat(true);

        gridLayout_2->addWidget(pushButton_5, 0, 3, 1, 1);

        pushButton_32 = new QPushButton(frame_body);
        pushButton_32->setObjectName(QString::fromUtf8("pushButton_32"));
        pushButton_32->setMinimumSize(QSize(105, 45));
        pushButton_32->setMaximumSize(QSize(105, 45));
        pushButton_32->setFont(font2);
        pushButton_32->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(155, 209, 255);\n"
""));
        pushButton_32->setIcon(icon5);
        pushButton_32->setIconSize(QSize(30, 30));
        pushButton_32->setFlat(true);

        gridLayout_2->addWidget(pushButton_32, 5, 0, 1, 1);

        pushButton_18 = new QPushButton(frame_body);
        pushButton_18->setObjectName(QString::fromUtf8("pushButton_18"));
        pushButton_18->setMinimumSize(QSize(105, 45));
        pushButton_18->setMaximumSize(QSize(105, 45));
        pushButton_18->setFont(font2);
        pushButton_18->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(155, 209, 255);\n"
""));
        pushButton_18->setIcon(icon5);
        pushButton_18->setIconSize(QSize(30, 30));
        pushButton_18->setFlat(true);

        gridLayout_2->addWidget(pushButton_18, 2, 3, 1, 1);

        pushButton_14 = new QPushButton(frame_body);
        pushButton_14->setObjectName(QString::fromUtf8("pushButton_14"));
        pushButton_14->setMinimumSize(QSize(105, 45));
        pushButton_14->setMaximumSize(QSize(105, 45));
        pushButton_14->setFont(font2);
        pushButton_14->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_14->setIcon(icon4);
        pushButton_14->setIconSize(QSize(30, 30));
        pushButton_14->setFlat(true);

        gridLayout_2->addWidget(pushButton_14, 2, 0, 1, 1);

        pushButton_4 = new QPushButton(frame_body);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));
        pushButton_4->setMinimumSize(QSize(105, 45));
        pushButton_4->setMaximumSize(QSize(105, 45));
        pushButton_4->setFont(font2);
        pushButton_4->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_4->setIcon(icon4);
        pushButton_4->setIconSize(QSize(30, 30));
        pushButton_4->setFlat(true);

        gridLayout_2->addWidget(pushButton_4, 0, 5, 1, 1);

        pushButton_34 = new QPushButton(frame_body);
        pushButton_34->setObjectName(QString::fromUtf8("pushButton_34"));
        pushButton_34->setMinimumSize(QSize(105, 45));
        pushButton_34->setMaximumSize(QSize(105, 45));
        pushButton_34->setFont(font2);
        pushButton_34->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(155, 209, 255);\n"
""));
        pushButton_34->setIcon(icon5);
        pushButton_34->setIconSize(QSize(30, 30));
        pushButton_34->setFlat(true);

        gridLayout_2->addWidget(pushButton_34, 5, 4, 1, 1);

        pushButton_38 = new QPushButton(frame_body);
        pushButton_38->setObjectName(QString::fromUtf8("pushButton_38"));
        pushButton_38->setMinimumSize(QSize(105, 45));
        pushButton_38->setMaximumSize(QSize(105, 45));
        pushButton_38->setFont(font2);
        pushButton_38->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_38->setIcon(icon4);
        pushButton_38->setIconSize(QSize(30, 30));
        pushButton_38->setFlat(true);

        gridLayout_2->addWidget(pushButton_38, 6, 0, 1, 1);

        pushButton_40 = new QPushButton(frame_body);
        pushButton_40->setObjectName(QString::fromUtf8("pushButton_40"));
        pushButton_40->setMinimumSize(QSize(105, 45));
        pushButton_40->setMaximumSize(QSize(105, 45));
        pushButton_40->setFont(font2);
        pushButton_40->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_40->setIcon(icon4);
        pushButton_40->setIconSize(QSize(30, 30));
        pushButton_40->setFlat(true);

        gridLayout_2->addWidget(pushButton_40, 6, 4, 1, 1);

        pushButton_9 = new QPushButton(frame_body);
        pushButton_9->setObjectName(QString::fromUtf8("pushButton_9"));
        pushButton_9->setMinimumSize(QSize(105, 45));
        pushButton_9->setMaximumSize(QSize(105, 45));
        pushButton_9->setFont(font2);
        pushButton_9->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_9->setIcon(icon4);
        pushButton_9->setIconSize(QSize(30, 30));
        pushButton_9->setFlat(true);

        gridLayout_2->addWidget(pushButton_9, 1, 1, 1, 1);

        pushButton_12 = new QPushButton(frame_body);
        pushButton_12->setObjectName(QString::fromUtf8("pushButton_12"));
        pushButton_12->setMinimumSize(QSize(105, 45));
        pushButton_12->setMaximumSize(QSize(105, 45));
        pushButton_12->setFont(font2);
        pushButton_12->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_12->setIcon(icon4);
        pushButton_12->setIconSize(QSize(30, 30));
        pushButton_12->setFlat(true);

        gridLayout_2->addWidget(pushButton_12, 1, 3, 1, 1);

        pushButton_25 = new QPushButton(frame_body);
        pushButton_25->setObjectName(QString::fromUtf8("pushButton_25"));
        pushButton_25->setMinimumSize(QSize(105, 45));
        pushButton_25->setMaximumSize(QSize(105, 45));
        pushButton_25->setFont(font2);
        pushButton_25->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(155, 209, 255);\n"
""));
        pushButton_25->setIcon(icon5);
        pushButton_25->setIconSize(QSize(30, 30));
        pushButton_25->setFlat(true);

        gridLayout_2->addWidget(pushButton_25, 4, 2, 1, 1);

        pushButton_30 = new QPushButton(frame_body);
        pushButton_30->setObjectName(QString::fromUtf8("pushButton_30"));
        pushButton_30->setMinimumSize(QSize(105, 45));
        pushButton_30->setMaximumSize(QSize(105, 45));
        pushButton_30->setFont(font2);
        pushButton_30->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(155, 209, 255);\n"
""));
        pushButton_30->setIcon(icon5);
        pushButton_30->setIconSize(QSize(30, 30));
        pushButton_30->setFlat(true);

        gridLayout_2->addWidget(pushButton_30, 4, 3, 1, 1);

        pushButton_36 = new QPushButton(frame_body);
        pushButton_36->setObjectName(QString::fromUtf8("pushButton_36"));
        pushButton_36->setMinimumSize(QSize(105, 45));
        pushButton_36->setMaximumSize(QSize(105, 45));
        pushButton_36->setFont(font2);
        pushButton_36->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(155, 209, 255);\n"
""));
        pushButton_36->setIcon(icon5);
        pushButton_36->setIconSize(QSize(30, 30));
        pushButton_36->setFlat(true);

        gridLayout_2->addWidget(pushButton_36, 5, 3, 1, 1);

        pushButton_42 = new QPushButton(frame_body);
        pushButton_42->setObjectName(QString::fromUtf8("pushButton_42"));
        pushButton_42->setMinimumSize(QSize(105, 45));
        pushButton_42->setMaximumSize(QSize(105, 45));
        pushButton_42->setFont(font2);
        pushButton_42->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_42->setIcon(icon4);
        pushButton_42->setIconSize(QSize(30, 30));
        pushButton_42->setFlat(true);

        gridLayout_2->addWidget(pushButton_42, 6, 3, 1, 1);

        pushButton_20 = new QPushButton(frame_body);
        pushButton_20->setObjectName(QString::fromUtf8("pushButton_20"));
        pushButton_20->setMinimumSize(QSize(105, 45));
        pushButton_20->setMaximumSize(QSize(105, 45));
        pushButton_20->setFont(font2);
        pushButton_20->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(155, 209, 255);\n"
""));
        pushButton_20->setIcon(icon5);
        pushButton_20->setIconSize(QSize(30, 30));
        pushButton_20->setFlat(true);

        gridLayout_2->addWidget(pushButton_20, 3, 0, 1, 1);

        pushButton_6 = new QPushButton(frame_body);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));
        pushButton_6->setMinimumSize(QSize(105, 45));
        pushButton_6->setMaximumSize(QSize(105, 45));
        pushButton_6->setFont(font2);
        pushButton_6->setStyleSheet(QString::fromUtf8("border:1px solid black;\n"
"background-color: rgb(103, 169, 255);\n"
"color:white;"));
        pushButton_6->setIcon(icon4);
        pushButton_6->setIconSize(QSize(30, 30));
        pushButton_6->setFlat(true);

        gridLayout_2->addWidget(pushButton_6, 0, 4, 1, 1);


        gridLayout_3->addLayout(gridLayout_2, 1, 1, 1, 1);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_9, 1, 3, 1, 1);

        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_10, 1, 0, 1, 1);


        gridLayout->addWidget(frame_body, 0, 0, 1, 1);


        retranslateUi(screen_17);

        QMetaObject::connectSlotsByName(screen_17);
    } // setupUi

    void retranslateUi(QWidget *screen_17)
    {
        screen_17->setWindowTitle(QApplication::translate("screen_17", "Form", 0, QApplication::UnicodeUTF8));
        pushButton_home->setText(QString());
        pushButton_done->setText(QString());
        pushButton_program->setText(QString());
        label->setText(QApplication::translate("screen_17", "Program 1", 0, QApplication::UnicodeUTF8));
        label_4->setText(QString());
        label_2->setText(QApplication::translate("screen_17", "Comfort", 0, QApplication::UnicodeUTF8));
        label_5->setText(QString());
        label_3->setText(QApplication::translate("screen_17", "Night", 0, QApplication::UnicodeUTF8));
        pushButton_45->setText(QApplication::translate("screen_17", "22:30", 0, QApplication::UnicodeUTF8));
        pushButton_47->setText(QApplication::translate("screen_17", "23:00", 0, QApplication::UnicodeUTF8));
        pushButton_44->setText(QApplication::translate("screen_17", "22:00", 0, QApplication::UnicodeUTF8));
        pushButton_17->setText(QApplication::translate("screen_17", "08:30", 0, QApplication::UnicodeUTF8));
        pushButton_46->setText(QApplication::translate("screen_17", "21:30", 0, QApplication::UnicodeUTF8));
        pushButton_10->setText(QApplication::translate("screen_17", "05:00", 0, QApplication::UnicodeUTF8));
        pushButton_13->setText(QApplication::translate("screen_17", "07:00", 0, QApplication::UnicodeUTF8));
        pushButton_43->setText(QApplication::translate("screen_17", "21:00", 0, QApplication::UnicodeUTF8));
        pushButton_15->setText(QApplication::translate("screen_17", "06:30", 0, QApplication::UnicodeUTF8));
        pushButton_26->setText(QApplication::translate("screen_17", "12:00", 0, QApplication::UnicodeUTF8));
        pushButton_24->setText(QApplication::translate("screen_17", "10:30", 0, QApplication::UnicodeUTF8));
        pushButton_23->setText(QApplication::translate("screen_17", "11:30", 0, QApplication::UnicodeUTF8));
        pushButton_16->setText(QApplication::translate("screen_17", "08:00", 0, QApplication::UnicodeUTF8));
        pushButton_11->setText(QApplication::translate("screen_17", "05:30", 0, QApplication::UnicodeUTF8));
        pushButton_3->setText(QApplication::translate("screen_17", "01:00", 0, QApplication::UnicodeUTF8));
        pushButton_48->setText(QApplication::translate("screen_17", "23:30", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("screen_17", "00:00", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("screen_17", "00:30", 0, QApplication::UnicodeUTF8));
        pushButton_8->setText(QApplication::translate("screen_17", "03:00", 0, QApplication::UnicodeUTF8));
        pushButton_31->setText(QApplication::translate("screen_17", "16:00", 0, QApplication::UnicodeUTF8));
        pushButton_33->setText(QApplication::translate("screen_17", "15:30", 0, QApplication::UnicodeUTF8));
        pushButton_22->setText(QApplication::translate("screen_17", "11:00", 0, QApplication::UnicodeUTF8));
        pushButton_29->setText(QApplication::translate("screen_17", "14:30", 0, QApplication::UnicodeUTF8));
        pushButton_37->setText(QApplication::translate("screen_17", "19:00", 0, QApplication::UnicodeUTF8));
        pushButton_39->setText(QApplication::translate("screen_17", "18:30", 0, QApplication::UnicodeUTF8));
        pushButton_27->setText(QApplication::translate("screen_17", "12:30", 0, QApplication::UnicodeUTF8));
        pushButton_7->setText(QApplication::translate("screen_17", "04:00", 0, QApplication::UnicodeUTF8));
        pushButton_21->setText(QApplication::translate("screen_17", "09:30", 0, QApplication::UnicodeUTF8));
        pushButton_19->setText(QApplication::translate("screen_17", "10:00", 0, QApplication::UnicodeUTF8));
        pushButton_28->setText(QApplication::translate("screen_17", "14:00", 0, QApplication::UnicodeUTF8));
        pushButton_35->setText(QApplication::translate("screen_17", "17:30", 0, QApplication::UnicodeUTF8));
        pushButton_41->setText(QApplication::translate("screen_17", "20:30", 0, QApplication::UnicodeUTF8));
        pushButton_5->setText(QApplication::translate("screen_17", "01:30", 0, QApplication::UnicodeUTF8));
        pushButton_32->setText(QApplication::translate("screen_17", "15:00", 0, QApplication::UnicodeUTF8));
        pushButton_18->setText(QApplication::translate("screen_17", "07:30", 0, QApplication::UnicodeUTF8));
        pushButton_14->setText(QApplication::translate("screen_17", "06:00", 0, QApplication::UnicodeUTF8));
        pushButton_4->setText(QApplication::translate("screen_17", "02:30", 0, QApplication::UnicodeUTF8));
        pushButton_34->setText(QApplication::translate("screen_17", "17:00", 0, QApplication::UnicodeUTF8));
        pushButton_38->setText(QApplication::translate("screen_17", "18:00", 0, QApplication::UnicodeUTF8));
        pushButton_40->setText(QApplication::translate("screen_17", "20:00", 0, QApplication::UnicodeUTF8));
        pushButton_9->setText(QApplication::translate("screen_17", "03:30", 0, QApplication::UnicodeUTF8));
        pushButton_12->setText(QApplication::translate("screen_17", "04:30", 0, QApplication::UnicodeUTF8));
        pushButton_25->setText(QApplication::translate("screen_17", "13:00", 0, QApplication::UnicodeUTF8));
        pushButton_30->setText(QApplication::translate("screen_17", "13:30", 0, QApplication::UnicodeUTF8));
        pushButton_36->setText(QApplication::translate("screen_17", "16:30", 0, QApplication::UnicodeUTF8));
        pushButton_42->setText(QApplication::translate("screen_17", "19:30", 0, QApplication::UnicodeUTF8));
        pushButton_20->setText(QApplication::translate("screen_17", "09:00", 0, QApplication::UnicodeUTF8));
        pushButton_6->setText(QApplication::translate("screen_17", "02:00", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class screen_17: public Ui_screen_17 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREEN_17_H
