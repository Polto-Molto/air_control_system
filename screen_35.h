#ifndef SCREEN_35_H
#define SCREEN_35_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_35;
}

class screen_35 : public QWidget
{
    Q_OBJECT
    
public:
    explicit screen_35(dataManager*,QWidget *parent = 0);
    ~screen_35();
signals:
      void touched(int);

public slots:
      void on_pushButton_home_clicked();
      void on_pushButton_done_clicked();

private:
    Ui::screen_35 *ui;
    dataManager *_datamgr;
};

#endif // SCREEN_35_H
