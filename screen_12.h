#ifndef SCREEN_12_H
#define SCREEN_12_H

#include <QWidget>
#include "datamanager.h"

namespace Ui {
class screen_12;
}

class screen_12 : public QWidget
{
    Q_OBJECT

public:
    explicit screen_12(dataManager*,QWidget *parent = 0);
    ~screen_12();
signals:
      void touched(int);

public slots:
    void on_pushButton_home_clicked();
    void on_pushButton_done_clicked();
    void on_pushButton_cancel_clicked();

private:
    Ui::screen_12 *ui;
    dataManager *_datamgr;

};

#endif // SCREEN_12_H
