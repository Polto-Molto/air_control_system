#include "screen_39.h"
#include "ui_screen_39.h"

screen_39::screen_39(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_39),
    _datamgr(dm)
{
    ui->setupUi(this);
    ui->label_temp_value->setNum(20);
}

screen_39::~screen_39()
{
    delete ui;
}

void screen_39::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_39::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(37));
}

void screen_39::on_pushButton_done_clicked()
{
    Q_EMIT(touched(9));
}

void screen_39::on_pushButton_temp_up_clicked()
{
    int value = ui->label_temp_value->text().toInt();
    ui->label_temp_value->setNum(value + 1);
}

void screen_39::on_pushButton_temp_down_clicked()
{
    int value = ui->label_temp_value->text().toInt();
    ui->label_temp_value->setNum(value - 1);
}
