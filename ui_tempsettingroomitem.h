/********************************************************************************
** Form generated from reading UI file 'tempsettingroomitem.ui'
**
** Created: Thu Apr 13 10:38:20 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TEMPSETTINGROOMITEM_H
#define UI_TEMPSETTINGROOMITEM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_tempSettingRoomItem
{
public:
    QGridLayout *gridLayout;
    QFrame *frame;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_room_number;
    QSpacerItem *verticalSpacer;
    QVBoxLayout *verticalLayout;
    QLabel *label_room_name;
    QHBoxLayout *horizontalLayout;
    QLabel *label_3;
    QLabel *label_temp;
    QLabel *label_5;
    QSpacerItem *horizontalSpacer;
    QLabel *label_room_point_type;
    QLabel *label_temp_point;

    void setupUi(QWidget *tempSettingRoomItem)
    {
        if (tempSettingRoomItem->objectName().isEmpty())
            tempSettingRoomItem->setObjectName(QString::fromUtf8("tempSettingRoomItem"));
        tempSettingRoomItem->resize(430, 80);
        tempSettingRoomItem->setMinimumSize(QSize(430, 80));
        tempSettingRoomItem->setMaximumSize(QSize(430, 80));
        gridLayout = new QGridLayout(tempSettingRoomItem);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame = new QFrame(tempSettingRoomItem);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setStyleSheet(QString::fromUtf8("#frame{\n"
"border:1px solid;\n"
"border-color: rgb(85, 170, 255);\n"
"border-radius:8px;\n"
"}"));
        frame->setFrameShape(QFrame::NoFrame);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(frame);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_room_number = new QLabel(frame);
        label_room_number->setObjectName(QString::fromUtf8("label_room_number"));
        label_room_number->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_room_number);

        verticalSpacer = new QSpacerItem(20, 28, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);


        horizontalLayout_2->addLayout(verticalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label_room_name = new QLabel(frame);
        label_room_name->setObjectName(QString::fromUtf8("label_room_name"));

        verticalLayout->addWidget(label_room_name);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_3 = new QLabel(frame);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMinimumSize(QSize(15, 30));
        label_3->setMaximumSize(QSize(15, 30));
        label_3->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/Thermo-Meter-.png")));
        label_3->setScaledContents(true);

        horizontalLayout->addWidget(label_3);

        label_temp = new QLabel(frame);
        label_temp->setObjectName(QString::fromUtf8("label_temp"));

        horizontalLayout->addWidget(label_temp);

        label_5 = new QLabel(frame);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout->addWidget(label_5);


        verticalLayout->addLayout(horizontalLayout);


        horizontalLayout_2->addLayout(verticalLayout);

        horizontalSpacer = new QSpacerItem(137, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        label_room_point_type = new QLabel(frame);
        label_room_point_type->setObjectName(QString::fromUtf8("label_room_point_type"));
        label_room_point_type->setMinimumSize(QSize(40, 40));
        label_room_point_type->setMaximumSize(QSize(40, 40));
        label_room_point_type->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/comfrot.png")));
        label_room_point_type->setScaledContents(true);

        horizontalLayout_2->addWidget(label_room_point_type);

        label_temp_point = new QLabel(frame);
        label_temp_point->setObjectName(QString::fromUtf8("label_temp_point"));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(18);
        label_temp_point->setFont(font);

        horizontalLayout_2->addWidget(label_temp_point);


        gridLayout->addWidget(frame, 0, 0, 1, 1);


        retranslateUi(tempSettingRoomItem);

        QMetaObject::connectSlotsByName(tempSettingRoomItem);
    } // setupUi

    void retranslateUi(QWidget *tempSettingRoomItem)
    {
        tempSettingRoomItem->setWindowTitle(QApplication::translate("tempSettingRoomItem", "Form", 0, QApplication::UnicodeUTF8));
        label_room_number->setText(QApplication::translate("tempSettingRoomItem", "1", 0, QApplication::UnicodeUTF8));
        label_room_name->setText(QApplication::translate("tempSettingRoomItem", "Kids Room", 0, QApplication::UnicodeUTF8));
        label_3->setText(QString());
        label_temp->setText(QApplication::translate("tempSettingRoomItem", "25", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("tempSettingRoomItem", "C", 0, QApplication::UnicodeUTF8));
        label_room_point_type->setText(QString());
        label_temp_point->setText(QApplication::translate("tempSettingRoomItem", "27", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class tempSettingRoomItem: public Ui_tempSettingRoomItem {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TEMPSETTINGROOMITEM_H
