#include "screen_9.h"
#include "ui_screen_9.h"

screen_9::screen_9(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_9),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_9::~screen_9()
{
    delete ui;
}

void screen_9::on_pushButton_apply_only_clicked()
{
    Q_EMIT(touched(5));
}

void screen_9::on_pushButton_apply_all_clicked()
{
    Q_EMIT(touched(10));
}
