#include "screen_32.h"
#include "ui_screen_32.h"

screen_32::screen_32(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_32),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_32::~screen_32()
{
    delete ui;
}

void screen_32::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_32::on_pushButton_done_clicked()
{
    Q_EMIT(touched(21));
}
