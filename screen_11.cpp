#include "screen_11.h"
#include "ui_screen_11.h"

screen_11::screen_11(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_11),
    _datamgr(dm)
{
    ui->setupUi(this);
    ui->label_comfort_value->setNum(20);
    ui->label_humidity_value->setNum(20);
    ui->label_night_value->setNum(20);
}

screen_11::~screen_11()
{
    delete ui;
}

void screen_11::setCurrentClima(CLIMA_MODBUS clima)
{
    _clima = clima;
    ui->label_comfort_value->setNum(_clima.summer_comfort_temp_point);
    ui->label_night_value->setNum(_clima.summer_night_temp_point);
    ui->label_humidity_value->setNum(_clima.summer_humidity_point);
    Q_EMIT(touched(11));
}

void screen_11::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_11::on_pushButton_done_clicked()
{
    _clima.summer_comfort_temp_point = ui->label_comfort_value->text().toInt();
    _clima.summer_night_temp_point = ui->label_night_value->text().toInt();
    _clima.summer_humidity_point = ui->label_humidity_value->text().toInt();

    _datamgr->updateClimaConfigData(_clima);
    Q_EMIT(touched(5));
}

void screen_11::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(5));
}

void screen_11::on_pushButton_comfort_up_clicked()
{
    int value = ui->label_comfort_value->text().toInt();
    ui->label_comfort_value->setNum(value + 1);
}

void screen_11::on_pushButton_comfort_down_clicked()
{
    int value = ui->label_comfort_value->text().toInt();
    ui->label_comfort_value->setNum(value - 1);
}

void screen_11::on_pushButton_humidity_up_clicked()
{
    int value = ui->label_humidity_value->text().toInt();
    ui->label_humidity_value->setNum(value + 1);
}

void screen_11::on_pushButton_humidity_down_clicked()
{
    int value = ui->label_humidity_value->text().toInt();
    ui->label_humidity_value->setNum(value - 1);
}

void screen_11::on_pushButton_night_up_clicked()
{
    int value = ui->label_night_value->text().toInt();
    ui->label_night_value->setNum(value + 1);
}

void screen_11::on_pushButton_night_down_clicked()
{
    int value = ui->label_night_value->text().toInt();
    ui->label_night_value->setNum(value - 1);
}
