/********************************************************************************
** Form generated from reading UI file 'screen_16.ui'
**
** Created: Thu Apr 13 00:46:20 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREEN_16_H
#define UI_SCREEN_16_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFormLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_screen_16
{
public:
    QFormLayout *formLayout;

    void setupUi(QWidget *screen_16)
    {
        if (screen_16->objectName().isEmpty())
            screen_16->setObjectName(QString::fromUtf8("screen_16"));
        screen_16->resize(1024, 600);
        screen_16->setMinimumSize(QSize(1024, 600));
        screen_16->setMaximumSize(QSize(1048, 630));
        formLayout = new QFormLayout(screen_16);
        formLayout->setContentsMargins(0, 0, 0, 0);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        formLayout->setHorizontalSpacing(0);
        formLayout->setVerticalSpacing(0);

        retranslateUi(screen_16);

        QMetaObject::connectSlotsByName(screen_16);
    } // setupUi

    void retranslateUi(QWidget *screen_16)
    {
        screen_16->setWindowTitle(QApplication::translate("screen_16", "Form", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class screen_16: public Ui_screen_16 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREEN_16_H
