/********************************************************************************
** Form generated from reading UI file 'screen_1.ui'
**
** Created: Thu Apr 13 00:46:20 2017
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCREEN_1_H
#define UI_SCREEN_1_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_screen_1
{
public:
    QVBoxLayout *verticalLayout_5;
    QFrame *frame_body;
    QVBoxLayout *verticalLayout_6;
    QLabel *label;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_7;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_2;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label_4;
    QSpacerItem *horizontalSpacer;
    QLabel *label_air_temp;
    QLabel *label_16;
    QSpacerItem *horizontalSpacer_3;
    QVBoxLayout *verticalLayout;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_5;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_6;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_humidity;
    QLabel *label_15;
    QSpacerItem *horizontalSpacer_4;
    QSpacerItem *horizontalSpacer_8;
    QFrame *line;
    QSpacerItem *horizontalSpacer_9;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_3;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_11;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_13;
    QLabel *label_21;
    QLabel *label_22;
    QSpacerItem *horizontalSpacer_14;
    QLabel *label_10;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_11;
    QLabel *label_18;
    QLabel *label_19;
    QLabel *label_20;
    QSpacerItem *horizontalSpacer_12;
    QSpacerItem *verticalSpacer_2;
    QSpacerItem *horizontalSpacer_10;
    QSpacerItem *verticalSpacer;
    QFrame *frame_footer;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_15;
    QPushButton *pushButton_close;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *pushButton_home;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *pushButton_next;
    QSpacerItem *horizontalSpacer_16;

    void setupUi(QWidget *screen_1)
    {
        if (screen_1->objectName().isEmpty())
            screen_1->setObjectName(QString::fromUtf8("screen_1"));
        screen_1->resize(1024, 600);
        screen_1->setMinimumSize(QSize(1024, 600));
        screen_1->setMaximumSize(QSize(1024, 648));
        verticalLayout_5 = new QVBoxLayout(screen_1);
        verticalLayout_5->setSpacing(0);
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        frame_body = new QFrame(screen_1);
        frame_body->setObjectName(QString::fromUtf8("frame_body"));
        frame_body->setMinimumSize(QSize(1024, 520));
        frame_body->setMaximumSize(QSize(1024, 520));
        frame_body->setFrameShape(QFrame::StyledPanel);
        frame_body->setFrameShadow(QFrame::Raised);
        verticalLayout_6 = new QVBoxLayout(frame_body);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        label = new QLabel(frame_body);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMinimumSize(QSize(0, 110));
        label->setMaximumSize(QSize(16777215, 110));
        label->setPixmap(QPixmap(QString::fromUtf8(":/images/images/logo.png")));
        label->setAlignment(Qt::AlignCenter);

        verticalLayout_6->addWidget(label);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_7);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label_2 = new QLabel(frame_body);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(350, 100));
        label_2->setMaximumSize(QSize(16777215, 100));
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(32);
        label_2->setFont(font);
        label_2->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(label_2);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_4 = new QLabel(frame_body);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMinimumSize(QSize(25, 50));
        label_4->setMaximumSize(QSize(25, 50));
        label_4->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/Thermo-Meter-.png")));
        label_4->setScaledContents(true);

        horizontalLayout->addWidget(label_4);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label_air_temp = new QLabel(frame_body);
        label_air_temp->setObjectName(QString::fromUtf8("label_air_temp"));
        label_air_temp->setMinimumSize(QSize(0, 100));
        label_air_temp->setMaximumSize(QSize(16777215, 100));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Sans Serif"));
        font1.setPointSize(64);
        label_air_temp->setFont(font1);

        horizontalLayout->addWidget(label_air_temp);

        label_16 = new QLabel(frame_body);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Sans Serif"));
        font2.setPointSize(40);
        label_16->setFont(font2);

        horizontalLayout->addWidget(label_16);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);


        gridLayout->addLayout(horizontalLayout, 0, 0, 1, 1);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label_7 = new QLabel(frame_body);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setMinimumSize(QSize(50, 50));
        label_7->setMaximumSize(QSize(50, 50));
        label_7->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/fire.png")));
        label_7->setScaledContents(true);

        verticalLayout->addWidget(label_7);

        label_8 = new QLabel(frame_body);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setMinimumSize(QSize(50, 50));
        label_8->setMaximumSize(QSize(50, 50));
        label_8->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/flake.png")));
        label_8->setScaledContents(true);

        verticalLayout->addWidget(label_8);

        label_5 = new QLabel(frame_body);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setMinimumSize(QSize(50, 50));
        label_5->setMaximumSize(QSize(50, 50));
        label_5->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/drop.png")));
        label_5->setScaledContents(true);

        verticalLayout->addWidget(label_5);


        gridLayout->addLayout(verticalLayout, 0, 1, 2, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_6 = new QLabel(frame_body);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setMinimumSize(QSize(30, 50));
        label_6->setMaximumSize(QSize(30, 50));
        label_6->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/Drop 2.png")));
        label_6->setScaledContents(true);

        horizontalLayout_2->addWidget(label_6);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        label_humidity = new QLabel(frame_body);
        label_humidity->setObjectName(QString::fromUtf8("label_humidity"));
        label_humidity->setMinimumSize(QSize(0, 100));
        label_humidity->setMaximumSize(QSize(16777215, 100));
        QFont font3;
        font3.setPointSize(64);
        label_humidity->setFont(font3);

        horizontalLayout_2->addWidget(label_humidity);

        label_15 = new QLabel(frame_body);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setFont(font2);
        label_15->setAutoFillBackground(false);

        horizontalLayout_2->addWidget(label_15);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_4);


        gridLayout->addLayout(horizontalLayout_2, 1, 0, 1, 1);


        verticalLayout_3->addLayout(gridLayout);


        horizontalLayout_4->addLayout(verticalLayout_3);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_8);

        line = new QFrame(frame_body);
        line->setObjectName(QString::fromUtf8("line"));
        line->setMinimumSize(QSize(0, 340));
        line->setFrameShadow(QFrame::Raised);
        line->setLineWidth(2);
        line->setFrameShape(QFrame::VLine);

        horizontalLayout_4->addWidget(line);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_9);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label_3 = new QLabel(frame_body);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMinimumSize(QSize(350, 100));
        label_3->setMaximumSize(QSize(16777215, 100));
        label_3->setFont(font);
        label_3->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(label_3);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_11 = new QLabel(frame_body);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Sans Serif"));
        font4.setPointSize(18);
        label_11->setFont(font4);
        label_11->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_11);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalSpacer_13 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_13);

        label_21 = new QLabel(frame_body);
        label_21->setObjectName(QString::fromUtf8("label_21"));
        label_21->setMinimumSize(QSize(50, 50));
        label_21->setMaximumSize(QSize(50, 50));
        label_21->setFrameShape(QFrame::NoFrame);
        label_21->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/good.png")));
        label_21->setScaledContents(true);
        label_21->setAlignment(Qt::AlignCenter);

        horizontalLayout_6->addWidget(label_21);

        label_22 = new QLabel(frame_body);
        label_22->setObjectName(QString::fromUtf8("label_22"));
        label_22->setMinimumSize(QSize(140, 50));
        label_22->setMaximumSize(QSize(140, 50));
        QFont font5;
        font5.setPointSize(32);
        font5.setBold(true);
        font5.setWeight(75);
        label_22->setFont(font5);
        label_22->setStyleSheet(QString::fromUtf8("color:rgb(0, 161, 118)"));
        label_22->setFrameShape(QFrame::NoFrame);
        label_22->setScaledContents(true);
        label_22->setAlignment(Qt::AlignCenter);

        horizontalLayout_6->addWidget(label_22);

        horizontalSpacer_14 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_14);


        verticalLayout_2->addLayout(horizontalLayout_6);

        label_10 = new QLabel(frame_body);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setMinimumSize(QSize(200, 0));
        QFont font6;
        font6.setPointSize(18);
        label_10->setFont(font6);
        label_10->setScaledContents(false);
        label_10->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_10);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_11);

        label_18 = new QLabel(frame_body);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setMinimumSize(QSize(50, 50));
        label_18->setMaximumSize(QSize(50, 50));
        label_18->setFrameShape(QFrame::NoFrame);
        label_18->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/fan.png")));
        label_18->setScaledContents(true);
        label_18->setAlignment(Qt::AlignCenter);

        horizontalLayout_5->addWidget(label_18);

        label_19 = new QLabel(frame_body);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        label_19->setMinimumSize(QSize(50, 50));
        label_19->setMaximumSize(QSize(50, 50));
        label_19->setFrameShape(QFrame::NoFrame);
        label_19->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/fan.png")));
        label_19->setScaledContents(true);
        label_19->setAlignment(Qt::AlignCenter);

        horizontalLayout_5->addWidget(label_19);

        label_20 = new QLabel(frame_body);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setMinimumSize(QSize(50, 50));
        label_20->setMaximumSize(QSize(50, 50));
        label_20->setFrameShape(QFrame::NoFrame);
        label_20->setPixmap(QPixmap(QString::fromUtf8(":/images/images/Icons/fan grey.png")));
        label_20->setScaledContents(true);
        label_20->setAlignment(Qt::AlignCenter);

        horizontalLayout_5->addWidget(label_20);

        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_12);


        verticalLayout_2->addLayout(horizontalLayout_5);

        verticalSpacer_2 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_2->addItem(verticalSpacer_2);


        verticalLayout_4->addLayout(verticalLayout_2);


        horizontalLayout_4->addLayout(verticalLayout_4);

        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_10);


        verticalLayout_6->addLayout(horizontalLayout_4);

        verticalSpacer = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_6->addItem(verticalSpacer);


        verticalLayout_5->addWidget(frame_body);

        frame_footer = new QFrame(screen_1);
        frame_footer->setObjectName(QString::fromUtf8("frame_footer"));
        frame_footer->setMinimumSize(QSize(1024, 80));
        frame_footer->setMaximumSize(QSize(1024, 80));
        frame_footer->setFrameShape(QFrame::StyledPanel);
        frame_footer->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_footer);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_15 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_15);

        pushButton_close = new QPushButton(frame_footer);
        pushButton_close->setObjectName(QString::fromUtf8("pushButton_close"));
        pushButton_close->setMinimumSize(QSize(50, 50));
        pushButton_close->setMaximumSize(QSize(50, 50));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/images/Icons/back.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_close->setIcon(icon);
        pushButton_close->setIconSize(QSize(50, 50));
        pushButton_close->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_close);

        horizontalSpacer_5 = new QSpacerItem(353, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);

        pushButton_home = new QPushButton(frame_footer);
        pushButton_home->setObjectName(QString::fromUtf8("pushButton_home"));
        pushButton_home->setMinimumSize(QSize(50, 50));
        pushButton_home->setMaximumSize(QSize(50, 50));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/images/Icons/home.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_home->setIcon(icon1);
        pushButton_home->setIconSize(QSize(50, 50));
        pushButton_home->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_home);

        horizontalSpacer_6 = new QSpacerItem(353, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);

        pushButton_next = new QPushButton(frame_footer);
        pushButton_next->setObjectName(QString::fromUtf8("pushButton_next"));
        pushButton_next->setMinimumSize(QSize(50, 50));
        pushButton_next->setMaximumSize(QSize(50, 50));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/images/Icons/next.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_next->setIcon(icon2);
        pushButton_next->setIconSize(QSize(50, 50));
        pushButton_next->setFlat(true);

        horizontalLayout_3->addWidget(pushButton_next);

        horizontalSpacer_16 = new QSpacerItem(50, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_16);


        verticalLayout_5->addWidget(frame_footer);


        retranslateUi(screen_1);

        QMetaObject::connectSlotsByName(screen_1);
    } // setupUi

    void retranslateUi(QWidget *screen_1)
    {
        screen_1->setWindowTitle(QApplication::translate("screen_1", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QString());
        label_2->setText(QApplication::translate("screen_1", "Kids Room", 0, QApplication::UnicodeUTF8));
        label_4->setText(QString());
        label_air_temp->setText(QApplication::translate("screen_1", "99", 0, QApplication::UnicodeUTF8));
        label_16->setText(QApplication::translate("screen_1", "\302\260C", 0, QApplication::UnicodeUTF8));
        label_8->setText(QString());
        label_5->setText(QString());
        label_6->setText(QString());
        label_humidity->setText(QApplication::translate("screen_1", "00", 0, QApplication::UnicodeUTF8));
        label_15->setText(QApplication::translate("screen_1", "%", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("screen_1", "First Floor", 0, QApplication::UnicodeUTF8));
        label_11->setText(QApplication::translate("screen_1", "Air Quality", 0, QApplication::UnicodeUTF8));
        label_21->setText(QString());
        label_22->setText(QApplication::translate("screen_1", "GOOD", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("screen_1", "Air Speed", 0, QApplication::UnicodeUTF8));
        label_18->setText(QString());
        label_19->setText(QString());
        label_20->setText(QString());
        pushButton_home->setText(QString());
        pushButton_next->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class screen_1: public Ui_screen_1 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCREEN_1_H
