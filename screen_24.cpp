#include "screen_24.h"
#include "ui_screen_24.h"

screen_24::screen_24(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_24),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_24::~screen_24()
{
    delete ui;
}

void screen_24::on_pushButton_home_clicked()
{
    Q_EMIT(touched(2));
}

void screen_24::on_pushButton_cancel_clicked()
{
    Q_EMIT(touched(22));
}

void screen_24::on_pushButton_back_clicked()
{
    Q_EMIT(touched(23));
}

