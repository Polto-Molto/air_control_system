/****************************************************************************
** Meta object code from reading C++ file 'screen_6.h'
**
** Created: Tue Apr 25 16:09:28 2017
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "screen_6.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'screen_6.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_screen_6[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x05,

 // slots: signature, parameters, type, tag, flags
      23,    9,    9,    9, 0x0a,
      52,    9,    9,    9, 0x0a,
      81,    9,    9,    9, 0x0a,
     118,    9,    9,    9, 0x0a,
     153,    9,    9,    9, 0x0a,
     188,    9,    9,    9, 0x0a,
     221,    9,    9,    9, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_screen_6[] = {
    "screen_6\0\0touched(int)\0"
    "on_pushButton_home_clicked()\0"
    "on_pushButton_done_clicked()\0"
    "on_pushButton_comfort_down_clicked()\0"
    "on_pushButton_comfort_up_clicked()\0"
    "on_pushButton_night_down_clicked()\0"
    "on_pushButton_night_up_clicked()\0"
    "setCurrentClima(CLIMA_MODBUS)\0"
};

void screen_6::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        screen_6 *_t = static_cast<screen_6 *>(_o);
        switch (_id) {
        case 0: _t->touched((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->on_pushButton_home_clicked(); break;
        case 2: _t->on_pushButton_done_clicked(); break;
        case 3: _t->on_pushButton_comfort_down_clicked(); break;
        case 4: _t->on_pushButton_comfort_up_clicked(); break;
        case 5: _t->on_pushButton_night_down_clicked(); break;
        case 6: _t->on_pushButton_night_up_clicked(); break;
        case 7: _t->setCurrentClima((*reinterpret_cast< CLIMA_MODBUS(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData screen_6::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject screen_6::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_screen_6,
      qt_meta_data_screen_6, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &screen_6::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *screen_6::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *screen_6::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_screen_6))
        return static_cast<void*>(const_cast< screen_6*>(this));
    return QWidget::qt_metacast(_clname);
}

int screen_6::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void screen_6::touched(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
