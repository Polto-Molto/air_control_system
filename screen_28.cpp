#include "screen_28.h"
#include "ui_screen_28.h"

screen_28::screen_28(dataManager *dm,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screen_28),
    _datamgr(dm)
{
    ui->setupUi(this);
}

screen_28::~screen_28()
{
    delete ui;
}

void screen_28::on_pushButton_keyboard_clicked()
{
    Q_EMIT(showkeyboard(ui->pushButton_keyboard));
}

void screen_28::setTextEdit(QString str)
{
    ui->pushButton_keyboard->setText(str);
}

QString screen_28::climaName()
{
    return ui->pushButton_keyboard->text();
}
